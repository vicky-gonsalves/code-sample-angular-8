## declare an array variable
declare -a imgarr=("platform-ui")
declare -a yamlarr=("gc-cert-platform-ui-deployment-prod.yaml")
declare -a deployarr=("cert-platform-ui-deployment")


## now loop through the above array

if [ "$#" -lt 3 ] || [ "$1" != "gc" -a "$1" != "mk" ]; then
    echo "Usage: deploy gc|mk test|prod <version>"
    exit
fi

ver=$3
if [[ $2 == prod ]]; then
    project="bhumapay-231007"
  	sed -i -- "s/test.bhumaitc.com/ppus.bhumaitc.com/g" "src/environments/environment.prod.ts"
elif [[ $2 == test ]]; then
    project="bhumapay"
  	sed -i -- "s/ppus.bhumaitc.com/test.bhumaitc.com/g" "src/environments/environment.prod.ts"
else
    echo "Usage: deploy gc|mk test|prod <version>"
    exit
fi

npm install
npm run build

docker build -t "cert-platform/platform-ui:$ver" -f Dockerfile-prod .
docker tag "cert-platform/platform-ui:$ver" "gcr.io/$project/platform-ui:$ver"
if [[ $1 == gc ]]; then
    docker push "gcr.io/$project/platform-ui:$ver"
fi

arraylength=${#yamlarr[@]}
for (( i=0; i<${arraylength}; i++ ));
do
	echo "${yamlarr[$i]}:$ver"
	cp "${yamlarr[$i]}" "${yamlarr[$i]}.backup"
	sed -i -- "s/bhumapay/$project/g" "${yamlarr[$i]}"
	sed -i -- "s/platform-ui:v2/platform-ui:$ver/g" "${yamlarr[$i]}"
#	kubectl delete deployment "${deployarr[$i]}"
	kubectl apply -f "${yamlarr[$i]}"
	cp "${yamlarr[$i]}.backup" "${yamlarr[$i]}"
	rm "${yamlarr[$i]}.backup"
done
