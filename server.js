'use strict';
const express = require('express');
const path = require('path');
const app = express();
const compression = require('compression');
//const https = require('https');
//const forceSSL = require('express-force-ssl');
const fs = require('fs');

// const ssl_options = {
//   key: fs.readFileSync('./keys/BHUMAIT_key.pem'),
//   cert: fs.readFileSync('./keys/BHUMAIT_crt.pem')
//   //key: fs.readFileSync('./keys/private.key'),
//   //cert: fs.readFileSync('./keys/cert.crt'),
//   //ca: fs.readFileSync('./keys/intermediate.crt')
// };

// const secureServer = https.createServer(ssl_options, app);
// let securePort = 8443;

// Run the app by serving the static files
// in the dist directory
app.use(express.static(__dirname + '/dist/payment-certification-platform'));
app.use(compression({level: 9}));

// app.set('forceSSLOptions', {
//   enable301Redirects: false,
//   trustXFPHeader: true,
//   httpsPort: securePort
// });
// app.use(forceSSL);

// Start server
function startServer() {
  const port = 4200;
  app.listen(port, () => {
    console.log('Express server listening on port ' + port);
  });
  // secureServer.listen(securePort, () => {
  //   console.log('Express Secure Server listening on port ' + securePort);
  // });
}

setImmediate(startServer);

app.get('/*', function (req, res) {
  res.sendFile(path.join(__dirname + '/dist/payment-certification-platform/index.html'));
});
