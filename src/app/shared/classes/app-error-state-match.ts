import {FormControl, FormGroupDirective, NgForm} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';

export class AppErrorStateMatch implements ErrorStateMatcher {
  formSubmit: Boolean;

  constructor(_formSubmit: Boolean) {
    this.formSubmit = _formSubmit;
  }

  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const controlTouched = !!(control && (control.dirty || control.touched));
    const controlInvalid = !!(control && control.invalid);
    return (this.formSubmit && controlInvalid) || (controlTouched && controlInvalid);
  }
}
