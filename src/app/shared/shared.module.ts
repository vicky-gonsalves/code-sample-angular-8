import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FlexLayoutModule} from '@angular/flex-layout';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatBadgeModule} from '@angular/material/badge';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatChipsModule} from '@angular/material/chips';
import {MatNativeDateModule} from '@angular/material/core';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatDialogModule} from '@angular/material/dialog';
import {MatDividerModule} from '@angular/material/divider';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatMenuModule} from '@angular/material/menu';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatSortModule} from '@angular/material/sort';
import {MatTableModule} from '@angular/material/table';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatTooltipModule} from '@angular/material/tooltip';
import {RouterModule} from '@angular/router';
import {OWL_DATE_TIME_FORMATS, OwlDateTimeModule} from 'ng-pick-datetime';
import {OwlMomentDateTimeModule} from 'ng-pick-datetime-moment';
import {DataViewerComponent} from './components/data-viewer/data-viewer.component';
import {ConfirmDialogComponent} from './components/dialogs/confirm-dialog/confirm-dialog.component';
import {ProgressBarComponent} from './components/progress-bar/progress-bar.component';
import {SearchComponent} from './components/search/search.component';
import {TooltipComponent} from './components/tooltip/tooltip.component';
import {XmlViewerComponent} from './components/xml-viewer/xml-viewer.component';
import {IfOnDomDirective} from './directives/if-on-dom/if-on-dom.directive';
import {PerfectScrollbarDirective} from './directives/perfect-scrollbar/perfect-scrollbar.directive';

// See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
export const MY_MOMENT_FORMATS = {
  parseInput: 'l LT',
  fullPickerInput: 'l LT',
  datePickerInput: 'l',
  timePickerInput: 'HH:mm',
  monthYearLabel: 'MMM YYYY',
  dateA11yLabel: 'LL',
  monthYearA11yLabel: 'MMMM YYYY',
};

const importsAndExports = [
  CommonModule,
  MatAutocompleteModule,
  MatButtonModule,
  MatCheckboxModule,
  MatChipsModule,
  MatBadgeModule,
  MatProgressBarModule,
  MatSidenavModule,
  MatIconModule,
  MatMenuModule,
  MatDividerModule,
  MatSelectModule,
  MatToolbarModule,
  MatDialogModule,
  MatTableModule,
  MatSortModule,
  MatTooltipModule,
  MatPaginatorModule,
  MatSnackBarModule,
  MatCardModule,
  MatInputModule,
  MatFormFieldModule,
  MatNativeDateModule,
  MatDatepickerModule,
  MatSlideToggleModule,
  MatExpansionModule,
  MatRadioModule,
  RouterModule,
  FormsModule,
  ReactiveFormsModule,
  FlexLayoutModule,
  OwlDateTimeModule,
  OwlMomentDateTimeModule
];

@NgModule({
  imports: [
    ...importsAndExports
  ],
  exports: [
    ...importsAndExports,
    ProgressBarComponent,
    IfOnDomDirective,
    PerfectScrollbarDirective,
    TooltipComponent,
    SearchComponent
  ],
  declarations: [
    PerfectScrollbarDirective,
    ProgressBarComponent,
    IfOnDomDirective,
    TooltipComponent,
    SearchComponent,
    ConfirmDialogComponent,
    DataViewerComponent,
    XmlViewerComponent
  ],
  providers: [{provide: OWL_DATE_TIME_FORMATS, useValue: MY_MOMENT_FORMATS}],
  entryComponents: [
    ConfirmDialogComponent,
    DataViewerComponent,
    XmlViewerComponent
  ]
})
export class SharedModule {
}
