import { TestBed, inject } from '@angular/core/testing';

import { BulkPerfSummaryService } from './bulk-perf-summary.service';

describe('BulkPerfSummaryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BulkPerfSummaryService]
    });
  });

  it('should be created', inject([BulkPerfSummaryService], (service: BulkPerfSummaryService) => {
    expect(service).toBeTruthy();
  }));
});
