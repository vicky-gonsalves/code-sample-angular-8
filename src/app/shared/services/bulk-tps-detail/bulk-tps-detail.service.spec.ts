import { TestBed, inject } from '@angular/core/testing';

import { BulkTpsDetailService } from './bulk-tps-detail.service';

describe('BulkTpsDetailService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BulkTpsDetailService]
    });
  });

  it('should be created', inject([BulkTpsDetailService], (service: BulkTpsDetailService) => {
    expect(service).toBeTruthy();
  }));
});
