import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {ErrorMessageService} from '../error-message/error-message.service';
import {ProgressBarService} from '../progress-bar/progress-bar.service';
import {BulkTpsDetailInterface} from '../../../interface/bulk-tps-detail.interface';

@Injectable({
  providedIn: 'root'
})
export class BulkTpsDetailService {
  private apiUrl: string = environment.apiUrl + 'bulk-tps-details';

  constructor(private http: HttpClient) {
  }

  public getBulkTPSDetails(params: any): Observable<any> {
    this.updateLoading(true);
    return this.http.post<any>(`${this.apiUrl}`, params)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public getBulkTPSDetailList(options: {
    q?: string,
    page?: number,
    limit?: number,
    sort?: string[],
    fields?: string[]
  }, bulkTpsDetailQry: BulkTpsDetailInterface): Observable<any> {
    let params = new HttpParams();
    for (const key in options) {
      if (key && options.hasOwnProperty(key)) {
        params = params.append(key, options[key]);
      }
    }
    this.updateLoading(true);
    return this.http.post<any>(`${this.apiUrl}/get-detail`, bulkTpsDetailQry, {params: params})
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()))
      ;
  }

  private updateLoading(flag: boolean): void {
    ProgressBarService.updateLoading(flag);
  }
}
