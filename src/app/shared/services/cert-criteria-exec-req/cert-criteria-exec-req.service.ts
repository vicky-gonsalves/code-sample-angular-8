import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {catchError, tap} from 'rxjs/internal/operators';
import {ErrorMessageService} from '../error-message/error-message.service';
import {ProgressBarService} from '../progress-bar/progress-bar.service';
import {CertCriteriaExecReqInterface} from '../../../interface/cert-criteria-exec-req.interface';

@Injectable({
  providedIn: 'root'
})
export class CertCriteriaExecReqService {

  private apiUrl: string = environment.apiUrl + 'cert-criteria-exec-req';

  constructor(private http: HttpClient) {
  }

  public addExecution(clhPlatform: CertCriteriaExecReqInterface): Observable<any> {
    this.updateLoading(true);
    return this.http.post<any>(`${this.apiUrl}`, clhPlatform)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public getExecutionHistory(options: {
                               term?: string,
                               page?: number,
                               limit?: number,
                               sort?: string[],
                               fields?: string[]
                             }, participantId?: string,
                             criteriaName?: string): Observable<any> {
    let params = new HttpParams();
    for (const key in options) {
      if (key && options.hasOwnProperty(key)) {
        params = params.append(key, options[key]);
      }
    }
    this.updateLoading(true);
    return this.http.get<any>(`${this.apiUrl}/${participantId}/${criteriaName}`, {params: params})
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public stopBulkCriteria(certCriteriaExecReqInterface: CertCriteriaExecReqInterface): Observable<any> {
    this.updateLoading(true);
    return this.http.put<any>(`${this.apiUrl}/stop-bulk-criteria`, certCriteriaExecReqInterface)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()))
      ;
  }

  public getBulkExecution(options: {
    q?: string,
    page?: number,
    limit?: number,
    sort?: string[],
    fields?: string[]
  }, certCriteriaExecReq: CertCriteriaExecReqInterface): Observable<any> {
    let params = new HttpParams();
    for (const key in options) {
      if (key && options.hasOwnProperty(key)) {
        params = params.append(key, options[key]);
      }
    }
    this.updateLoading(true);
    return this.http.post<any>(`${this.apiUrl}/bulk-execution`, certCriteriaExecReq, {params: params})
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()))
      ;
  }

  private updateLoading(flag: boolean): void {
    ProgressBarService.updateLoading(flag);
  }

}
