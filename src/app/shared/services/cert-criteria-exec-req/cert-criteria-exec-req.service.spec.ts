import {inject, TestBed} from '@angular/core/testing';

import {CertCriteriaExecReqService} from './cert-criteria-exec-req.service';

describe('CertCriteriaExecReqService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CertCriteriaExecReqService]
    });
  });

  it('should be created', inject([CertCriteriaExecReqService], (service: CertCriteriaExecReqService) => {
    expect(service).toBeTruthy();
  }));
});
