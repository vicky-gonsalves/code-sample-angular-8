import { TestBed, inject } from '@angular/core/testing';

import { QueryResultStoreSenMessageService } from './query-result-store-sen-message.service';

describe('QueryResultStoreSenMessageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [QueryResultStoreSenMessageService]
    });
  });

  it('should be created', inject([QueryResultStoreSenMessageService], (service: QueryResultStoreSenMessageService) => {
    expect(service).toBeTruthy();
  }));
});
