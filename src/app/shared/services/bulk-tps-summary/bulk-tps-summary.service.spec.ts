import { TestBed, inject } from '@angular/core/testing';

import { BulkTpsSummaryService } from './bulk-tps-summary.service';

describe('BulkTpsSummaryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BulkTpsSummaryService]
    });
  });

  it('should be created', inject([BulkTpsSummaryService], (service: BulkTpsSummaryService) => {
    expect(service).toBeTruthy();
  }));
});
