import { TestBed, inject } from '@angular/core/testing';

import { QueryResultStoreSettlementSummaryService } from './query-result-store-settlement-summary.service';

describe('QueryResultStoreSettlementSummaryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [QueryResultStoreSettlementSummaryService]
    });
  });

  it('should be created', inject([QueryResultStoreSettlementSummaryService], (service: QueryResultStoreSettlementSummaryService) => {
    expect(service).toBeTruthy();
  }));
});
