import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class QueryResultStoreSettlementSummaryService {
  private query: any;
  private finalQuery: any;
  private results: any;

  constructor() {
  }

  public getQuery(): any {
    return this.query;
  }

  public setQuery(query: any): void {
    this.query = query;
  }

  public getFinalQuery(): any {
    return this.finalQuery;
  }

  public setFinalQuery(finalQuery: any): void {
    this.finalQuery = finalQuery;
  }

  public getResults(): any {
    return this.results;
  }

  public setResults(results: any): void {
    this.results = results;
  }
}
