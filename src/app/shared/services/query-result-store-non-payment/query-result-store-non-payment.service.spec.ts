import {inject, TestBed} from '@angular/core/testing';

import {QueryResultStoreNonPaymentService} from './query-result-store-non-payment.service';

describe('QueryResultStoreNonPaymentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [QueryResultStoreNonPaymentService]
    });
  });

  it('should be created', inject([QueryResultStoreNonPaymentService], (service: QueryResultStoreNonPaymentService) => {
    expect(service).toBeTruthy();
  }));
});
