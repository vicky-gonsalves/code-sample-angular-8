import {inject, TestBed} from '@angular/core/testing';

import {MatchOtherValidatorService} from './match-other-validator.service';

describe('MatchOtherValidatorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MatchOtherValidatorService]
    });
  });

  it('should be created', inject([MatchOtherValidatorService], (service: MatchOtherValidatorService) => {
    expect(service).toBeTruthy();
  }));
});
