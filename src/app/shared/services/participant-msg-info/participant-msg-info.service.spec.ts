import { TestBed, inject } from '@angular/core/testing';

import { ParticipantMsgInfoService } from './participant-msg-info.service';

describe('ParticipantMsgInfoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ParticipantMsgInfoService]
    });
  });

  it('should be created', inject([ParticipantMsgInfoService], (service: ParticipantMsgInfoService) => {
    expect(service).toBeTruthy();
  }));
});
