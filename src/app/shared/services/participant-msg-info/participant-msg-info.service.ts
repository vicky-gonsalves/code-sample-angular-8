import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {ErrorMessageService} from '../error-message/error-message.service';
import {ProgressBarService} from '../progress-bar/progress-bar.service';
import {ParticipantMsgInfoInterface} from '../../../interface/participant-msg-info.interface';

@Injectable({
  providedIn: 'root'
})
export class ParticipantMsgInfoService {
  private apiUrl: string = environment.apiUrl + 'participant-msg-info';

  constructor(private http: HttpClient) {
  }

  public getParticipantMsgInfo(options: {
    q?: string,
    page?: number,
    limit?: number,
    sort?: string[],
    fields?: string[]
  }, participantId: string): Observable<any> {
    let params = new HttpParams();
    for (const key in options) {
      if (key && options.hasOwnProperty(key)) {
        params = params.append(key, options[key]);
      }
    }
    this.updateLoading(true);
    return this.http.get<any>(`${this.apiUrl}/get-list/${participantId}`, {params: params})
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public addParticipantMsgInfo(participantInfo: ParticipantMsgInfoInterface): Observable<any> {
    this.updateLoading(true);
    return this.http.post<any>(`${this.apiUrl}`, participantInfo)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public updateParticipantMsgInfo(participantInfo: ParticipantMsgInfoInterface): Observable<any> {
    this.updateLoading(true);
    return this.http.put<any>(`${this.apiUrl}`, participantInfo)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public deleteParticipantMsgInfo(participantInfo: ParticipantMsgInfoInterface): Observable<any> {
    this.updateLoading(true);
    return this.http.delete<any>(`${this.apiUrl}/${participantInfo.id}/${participantInfo.msg_type}`)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  private updateLoading(flag: boolean): void {
    ProgressBarService.updateLoading(flag);
  }
}
