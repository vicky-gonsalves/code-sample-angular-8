import {inject, TestBed} from '@angular/core/testing';

import {QueryResultStoreService} from './query-result-store-settlement-snapshot.service';

describe('QueryResultStoreSettlementSnapshotService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [QueryResultStoreService]
    });
  });

  it('should be created', inject([QueryResultStoreService], (service: QueryResultStoreService) => {
    expect(service).toBeTruthy();
  }));
});
