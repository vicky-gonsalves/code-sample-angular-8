import {inject, TestBed} from '@angular/core/testing';

import {ParticipantRoutingDetailService} from './participant-routing-detail.service';

describe('ParticipantRoutingDetailService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ParticipantRoutingDetailService]
    });
  });

  it('should be created', inject([ParticipantRoutingDetailService], (service: ParticipantRoutingDetailService) => {
    expect(service).toBeTruthy();
  }));
});
