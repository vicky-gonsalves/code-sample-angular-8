import {inject, TestBed} from '@angular/core/testing';

import {ConnectionOwnerService} from './connection-owner.service';

describe('ConnectionOwnerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ConnectionOwnerService]
    });
  });

  it('should be created', inject([ConnectionOwnerService], (service: ConnectionOwnerService) => {
    expect(service).toBeTruthy();
  }));
});
