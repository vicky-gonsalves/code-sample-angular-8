import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs';
import {ErrorMessageService} from '../error-message/error-message.service';
import {ProgressBarService} from '../progress-bar/progress-bar.service';
import {HttpClient, HttpParams} from '@angular/common/http';
import {catchError, map, tap} from 'rxjs/internal/operators';
import {ConnectionOwnerInterface} from '../../../interface/connection-owner.interface';

@Injectable({
  providedIn: 'root'
})
export class ConnectionOwnerService {

  private apiUrl: string = environment.apiUrl + 'connection-owners';

  constructor(private http: HttpClient) {
  }

  public getConnectionOwners(options: {
    q?: string,
    page?: number,
    limit?: number,
    sort?: string[],
    fields?: string[]
  }): Observable<any> {
    let params = new HttpParams();
    for (const key in options) {
      if (key && options.hasOwnProperty(key)) {
        params = params.append(key, options[key]);
      }
    }
    this.updateLoading(true);
    return this.http.get<any>(`${this.apiUrl}`, {params: params})
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public getConnectionOwnersForAutocomplete(options: {
    term?: string,
    page?: number,
    limit?: number,
    sort?: string[],
    fields?: string[]
  }): Observable<any> {
    let params = new HttpParams();
    for (const key in options) {
      if (key && options.hasOwnProperty(key)) {
        params = params.append(key, options[key]);
      }
    }
    this.updateLoading(true);
    return this.http.get<any>(`${this.apiUrl}/get-owners/autocomplete`, {params: params})
      .pipe(
        map(data => data.data && data.data.rows && data.data.count ? data.data.rows : []),
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public addConnectionOwner(connectionOwner: ConnectionOwnerInterface): Observable<any> {
    this.updateLoading(true);
    return this.http.post<any>(`${this.apiUrl}`, connectionOwner)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()))
      ;
  }

  public updateConnectionOwner(connectionOwner: ConnectionOwnerInterface): Observable<any> {
    this.updateLoading(true);
    return this.http.put<any>(`${this.apiUrl}/${connectionOwner._id}`, connectionOwner)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public getConnectionOwner(id: string): Observable<any> {
    this.updateLoading(true);
    return this.http.get<any>(`${this.apiUrl}/${id}`)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  private updateLoading(flag: boolean): void {
    ProgressBarService.updateLoading(flag);
  }
}
