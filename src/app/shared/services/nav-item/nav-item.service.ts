import {Injectable} from '@angular/core';
import {NavItemInterface} from '../../../interface/nav-item.interface';
import {NavCategoryInterface} from '../../../interface/nav-category.interface';
import {IdentityService} from '../../../core/services/identity/identity.service';
import {Identity} from '../../../core/services/identity/identity';
import {filter, includes, some} from 'lodash';
import {NavItems} from './nav';

let ALL_COMPONENTS: any[];
ALL_COMPONENTS = [];
let ALL_CATEGORIES = [];
const COMPONENTS = 'main';
const PRIVILEGED_COMPONENTS = 'privileged';
const PUBLIC_PRIVILEGES = ['dashboard'];

const MENUS: { [key: string]: NavCategoryInterface[] } = {
  [PRIVILEGED_COMPONENTS]: [],
  [COMPONENTS]: NavItems.items
};

@Injectable({
  providedIn: 'root'
})
export class NavItemService {
  private identity: Identity;

  constructor() {
    IdentityService.identity.subscribe(contactIdentity => {
      this.identity = contactIdentity;
      if (this.identity &&
        this.identity.contactRoles &&
        this.identity.contactRoles.privileges &&
        this.identity.contactRoles.privileges.length) {
        const allPrivileges = this.identity.contactRoles.privileges.concat(PUBLIC_PRIVILEGES);
        MENUS[PRIVILEGED_COMPONENTS] = this.filterByPrivileges(MENUS[COMPONENTS], allPrivileges);
        MENUS[PRIVILEGED_COMPONENTS].forEach(nav => {
          if (nav.items && nav.items.length) {
            nav.items.forEach(navItem => {
              ALL_COMPONENTS.push(navItem);
            });
          } else {
            ALL_COMPONENTS.push(nav);
          }
        });
        ALL_CATEGORIES = MENUS[PRIVILEGED_COMPONENTS];
      } else {
        MENUS[PRIVILEGED_COMPONENTS].length = 0;
        ALL_CATEGORIES.length = 0;
        ALL_COMPONENTS.length = 0;
      }
    });
  }

  public getAllCategories(): NavCategoryInterface[] {
    return ALL_CATEGORIES;
  }

  public getCategories(section: string): NavCategoryInterface[] {
    return MENUS[section];
  }

  public getAllComponents(): NavCategoryInterface[] {
    return ALL_COMPONENTS;
  }

  public getItems(section: string): NavItemInterface[] {
    if (section === COMPONENTS) {
      return ALL_COMPONENTS;
    }
    return [];
  }

  public getItemById(id: string): NavItemInterface {
    return ALL_COMPONENTS.find(nav => nav.id === id);
  }

  public filterByPrivileges(menus, privileges) {
    return filter(menus, function (menu) {
      if (menu.items && menu.items.length) {
        menu.items = filter(menu.items, function (submenu) {
          if (submenu && submenu.privileges) {
            return some(submenu.privileges, function (subprivilege) {
              return includes(privileges, subprivilege);
            });
          } else {
            return submenu;
          }
        });
      }
      return some(menu.privileges, function (privilege) {
        return includes(privileges, privilege);
      });
    });
  }
}
