import {NavCategoryInterface} from '../../../interface/nav-category.interface';

export class NavItems {
  public static items: NavCategoryInterface[] = [
    {
      'id': 'dashboard',
      'name': 'Dashboard',
      'items': [],
      'icon': 'multiline chart',
      'route': 'dashboard',
      'showInContent': true,
      'showInSidenav': true,
      'privileges': [
        'dashboard'
      ]
    },
    {
      'id': 'connection-management',
      'name': 'Maintain Connection',
      'items': [
        {
          'id': 'connections',
          'name': 'Connections',
          'route': 'manage-connection/connections',
          'showInContent': true,
          'showInSidenav': true
        },
        {
          'id': 'connection-add',
          'name': 'Add New Connection',
          'route': 'manage-connection/connections/add',
          'showInContent': false,
          'showInSidenav': false
        },
        {
          'id': 'connection-edit',
          'name': 'Edit Connection',
          'route': 'manage-connection/connections/edit',
          'showInContent': false,
          'showInSidenav': false
        },
        {
          'id': 'connection-owners',
          'name': 'Connection Owners',
          'route': 'manage-connection/connection-owners',
          'showInContent': true,
          'showInSidenav': true
        },
        {
          'id': 'connection-owners-add',
          'name': 'Add New Connection Owner',
          'route': 'manage-connection/connection-owners/add',
          'showInContent': false,
          'showInSidenav': false
        },
        {
          'id': 'connection-owners-edit',
          'name': 'Edit Connection Owner',
          'route': 'manage-connection/connection-owners/edit',
          'showInContent': false,
          'showInSidenav': false
        }
      ],
      'icon': 'multiline chart',
      'showInContent': true,
      'showInSidenav': true,
      'privileges': [
        'maintainConnection'
      ]
    },
    {
      'id': 'settlement-account',
      'name': 'Maintain Settlement Account',
      'items': [
        {
          'id': 'settlement',
          'name': 'Settlement Accounts',
          'route': 'manage-account/settlement',
          'showInContent': true,
          'showInSidenav': true
        },
        {
          'id': 'settlement-add',
          'name': 'Add New Settlement Account',
          'route': 'manage-account/settlement/add',
          'showInContent': false,
          'showInSidenav': false
        },
        {
          'id': 'settlement-edit',
          'name': 'Edit Settlement Account',
          'route': 'manage-account/settlement/edit',
          'showInContent': false,
          'showInSidenav': false
        },
        {
          'id': 'settlement-owners',
          'name': 'Settlement Account Owners',
          'route': 'manage-account/settlement-owners',
          'showInContent': true,
          'showInSidenav': true
        },
        {
          'id': 'settlement-owners-add',
          'name': 'Add New Settlement Account Owner',
          'route': 'manage-account/settlement-owners/add',
          'showInContent': false,
          'showInSidenav': false
        },
        {
          'id': 'settlement-owners-edit',
          'name': 'Edit Settlement Account Owner',
          'route': 'manage-account/settlement-owners/edit',
          'showInContent': false,
          'showInSidenav': false
        }
      ],
      'icon': 'multiline chart',
      'showInContent': true,
      'showInSidenav': true,
      'privileges': [
        'maintainSettlementAccount'
      ]
    },
    {
      'id': 'contacts',
      'name': 'Maintain Contact',
      'items': [
        {
          'id': 'contact',
          'name': 'Contacts',
          'route': 'manage-contact/contacts',
          'showInContent': true,
          'showInSidenav': true
        },
        {
          'id': 'contact-add',
          'name': 'Add New Contact',
          'route': 'manage-contact/contacts/add',
          'showInContent': false,
          'showInSidenav': false
        },
        {
          'id': 'contact-edit',
          'name': 'Edit Contact',
          'route': 'manage-contact/contacts/edit',
          'showInContent': false,
          'showInSidenav': false
        }
      ],
      'icon': 'recent_actors',
      'showInContent': true,
      'showInSidenav': true,
      'privileges': [
        'maintainContact'
      ]
    },
    {
      'id': 'participant-management',
      'name': 'Maintain Participant',
      'items': [
        {
          'id': 'participant',
          'name': 'Participants',
          'route': 'manage-participant/participants',
          'showInContent': true,
          'showInSidenav': true
        },
        {
          'id': 'participant-add',
          'name': 'Add New Participant',
          'route': 'manage-participant/participants/add',
          'showInContent': false,
          'showInSidenav': false
        },
        {
          'id': 'participant-edit',
          'name': 'Edit Participant',
          'route': 'manage-participant/participants/edit',
          'showInContent': false,
          'showInSidenav': false
        },
        {
          'id': 'view-timeout-offset',
          'name': 'View Timeout Offset',
          'route': 'manage-participant/participants/view-timeout-offset/:id',
          'showInContent': false,
          'showInSidenav': false
        }
      ],
      'icon': 'group',
      'showInContent': true,
      'showInSidenav': true,
      'privileges': [
        'maintainParticipant'
      ]
    },
    {
      'id': 'participant-routing-management',
      'name': 'Maintain Participant Routing',
      'items': [
        {
          'id': 'participant-routing-detail',
          'name': 'Participant Routing Details',
          'route': 'manage-participant-routing/participant-routing-details',
          'showInContent': true,
          'showInSidenav': true
        },
        {
          'id': 'participant-routing-detail-add',
          'name': 'Add New Participant Routing Details',
          'route': 'manage-participant-routing/participant-routing-details/add',
          'showInContent': false,
          'showInSidenav': false
        },
        {
          'id': 'participant-routing-detail-edit',
          'name': 'Edit Participant Routing Details',
          'route': 'manage-participant-routing/participant-routing-details/edit',
          'showInContent': false,
          'showInSidenav': false
        }
      ],
      'icon': 'call_split',
      'showInContent': true,
      'showInSidenav': true,
      'privileges': [
        'maintainParticipantRouting'
      ]
    },
    {
      'id': 'test-case-management',
      'name': 'Maintain Test Case',
      'items': [
        {
          'id': 'test-template',
          'name': 'Test Template',
          'route': 'manage-test-case/test-templates',
          'showInContent': true,
          'showInSidenav': true
        },
        {
          'id': 'test-template-add',
          'name': 'Add New Test Template',
          'route': 'manage-test-case/test-templates/add',
          'showInContent': false,
          'showInSidenav': false
        },
        {
          'id': 'test-template-view',
          'name': 'View Test Template',
          'route': 'manage-test-case/test-templates/view',
          'showInContent': false,
          'showInSidenav': false
        },
        {
          'id': 'test-template-edit',
          'name': 'Edit Test Template',
          'route': 'manage-test-case/test-templates/edit',
          'showInContent': false,
          'showInSidenav': false
        }
      ],
      'icon': 'call_split',
      'showInContent': true,
      'showInSidenav': true,
      'privileges': [
        'maintainParticpantTestCaseTemplate'
      ]
    },
    {
      'id': 'manage-certificate-criteria',
      'name': 'Maintain Certification Criteria',
      'items': [
        {
          'id': 'certification-criteria',
          'name': 'Certification Criteria',
          'route': 'manage-certificate-criteria/certification-criteria',
          'showInContent': true,
          'showInSidenav': true
        },
        {
          'id': 'certification-criteria-add',
          'name': 'Add New Certification Criteria',
          'route': 'manage-certificate-criteria/certification-criteria/add',
          'showInContent': false,
          'showInSidenav': false
        },
        {
          'id': 'certification-criteria-edit',
          'name': 'Edit Certification Criteria',
          'route': 'manage-certificate-criteria/certification-criteria/edit',
          'showInContent': false,
          'showInSidenav': false
        },
        {
          'id': 'certification-criteria-view',
          'name': 'View Certification Criteria',
          'route': 'manage-certificate-criteria/certification-criteria/view',
          'showInContent': false,
          'showInSidenav': false
        }
      ],
      'icon': 'class',
      'showInContent': true,
      'showInSidenav': true,
      'privileges': [
        'maintainParticipantCriteria'
      ]
    },
    {
      'id': 'execute-certificate-criteria',
      'name': 'View & Execute Certification',
      'items': [
        {
          'id': 'exec-certification-criteria',
          'name': 'View/Exec Certification Criteria',
          'route': 'execute-certification/exec-certification-criteria',
          'showInContent': true,
          'showInSidenav': true
        },
        {
          'id': 'stop-bulk-criteria',
          'name': 'Stop Bulk Criteria',
          'route': 'execute-certification/stop-bulk-criteria',
          'showInContent': true,
          'showInSidenav': true,
          'privileges': [
            'stopBulkExecution'
          ]
        },
        {
          'id': 'exec-certification-criteria-view',
          'name': 'View/Execute Certification Criteria Templates',
          'route': 'execute-certification/exec-certification-criteria/view',
          'showInContent': false,
          'showInSidenav': false
        }
      ],
      'icon': 'class',
      'showInContent': true,
      'showInSidenav': true,
      'privileges': [
        'viewParticipantCriteria',
        'runParticipantCriteria'
      ]
    },
    {
      'id': 'view-report',
      'name': 'View Report',
      'items': [
        {
          'id': 'certification-criteria-report',
          'name': 'Certification Criteria Report',
          'route': 'view-report/certification-criteria-report',
          'showInContent': true,
          'showInSidenav': true,
          'privileges': [
            'viewParticpantCriteriaReport'
          ]
        },
        {
          'id': 'certification-criteria-report-view',
          'name': 'View Certification Criteria Report',
          'route': 'view-report/certification-criteria-report/view',
          'showInContent': false,
          'showInSidenav': false,
          'privileges': [
            'viewParticpantCriteriaReport'
          ]
        },
        {
          'id': 'bulk-criteria-report',
          'name': 'View Bulk Criteria Report',
          'route': 'view-report/bulk-execution-report',
          'showInContent': true,
          'showInSidenav': true,
          'privileges': [
            'viewBulkExecutionReport'
          ]
        },
        {
          'id': 'bulk-execution-result',
          'name': 'Bulk Execution Results',
          'route': 'view-report/bulk-execution-report/search-results',
          'showInContent': false,
          'showInSidenav': false,
          'privileges': [
            'viewBulkExecutionReport'
          ]
        }
      ],
      'icon': 'report',
      'showInContent': true,
      'showInSidenav': true,
      'privileges': [
        'viewParticpantCriteriaReport',
        'viewBulkExecutionReport'
      ]
    },
    {
      'id': 'manage-settlement-summary',
      'name': 'Settlement',
      'items': [
        {
          'id': 'settlement-summary',
          'name': 'View Settlement Summary',
          'route': 'manage-settlement-summary/settlement-summary',
          'showInContent': true,
          'showInSidenav': true,
          'privileges': [
            'viewSettlementSummary'
          ]
        },
        {
          'id': 'settlement-summary-result',
          'name': 'Settlement Summary Results',
          'route': 'manage-settlement-summary/settlement-summary/search-results',
          'showInContent': false,
          'showInSidenav': false,
          'privileges': [
            'viewSettlementSummary'
          ]
        },
        {
          'id': 'credit-debit-settlement-account',
          'name': 'Credit Debit Settlement Account',
          'route': 'manage-settlement-summary/credit-debit-settlement-account',
          'showInContent': true,
          'showInSidenav': true,
          'privileges': [
            'creditDebitSettAccount'
          ]
        },
        {
          'id': 'snapshot-settlement-position',
          'name': 'Snapshot Settlement Position',
          'route': 'manage-settlement-summary/snapshot-settlement-position',
          'showInContent': true,
          'showInSidenav': true,
          'privileges': [
            'snapSettposition'
          ]
        },
        {
          'id': 'view-settlement-snapshot',
          'name': 'View Settlement Snapshot',
          'route': 'manage-settlement-summary/view-settlement-snapshot',
          'showInContent': true,
          'showInSidenav': true,
          'privileges': [
            'viewSettlementSnapshot'
          ]
        },
        {
          'id': 'settlement-snapshot-result',
          'name': 'Settlement Snapshot Results',
          'route': 'manage-settlement-summary/view-settlement-snapshot/search-results',
          'showInContent': false,
          'showInSidenav': false,
          'privileges': [
            'viewSettlementSnapshot'
          ]
        }
      ],
      'icon': 'description',
      'showInContent': true,
      'showInSidenav': true,
      'privileges': [
        'viewSettlementSummary', 'creditDebitSettAccount', 'snapSettposition', 'viewSettlementSnapshot'
      ]
    },
    {
      'id': 'view-payment-transactions',
      'name': 'View Transaction',
      'items': [
        {
          'id': 'payment-transaction',
          'name': 'Payment Transaction',
          'route': 'view-payment-transactions/payment-transaction',
          'showInContent': true,
          'showInSidenav': true,
          'privileges': [
            'viewTransactions'
          ]
        },
        {
          'id': 'payment-transaction-result',
          'name': 'Payment Transaction Search Results',
          'route': 'view-payment-transactions/payment-transaction/search-results',
          'showInContent': false,
          'showInSidenav': false,
          'privileges': [
            'viewTransactions'
          ]
        },
        {
          'id': 'non-payment-transaction',
          'name': 'Payment Related Transaction',
          'route': 'view-payment-transactions/non-payment-transaction',
          'showInContent': true,
          'showInSidenav': true,
          'privileges': [
            'viewTransactions'
          ]
        },
        {
          'id': 'non-payment-transaction-result',
          'name': 'Payment Related Transaction Search Results',
          'route': 'view-payment-transactions/non-payment-transaction/search-results',
          'showInContent': false,
          'showInSidenav': false,
          'privileges': [
            'viewTransactions'
          ]
        },
        {
          'id': 'view-sen-message',
          'name': 'View SEN Message',
          'route': 'view-payment-transactions/view-sen-message',
          'showInContent': true,
          'showInSidenav': true,
          'privileges': [
            'viewSenMessage'
          ]
        },
        {
          'id': 'sen-message-result',
          'name': 'SEN Message Results',
          'route': 'view-payment-transactions/view-sen-message/search-results',
          'showInContent': false,
          'showInSidenav': false,
          'privileges': [
            'viewSenMessage'
          ]
        },
        {
          'id': 'view-admin-message',
          'name': 'View Admin Message',
          'route': 'view-payment-transactions/view-admin-message',
          'showInContent': true,
          'showInSidenav': true,
          'privileges': [
            'viewAdminMessage'
          ]
        },
        {
          'id': 'admin-message-result',
          'name': 'Admin Message Results',
          'route': 'view-payment-transactions/view-admin-message/search-results',
          'showInContent': false,
          'showInSidenav': false,
          'privileges': [
            'viewAdminMessage'
          ]
        }
      ],
      'icon': 'credit_card',
      'showInContent': true,
      'showInSidenav': true,
      'privileges': [
        'viewTransactions', 'viewSenMessage', 'viewAdminMessage'
      ]
    }
  ];
}
