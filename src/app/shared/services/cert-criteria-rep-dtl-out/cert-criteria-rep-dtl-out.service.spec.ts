import {inject, TestBed} from '@angular/core/testing';

import {CertCriteriaRepDtlOutService} from './cert-criteria-rep-dtl-out.service';

describe('CertCriteriaRepDtlOutService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CertCriteriaRepDtlOutService]
    });
  });

  it('should be created', inject([CertCriteriaRepDtlOutService], (service: CertCriteriaRepDtlOutService) => {
    expect(service).toBeTruthy();
  }));
});
