import {Injectable} from '@angular/core';
import {catchError, tap} from 'rxjs/internal/operators';
import {ErrorMessageService} from '../error-message/error-message.service';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import {ProgressBarService} from '../progress-bar/progress-bar.service';

@Injectable({
  providedIn: 'root'
})
export class CertCriteriaRepDtlOutService {


  private apiUrl: string = environment.apiUrl + 'cert-criteria-rep-dtl-out';

  constructor(private http: HttpClient) {
  }

  public getCertCriteriaRepDtlOutList(options: {
    q?: string,
    page?: number,
    limit?: number,
    sort?: string[],
    fields?: string[]
  }, participant_id: string, criteria_name: string, template_name: string): Observable<any> {
    let params = new HttpParams();
    for (const key in options) {
      if (key && options.hasOwnProperty(key)) {
        params = params.append(key, options[key]);
      }
    }
    this.updateLoading(true);
    return this.http.get<any>(`${this.apiUrl}/${participant_id}/${criteria_name}/${template_name}`, {params: params})
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public getCertCriteriaRepDtlOutValidationList(options: {
    q?: string,
    page?: number,
    limit?: number,
    sort?: string[],
    fields?: string[]
  }, id: string): Observable<any> {
    let params = new HttpParams();
    for (const key in options) {
      if (key && options.hasOwnProperty(key)) {
        params = params.append(key, options[key]);
      }
    }
    this.updateLoading(true);
    return this.http.get<any>(`${this.apiUrl}/get-validations/${id}`, {params: params})
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public getCertCriteriaRepDtlOutValidationFldDetailsList(options: {
    q?: string,
    page?: number,
    limit?: number,
    sort?: string[],
    fields?: string[]
  }, id: string, validationName: string, targetMsgType: string, sourceMsgType: string): Observable<any> {
    let params = new HttpParams();
    for (const key in options) {
      if (key && options.hasOwnProperty(key)) {
        params = params.append(key, options[key]);
      }
    }
    this.updateLoading(true);
    return this.http.post<any>(`${this.apiUrl}/get-validations/fld-details/${id}/${validationName}`, {
      targetMsgType: targetMsgType,
      sourceMsgType: sourceMsgType
    }, {params: params})
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  private updateLoading(flag: boolean): void {
    ProgressBarService.updateLoading(flag);
  }
}
