import {Injectable} from '@angular/core';
import {ErrorMessageService} from '../error-message/error-message.service';
import {catchError, map, tap} from 'rxjs/internal/operators';
import {Observable} from 'rxjs';
import {ClhPlatformInterface} from '../../../interface/clh-platform.interface';
import {HttpClient, HttpParams} from '@angular/common/http';
import {ProgressBarService} from '../progress-bar/progress-bar.service';
import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ClhPlatformService {
  private apiUrl: string = environment.apiUrl + 'clh-platforms';

  constructor(private http: HttpClient) {
  }

  public getClhPlatforms(options: {
    q?: string,
    page?: number,
    limit?: number,
    sort?: string[],
    fields?: string[]
  }): Observable<any> {
    let params = new HttpParams();
    for (const key in options) {
      if (key && options.hasOwnProperty(key)) {
        params = params.append(key, options[key]);
      }
    }
    this.updateLoading(true);
    return this.http.get<any>(`${this.apiUrl}`, {params: params})
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public getClhPlatformsForAutocomplete(options: {
    term?: string,
    page?: number,
    limit?: number,
    sort?: string[],
    fields?: string[]
  }): Observable<any> {
    let params = new HttpParams();
    for (const key in options) {
      if (key && options.hasOwnProperty(key)) {
        params = params.append(key, options[key]);
      }
    }
    this.updateLoading(true);
    return this.http.get<any>(`${this.apiUrl}/get-clh-platforms/autocomplete`, {params: params})
      .pipe(
        map(data => data.data && data.data.rows && data.data.count ? data.data.rows : []),
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public addClhPlatform(clhPlatform: ClhPlatformInterface): Observable<any> {
    this.updateLoading(true);
    return this.http.post<any>(`${this.apiUrl}`, clhPlatform)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()))
      ;
  }

  public updateClhPlatform(clhPlatform: ClhPlatformInterface): Observable<any> {
    this.updateLoading(true);
    return this.http.put<any>(`${this.apiUrl}/${clhPlatform._id}`, clhPlatform)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public getClhPlatform(id: string): Observable<any> {
    this.updateLoading(true);
    return this.http.get<any>(`${this.apiUrl}/${id}`)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  private updateLoading(flag: boolean): void {
    ProgressBarService.updateLoading(flag);
  }
}
