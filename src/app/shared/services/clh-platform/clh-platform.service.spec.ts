import {inject, TestBed} from '@angular/core/testing';

import {ClhPlatformService} from './clh-platform.service';

describe('ClhPlatformService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ClhPlatformService]
    });
  });

  it('should be created', inject([ClhPlatformService], (service: ClhPlatformService) => {
    expect(service).toBeTruthy();
  }));
});
