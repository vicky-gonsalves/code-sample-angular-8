import {inject, TestBed} from '@angular/core/testing';

import {ContactProfileService} from './contact-profile.service';

describe('ContactProfileService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ContactProfileService]
    });
  });

  it('should be created', inject([ContactProfileService], (service: ContactProfileService) => {
    expect(service).toBeTruthy();
  }));
});
