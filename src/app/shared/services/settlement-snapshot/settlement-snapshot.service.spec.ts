import {inject, TestBed} from '@angular/core/testing';

import {SettlementSnapshotService} from './settlement-snapshot.service';

describe('SettlementSnapshotService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SettlementSnapshotService]
    });
  });

  it('should be created', inject([SettlementSnapshotService], (service: SettlementSnapshotService) => {
    expect(service).toBeTruthy();
  }));
});
