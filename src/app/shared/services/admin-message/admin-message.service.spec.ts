import { TestBed, inject } from '@angular/core/testing';

import { AdminMessageService } from './admin-message.service';

describe('AdminMessageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminMessageService]
    });
  });

  it('should be created', inject([AdminMessageService], (service: AdminMessageService) => {
    expect(service).toBeTruthy();
  }));
});
