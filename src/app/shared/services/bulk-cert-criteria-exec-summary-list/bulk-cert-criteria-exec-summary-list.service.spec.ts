import { TestBed, inject } from '@angular/core/testing';

import { BulkCertCriteriaExecSummaryListService } from './bulk-cert-criteria-exec-summary-list.service';

describe('BulkCertCriteriaExecSummaryListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BulkCertCriteriaExecSummaryListService]
    });
  });

  it('should be created', inject([BulkCertCriteriaExecSummaryListService], (service: BulkCertCriteriaExecSummaryListService) => {
    expect(service).toBeTruthy();
  }));
});
