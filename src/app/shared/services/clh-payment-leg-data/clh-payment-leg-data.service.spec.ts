import {inject, TestBed} from '@angular/core/testing';

import {ClhPaymentLegDataService} from './clh-payment-leg-data.service';

describe('ClhPaymentLegDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ClhPaymentLegDataService]
    });
  });

  it('should be created', inject([ClhPaymentLegDataService], (service: ClhPaymentLegDataService) => {
    expect(service).toBeTruthy();
  }));
});
