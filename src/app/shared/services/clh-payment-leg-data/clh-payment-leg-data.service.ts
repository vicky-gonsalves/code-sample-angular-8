import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {catchError, tap} from 'rxjs/internal/operators';
import {ProgressBarService} from '../progress-bar/progress-bar.service';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs';
import {ErrorMessageService} from '../error-message/error-message.service';

@Injectable({
  providedIn: 'root'
})
export class ClhPaymentLegDataService {
  private apiUrl: string = environment.apiUrl + 'clh-payment-leg-data';

  constructor(private http: HttpClient) {
  }

  public getClhPaymentLegData(options: {
    transaction_type?: string,
    q?: string,
    page?: number,
    limit?: number,
    sort?: string[],
    fields?: string[]
  }, id: string): Observable<any> {
    let params = new HttpParams();
    for (const key in options) {
      if (key && options.hasOwnProperty(key)) {
        params = params.append(key, options[key]);
      }
    }
    this.updateLoading(true);
    return this.http.get<any>(`${this.apiUrl}/${id}`, {params: params})
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  private updateLoading(flag: boolean): void {
    ProgressBarService.updateLoading(flag);
  }
}
