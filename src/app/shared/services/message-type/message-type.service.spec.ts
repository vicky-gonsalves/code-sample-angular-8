import { TestBed, inject } from '@angular/core/testing';

import { MessageTypeService } from './message-type.service';

describe('MessageTypeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MessageTypeService]
    });
  });

  it('should be created', inject([MessageTypeService], (service: MessageTypeService) => {
    expect(service).toBeTruthy();
  }));
});
