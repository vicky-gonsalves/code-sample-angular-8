import {inject, TestBed} from '@angular/core/testing';

import {UiConfigParameterService} from './ui-config-parameter.service';

describe('UiConfigParameterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UiConfigParameterService]
    });
  });

  it('should be created', inject([UiConfigParameterService], (service: UiConfigParameterService) => {
    expect(service).toBeTruthy();
  }));
});
