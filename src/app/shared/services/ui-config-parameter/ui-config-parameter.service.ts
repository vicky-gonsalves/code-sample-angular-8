import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs';
import {ErrorMessageService} from '../error-message/error-message.service';
import {catchError, tap} from 'rxjs/internal/operators';
import {ProgressBarService} from '../progress-bar/progress-bar.service';
import {HttpClient} from '@angular/common/http';
import {UiConfigService} from '../../../core/services/ui-config/ui-config.service';
import {UiConfig} from '../../../core/services/ui-config/ui-config';
import {find} from 'lodash';
import {HelpTextService} from '../../../core/services/help-text/help-text.service';
import moment from 'moment-timezone';

@Injectable({
  providedIn: 'root'
})
export class UiConfigParameterService {
  private uiConfig: UiConfig;
  private apiUrl: string = environment.apiUrl + 'ui-config-parameters';

  constructor(private http: HttpClient,
              private uiConfigService: UiConfigService,
              private helpTextService: HelpTextService) {
    UiConfigService.uiConfig.subscribe(config => {
      this.uiConfig = config;
    });
  }

  public getUIConfigs(): Observable<any> {
    this.updateLoading(true);
    return this.http.get<any>(`${this.apiUrl}`)
      .pipe(
        tap(() => this.updateLoading(false)),
        tap((response: any) => {
          if (response && response.hasOwnProperty('data') && response.data && response.data.rows) {
            this.uiConfigService.updateUiConfig({config: response.data.rows});
          }
        }),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public getHelpTextFile(): Observable<any> {
    this.updateLoading(true);
    return this.http.get<any>(`${this.apiUrl}/get-help-file`)
      .pipe(
        tap(() => this.updateLoading(false)),
        tap((response: any) => {
          if (response) {
            this.helpTextService.updateHelpText(response);
          }
        }),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public getLocalConfig(param: string): string | null {
    if (this.uiConfig && this.uiConfig.config) {
      const conf = find(this.uiConfig.config, {param_name: param});
      if (conf) {
        return conf.param_value;
      }
    }
    return null;
  }

  public convertTimezone(date: Date): moment {
    if (moment(new Date(date)).isValid()) {
      return moment(date).tz(this.getLocalConfig('UI_TIMEZONE'));
    }
    return null;
  }

  public convertTimezoneWithMillis(date: Date): moment {
    if (moment(new Date(date)).isValid()) {
      return moment(date).tz(this.getLocalConfig('UI_TIMEZONE')).format('ddd MMM D YYYY HH:mm:ss.SSS [GMT]ZZ');
    }
    return null;
  }

  private updateLoading(flag: boolean): void {
    ProgressBarService.updateLoading(flag);
  }
}
