import {inject, TestBed} from '@angular/core/testing';

import {ClhNonPaymentLegDataService} from './clh-non-payment-leg-data.service';

describe('ClhNonPaymentLegDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ClhNonPaymentLegDataService]
    });
  });

  it('should be created', inject([ClhNonPaymentLegDataService], (service: ClhNonPaymentLegDataService) => {
    expect(service).toBeTruthy();
  }));
});
