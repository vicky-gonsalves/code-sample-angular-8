export class PageTitle {
  title?: string;

  constructor(data?: PageTitle) {
    if (data) {
      this.title = data.title;
    }
  }
}
