import {Injectable} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {BehaviorSubject} from 'rxjs';
import {PageTitle} from './page-title';

@Injectable({
  providedIn: 'root'
})
export class PageTitleService {
  constructor(private bodyTitle: Title) {
  }

  public _title = new BehaviorSubject<PageTitle>({title: ''});

  get title() {
    return this._title;
  }

  setTitle(title: string) {
    this._title.next({title: title});

    if (title !== '') {
      title = `Payment Certification Platform | ${title} `;
    }
    this.bodyTitle.setTitle(`${title}`);
  }


}
