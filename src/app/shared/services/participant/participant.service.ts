import {Injectable} from '@angular/core';
import {ErrorMessageService} from '../error-message/error-message.service';
import {catchError, map, tap} from 'rxjs/internal/operators';
import {Observable} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import {ProgressBarService} from '../progress-bar/progress-bar.service';
import {ParticipantInterface} from '../../../interface/participant.interface';
import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ParticipantService {
  private apiUrl: string = environment.apiUrl + 'participants';

  constructor(private http: HttpClient) {
  }

  public getParticipants(options: {
    q?: string,
    page?: number,
    limit?: number,
    sort?: string[],
    fields?: string[]
  }): Observable<any> {
    let params = new HttpParams();
    for (const key in options) {
      if (key && options.hasOwnProperty(key)) {
        params = params.append(key, options[key]);
      }
    }
    this.updateLoading(true);
    return this.http.get<any>(`${this.apiUrl}`, {params: params})
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public getParticipantsForAutocomplete(options: {
    term?: string,
    page?: number,
    limit?: number,
    sort?: string[],
    fields?: string[]
  }): Observable<any> {
    let params = new HttpParams();
    for (const key in options) {
      if (key && options.hasOwnProperty(key)) {
        params = params.append(key, options[key]);
      }
    }
    this.updateLoading(true);
    return this.http.get<any>(`${this.apiUrl}/get-participants/autocomplete`, {params: params})
      .pipe(
        map(data => data.data && data.data.rows && data.data.count ? data.data.rows : []),
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public addParticipant(participant: ParticipantInterface): Observable<any> {
    this.updateLoading(true);
    return this.http.post<any>(`${this.apiUrl}`, participant)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()))
      ;
  }

  public updateParticipant(participant: ParticipantInterface): Observable<any> {
    this.updateLoading(true);
    return this.http.put<any>(`${this.apiUrl}/${participant._id}`, participant)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public getParticipant(id: string): Observable<any> {
    this.updateLoading(true);
    return this.http.get<any>(`${this.apiUrl}/${id}`)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  private updateLoading(flag: boolean): void {
    ProgressBarService.updateLoading(flag);
  }
}
