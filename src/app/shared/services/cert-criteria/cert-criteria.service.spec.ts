import {inject, TestBed} from '@angular/core/testing';

import {CertCriteriaService} from './cert-criteria.service';

describe('CertCriteriaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CertCriteriaService]
    });
  });

  it('should be created', inject([CertCriteriaService], (service: CertCriteriaService) => {
    expect(service).toBeTruthy();
  }));
});
