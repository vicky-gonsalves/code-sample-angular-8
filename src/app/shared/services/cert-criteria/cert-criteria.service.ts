import {Injectable} from '@angular/core';
import {CertCriteriaInterface} from '../../../interface/cert-criteria.interface';
import {HttpClient, HttpParams} from '@angular/common/http';
import {ProgressBarService} from '../progress-bar/progress-bar.service';
import {catchError, map, tap} from 'rxjs/internal/operators';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs';
import {ErrorMessageService} from '../error-message/error-message.service';

@Injectable({
  providedIn: 'root'
})
export class CertCriteriaService {
  private apiUrl: string = environment.apiUrl + 'cert-criterias';

  constructor(private http: HttpClient) {
  }

  public getCertCriterias(options: {
    q?: string,
    page?: number,
    limit?: number,
    sort?: string[],
    fields?: string[],
    is_deactive?: Boolean,
    forExecution?: Boolean
  }): Observable<any> {
    let params = new HttpParams();
    for (const key in options) {
      if (key && options.hasOwnProperty(key)) {
        params = params.append(key, options[key]);
      }
    }
    this.updateLoading(true);
    return this.http.get<any>(`${this.apiUrl}`, {params: params})
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public getCertCriteriasForAutocomplete(options: {
    term?: string,
    page?: number,
    limit?: number,
    sort?: string[],
    fields?: string[],
    is_bulk?: boolean
  }): Observable<any> {
    let params = new HttpParams();
    for (const key in options) {
      if (key && options.hasOwnProperty(key)) {
        params = params.append(key, options[key]);
      }
    }
    this.updateLoading(true);
    return this.http.get<any>(`${this.apiUrl}/get-cert-criterias/autocomplete`, {params: params})
      .pipe(
        map(data => data.data && data.data.rows && data.data.count ? data.data.rows : []),
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public getCertCriteriaTestTemplates(options: {
    q?: string,
    page?: number,
    limit?: number,
    sort?: string[],
    fields?: string[]
  }, id: string): Observable<any> {
    let params = new HttpParams();
    for (const key in options) {
      if (key && options.hasOwnProperty(key)) {
        params = params.append(key, options[key]);
      }
    }
    this.updateLoading(true);
    return this.http.get<any>(`${this.apiUrl}/get-test-templates/${id}`, {params: params})
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public addCertCriteria(certCriteria: CertCriteriaInterface): Observable<any> {
    this.updateLoading(true);
    return this.http.post<any>(`${this.apiUrl}`, certCriteria)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public updateCertCriteria(certCriteria: CertCriteriaInterface): Observable<any> {
    this.updateLoading(true);
    return this.http.put<any>(`${this.apiUrl}/${certCriteria._id}`, certCriteria)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public updateCertCriteriaTemplateName(id: string, certCriteria: any): Observable<any> {
    this.updateLoading(true);
    return this.http.patch<any>(`${this.apiUrl}/update-template/${id}`, certCriteria)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public getCertCriteria(id: string, forExecution?: Boolean): Observable<any> {
    this.updateLoading(true);
    let url = `${this.apiUrl}/${id}`;
    if (forExecution) {
      url = `${this.apiUrl}/${id}?forExecution=true`;
    }
    return this.http.get<any>(url)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public deleteFieldValue(id: string, templateName: string): Observable<any> {
    this.updateLoading(true);
    return this.http.delete<any>(`${this.apiUrl}/${id}/${templateName}`)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  private updateLoading(flag: boolean): void {
    ProgressBarService.updateLoading(flag);
  }
}
