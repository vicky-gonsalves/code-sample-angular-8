import {inject, TestBed} from '@angular/core/testing';

import {CertCriteriaRepOutService} from './cert-criteria-rep-out.service';

describe('CertCriteriaRepOutService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CertCriteriaRepOutService]
    });
  });

  it('should be created', inject([CertCriteriaRepOutService], (service: CertCriteriaRepOutService) => {
    expect(service).toBeTruthy();
  }));
});
