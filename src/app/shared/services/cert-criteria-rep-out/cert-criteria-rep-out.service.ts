import {Injectable} from '@angular/core';
import {catchError, tap} from 'rxjs/internal/operators';
import {ErrorMessageService} from '../error-message/error-message.service';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import {ProgressBarService} from '../progress-bar/progress-bar.service';

@Injectable({
  providedIn: 'root'
})
export class CertCriteriaRepOutService {

  private apiUrl: string = environment.apiUrl + 'cert-criteria-rep-out';

  constructor(private http: HttpClient) {
  }

  public getCertCriteriaRepOutList(options: {
    q?: string,
    page?: number,
    limit?: number,
    sort?: string[],
    fields?: string[]
  }): Observable<any> {
    let params = new HttpParams();
    for (const key in options) {
      if (key && options.hasOwnProperty(key)) {
        params = params.append(key, options[key]);
      }
    }
    this.updateLoading(true);
    return this.http.get<any>(`${this.apiUrl}`, {params: params})
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public getCertCriteriaRepOutTemplates(options: {
    q?: string,
    page?: number,
    limit?: number,
    sort?: string[],
    fields?: string[]
  }, id: string): Observable<any> {
    let params = new HttpParams();
    for (const key in options) {
      if (key && options.hasOwnProperty(key)) {
        params = params.append(key, options[key]);
      }
    }
    this.updateLoading(true);
    return this.http.get<any>(`${this.apiUrl}/get-templates/${id}`, {params: params})
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  // public addCertCriteria(certCriteria: CertCriteriaInterface): Observable<any> {
  //   this.updateLoading(true);
  //   return this.http.post<any>(`${this.apiUrl}`, certCriteria)
  //     .pipe(
  //       tap(() => this.updateLoading(false)),
  //       catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  // }
  //
  // public updateCertCriteria(certCriteria: CertCriteriaInterface): Observable<any> {
  //   this.updateLoading(true);
  //   return this.http.put<any>(`${this.apiUrl}/${certCriteria._id}`, certCriteria)
  //     .pipe(
  //       tap(() => this.updateLoading(false)),
  //       catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  // }
  //
  // public updateCertCriteriaTemplateName(id: string, certCriteria: any): Observable<any> {
  //   this.updateLoading(true);
  //   return this.http.patch<any>(`${this.apiUrl}/update-template/${id}`, certCriteria)
  //     .pipe(
  //       tap(() => this.updateLoading(false)),
  //       catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  // }
  //
  public getCertCriteriaRepOut(id: string): Observable<any> {
    this.updateLoading(true);
    return this.http.get<any>(`${this.apiUrl}/${id}`)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  // public deleteFieldValue(id: string, templateName: string): Observable<any> {
  //   this.updateLoading(true);
  //   return this.http.delete<any>(`${this.apiUrl}/${id}/${templateName}`)
  //     .pipe(
  //       tap(() => this.updateLoading(false)),
  //       catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  // }

  private updateLoading(flag: boolean): void {
    ProgressBarService.updateLoading(flag);
  }
}
