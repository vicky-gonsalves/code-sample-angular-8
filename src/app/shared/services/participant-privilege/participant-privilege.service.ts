import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ErrorMessageService} from '../error-message/error-message.service';
import {catchError, map, tap} from 'rxjs/internal/operators';
import {environment} from '../../../../environments/environment';
import {ProgressBarService} from '../progress-bar/progress-bar.service';

@Injectable({
  providedIn: 'root'
})
export class ParticipantPrivilegeService {


  private apiUrl: string = environment.apiUrl + 'participant-privileges';

  constructor(private http: HttpClient) {
  }

  public getParticipantPrivilegesForAutocomplete(options: {
    term?: string,
    page?: number,
    limit?: number,
    sort?: string[],
    fields?: string[],
    exists?: Boolean
  }, excludeList: string[]): Observable<any> {
    let params = new HttpParams();
    for (const key in options) {
      if (key && options.hasOwnProperty(key)) {
        params = params.append(key, options[key]);
      }
    }
    this.updateLoading(true);
    return this.http.post<any>(`${this.apiUrl}/get-participant-privileges/autocomplete`, {excludeList: excludeList}, {params: params})
      .pipe(
        map(data => data.data && data.data.rows && data.data.count ? data.data.rows : []),
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  private updateLoading(flag: boolean): void {
    ProgressBarService.updateLoading(flag);
  }
}
