import {inject, TestBed} from '@angular/core/testing';

import {ParticipantPrivilegeService} from './participant-privilege.service';

describe('ParticipantPrivilegeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ParticipantPrivilegeService]
    });
  });

  it('should be created', inject([ParticipantPrivilegeService], (service: ParticipantPrivilegeService) => {
    expect(service).toBeTruthy();
  }));
});
