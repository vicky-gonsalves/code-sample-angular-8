import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {ErrorMessageService} from '../error-message/error-message.service';
import {ProgressBarService} from '../progress-bar/progress-bar.service';

@Injectable({
  providedIn: 'root'
})
export class CertCriteriaExecLogService {

  private apiUrl: string = environment.apiUrl + 'cert-criteria-exec-log';

  constructor(private http: HttpClient) {
  }

  public getTransactions(options: {
    term?: string,
    page?: number,
    limit?: number,
    sort?: string[],
    fields?: string[]
  }, clhPlatform: any): Observable<any> {
    let params = new HttpParams();
    for (const key in options) {
      if (key && options.hasOwnProperty(key)) {
        params = params.append(key, options[key]);
      }
    }
    this.updateLoading(true);
    return this.http.post<any>(`${this.apiUrl}/get-transactions`, clhPlatform, {params: params})
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  private updateLoading(flag: boolean): void {
    ProgressBarService.updateLoading(flag);
  }

}
