import { TestBed, inject } from '@angular/core/testing';

import { CertCriteriaExecLogService } from './cert-criteria-exec-log.service';

describe('CertCriteriaExecLogService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CertCriteriaExecLogService]
    });
  });

  it('should be created', inject([CertCriteriaExecLogService], (service: CertCriteriaExecLogService) => {
    expect(service).toBeTruthy();
  }));
});
