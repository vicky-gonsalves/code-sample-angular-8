import { TestBed, inject } from '@angular/core/testing';

import { QueryResultStoreAdminMessageService } from './query-result-store-admin-message.service';

describe('QueryResultStoreAdminMessageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [QueryResultStoreAdminMessageService]
    });
  });

  it('should be created', inject([QueryResultStoreAdminMessageService], (service: QueryResultStoreAdminMessageService) => {
    expect(service).toBeTruthy();
  }));
});
