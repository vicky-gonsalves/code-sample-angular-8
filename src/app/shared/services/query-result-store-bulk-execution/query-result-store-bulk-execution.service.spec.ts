import { TestBed, inject } from '@angular/core/testing';

import { QueryResultStoreBulkExecutionService } from './query-result-store-bulk-execution.service';

describe('QueryResultStoreBulkExecutionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [QueryResultStoreBulkExecutionService]
    });
  });

  it('should be created', inject([QueryResultStoreBulkExecutionService], (service: QueryResultStoreBulkExecutionService) => {
    expect(service).toBeTruthy();
  }));
});
