import {Injectable} from '@angular/core';
import {ErrorMessageService} from '../error-message/error-message.service';
import {Observable} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import {ContactInterface} from '../../../interface/contact.interface';
import {ProgressBarService} from '../progress-bar/progress-bar.service';
import {catchError, tap} from 'rxjs/internal/operators';
import {environment} from '../../../../environments/environment';
import {IdentityService} from '../../../core/services/identity/identity.service';
import {AuthenticationService} from '../../../core/services/authentication/authentication.service';
import {Contact} from '../../../model/contact.model';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  private apiUrl: string = environment.apiUrl + 'contacts';

  constructor(private http: HttpClient,
              private identityService: IdentityService) {
  }

  public getContacts(options: { q?: string, page?: number, limit?: number, sort?: string[], fields?: string[] }): Observable<any> {
    let params = new HttpParams();
    for (const key in options) {
      if (key && options.hasOwnProperty(key)) {
        params = params.append(key, options[key]);
      }
    }
    this.updateLoading(true);
    return this.http.get<any>(`${this.apiUrl}`, {params: params})
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public addContact(contact: ContactInterface): Observable<any> {
    this.updateLoading(true);
    return this.http.post<any>(`${this.apiUrl}`, contact)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public updateContact(contact: ContactInterface): Observable<any> {
    this.updateLoading(true);
    return this.http.put<any>(`${this.apiUrl}/${contact._id}`, contact)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public sendUnlockToken(contact: ContactInterface): Observable<any> {
    this.updateLoading(true);
    return this.http.patch<any>(`${this.apiUrl}/send-unlock-token/${contact._id}`, null)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public sendActivationToken(contact: ContactInterface): Observable<any> {
    this.updateLoading(true);
    return this.http.patch<any>(`${this.apiUrl}/resend-activation-token/${contact._id}`, null)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public getContact(id: string): Observable<any> {
    this.updateLoading(true);
    return this.http.get<any>(`${this.apiUrl}/${id}`)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public me(): Observable<any> {
    this.updateLoading(true);
    return this.http.get<any>(`${this.apiUrl}/me`)
      .pipe(
        tap(() => this.updateLoading(false)),
        tap((response: any) => {
          if (response && response.hasOwnProperty('data') &&
            response.data && response.data.hasOwnProperty('contact') && response.data.contact) {
            this.identityService.updateIdentity(response.data.contact);
          }
        }),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public activateContact(id: string, body: any): Observable<any> {
    this.updateLoading(true);
    return this.http.patch<any>(`${this.apiUrl}/${id}/activate`, body)
      .pipe(
        tap(() => this.updateLoading(false)),
        tap((response: any) => {
          if (response && response.hasOwnProperty('data') &&
            response.data && response.data.hasOwnProperty('contact') && response.data.contact) {
            AuthenticationService.saveAuthToken(response);
            this.identityService.updateIdentity(response.data.contact);
          }
        }),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public unlockContact(id: string, body: any): Observable<any> {
    this.updateLoading(true);
    return this.http.patch<any>(`${this.apiUrl}/${id}/unlock`, body)
      .pipe(
        tap(() => this.updateLoading(false)),
        tap((response: any) => {
          if (response && response.hasOwnProperty('data') &&
            response.data && response.data.hasOwnProperty('contact') && response.data.contact) {
            AuthenticationService.saveAuthToken(response);
            this.identityService.updateIdentity(response.data.contact);
          }
        }),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public resetPassword(id: string, body: any): Observable<any> {
    this.updateLoading(true);
    return this.http.patch<any>(`${this.apiUrl}/${id}/reset-password`, body)
      .pipe(
        tap(() => this.updateLoading(false)),
        tap((response: any) => {
          if (response && response.hasOwnProperty('data') &&
            response.data && response.data.hasOwnProperty('contact') && response.data.contact) {
            AuthenticationService.saveAuthToken(response);
            this.identityService.updateIdentity(response.data.contact);
          }
        }),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public verifyTempToken(id: string, body: any): Observable<any> {
    this.updateLoading(true);
    return this.http.post<any>(`${this.apiUrl}/${id}/verify`, body)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public requestResetPassword(contact: Contact): Observable<any> {
    this.updateLoading(true);
    return this.http.patch<any>(`${this.apiUrl}/send-reset-password-token`, contact)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>())
      );
  }

  public verifyResetToken(token: string): Observable<any> {
    this.updateLoading(true);
    return this.http.get<any>(`${this.apiUrl}/${token}`)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>())
      );
  }

  public changePassword(id: string, contact: Contact): Observable<any> {
    this.updateLoading(true);
    return this.http.patch<any>(`${this.apiUrl}/${id}/change-password`, contact)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>())
      );
  }

  private updateLoading(flag: boolean): void {
    ProgressBarService.updateLoading(flag);
  }
}
