import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {PageHeaderInterface} from '../../../interface/page-header.interface';
import {PageHeader} from '../../../model/page-header.model';

@Injectable({
  providedIn: 'root'
})
export class PageHeaderService {

  constructor() {
  }

  private static _pageHeader = new BehaviorSubject<PageHeader>(null);

  public static get pageHeader() {
    return PageHeaderService._pageHeader;
  }

  public static updatePageHeader(details: PageHeaderInterface) {
    PageHeaderService._pageHeader.next(details);
  }
}
