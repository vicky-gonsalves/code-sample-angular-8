import {Injectable} from '@angular/core';
import {FormControl} from '@angular/forms';
import * as moment from 'moment-timezone';

@Injectable({
  providedIn: 'root'
})
export class InstructionIdValidatorService {

  constructor() {
  }

  validate() {
    let thisControl: FormControl;
    return function matchOtherValidate(control: FormControl) {
      if (!control.parent) {
        return null;
      }
      // Initializing the validator.
      if (!thisControl) {
        thisControl = control;
      }
      if (thisControl.value) {
        if (thisControl.value.match(/^[\d]/)) {
          const date = moment(thisControl.value.substring(0, 8), 'YYYYMMDD', true);
          if (!date.isValid()) {
            return {format: true};
          }
        } else if (thisControl.value.match(/^M\d/)) {
          const date = moment(thisControl.value.substring(1, 9), 'YYYYMMDD', true);
          if (!date.isValid()) {
            return {format: true};
          }
        } else {
          return {format: true};
        }
      }
      return null;
    };
  }
}
