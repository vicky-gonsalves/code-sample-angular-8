import { TestBed, inject } from '@angular/core/testing';

import { InstructionIdValidatorService } from './instruction-id-validator.service';

describe('InstructionIdValidatorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InstructionIdValidatorService]
    });
  });

  it('should be created', inject([InstructionIdValidatorService], (service: InstructionIdValidatorService) => {
    expect(service).toBeTruthy();
  }));
});
