import {inject, TestBed} from '@angular/core/testing';

import {TestTemplateService} from './test-template.service';

describe('TestTemplateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TestTemplateService]
    });
  });

  it('should be created', inject([TestTemplateService], (service: TestTemplateService) => {
    expect(service).toBeTruthy();
  }));
});
