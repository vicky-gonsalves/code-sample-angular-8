import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs';
import {TestTemplateFieldValuesInterface, TestTemplateInterface} from '../../../interface/test-template.interface';
import {ErrorMessageService} from '../error-message/error-message.service';
import {HttpClient, HttpParams} from '@angular/common/http';
import {catchError, map, tap} from 'rxjs/internal/operators';
import {ProgressBarService} from '../progress-bar/progress-bar.service';

@Injectable({
  providedIn: 'root'
})
export class TestTemplateService {

  private apiUrl: string = environment.apiUrl + 'test-templates';

  constructor(private http: HttpClient) {
  }

  public getTestTemplates(options: {
    q?: string,
    page?: number,
    limit?: number,
    sort?: string[],
    fields?: string[]
  }): Observable<any> {
    let params = new HttpParams();
    for (const key in options) {
      if (key && options.hasOwnProperty(key)) {
        params = params.append(key, options[key]);
      }
    }
    this.updateLoading(true);
    return this.http.get<any>(`${this.apiUrl}`, {params: params})
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public getTestTemplateFieldValues(options: {
    q?: string,
    page?: number,
    limit?: number,
    sort?: string[],
    fields?: string[]
  }, id: string): Observable<any> {
    let params = new HttpParams();
    for (const key in options) {
      if (key && options.hasOwnProperty(key)) {
        params = params.append(key, options[key]);
      }
    }
    this.updateLoading(true);
    return this.http.get<any>(`${this.apiUrl}/get-field-values/${id}`, {params: params})
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public getTestTemplatesForAutocomplete(options: {
    default?: string,
    term?: string,
    page?: number,
    limit?: number,
    sort?: string[],
    fields?: string[]
  }, criteriaType: string, participantId: string, excludeList: string[]): Observable<any> {
    let params = new HttpParams();
    for (const key in options) {
      if (key && options.hasOwnProperty(key)) {
        params = params.append(key, options[key]);
      }
    }
    this.updateLoading(true);
    return this.http.post<any>(`${this.apiUrl}/get-test-templates/autocomplete`, {
      excludeList: excludeList,
      criteriaType: criteriaType,
      participantId: participantId
    }, {params: params})
      .pipe(
        map(data => data.data && data.data.rows && data.data.count ? data.data.rows : []),
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public addTestTemplate(testTemplate: TestTemplateInterface): Observable<any> {
    this.updateLoading(true);
    return this.http.post<any>(`${this.apiUrl}`, testTemplate)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public updateTestTemplate(testTemplate: TestTemplateInterface): Observable<any> {
    this.updateLoading(true);
    return this.http.put<any>(`${this.apiUrl}/${testTemplate._id}`, testTemplate)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public updateTestTemplateFieldValues(testTemplateFields: TestTemplateFieldValuesInterface, id: string, fieldId: string): Observable<any> {
    this.updateLoading(true);
    return this.http.patch<any>(`${this.apiUrl}/${id}/${fieldId}`, testTemplateFields)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public addTestTemplateFieldValues(testTemplateFields: TestTemplateFieldValuesInterface, id: string): Observable<any> {
    this.updateLoading(true);
    return this.http.put<any>(`${this.apiUrl}/${id}/add`, testTemplateFields)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public getTestTemplate(id: string): Observable<any> {
    this.updateLoading(true);
    return this.http.get<any>(`${this.apiUrl}/${id}`)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public deleteFieldValue(id: string, fieldId: string): Observable<any> {
    this.updateLoading(true);
    return this.http.delete<any>(`${this.apiUrl}/${id}/${fieldId}`)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  private updateLoading(flag: boolean): void {
    ProgressBarService.updateLoading(flag);
  }
}
