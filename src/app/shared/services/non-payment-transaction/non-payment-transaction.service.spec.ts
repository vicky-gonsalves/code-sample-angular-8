import {inject, TestBed} from '@angular/core/testing';

import {NonPaymentTransactionService} from './non-payment-transaction.service';

describe('NonPaymentTransactionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NonPaymentTransactionService]
    });
  });

  it('should be created', inject([NonPaymentTransactionService], (service: NonPaymentTransactionService) => {
    expect(service).toBeTruthy();
  }));
});
