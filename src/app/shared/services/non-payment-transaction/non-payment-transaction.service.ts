import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {catchError, tap} from 'rxjs/internal/operators';
import {ProgressBarService} from '../progress-bar/progress-bar.service';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs';
import {ErrorMessageService} from '../error-message/error-message.service';
import {NonPaymentTransactionInterface} from '../../../interface/non-payment-transaction.interface';

@Injectable({
  providedIn: 'root'
})
export class NonPaymentTransactionService {
  private apiUrl: string = environment.apiUrl + 'non-payment-transactions';

  constructor(private http: HttpClient) {
  }

  public getNonPaymentTransactions(options: {
    q?: string,
    page?: number,
    limit?: number,
    sort?: string[],
    fields?: string[]
  }, query: NonPaymentTransactionInterface): Observable<any> {
    let params = new HttpParams();
    for (const key in options) {
      if (key && options.hasOwnProperty(key)) {
        params = params.append(key, options[key]);
      }
    }
    this.updateLoading(true);
    return this.http.post<any>(`${this.apiUrl}`, query, {params: params})
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  private updateLoading(flag: boolean): void {
    ProgressBarService.updateLoading(flag);
  }
}
