import { TestBed, inject } from '@angular/core/testing';

import { ClhSenMsgService } from './clh-sen-msg.service';

describe('ClhSenMsgService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ClhSenMsgService]
    });
  });

  it('should be created', inject([ClhSenMsgService], (service: ClhSenMsgService) => {
    expect(service).toBeTruthy();
  }));
});
