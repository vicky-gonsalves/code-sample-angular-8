import {Injectable} from '@angular/core';
import {ProgressBarService} from '../progress-bar/progress-bar.service';
import {ErrorMessageService} from '../error-message/error-message.service';
import {environment} from '../../../../environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {catchError, map, tap} from 'rxjs/internal/operators';

@Injectable({
  providedIn: 'root'
})
export class MsgFieldDetailService {
  private apiUrl: string = environment.apiUrl + 'msg-fld-details';

  constructor(private http: HttpClient) {
  }

  public getMsgFldDetails(options: {
    q?: string,
    page?: number,
    limit?: number,
    sort?: string[],
    fields?: string[]
  }): Observable<any> {
    let params = new HttpParams();
    for (const key in options) {
      if (key && options.hasOwnProperty(key)) {
        params = params.append(key, options[key]);
      }
    }
    this.updateLoading(true);
    return this.http.get<any>(`${this.apiUrl}`, {params: params})
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public getMsgFldDetailsForAutocomplete(options: {
    term?: string,
    msg_type?: string,
    specific?: Boolean,
    page?: number,
    limit?: number,
    sort?: string[],
    fields?: string[]
  }, usedFields: string[]): Observable<any> {
    let params = new HttpParams();
    for (const key in options) {
      if (key && options.hasOwnProperty(key)) {
        params = params.append(key, options[key]);
      }
    }
    this.updateLoading(true);
    return this.http.post<any>(`${this.apiUrl}/get-msg-fld-details/autocomplete`, {usedFields: usedFields}, {params: params})
      .pipe(
        map(data => data.data && data.data.rows && data.data.count ? data.data.rows : []),
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  private updateLoading(flag: boolean): void {
    ProgressBarService.updateLoading(flag);
  }
}
