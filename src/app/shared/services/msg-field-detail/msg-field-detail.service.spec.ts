import {inject, TestBed} from '@angular/core/testing';

import {MsgFieldDetailService} from './msg-field-detail.service';

describe('MsgFieldDetailService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MsgFieldDetailService]
    });
  });

  it('should be created', inject([MsgFieldDetailService], (service: MsgFieldDetailService) => {
    expect(service).toBeTruthy();
  }));
});
