import {inject, TestBed} from '@angular/core/testing';

import {PaymentTransactionService} from './payment-transaction.service';

describe('PaymentTransactionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PaymentTransactionService]
    });
  });

  it('should be created', inject([PaymentTransactionService], (service: PaymentTransactionService) => {
    expect(service).toBeTruthy();
  }));
});
