import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {catchError, tap} from 'rxjs/internal/operators';
import {ErrorMessageService} from '../error-message/error-message.service';
import {Observable} from 'rxjs';
import {ProgressBarService} from '../progress-bar/progress-bar.service';
import {PaymentTransactionInterface} from '../../../interface/payment-transaction.interface';

@Injectable({
  providedIn: 'root'
})
export class PaymentTransactionService {
  private apiUrl: string = environment.apiUrl + 'payment-transactions';

  constructor(private http: HttpClient) {
  }

  public getPaymentTransactions(options: {
    q?: string,
    page?: number,
    limit?: number,
    sort?: string[],
    fields?: string[]
  }, query: PaymentTransactionInterface): Observable<any> {
    let params = new HttpParams();
    for (const key in options) {
      if (key && options.hasOwnProperty(key)) {
        params = params.append(key, options[key]);
      }
    }
    this.updateLoading(true);
    return this.http.post<any>(`${this.apiUrl}`, query, {params: params})
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  private updateLoading(flag: boolean): void {
    ProgressBarService.updateLoading(flag);
  }
}
