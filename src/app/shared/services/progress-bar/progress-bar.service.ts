import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProgressBarService {
  constructor() {
  }

  private static _isProgressive = new BehaviorSubject<Boolean>(false);

  public static get isProgressive() {
    return ProgressBarService._isProgressive;
  }

  public static updateLoading(flag: Boolean) {
    ProgressBarService._isProgressive.next(flag);
  }
}
