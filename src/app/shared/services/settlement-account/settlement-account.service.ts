import {Injectable} from '@angular/core';
import {SettlementAccountInterface} from '../../../interface/settlement-account.interface';
import {ProgressBarService} from '../progress-bar/progress-bar.service';
import {catchError, map, tap} from 'rxjs/internal/operators';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {ErrorMessageService} from '../error-message/error-message.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SettlementAccountService {

  private apiUrl: string = environment.apiUrl + 'settlement-accounts';

  constructor(private http: HttpClient) {
  }

  public getSettlementAccounts(options: {
    q?: string, page?: number, limit?: number, sort?: string[],
    fields?: string[]
  }): Observable<any> {
    let params = new HttpParams();
    for (const key in options) {
      if (key && options.hasOwnProperty(key)) {
        params = params.append(key, options[key]);
      }
    }
    this.updateLoading(true);
    return this.http.get<any>(`${this.apiUrl}`, {params: params})
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public getSettlementAccountsForAutocomplete(options: {
    term?: string,
    page?: number,
    limit?: number,
    sort?: string[],
    fields?: string[],
    exists?: Boolean
  }, id?: string): Observable<any> {
    let params = new HttpParams();
    for (const key in options) {
      if (key && options.hasOwnProperty(key)) {
        params = params.append(key, options[key]);
      }
    }
    let path = `${this.apiUrl}/get-settlement-accounts/autocomplete`;
    if (id) {
      path += ('/' + id);
    }
    this.updateLoading(true);
    return this.http.get<any>(path, {params: params})
      .pipe(
        map(data => data.data && data.data.rows && data.data.count ? data.data.rows : []),
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public addSettlementAccount(settlementAccount: SettlementAccountInterface): Observable<any> {
    this.updateLoading(true);
    return this.http.post<any>(`${this.apiUrl}`, settlementAccount)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public updateSettlementAccount(settlementAccount: SettlementAccountInterface): Observable<any> {
    this.updateLoading(true);
    return this.http.put<any>(`${this.apiUrl}/${settlementAccount._id}`, settlementAccount)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public getSettlementAccount(id: string): Observable<any> {
    this.updateLoading(true);
    return this.http.get<any>(`${this.apiUrl}/${id}`)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public getCreditDebitSettlementAccount(query: any): Observable<any> {
    this.updateLoading(true);
    return this.http.post<any>(`${this.apiUrl}/get-credit-debit-settlement-account`, query)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public getSnapshotSettlementPosition(query: any): Observable<any> {
    this.updateLoading(true);
    return this.http.post<any>(`${this.apiUrl}/get-snapshot-settlement-position`, query)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  private updateLoading(flag: boolean): void {
    ProgressBarService.updateLoading(flag);
  }
}
