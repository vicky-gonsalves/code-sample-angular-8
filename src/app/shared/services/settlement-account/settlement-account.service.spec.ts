import {inject, TestBed} from '@angular/core/testing';

import {SettlementAccountService} from './settlement-account.service';

describe('SettlementAccountService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SettlementAccountService]
    });
  });

  it('should be created', inject([SettlementAccountService], (service: SettlementAccountService) => {
    expect(service).toBeTruthy();
  }));
});
