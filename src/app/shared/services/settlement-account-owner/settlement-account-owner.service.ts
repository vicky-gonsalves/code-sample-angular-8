import {Injectable} from '@angular/core';
import {ProgressBarService} from '../progress-bar/progress-bar.service';
import {catchError, map, tap} from 'rxjs/internal/operators';
import {SettlementAccountOwnerInterface} from '../../../interface/settlement-account-owner.interface';
import {environment} from '../../../../environments/environment';
import {ErrorMessageService} from '../error-message/error-message.service';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SettlementAccountOwnerService {

  private apiUrl: string = environment.apiUrl + 'settlement-account-owners';

  constructor(private http: HttpClient) {
  }

  public getSettlementAccountOwners(options: {
    q?: string,
    page?: number,
    limit?: number,
    sort?: string[],
    fields?: string[]
  }): Observable<any> {
    let params = new HttpParams();
    for (const key in options) {
      if (key && options.hasOwnProperty(key)) {
        params = params.append(key, options[key]);
      }
    }
    this.updateLoading(true);
    return this.http.get<any>(`${this.apiUrl}`, {params: params})
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public getSettlementAccountOwnersForAutocomplete(options: {
    account_type: string,
    term?: string,
    page?: number,
    limit?: number,
    sort?: string[],
    fields?: string[]
  }): Observable<any> {
    let params = new HttpParams();
    for (const key in options) {
      if (key && options.hasOwnProperty(key)) {
        params = params.append(key, options[key]);
      }
    }
    this.updateLoading(true);
    return this.http.get<any>(`${this.apiUrl}/get-owners/autocomplete`, {params: params})
      .pipe(
        map(data => data.data && data.data.rows && data.data.count ? data.data.rows : []),
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public addSettlementAccountOwner(settlementAccountOwner: SettlementAccountOwnerInterface): Observable<any> {
    this.updateLoading(true);
    return this.http.post<any>(`${this.apiUrl}`, settlementAccountOwner)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()))
      ;
  }

  public updateSettlementAccountOwner(settlementAccountOwner: SettlementAccountOwnerInterface): Observable<any> {
    this.updateLoading(true);
    return this.http.put<any>(`${this.apiUrl}/${settlementAccountOwner._id}`, settlementAccountOwner)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public getSettlementAccountOwner(id: string): Observable<any> {
    this.updateLoading(true);
    return this.http.get<any>(`${this.apiUrl}/${id}`)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  private updateLoading(flag: boolean): void {
    ProgressBarService.updateLoading(flag);
  }
}
