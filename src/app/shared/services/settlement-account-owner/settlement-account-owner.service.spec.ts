import { TestBed, inject } from '@angular/core/testing';

import { SettlementAccountOwnerService } from './settlement-account-owner.service';

describe('SettlementAccountOwnerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SettlementAccountOwnerService]
    });
  });

  it('should be created', inject([SettlementAccountOwnerService], (service: SettlementAccountOwnerService) => {
    expect(service).toBeTruthy();
  }));
});
