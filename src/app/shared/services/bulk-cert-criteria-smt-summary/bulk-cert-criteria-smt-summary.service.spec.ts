import { TestBed, inject } from '@angular/core/testing';

import { BulkCertCriteriaSmtSummaryService } from './bulk-cert-criteria-smt-summary.service';

describe('BulkCertCriteriaSmtSummaryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BulkCertCriteriaSmtSummaryService]
    });
  });

  it('should be created', inject([BulkCertCriteriaSmtSummaryService], (service: BulkCertCriteriaSmtSummaryService) => {
    expect(service).toBeTruthy();
  }));
});
