import {Injectable} from '@angular/core';
import {catchError, tap} from 'rxjs/internal/operators';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs';
import {ErrorMessageService} from '../error-message/error-message.service';
import {HttpClient, HttpParams} from '@angular/common/http';
import {ProgressBarService} from '../progress-bar/progress-bar.service';
import {SettlementSummaryInterface} from '../../../interface/settlement-summary.interface';

@Injectable({
  providedIn: 'root'
})
export class SettlementSummaryService {
  private apiUrl: string = environment.apiUrl + 'settlement-summary';

  constructor(private http: HttpClient) {
  }

  public getSettlementSummaries(options: {
    q?: string,
    page?: number,
    limit?: number,
    sort?: string[],
    fields?: string[]
  }, settlementSummary: SettlementSummaryInterface): Observable<any> {
    let params = new HttpParams();
    for (const key in options) {
      if (key && options.hasOwnProperty(key)) {
        params = params.append(key, options[key]);
      }
    }
    this.updateLoading(true);
    return this.http.post<any>(`${this.apiUrl}`, settlementSummary, {params: params})
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }


  public getSettlementSummary(id: string): Observable<any> {
    this.updateLoading(true);
    return this.http.get<any>(`${this.apiUrl}/${id}`)
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public getSettlementSummaryCreditList(options: {
    q?: string,
    page?: number,
    limit?: number,
    sort?: string[],
    fields?: string[]
  }, id: string): Observable<any> {
    let params = new HttpParams();
    for (const key in options) {
      if (key && options.hasOwnProperty(key)) {
        params = params.append(key, options[key]);
      }
    }
    this.updateLoading(true);
    return this.http.get<any>(`${this.apiUrl}/get-credit-list/${id}`, {params: params})
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  public getSettlementSummaryDebitList(options: {
    q?: string,
    page?: number,
    limit?: number,
    sort?: string[],
    fields?: string[]
  }, id: string): Observable<any> {
    let params = new HttpParams();
    for (const key in options) {
      if (key && options.hasOwnProperty(key)) {
        params = params.append(key, options[key]);
      }
    }
    this.updateLoading(true);
    return this.http.get<any>(`${this.apiUrl}/get-debit-list/${id}`, {params: params})
      .pipe(
        tap(() => this.updateLoading(false)),
        catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }


  private updateLoading(flag: boolean): void {
    ProgressBarService.updateLoading(flag);
  }
}
