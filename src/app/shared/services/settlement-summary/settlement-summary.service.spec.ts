import {inject, TestBed} from '@angular/core/testing';

import {SettlementSummaryService} from './settlement-summary.service';

describe('SettlementSummaryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SettlementSummaryService]
    });
  });

  it('should be created', inject([SettlementSummaryService], (service: SettlementSummaryService) => {
    expect(service).toBeTruthy();
  }));
});
