import {inject, TestBed} from '@angular/core/testing';

import {QueryResultStoreService} from './query-result-store.service';

describe('QueryResultStoreNonPaymentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [QueryResultStoreService]
    });
  });

  it('should be created', inject([QueryResultStoreService], (service: QueryResultStoreService) => {
    expect(service).toBeTruthy();
  }));
});
