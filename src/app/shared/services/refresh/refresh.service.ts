import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RefreshService {
  private refresh = new Subject<any>();
  refreshObservable$ = this.refresh.asObservable();

  constructor() {
  }

  public refreshResults(data: any) {
    if (data) {
      this.refresh.next(data);
    }
  }
}
