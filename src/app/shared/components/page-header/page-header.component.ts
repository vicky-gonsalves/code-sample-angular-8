import {Component, OnInit} from '@angular/core';
import {appAnimations} from '../../../core/animations/animations';
import {PageHeaderInterface} from '../../../interface/page-header.interface';
import {PageHeaderService} from '../../services/page-header/page-header.service';
import {PageTitleService} from '../../services/page-title/page-title.service';
import {RefreshService} from '../../services/refresh/refresh.service';

@Component({
  selector: 'app-page-header',
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.scss'],
  animations: appAnimations
})
export class PageHeaderComponent implements OnInit {
  public title: String = '';
  public headerDetails: PageHeaderInterface;

  constructor(public pageTitleService: PageTitleService,
              private refreshService: RefreshService) {
  }

  ngOnInit() {
    PageHeaderService.pageHeader.subscribe((data: PageHeaderInterface) => {
      setTimeout(() => {
        this.headerDetails = data;
      });
    });
    this.pageTitleService._title.subscribe(data => {
      if (data && data.title) {
        setTimeout(() => {
          this.title = data.title;
        });
      }
    });
  }

  public refreshResults(): void {
    this.refreshService.refreshResults({refresh: true});
  }
}
