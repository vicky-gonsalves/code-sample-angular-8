import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent implements OnInit {
  public title: string;
  public text: string;
  public okButtonText: string;
  public cancelButtonText: string;

  constructor(public dialogRef: MatDialogRef<ConfirmDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.title = data.title;
    this.text = data.text;
    this.okButtonText = data.okButtonText;
    this.cancelButtonText = data.cancelButtonText;
  }

  ngOnInit() {
  }

  public onNoClick(): void {
    this.dialogRef.close();
  }

  public confirm(): void {
    this.dialogRef.close(true);
  }
}
