import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ErrorMessageService} from '../../services/error-message/error-message.service';

@Component({
  selector: 'search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  @Input() headerName: string;
  @Input() searchKey: string;
  @Input() sort: Boolean = true;
  @Output() search = new EventEmitter();
  @ViewChild('textInput', {static: false}) textInput;
  public cleared = false;
  public searchForm: FormGroup;
  public enabledSearch: Boolean = false;

  constructor(private errorMessageService: ErrorMessageService) {
  }

  get searchInput() {
    return this.searchForm.get('searchInput');
  }

  ngOnInit() {
    this.setupForm();
  }

  public applyFilter(clear?: Boolean): void {
    const formModel = this.searchForm.value;
    if (clear) {
      this.search.emit({key: this.searchKey, value: formModel.searchInput});
    } else {
      this.searchInput.setValidators([Validators.required]);
      this.searchInput.updateValueAndValidity();
      if (this.searchForm.valid) {
        this.search.emit({key: this.searchKey, value: formModel.searchInput});
      }
    }
  }

  public getErrorMessage(fieldName: string): string {
    return this.errorMessageService.getError(this[fieldName], fieldName);
  }

  public clearFilter(): void {
    this.searchInput.reset();
    this.applyFilter(true);
    this.removeFocus();
    this.searchInput.clearValidators();
    this.searchInput.updateValueAndValidity();
  }

  public removeFocus(): void {
    this.textInput.nativeElement.blur();
  }

  private setupForm(): void {
    this.searchForm = new FormGroup({
      'searchInput': new FormControl('', [])
    });
  }
}
