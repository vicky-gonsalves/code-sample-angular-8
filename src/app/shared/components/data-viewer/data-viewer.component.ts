import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'data-viewer',
  templateUrl: './data-viewer.component.html',
  styleUrls: ['./data-viewer.component.scss']
})

export class DataViewerComponent implements OnInit {
  public data: any;
  public inProgress: boolean;

  constructor(private dialogRef: MatDialogRef<DataViewerComponent>,
              @Inject(MAT_DIALOG_DATA) private response: any) {
    this.data = response.response;
  }

  ngOnInit() {
  }

  public okay(): void {
    this.dialogRef.close();
  }

}
