import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'xml-viewer',
  templateUrl: './xml-viewer.component.html',
  styleUrls: ['./xml-viewer.component.scss']
})
export class XmlViewerComponent implements OnInit {
  public xmlData: string;
  public inProgress: Boolean = false;

  constructor(@Inject(MAT_DIALOG_DATA) private data: any) {
    this.xmlData = data.xmlData;
  }

  ngOnInit() {
  }

}
