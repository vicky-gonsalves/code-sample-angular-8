import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'tooltip',
  templateUrl: './tooltip.component.html',
  styleUrls: ['./tooltip.component.scss']
})
export class TooltipComponent implements OnInit {
  @Input() text?: String = 'This is tooltip';
  @Input() position?: String = 'before';
  @Input() delay?: Number = 1000;
  @Input() icon?: String = 'help';

  constructor() {
  }

  ngOnInit() {
  }

}
