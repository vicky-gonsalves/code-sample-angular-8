import {BreakpointObserver} from '@angular/cdk/layout';
import {Component, Input, NgZone, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {MatSidenav} from '@angular/material/sidenav';
import {Event, Params, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {appAnimations} from '../../../core/animations/animations';
import {NavItemService} from '../../services/nav-item/nav-item.service';

const SMALL_WIDTH_BREAKPOINT = 720;

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: appAnimations
})
export class SidenavComponent implements OnInit {
  @Input() shouldKeepSideNavOpen: Boolean;
  @Input() hideHeader: Boolean;
  public params: Observable<Params>;
  @ViewChild(MatSidenav, {static: false}) sidenav: MatSidenav;
  isScreenSmall: Observable<boolean>;

  constructor(public navItems: NavItemService,
              private _router: Router,
              breakpoints: BreakpointObserver,
              zone: NgZone) {
    this.isScreenSmall = breakpoints.observe(`(max-width: ${SMALL_WIDTH_BREAKPOINT}px)`)
      .pipe(map(breakpoint => breakpoint.matches));
    console.log(this.isScreenSmall);
  }

  ngOnInit() {
    // this._router.events.subscribe((event: Event) => {
    //   if (this.isScreenSmall) {
    //     this.sidenav.close();
    //   }
    // });

    // Combine params from all of the path into a single object.
    this.params = new Observable(observer => {
      observer.next({section: 'privileged'});
      observer.complete();
    });

  }
}
