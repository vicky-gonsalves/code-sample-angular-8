import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {appAnimations} from '../../../core/animations/animations';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {IdentityService} from '../../../core/services/identity/identity.service';
import {Identity} from '../../../core/services/identity/identity';
import {ContactInterface} from '../../../interface/contact.interface';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  animations: appAnimations
})
export class NavbarComponent implements OnInit, OnDestroy {
  @Output() toggleSidenav = new EventEmitter<void>();
  public identity: Identity;
  public identitySubscription: Subscription;

  constructor(private router: Router) {
  }

  ngOnInit() {
    this.identitySubscription = IdentityService.identity.subscribe((userData: ContactInterface) => {
      if (userData) {
        this.identity = userData;
      }
    });
  }

  public logout(): void {
    this.router.navigate(['account/login'], {replaceUrl: true});
  }

  public changePassword(): void {
    this.router.navigate(['account/change-password']);
  }

  ngOnDestroy() {
    if (this.identitySubscription) {
      this.identitySubscription.unsubscribe();
    }
  }

}
