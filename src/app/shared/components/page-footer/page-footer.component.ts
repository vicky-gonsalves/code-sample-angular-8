import {Component, OnInit} from '@angular/core';
import {appAnimations} from '../../../core/animations/animations';

@Component({
  selector: 'app-page-footer',
  templateUrl: './page-footer.component.html',
  styleUrls: ['./page-footer.component.scss'],
  animations: appAnimations
})
export class PageFooterComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
