export class TestTemplateFieldValues {
  constructor(public field_name?: string,
              public field_value?: string,
              public field_description?: string,
              public _id?: string) {
  }
}

export class TestTemplate {
  constructor(public participant_id?: string,
              public template_name?: string,
              public template_type?: string,
              public testcase_name?: string,
              public initiating_msg_type?: string,
              public weightage?: number,
              public conn_name?: string,
              public field_values?: TestTemplateFieldValues[],
              public participant?: any,
              public testcase?: any,
              public _id?: string) {
  }
}
