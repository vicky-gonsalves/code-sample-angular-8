export class ParticipantPrivilege {
  constructor(public id?: string,
              public description?: string,
              public _id?: string) {
  }
}
