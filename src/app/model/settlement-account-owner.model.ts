export class SettlementAccountOwner {
  constructor(public  id: string,
              public name: string,
              public _id?: string) {
  }
}
