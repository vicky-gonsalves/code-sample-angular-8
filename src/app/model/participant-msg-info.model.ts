export class ParticipantMsgInfo {
  constructor(public id?: string,
              public msg_type?: string,
              public timeout_offset?: number,
              public _id?: string) {
  }
}
