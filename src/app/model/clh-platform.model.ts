export class ClhPlatform {
  constructor(public id: string,
              public routing_id: string,
              public is_suspended: boolean,
              public _id?: string) {
  }
}
