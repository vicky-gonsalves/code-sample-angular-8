export class CertCriteria {
  constructor(public participant_id?: string,
              public criteria_name?: string,
              public criteria_type?: string,
              public criteria_description?: string,
              public template_names?: string[],
              public is_deactive?: Boolean,
              public is_bulk?: Boolean,
              public duration?: number,
              public iterations?: number,
              public request_interval?: number,
              public participant?: any,
              public _id?: string) {
  }
}
