export class ConnectionOwner {
  constructor(public  id: string,
              public name: string,
              public _id?: string) {
  }
}
