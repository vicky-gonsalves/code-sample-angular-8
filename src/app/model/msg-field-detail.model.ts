export class MsgFieldDetailModel {
  constructor(public msg_type?: string,
              public field_name?: string,
              public is_needed_for_template?: Boolean,
              public field_xpath?: string,
              public field_description?: string,
              public _id?: string) {
  }
}
