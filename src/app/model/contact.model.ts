export class Contact {
  constructor(public id?: string,
              public firstName?: string,
              public lastName?: string,
              public contactProfile_id?: string,
              public role_id?: string,
              public participant_id?: string,
              public is_deactive?: boolean,
              public _id?: string,
              public failed_login_attempt?: number,
              public is_activation_complete?: boolean,
              public is_locked?: boolean,
              public password?: string,
              public confirmPassword?: string,
              public last_tmptoken_gen_date?: Date,
              public isTempTokenExpired?: boolean,
              public temporary_token?: string,
              public oldPassword?: string,
              public contactRoles?: any) {
  }
}
