export class SettlementAccount {
  constructor(public  id: string,
              public owner_id: string,
              public account_type: string,
              public max_transaction_limit: string,
              public balance: string,
              public min_prefund_req_amount: string,
              public alertmark: string,
              public resetalertmark: string,
              public _id?: string) {
  }
}
