export class Participant {
  constructor(public id: string,
              public short_id: string,
              public name: string,
              public connections?: {
                connection_id?: string,
                default_flag?: Boolean
              }[],
              public settlementAccount?: string,
              public privileges?: string[],
              public initial_signon?: Boolean,
              public is_suspended?: Boolean,
              public effective_date?: Date,
              public is_deactive?: Boolean,
              public default_connection_id?: {
                connection_id: string,
                default_flag: Boolean
              }[],
              public other_connection_id?: {
                connection_id: string,
                default_flag: Boolean
              }[],
              public _id?: string) {
  }
}
