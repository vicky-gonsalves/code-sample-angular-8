export class ContactRole {
  constructor(public id: string,
              public description: string,
              public privileges: string[],
              public _id?: string) {
  }
}
