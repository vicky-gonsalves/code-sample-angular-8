export class TestCase {
  constructor(public testcase_name?: string,
              public testcase_description?: string,
              public initiating_msg_type?: string,
              public precondition_commands?: string[],
              public validation_criterias?: string[],
              public _id?: string) {
  }
}
