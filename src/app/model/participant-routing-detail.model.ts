export class ParticipantRoutingDetail {
  constructor(public id?: string,
              public participant_id?: string,
              public effective_date?: Date,
              public is_deactive?: Boolean,
              public _id?: string) {
  }
}
