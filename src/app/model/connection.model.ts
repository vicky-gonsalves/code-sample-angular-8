export class Connection {
  constructor(public  id: string,
              public owner_id: string,
              public initial_available: boolean,
              public effective_date: Date,
              public msg_set_version?: string,
              public validate_signature?: boolean,
              public echo_enabled?: boolean,
              public echo_conn_status_update?: boolean,
              public echo_conn_unavailable_mark_duration?: number,
              public echo_interval?: number,
              public is_deactive?: boolean,
              public _id?: string) {
  }
}
