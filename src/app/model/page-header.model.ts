export class PageHeader {
  constructor(public  backPath?: string,
              public showImage?: boolean,
              public imageUrl?: string,
              public subTitle?: string,
              public icon?: string,
              public actionButton?: {
                title: string,
                url: string
              },
              public actionButton2?: {
                title: string,
                url: string
              }) {
  }
}
