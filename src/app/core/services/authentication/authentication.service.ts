import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import jwtDecode from 'jwt-decode';
import {Observable} from 'rxjs/internal/Observable';
import {catchError, tap} from 'rxjs/operators';
import {environment} from '../../../../environments/environment';
import {ErrorMessageService} from '../../../shared/services/error-message/error-message.service';
import {ProgressBarService} from '../../../shared/services/progress-bar/progress-bar.service';
import {IdentityService} from '../identity/identity.service';
import {Contact} from '../../../model/contact.model';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  public redirectUrl: string;
  private apiUrl: string = environment.apiUrl + 'auth';

  constructor(private http: HttpClient,
              private router: Router,
              private identityService: IdentityService) {
  }

  public static saveAuthToken(response: any): void {
    if (response && response.hasOwnProperty('data') && response.data && response.data.hasOwnProperty('token') && response.data.token) {
      localStorage.setItem('token', response.data.token);
    }
  }

  public static unsetIdentity(): void {
    IdentityService.unsetIdentity();
    localStorage.removeItem('token');
    localStorage.clear();
  }

  public loggedIn(): boolean {
    const token: string = localStorage.getItem('token');
    if (!token) {
      return false;
    }
    const decoded = jwtDecode(token);
    return decoded.exp < Date.now();
  }

  public getToken(): string {
    const token: string = localStorage.getItem('token');
    if (!token) {
      return null;
    }
    return token;
  }

  public signIn(email: String, password: String): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Basic ' + btoa(email + ':' + password)
      })
    };
    this.updateLoading(true);
    return this.http.post<Contact>(`${this.apiUrl}`, null, httpOptions)
      .pipe(
        tap(() => this.updateLoading(false)),
        tap((response: any) => {
          if (response && response.hasOwnProperty('data') &&
            response.data && response.data.hasOwnProperty('contact') && response.data.contact) {
            AuthenticationService.saveAuthToken(response);
            this.identityService.updateIdentity(response.data.contact);
          }
        }),
        catchError(ErrorMessageService.handleServerErrorInternally<any>())
      );
  }

  public signOut(): void {
    AuthenticationService.unsetIdentity();
    this.router.navigate(['/account/login']);
  }

  verifyActivationToken(actToken: string): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/verify-activation-token?act_token=${actToken}`)
      .pipe(catchError(ErrorMessageService.handleServerErrorInternally<any>()));
  }

  getAuthTokenUpdate(): Observable<any> {
    this.updateLoading(true);
    return this.http.get<any>(`${this.apiUrl}/refresh-token`)
      .pipe(
        tap(() => this.updateLoading(false)),
        tap((response: any) => {
          if (response && response.hasOwnProperty('data') &&
            response.data && response.data.hasOwnProperty('contact') && response.data.contact) {
            AuthenticationService.saveAuthToken(response);
            this.identityService.updateIdentity(response.data.contact);
          }
        }),
        catchError(ErrorMessageService.handleServerErrorInternally<any>())
      );
  }

  private updateLoading(flag: boolean): void {
    ProgressBarService.updateLoading(flag);
  }

}
