export class HelpText {
  connection: {
    initial_available: string;
    effective_date: string;
  };
  connectionOwner: any;
  settlementAccount: {
    alertmark: string;
    resetalertmark: string;
  };
  settlementAccountOwner: any;
  contact: {
    participant_id: string;
    contactProfile_id: string;
  };
  participant: {
    initial_signon: string;
    default_connection_id: string;
    privilege: string;
    effective_date: string;
  };
  participantRoutingDetails: {
    id: string;
    effective_date: string;
  };
  testTemplate: {
    initiating_msg_type: string;
    short_id: string;
    field_name: string;
  };
  certCriteria: {};
}
