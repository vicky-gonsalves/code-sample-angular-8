import {inject, TestBed} from '@angular/core/testing';

import {HelpTextService} from './help-text.service';

describe('HelpTextService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HelpTextService]
    });
  });

  it('should be created', inject([HelpTextService], (service: HelpTextService) => {
    expect(service).toBeTruthy();
  }));
});
