import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/internal/BehaviorSubject';
import {HelpText} from './help-text';

@Injectable({
  providedIn: 'root'
})
export class HelpTextService {
  constructor() {
  }

  private static _helpText = new BehaviorSubject<HelpText>(null);

  public static get helpText() {
    return HelpTextService._helpText;
  }

  public static unsetHelpText() {
    HelpTextService._helpText.next(null);
  }

  public updateHelpText(helpText) {
    HelpTextService._helpText.next(helpText);
  }

  public getHelpText() {
    return HelpTextService.helpText;
  }
}
