import {Injectable} from '@angular/core';

@Injectable()
export class NavigationEvent {

  private readonly _lookupName = 'navigationEvent';

  constructor() {
  }

  /**
   * hasAction - is there a navigation event that has not been processed
   */
  public hasAction(): boolean {
    const evt = sessionStorage.getItem(this._lookupName);
    return !!evt;
  }

  /**
   * setEvent - sets navigation information
   * @param event - name of event
   * @param args - object of arguments
   */
  public set(event, args): void {
    const action = {
      event: event,
      args: args || {}
    };

    sessionStorage.setItem(this._lookupName, JSON.stringify(action));
  }

  /**
   * getEvent - gets navigation information
   */
  public get() {
    return JSON.parse(sessionStorage.getItem(this._lookupName));
  }

  /**
   * clear - removes all navigation information
   */
  public clear(): void {
    sessionStorage.removeItem(this._lookupName);
  }

}
