import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/internal/BehaviorSubject';
import {Identity} from './identity';
import {includes, some} from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class IdentityService {
  constructor() {
  }

  private static _identity = new BehaviorSubject<Identity>(null);

  public static get identity() {
    return IdentityService._identity;
  }

  public static unsetIdentity() {
    IdentityService._identity.next(null);
  }

  public static hasPrivileges(privileges: string[]): Boolean {
    const _identity = IdentityService._identity.getValue();
    if (_identity && _identity.contactRoles && _identity.contactRoles.privileges) {
      return some(_identity.contactRoles.privileges, function (privilege) {
        return includes(privileges, privilege);
      });
    }
    return false;
  }

  public updateIdentity(identity) {
    IdentityService._identity.next(identity);
  }

  public getIdentity() {
    return IdentityService.identity;
  }
}
