import {ContactInterface} from '../../../interface/contact.interface';

export class Identity implements ContactInterface {
  id?: string;
  firstName?: string;
  lastName?: string;
  contactProfile_id?: string;
  role_id?: string;
  participant_id?: string;
  is_deactive?: boolean;
  _id?: string;
  failed_login_attempt?: number;
  is_activation_complete?: boolean;
  is_locked?: boolean;
  password?: string;
  temporary_token?: string;
  isTempTokenExpired?: boolean;
  contactRoles?: any;

  constructor(data?: Identity) {
    if (data) {
      this.id = data.id;
      this.firstName = data.firstName;
      this.lastName = data.lastName;
      this.contactProfile_id = data.contactProfile_id;
      this.role_id = data.role_id;
      this.participant_id = data.participant_id;
      this.is_deactive = data.is_deactive;
      this._id = data._id;
      this.failed_login_attempt = data.failed_login_attempt;
      this.is_activation_complete = data.is_activation_complete;
      this.is_locked = data.is_locked;
      this.temporary_token = data.temporary_token;
      this.isTempTokenExpired = data.isTempTokenExpired;
      this.contactRoles = data.contactRoles;
    }
  }
}
