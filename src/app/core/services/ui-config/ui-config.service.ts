import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/internal/BehaviorSubject';
import {UiConfig} from './ui-config';

@Injectable({
  providedIn: 'root'
})
export class UiConfigService {
  constructor() {
  }

  private static _uiConfig = new BehaviorSubject<UiConfig>({config: []});

  public static get uiConfig() {
    return UiConfigService._uiConfig;
  }

  public static unsetUiConfig() {
    UiConfigService._uiConfig.next(null);
  }

  public updateUiConfig(uiConfig) {
    UiConfigService._uiConfig.next(uiConfig);
  }

  public getUiConfig() {
    return UiConfigService.uiConfig;
  }
}
