import {inject, TestBed} from '@angular/core/testing';

import {UiConfigService} from './ui-config.service';

describe('UiConfigService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UiConfigService]
    });
  });

  it('should be created', inject([UiConfigService], (service: UiConfigService) => {
    expect(service).toBeTruthy();
  }));
});
