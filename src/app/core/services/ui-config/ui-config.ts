export class UiConfig {
  config: {
    param_name?: string;
    param_value?: string;
  }[] = [];
}
