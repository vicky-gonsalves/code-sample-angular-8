import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/internal/Observable';
import {AuthenticationService} from '../../services/authentication/authentication.service';
import {Identity} from '../../services/identity/identity';
import {IdentityService} from '../../services/identity/identity.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  private identity: Identity;

  constructor(private authService: AuthenticationService, private router: Router) {
    IdentityService.identity.subscribe(contactIdentity => {
      this.identity = contactIdentity;
    });
  }

  canActivate(next: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const url: string = state.url;
    return this.checkLogin(url);
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.canActivate(route, state);
  }

  checkLogin(url: string): boolean {
    if (this.authService.loggedIn() &&
      this.identity && this.identity.is_deactive === false &&
      this.identity.is_locked === false && this.identity.is_activation_complete === true) {
      return true;
    }

    // Store the attempted URL for redirecting
    this.authService.redirectUrl = url;

    // Navigate to the signin page with extras
    this.router.navigate(['/account/login']);
    return false;
  }
}
