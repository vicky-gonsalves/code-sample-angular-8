import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {Subject} from 'rxjs/internal/Subject';
import {UiConfigService} from '../../services/ui-config/ui-config.service';
import {UiConfig} from '../../services/ui-config/ui-config';
import {UiConfigParameterService} from '../../../shared/services/ui-config-parameter/ui-config-parameter.service';
import {IdentityService} from '../../services/identity/identity.service';
import {Identity} from '../../services/identity/identity';

@Injectable({
  providedIn: 'root'
})
export class UiConfigGuard implements CanActivate {
  private uiConfig: UiConfig;
  private identity: Identity;
  private hasHelpFile: Boolean = false;

  constructor(private uiConfigParameterService: UiConfigParameterService,
              private router: Router) {
    UiConfigService.uiConfig.subscribe(config => {
      this.uiConfig = config;
    });
    IdentityService.identity.subscribe(contactIdentity => {
      this.identity = contactIdentity;
    });
  }

  canActivate(next: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const url: string = state.url;
    return this.hasUiConfig(url, state);
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.canActivate(route, state);
  }

  hasUiConfig(url: string, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (!this.uiConfig || (this.uiConfig.config && !this.uiConfig.config.length)) {
      const sub = new Subject<any>();
      this.uiConfigParameterService.getUIConfigs().subscribe(
        (res) => {
          if (res && res.data && res.data.rows && !res.data.rows.length) {
            sub.next(false);
          } else {
            if (this.identity && !this.hasHelpFile) {
              this.uiConfigParameterService.getHelpTextFile().subscribe(
                (resFile) => {
                  if (resFile) {
                    this.hasHelpFile = true;
                    sub.next(true);
                  } else {
                    this.hasHelpFile = false;
                    sub.next(false);
                  }
                }, err => {
                  this.hasHelpFile = false;
                  sub.error(false);
                });
            } else {
              sub.next(true);
            }
          }
        }, err => {
          sub.error(false);
        });
      return sub.asObservable();
    } else if (this.uiConfig && this.uiConfig.config && this.uiConfig.config.length && this.identity && !this.hasHelpFile) {
      const sub = new Subject<any>();
      this.uiConfigParameterService.getHelpTextFile().subscribe(
        (resFile) => {
          if (resFile) {
            this.hasHelpFile = true;
            sub.next(true);
          } else {
            this.hasHelpFile = false;
            sub.next(false);
          }
        }, err => {
          this.hasHelpFile = false;
          sub.error(false);
        });

      return sub.asObservable();
    } else {
      return true;
    }
  }
}
