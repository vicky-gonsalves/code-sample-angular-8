import {inject, TestBed} from '@angular/core/testing';

import {UiConfigGuard} from './ui-config.guard';

describe('UiConfigGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UiConfigGuard]
    });
  });

  it('should ...', inject([UiConfigGuard], (guard: UiConfigGuard) => {
    expect(guard).toBeTruthy();
  }));
});
