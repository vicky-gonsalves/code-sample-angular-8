import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/internal/Observable';
import {Subject} from 'rxjs/internal/Subject';
import {AuthenticationService} from '../../services/authentication/authentication.service';
import {Identity} from '../../services/identity/identity';
import {IdentityService} from '../../services/identity/identity.service';
import {ContactService} from '../../../shared/services/contact/contact.service';

@Injectable({
  providedIn: 'root'
})
export class UserDataGuard implements CanActivate {
  private identity: Identity;

  constructor(private authService: AuthenticationService,
              private contactService: ContactService,
              private router: Router) {
    IdentityService.identity.subscribe(contactIdentity => {
      this.identity = contactIdentity;
    });
  }

  canActivate(next: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const url: string = state.url;
    return this.hasContactData(url, state);
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.canActivate(route, state);
  }

  hasContactData(url: string, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (!this.authService.loggedIn()) {
      this.authService.redirectUrl = url;
      this.router.navigate(['/account/login']);
      return false;
    } else if (this.authService.loggedIn() && !this.identity) {
      const sub = new Subject<any>();
      this.contactService.me().subscribe(
        (res) => {
          if ((this.identity &&
            (this.identity.is_deactive === true || this.identity.is_locked === true || this.identity.is_activation_complete === false))) {
            this.authService.signOut();
          } else if (!res && state.url !== '/account/login') {
            this.authService.redirectUrl = url;
            this.router.navigate(['/account/login']);
          }
          sub.next(true);
        }, err => {
          this.authService.redirectUrl = url;
          this.router.navigate(['/account/login']);
          sub.error(false);
        });
      return sub.asObservable();
    } else {
      return (this.identity &&
        (this.identity.is_deactive === false || this.identity.is_locked === false || this.identity.is_activation_complete === true));
    }
  }
}
