import {
  HTTP_INTERCEPTORS,
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse
} from '@angular/common/http';
import {Injectable, Injector} from '@angular/core';
import {Router} from '@angular/router';
import jwtDecode from 'jwt-decode';
import {Observable} from 'rxjs/internal/Observable';
import {catchError, tap} from 'rxjs/operators';
import {ProgressBarService} from '../../../shared/services/progress-bar/progress-bar.service';
import {AuthenticationService} from '../../services/authentication/authentication.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  private isUpdating = false;
  private offsetSeconds = 3600;
  private subscriptions = [];
  private headers: { setHeaders?: { 'Authorization'?: string, 'Content-Type'?: string } } = {
    setHeaders: {
      'Content-Type': 'application/json'
    }
  };

  constructor(private authService: AuthenticationService,
              private injector: Injector,
              private router: Router) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = this.authService.getToken();
    if (!request.headers.get('Content-Type')) {
      this.headers.setHeaders['Content-Type'] = 'application/json';
    } else {
      this.headers.setHeaders['Content-Type'] = request.headers.get('Content-Type');
    }
    // add authorization header with jwt token if available
    if (this.authService.loggedIn() && token && token.length && !request.headers.get('Authorization')) {
      this.headers.setHeaders.Authorization = `Bearer ${token}`;
    } else if (request.headers.get('Authorization')) {
      this.headers.setHeaders.Authorization = request.headers.get('Authorization');
    } else {
      delete this.headers.setHeaders.Authorization;
    }
    const newRequest = request.clone(this.headers);

    return next.handle(newRequest)
      .pipe(
        tap(event => {
          if (event instanceof HttpResponse) {
            this.updateLoading(false);
            if (!newRequest.url.includes('refresh-token')) {
              // my api endpoint to avoid to trigger the interceptor when I refresh the token.
              try {
                const decoded = jwtDecode(token);
                const expirationLeft = decoded.exp - Date.now();
                if (expirationLeft > 0) {
                  if (expirationLeft < (this.offsetSeconds * 1000) && !this.isUpdating) {
                    this.updateAuthToken();
                  }
                }
              } catch (e) {
              }
            }
          }
        }),
        catchError((errors: any): Observable<any> => {
          this.updateLoading(false);
          if (errors instanceof HttpErrorResponse) {
            if (errors.status === 401 && !newRequest.url.includes('auth')) {
              AuthenticationService.unsetIdentity();
              for (const sub of this.subscriptions) {
                sub.unsubscribe();
              }
              this.router.navigateByUrl('/');
            }
          }
          return new Observable(subscriber => subscriber.error(errors.error));
        })
      );
  }

  private updateLoading(flag: boolean): void {
    ProgressBarService.updateLoading(flag);
  }

  /**
   * Updates the token if it will expire and the current user is navigating.
   */
  private updateAuthToken(): void {
    this.isUpdating = true;
    const authTokenRepository = this.injector.get(AuthenticationService);
    this.subscriptions.push(authTokenRepository.getAuthTokenUpdate().subscribe(
      (authToken: string) => {
        this.isUpdating = false;
      }
    ));
  }
}

export const JwtInterceptorProvider = {
  provide: HTTP_INTERCEPTORS,
  useClass: JwtInterceptor,
  multi: true,
};
