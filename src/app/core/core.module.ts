import {NgModule} from '@angular/core';
import {SharedModule} from '../shared/shared.module';
import {RouterModule} from '@angular/router';
import {JwtInterceptorProvider} from './interceptors/jwt/jwt.interceptor';

const importsAndExports = [
  SharedModule
];

@NgModule({
  imports: [
    RouterModule,
    ...importsAndExports
  ],
  exports: [...importsAndExports],
  declarations: [],
  providers: [
    JwtInterceptorProvider
  ]
})
export class CoreModule {
}
