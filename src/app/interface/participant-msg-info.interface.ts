export interface ParticipantMsgInfoInterface {
  id?: string;
  msg_type?: string;
  timeout_offset?: number;
  _id?: string;
}
