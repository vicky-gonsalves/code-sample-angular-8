export interface ParticipantRoutingDetailInterface {
  id?: string;
  participant_id?: string;
  effective_date?: Date;
  is_deactive?: Boolean;
  _id?: string;
}
