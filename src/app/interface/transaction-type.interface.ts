export interface TransactionTypeInterface {
  id?: string;
  transaction_type?: string;
  description?: string;
  _id?: string;
}
