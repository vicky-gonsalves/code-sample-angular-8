export interface AdminMessageInterface {
  id?: string;
  creation_tmstmp?: Date;
  receive_tmstmp?: Date;
  gen_tmstmp?: Date;
  participant_id?: string;
  instruction_id?: string;
  res_msg_id?: string;
  status?: string;
  reason_code?: string;
  request_message?: string;
  response_message?: string;
  _id?: string;
  frmTimestamp?: Date;
  toTimestamp?: Date;
  message_type?: any;
}
