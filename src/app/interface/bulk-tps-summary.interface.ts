export interface BulkTpsSummaryInterface {
  run_id?: string;
  transaction_type?: string;
  min_total?: number;
  avg_total?: string;
  max_total?: number;
  min_records?: Array<any>;
  _id?: string;
}
