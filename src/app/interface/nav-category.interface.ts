import {NavItemInterface} from './nav-item.interface';

export interface NavCategoryInterface {
  id: string;
  name: string;
  items?: NavItemInterface[];
  icon?: string;
  route?: string;
  packageName?: string;
  showInContent: boolean;
  showInSidenav: boolean;
  privileges?: string[];
}
