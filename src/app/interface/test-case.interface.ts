export interface TestCaseInterface {
  testcase_name?: string;
  testcase_description?: string;
  initiating_msg_type?: string;
  precondition_commands?: string[];
  validation_criterias?: string[];
  _id?: string;
}
