export interface SettlementSnapshotAccountsInterface {
  date_time: Date;
  amount: string;
  _id?: string;
}

export interface SettlementSnapshotInterface {
  snap_id?: string;
  creation_tmstmp?: Date;
  cycle_start_dttm?: Date;
  cycle_end_dttm?: Date;
  participant_id?: string;
  account_id?: string;
  account_type?: string;
  tracking_account_id?: string;
  initial_cap?: string;
  aval_balance?: string;
  effective_cap?: string;
  account_position?: string;
  outward_accepted_count?: string;
  outward_accepted_value?: string;
  outward_rejected_count?: string;
  outward_rejected_value?: string;
  inward_accepted_count?: string;
  inward_accepted_value?: string;
  inward_rejected_count?: string;
  inward_rejected_value?: string;
  account_credits?: SettlementSnapshotAccountsInterface[];
  account_debits?: SettlementSnapshotAccountsInterface[];
  frmTimestamp?: Date;
  toTimestamp?: Date;
  id?: string;
  _id?: string;
}
