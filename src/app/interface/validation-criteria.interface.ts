export interface ValidationCriteriaInterface {
  validation_name: string;
  mapping_name: string;
  _id?: string;
}
