export interface BulkCertCriteriaSmtSummaryInterface {
  run_id?: string;
  template_run_id?: string;
  smt_cycle_id?: string;
  account_id?: string;
  accept_reject_code?: string;
  reason_code?: string;
  in_out?: string;
  count?: number;
  amount?: string;
  _id?: string;
}
