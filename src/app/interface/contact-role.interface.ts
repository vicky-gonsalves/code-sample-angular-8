export interface ContactRoleInterface {
  id: string;
  description: string;
  privileges: string[];
  _id?: string;
}
