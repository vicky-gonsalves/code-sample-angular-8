export interface UiConfigInterface {
  config: {
    param_name?: string;
    param_value?: string;
  }[];
}
