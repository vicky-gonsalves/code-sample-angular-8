export interface BulkTpsDetailInterface {
  run_id?: string;
  transaction_type?: string;
  leg_type?: string;
  instruction_id?: string;
  leg_tmstmp?: Date;
  leg1_tmstmp?: Date;
  leg2_tmstmp?: Date;
  leg3_tmstmp?: Date;
  leg4_tmstmp?: Date;
  total?: number;
  records?: Array<any>;
  _id?: string;
}
