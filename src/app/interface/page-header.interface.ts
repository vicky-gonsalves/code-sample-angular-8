export interface PageHeaderInterface {
  backPath?: string;
  refreshButton?: boolean;
  showImage?: boolean;
  imageUrl?: string;
  icon?: string;
  subTitle?: string;
  actionButton?: {
    title: string,
    url: string
  };
  actionButton2?: {
    title: string,
    url: string
  };
}
