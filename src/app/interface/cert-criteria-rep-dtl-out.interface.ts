import {ValidationCriteriaInterface} from './validation-criteria.interface';
import {MsgFldMappingInterface} from './msg-fld-mapping.interface';

export interface CertCriteriaRepDtlOutValidationStatusInterface {
  source_field_name?: string;
  source_field_value?: string;
  target_field_name?: string;
  target_field_value?: string;
  exp_target_field_value?: string;
  is_passed?: Boolean;
}

export interface CertCriteriaRepDtlOutValCriteriaResultsInterface {
  validation_name?: string;
  validation_status?: string;
  reason_code?: string;
  validation_dtl_status?: CertCriteriaRepDtlOutValidationStatusInterface[];
  valCriteria?: ValidationCriteriaInterface;
  msgFldMapping?: MsgFldMappingInterface;
}

export interface CertCriteriaRepDtlOutInterface {
  participant_id?: string;
  criteria_name?: string;
  template_name?: string;
  testcase_name?: string;
  instruction_id?: string;
  initiating_msg_type?: string;
  msg_rcvd_dttm?: Date;
  status_update_dttm?: Date;
  status?: string;
  is_incorporated?: Boolean;
  valcriteria_results?: CertCriteriaRepDtlOutValCriteriaResultsInterface[];
  _id?: string;
}
