export interface NavItemInterface {
  id: string;
  name: string;
  packageName?: string;
  icon?: string;
  route?: string;
  showInContent: boolean;
  showInSidenav: boolean;
  privileges?: string[];
}
