export interface SettlementAccountInterface {
  id: string;
  owner_id: string;
  balance: string;
  alertmark: string;
  resetalertmark: string;
  _id?: string;
}
