export interface ContactInterface {
  id?: string;
  firstName?: string;
  lastName?: string;
  contactProfile_id?: string;
  role_id?: string;
  participant_id?: string;
  is_deactive?: boolean;
  _id?: string;
  failed_login_attempt?: number;
  is_activation_complete?: boolean;
  is_locked?: boolean;
  password?: string;
  confirmPassword?: string;
  temporary_token?: string;
  isTempTokenExpired?: boolean;
  last_tmptoken_gen_date?: Date;
  oldPassword?: string;
  contactRoles?: any;
}
