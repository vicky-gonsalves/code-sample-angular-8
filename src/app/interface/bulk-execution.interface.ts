export interface BulkExecutionInterface {
  run_id?: string;
  criteria_name?: string;
  criteria_description?: string;
  status?: string;
  template_names?: string[];
  exe_requested_datetime?: Date;
  exe_end_datetime?: Date;
  rpt_aggrn_status?: string;
  created_at?: Date;
  request_interval?: string;
  duration?: string;
  iterations?: string;
  _id?: string;
}
