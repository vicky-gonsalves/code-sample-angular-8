export interface ParticipantInterface {
  id: string;
  name: string;
  connections?: {
    connection_id?: string,
    default_flag?: Boolean
  }[];
  settlementAccount?: string;
  privileges?: string[];
  initial_signon?: Boolean;
  is_suspended?: Boolean;
  effective_date?: Date;
  is_deactive?: Boolean;
  default_connection_id?: {
    connection_id: string,
    default_flag: Boolean
  }[];
  other_connection_id?: {
    connection_id: string,
    default_flag: Boolean
  }[];
  _id?: string;
}
