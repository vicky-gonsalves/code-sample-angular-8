export interface ClhPaymentLegInterface {
  id?: string;
  message_identifier?: string;
  leg_name?: string;
  transaction_type?: string;
  send_receive_tmstmp?: Date;
  leg_xml_data?: string;
  leg_json_data?: string;
  connection_id?: string;
  msg_set_version?: string;
  _id?: string;
}
