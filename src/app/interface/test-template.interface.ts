export interface TestTemplateFieldValuesInterface {
  field_name?: string;
  field_value?: string;
  field_description?: string;
}

export interface TestTemplateInterface {
  participant_id?: string;
  template_name?: string;
  template_type?: string;
  testcase_name?: string;
  initiating_msg_type?: string;
  weightage?: number;
  conn_name?: string;
  field_values?: TestTemplateFieldValuesInterface[];
  participant?: any;
  testcase?: any;
  _id?: string;
}
