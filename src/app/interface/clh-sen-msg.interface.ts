export interface ClhSenMsgInterface {
  id?: string;
  receiver_participant_id?: string;
  event_tmstmp?: Date;
  event_code?: string;
  event_description?: string;
  event_params?: any;
  event_xml_message?: string;
  event_json_message?: any;
  _id?: string;
  frmTimestamp?: Date;
  toTimestamp?: Date;
}
