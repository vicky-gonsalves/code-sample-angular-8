export interface ConnectionInterface {
  id: string;
  owner_id: string;
  initial_available: boolean;
  effective_date: Date;
  msg_set_version?: string;
  validate_signature?: boolean;
  echo_enabled?: boolean;
  echo_conn_status_update?: boolean;
  echo_conn_unavailable_mark_duration?: number;
  echo_interval?: number;
  is_deactive?: boolean;
  _id?: string;
}
