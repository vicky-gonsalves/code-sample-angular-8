export interface ConnectionOwnerInterface {
  id: string;
  name: string;
  _id?: string;
}
