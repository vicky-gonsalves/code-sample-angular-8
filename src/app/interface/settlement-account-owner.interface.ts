export interface SettlementAccountOwnerInterface {
  id: string;
  name: string;
  _id?: string;
}
