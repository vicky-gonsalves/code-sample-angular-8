import {CertCriteriaInterface} from './cert-criteria.interface';
import {ParticipantInterface} from './participant.interface';

export interface CertCriteriaTemplateResultsInterface {
  template_name?: string;
  testcase_name?: string;
  initiating_msg_type?: string;
  template_status?: string;
  status_update_dttm?: Date;
  testcase_description?: string;
}

export interface CertCriteriaRepOutInterface {
  participant_id?: string;
  criteria_name?: string;
  criteria_status?: string;
  last_update_dttm?: Date;
  template_results?: CertCriteriaTemplateResultsInterface[];
  participant?: ParticipantInterface;
  certCriteria?: CertCriteriaInterface;
  _id?: string;
}
