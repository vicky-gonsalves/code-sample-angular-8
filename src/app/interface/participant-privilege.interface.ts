export interface ParticipantPrivilegeInterface {
  id?: string;
  description?: string;
  _id?: string;
}
