export interface ContactProfileInterface {
  id: string;
  description: string;
  _id?: string;
}
