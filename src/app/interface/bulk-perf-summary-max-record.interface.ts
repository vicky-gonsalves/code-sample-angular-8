export interface BulkPerfSummaryMaxRecordInterface {
  instruction_id?: string;
  leg2_leg1?: string;
  leg3_leg2?: string;
  leg3_leg4?: string;
}
