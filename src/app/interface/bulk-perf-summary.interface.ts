export interface BulkPerfSummaryInterface {
  run_id?: string;
  transaction_type?: string;
  max_leg4_leg1?: string;
  min_leg4_leg1?: string;
  avg_leg4_leg1?: string;
  max_records?: Array<any>;
  _id?: string;
}
