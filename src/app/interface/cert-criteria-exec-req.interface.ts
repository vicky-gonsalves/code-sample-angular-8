export interface CertCriteriaExecReqInterface {
  participant_id?: string;
  criteria_name?: string;
  template_names?: string[];
  exec_status?: string;
  request_interval?: number;
  duration?: number;
  iterations?: number;
}
