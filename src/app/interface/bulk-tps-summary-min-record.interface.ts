export interface BulkTpsSummaryMinRecordInterface {
  instruction_id?: string;
  leg1_tmstmp?: Date;
  leg2_tmstmp?: Date;
  leg3_tmstmp?: Date;
  leg4_tmstmp?: Date;
}
