export interface BulkCertCriteriaExecSummaryInterface {
  run_id?: string;
  template_run_id?: string;
  transaction_type?: string;
  msg_type?: string;
  template_name?: string;
  template_description?: string;
  accept_reject_code?: string;
  reason_code?: string;
  count?: number;
  weightage?: number;
  _id?: string;
}
