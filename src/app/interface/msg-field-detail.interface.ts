export interface MsgFieldDetailInterface {
  msg_type?: string;
  field_name?: string;
  is_needed_for_template?: Boolean;
  field_xpath?: string;
  field_description?: string;
  _id?: string;
}
