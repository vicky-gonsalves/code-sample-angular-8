export interface CertCriteriaInterface {
  participant_id?: string;
  criteria_name?: string;
  criteria_type?: string;
  criteria_description?: string;
  template_names?: string[];
  is_deactive?: Boolean;
  is_bulk?: Boolean;
  duration?: number;
  iterations?: number;
  request_interval?: number;
  participant?: any;
  _id?: string;
}
