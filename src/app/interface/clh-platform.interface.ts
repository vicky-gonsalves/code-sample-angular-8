export interface ClhPlatformInterface {
  id: string;
  routing_id: string;
  is_suspended: boolean;
  _id?: string;
}
