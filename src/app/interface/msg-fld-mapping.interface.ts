export interface MsgFldMappingInterface {
  mapping_name: string;
  source_msg_type: string;
  source_msg_leg: string;
  target_msg_type: string;
  target_msg_leg: string;
  _id?: string;
}
