export interface MessageTypeInterface {
  msg_type?: string;
  msg_name?: string;
  msg_description?: string;
  transaction_type?: string;
  msg_category?: string;
  msg_id_prefix?: string;
  blackboard_name?: string;
  from_debtor?: boolean;
  _id?: string;
}
