import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserDataGuard} from '../../../core/guards/user-data/user-data.guard';
import {CertificationCriteriaListComponent} from './components/certification-criteria-list/certification-criteria-list.component';
import {ExecCertificationCriteriaComponent} from './exec-certification-criteria.component';
import {CertificationCriteriaViewComponent} from './components/certification-criteria-view/certification-criteria-view.component';

export const routes: Routes = [
  {
    path: '',
    canActivate: [UserDataGuard],
    component: ExecCertificationCriteriaComponent,
    children: [
      {
        path: '',
        canActivateChild: [UserDataGuard],
        component: CertificationCriteriaListComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'exec-certification-criteria'}
      },
      {
        path: 'view/:id',
        canActivateChild: [UserDataGuard],
        component: CertificationCriteriaViewComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'exec-certification-criteria-view'}
      }
    ]
  },
  {path: '**', redirectTo: '', pathMatch: 'full'}
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);

