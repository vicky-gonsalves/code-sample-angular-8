import {Component, OnInit} from '@angular/core';
import {appAnimations} from '../../../core/animations/animations';

@Component({
  selector: 'exec-certification-criteria',
  templateUrl: './exec-certification-criteria.component.html',
  styleUrls: ['./exec-certification-criteria.component.scss'],
  animations: appAnimations
})
export class ExecCertificationCriteriaComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
