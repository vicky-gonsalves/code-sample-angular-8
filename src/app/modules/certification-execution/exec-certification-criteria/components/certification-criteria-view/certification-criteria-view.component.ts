import {Component, OnInit} from '@angular/core';
import {appAnimations} from '../../../../../core/animations/animations';
import {NavItemService} from '../../../../../shared/services/nav-item/nav-item.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {PageTitleService} from '../../../../../shared/services/page-title/page-title.service';
import {CertCriteriaService} from '../../../../../shared/services/cert-criteria/cert-criteria.service';
import {PageHeaderService} from '../../../../../shared/services/page-header/page-header.service';
import {CertCriteriaInterface} from '../../../../../interface/cert-criteria.interface';
import {NavItemInterface} from '../../../../../interface/nav-item.interface';

@Component({
  selector: 'certification-criteria-view',
  templateUrl: './certification-criteria-view.component.html',
  styleUrls: ['./certification-criteria-view.component.scss'],
  animations: appAnimations
})
export class CertificationCriteriaViewComponent implements OnInit {
  public certCriteriaId: string;
  public certCriteria: CertCriteriaInterface;
  public inProgress: Boolean = true;
  private componentNavItem: NavItemInterface;
  public flexWidth: number = 33;

  constructor(public navItems: NavItemService,
              public pageTitle: PageTitleService,
              private errorMessageService: ErrorMessageService,
              private router: Router,
              private certCriteriaService: CertCriteriaService,
              private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe(params => {
      if (params.hasOwnProperty('id') && params.id.length) {
        this.certCriteriaId = params.id;
      }
    });
  }

  ngOnInit() {
    this.componentNavItem = this.navItems.getItemById('exec-certification-criteria-view');
    if (this.componentNavItem) {
      this.pageTitle.setTitle(this.componentNavItem.name);
    }
    // Set Page Header Details
    PageHeaderService.updatePageHeader({
      backPath: 'execute-certification/exec-certification-criteria',
      subTitle: 'Loading...'
    });

    if (this.certCriteriaId) {
      this.fetchCertCriteria(this.certCriteriaId);
    }
  }

  private setInProgress(): void {
    this.inProgress = true;
  }

  private setNotInProgress(): void {
    this.inProgress = false;
  }

  private fetchCertCriteria(id: string): void {
    this.certCriteriaService.getCertCriteria(id, true)
      .subscribe(response => {
          if (response && response.hasOwnProperty('data') && response.data) {
            this.certCriteria = response.data;
            PageHeaderService.updatePageHeader({
              backPath: 'execute-certification/exec-certification-criteria',
              subTitle: this.certCriteria.criteria_name
            });
            if (this.certCriteria && this.certCriteria.is_bulk && this.certCriteria.hasOwnProperty('request_interval')) {
              this.flexWidth = 25;
            } else {
              this.flexWidth = 33;
            }
          }
          this.setNotInProgress();
        },
        err => {
          this.setNotInProgress();
          this.router.navigate(['execute-certification/exec-certification-criteria']);
          this.errorMessageService.handleServerErrors(err);
        });
  }
}
