import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CertificationCriteriaViewComponent} from './certification-criteria-view.component';

describe('CertificationCriteriaViewComponent', () => {
  let component: CertificationCriteriaViewComponent;
  let fixture: ComponentFixture<CertificationCriteriaViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CertificationCriteriaViewComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertificationCriteriaViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
