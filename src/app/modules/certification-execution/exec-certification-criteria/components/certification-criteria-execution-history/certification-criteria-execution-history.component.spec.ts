import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CertificationCriteriaExecutionHistoryComponent} from './certification-criteria-execution-history.component';

describe('CertificationCriteriaExecutionHistoryComponent', () => {
  let component: CertificationCriteriaExecutionHistoryComponent;
  let fixture: ComponentFixture<CertificationCriteriaExecutionHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CertificationCriteriaExecutionHistoryComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertificationCriteriaExecutionHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
