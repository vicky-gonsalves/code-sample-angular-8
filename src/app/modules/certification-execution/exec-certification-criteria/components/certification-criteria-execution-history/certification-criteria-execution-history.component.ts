import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import moment from 'moment-timezone';
import {appAnimations} from '../../../../../core/animations/animations';
import {CertCriteriaExecReqInterface} from '../../../../../interface/cert-criteria-exec-req.interface';
import {CertCriteriaInterface} from '../../../../../interface/cert-criteria.interface';
import {CertCriteriaExecReqService} from '../../../../../shared/services/cert-criteria-exec-req/cert-criteria-exec-req.service';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {UiConfigParameterService} from '../../../../../shared/services/ui-config-parameter/ui-config-parameter.service';
import {CertificationCriteriaFieldValuesDetailListComponent} from '../certification-criteria-field-values-detail-list/certification-criteria-field-values-detail-list.component';

@Component({
  selector: 'certification-criteria-execution-history',
  templateUrl: './certification-criteria-execution-history.component.html',
  styleUrls: ['./certification-criteria-execution-history.component.scss'],
  animations: appAnimations
})
export class CertificationCriteriaExecutionHistoryComponent implements OnInit {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  public displayedColumns =
    ['createdAt', 'template_names', 'exec_status'];
  public dataSource: MatTableDataSource<CertCriteriaExecReqInterface>;
  public paginatorOptions = {
    count: 0,
    pageSize: 10,
    pageSizeOptions: [5, 10, 25, 50, 100],
    showFirstLastButtons: true
  };
  public inProgress: Boolean = true;
  public pageEvent: any;
  public certCriteria: CertCriteriaInterface;
  private sortBy = '-createdAt';
  private pageSize = 10;

  constructor(private dialogRef: MatDialogRef<CertificationCriteriaFieldValuesDetailListComponent>,
              @Inject(MAT_DIALOG_DATA) private data: any,
              private certCriteriaExecReqService: CertCriteriaExecReqService,
              private uiConfigParameterService: UiConfigParameterService,
              private errorMsgService: ErrorMessageService) {
    this.certCriteria = this.data.certCriteria;
  }

  ngOnInit() {
    this.fetchExecutionHistory();
  }

  public sortData(sort: Sort): void {
    const data = this.dataSource;
    if (!sort.active || sort.direction === '') {
      this.sortBy = '-_id';
    } else {
      this.sortBy = sort.direction === 'asc' ? sort.active : '-' + sort.active;
    }
    this.paginator.pageIndex = 0;
    this.fetchExecutionHistory();
  }

  public fetchExecutionHistory(_pageEvent?: PageEvent): void {
    if (_pageEvent) {
      this.pageEvent = _pageEvent;
      this.pageSize = _pageEvent.pageSize;
    }
    const options = {
      limit: (this.pageEvent ? (this.pageEvent.pageSize) : this.pageSize ? this.pageSize : this.paginatorOptions.pageSize),
      page: (this.pageEvent ? (this.pageEvent.pageIndex + 1) : 1),
      sort: [this.sortBy]
    };

    this.inProgress = true;
    this.certCriteriaExecReqService.getExecutionHistory(options, this.certCriteria.participant_id, this.certCriteria.criteria_name)
      .subscribe(response => {
          if (response && response.data && response.data.rows && response.data.rows.length) {
            response.data.rows.forEach(row => {
              row.createdAt = this.convertTimezone(row.createdAt);
            });
            this.dataSource = new MatTableDataSource(response.data.rows);
            this.paginatorOptions.count = response.data.count;
            this.dataSource.sort = this.sort;
          } else {
            this.errorMsgService.openSnackBar(this.errorMsgService.getNonFormError('NO_DATA'));
          }
          this.inProgress = false;
        },
        err => {
          this.errorMsgService.handleServerErrors(err);
          this.inProgress = false;
        });
  }

  public getFirstTestTemplateName(templates): string {
    return templates[0];
  }

  public getOtherTestTemplateNames(templates): string | null {
    if (templates && templates.length > 1) {
      return templates.join(', ');
    }
    return null;
  }

  public convertTimezone(date: Date): moment {
    return this.uiConfigParameterService.convertTimezone(date);
  }

}
