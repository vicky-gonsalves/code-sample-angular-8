import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CertificationCriteriaListComponent} from './certification-criteria-list.component';

describe('CertificationCriteriaListComponent', () => {
  let component: CertificationCriteriaListComponent;
  let fixture: ComponentFixture<CertificationCriteriaListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CertificationCriteriaListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertificationCriteriaListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
