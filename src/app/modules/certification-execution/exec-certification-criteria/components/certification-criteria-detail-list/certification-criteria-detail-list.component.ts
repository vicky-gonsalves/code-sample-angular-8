import {SelectionModel} from '@angular/cdk/collections';
import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {flatMap} from 'lodash';
import {appAnimations} from '../../../../../core/animations/animations';
import {Identity} from '../../../../../core/services/identity/identity';
import {IdentityService} from '../../../../../core/services/identity/identity.service';
import {CertCriteriaExecReqInterface} from '../../../../../interface/cert-criteria-exec-req.interface';
import {CertCriteriaInterface} from '../../../../../interface/cert-criteria.interface';
import {TestTemplateInterface} from '../../../../../interface/test-template.interface';
import {CertCriteriaExecReqService} from '../../../../../shared/services/cert-criteria-exec-req/cert-criteria-exec-req.service';
import {CertCriteriaService} from '../../../../../shared/services/cert-criteria/cert-criteria.service';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {CertificationCriteriaExecutionHistoryComponent} from '../certification-criteria-execution-history/certification-criteria-execution-history.component';
import {CertificationCriteriaFieldValuesDetailListComponent} from '../certification-criteria-field-values-detail-list/certification-criteria-field-values-detail-list.component';

@Component({
  selector: 'certification-criteria-detail-list-exec',
  templateUrl: './certification-criteria-detail-list.component.html',
  styleUrls: ['./certification-criteria-detail-list.component.scss'],
  animations: appAnimations
})
export class CertificationCriteriaDetailListComponent implements OnInit {
  @Input() id: string;
  @Input() certCriteria: CertCriteriaInterface;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  public hasPermissionToExecute: Boolean = false;
  public searchTerm: {
    template_name: string,
    testcase_name: string,
    initiating_msg_type: string
  };
  public displayedColumns =
    ['template_name', 'template_type', 'testcase_description', 'initiating_msg_type', 'actions'];
  public dataSource: MatTableDataSource<TestTemplateInterface>;
  public selection = new SelectionModel<TestTemplateInterface>(true, []);
  public paginatorOptions = {
    count: 0,
    pageSize: 10,
    pageSizeOptions: [5, 10, 25, 50, 100],
    showFirstLastButtons: true
  };
  public inProgress: Boolean = true;
  public pageEvent: any;
  private identity: Identity;
  private sortBy = '-_id';
  private pageSize = 10;

  constructor(private certCriteriaService: CertCriteriaService,
              private dialog: MatDialog,
              private errorMessageService: ErrorMessageService,
              private certCriteriaExecReqService: CertCriteriaExecReqService) {
    this.searchTerm = {
      template_name: null,
      testcase_name: null,
      initiating_msg_type: null,
    };
    IdentityService.identity.subscribe(contactIdentity => {
      this.identity = contactIdentity;
      // this.IS_PARTICIPANT_CONTACT = this.identity && this.identity.contactProfile_id &&
      //   this.identity.contactProfile_id === 'participant';
    });
  }

  ngOnInit() {
    // Fetch Test Templates
    this.fetchCertCriteriaTestTemplatesListing();
  }

  /** Whether the number of selected elements matches the total number of rows. */
  public isAllSelected(): Boolean {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  public masterToggle(): void {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  public sortData(sort: Sort): void {
    const data = this.dataSource;
    this.selection.clear();
    if (!sort.active || sort.direction === '') {
      this.sortBy = '-_id';
    } else {
      this.sortBy = sort.direction === 'asc' ? sort.active : '-' + sort.active;
    }
    this.paginator.pageIndex = 0;
    this.fetchCertCriteriaTestTemplatesListing();
  }

  public fetchCertCriteriaTestTemplatesListing(_pageEvent?: PageEvent): void {
    if (_pageEvent) {
      this.pageEvent = _pageEvent;
      this.pageSize = _pageEvent.pageSize;
    }
    const options = {
      limit: (this.pageEvent ? (this.pageEvent.pageSize) : this.pageSize ? this.pageSize : this.paginatorOptions.pageSize),
      page: (this.pageEvent ? (this.pageEvent.pageIndex + 1) : 1),
      sort: [this.sortBy]
    };
    if (this.searchTerm.template_name) {
      options['template_name'] = this.searchTerm.template_name;
    }
    if (this.searchTerm.initiating_msg_type) {
      options['initiating_msg_type'] = this.searchTerm.initiating_msg_type;
    }
    if (this.searchTerm.testcase_name) {
      options['testcase_name'] = this.searchTerm.testcase_name;
    }
    this.inProgress = true;
    this.certCriteriaService.getCertCriteriaTestTemplates(options, this.id)
      .subscribe(response => {
          if (response && response.data && response.data.rows && response.data.rows.length) {
            const hasConnName = response.data.rows.filter(template => {
              return !!(template.hasOwnProperty('conn_name') && template.conn_name);
            });
            this.showHideFields(hasConnName.length > 0);
            this.dataSource = new MatTableDataSource(response.data.rows);
            this.paginatorOptions.count = response.data.count;
            this.dataSource.sort = this.sort;
          } else {
            this.errorMessageService.openSnackBar(this.errorMessageService.getNonFormError('NO_DATA'));
          }
          this.inProgress = false;
        },
        err => {
          this.errorMessageService.handleServerErrors(err);
          this.inProgress = false;
        });
  }

  public applyFilter(searchCriteria: { key: string, value: string }): void {
    this.selection.clear();
    this.searchTerm[searchCriteria.key] = searchCriteria.value;
    this.paginator.pageIndex = 0;
    this.fetchCertCriteriaTestTemplatesListing();
  }

  public showFieldMappings(id: string): void {
    const conf: MatDialogConfig = {
      autoFocus: false,
      closeOnNavigation: true,
      panelClass: 'panelClass',
      maxWidth: '1024px',
      data: {id: id}
    };
    this.dialog.open(CertificationCriteriaFieldValuesDetailListComponent, conf);
  }

  public viewHistory(): void {
    const conf: MatDialogConfig = {
      autoFocus: false,
      closeOnNavigation: true,
      panelClass: 'panelClass',
      maxWidth: '1024px',
      data: {certCriteria: this.certCriteria}
    };
    this.dialog.open(CertificationCriteriaExecutionHistoryComponent, conf);
  }

  public execute(): void {
    const executionCriteria = this.prepareCriteria();
    this.certCriteriaExecReqService.addExecution(executionCriteria)
      .subscribe(response => {
          if (response && response.hasOwnProperty('data') && response.data) {
            this.errorMessageService.openSnackBar('Certification Criteria successfully executed.', 5000, 'OK');
          }
          this.setNotInProgress();
          this.selection.clear();
        },
        err => {
          this.setNotInProgress();
          this.errorMessageService.handleServerErrors(err);
        });
  }

  private showHideFields(hasConnName: Boolean) {
    if (hasConnName) {
      this.displayedColumns =
        ['template_name', 'template_type', 'testcase_description', 'initiating_msg_type', 'conn_name', 'actions'];
    }
    if (this.certCriteria && this.certCriteria.is_bulk) {
      this.displayedColumns =
        ['template_name', 'template_type', 'testcase_description', 'initiating_msg_type', 'weightage', 'actions'];
      if (hasConnName) {
        this.displayedColumns =
          ['template_name', 'template_type', 'testcase_description', 'initiating_msg_type', 'weightage', 'conn_name', 'actions'];
      }
    }

    if (IdentityService.hasPrivileges(['runParticipantCriteria'])) {
      this.hasPermissionToExecute = true;
      if (this.displayedColumns.indexOf('select') < 0) {
        this.displayedColumns.unshift('select');
      }
    } else {
      this.hasPermissionToExecute = false;
      const index = this.displayedColumns.indexOf('select');
      if (index > -1) {
        this.displayedColumns.splice(index, 1);
      }
    }
  }

  private setInProgress(): void {
    this.inProgress = true;
  }

  private setNotInProgress(): void {
    this.inProgress = false;
  }

  private prepareCriteria(): CertCriteriaExecReqInterface {
    return {
      participant_id: this.certCriteria.participant_id,
      criteria_name: this.certCriteria.criteria_name,
      template_names: flatMap(this.selection.selected, 'template_name'),
      exec_status: 'PENDING'
    };
  }
}
