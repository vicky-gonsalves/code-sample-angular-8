import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CertificationCriteriaFieldValuesDetailListComponent} from './certification-criteria-field-values-detail-list.component';

describe('CertificationCriteriaFieldValuesDetailListComponent', () => {
  let component: CertificationCriteriaFieldValuesDetailListComponent;
  let fixture: ComponentFixture<CertificationCriteriaFieldValuesDetailListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CertificationCriteriaFieldValuesDetailListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertificationCriteriaFieldValuesDetailListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
