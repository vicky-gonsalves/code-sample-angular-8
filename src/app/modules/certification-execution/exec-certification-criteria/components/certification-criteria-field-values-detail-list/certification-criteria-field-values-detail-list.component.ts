import {Component, Inject, Input, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {appAnimations} from '../../../../../core/animations/animations';
import {TestTemplateFieldValuesInterface} from '../../../../../interface/test-template.interface';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {TestTemplateService} from '../../../../../shared/services/test-template/test-template.service';

@Component({
  selector: 'certification-criteria-field-values-detail-list',
  templateUrl: './certification-criteria-field-values-detail-list.component.html',
  styleUrls: ['./certification-criteria-field-values-detail-list.component.scss'],
  animations: appAnimations
})
export class CertificationCriteriaFieldValuesDetailListComponent implements OnInit {
  @Input() msgType: string;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  public searchTerm: {
    field_name: string
  };
  public displayedColumns =
    ['field_name', 'field_description', 'field_value'];
  public dataSource: MatTableDataSource<TestTemplateFieldValuesInterface>;
  public paginatorOptions = {
    count: 0,
    pageSize: 10,
    pageSizeOptions: [5, 10, 25, 50, 100],
    showFirstLastButtons: true
  };
  public inProgress: Boolean = true;
  public pageEvent: any;
  private sortBy = '-_id';
  private pageSize = 10;

  constructor(private dialogRef: MatDialogRef<CertificationCriteriaFieldValuesDetailListComponent>,
              @Inject(MAT_DIALOG_DATA) private data: any,
              private testTemplateService: TestTemplateService,
              private errorMsgService: ErrorMessageService) {
    this.searchTerm = {
      field_name: null
    };
  }

  ngOnInit() {
    // Fetch Test Template Field Valuess
    this.fetchTestTemplateFieldsListing();
  }

  public sortData(sort: Sort): void {
    const data = this.dataSource;
    if (!sort.active || sort.direction === '') {
      this.sortBy = '-_id';
    } else {
      this.sortBy = sort.direction === 'asc' ? sort.active : '-' + sort.active;
    }
    this.paginator.pageIndex = 0;
    this.fetchTestTemplateFieldsListing();
  }

  public fetchTestTemplateFieldsListing(_pageEvent?: PageEvent): void {
    if (_pageEvent) {
      this.pageEvent = _pageEvent;
      this.pageSize = _pageEvent.pageSize;
    }
    const options = {
      limit: (this.pageEvent ? (this.pageEvent.pageSize) : this.pageSize ? this.pageSize : this.paginatorOptions.pageSize),
      page: (this.pageEvent ? (this.pageEvent.pageIndex + 1) : 1),
      sort: [this.sortBy]
    };
    if (this.searchTerm.field_name) {
      options['field_name'] = this.searchTerm.field_name;
    }
    this.inProgress = true;
    this.testTemplateService.getTestTemplateFieldValues(options, this.data.id)
      .subscribe(response => {
          if (response && response.data && response.data.rows && response.data.rows.length) {
            this.dataSource = new MatTableDataSource(response.data.rows);
            this.paginatorOptions.count = response.data.count;
            this.dataSource.sort = this.sort;
          } else {
            this.errorMsgService.openSnackBar(this.errorMsgService.getNonFormError('NO_DATA'));
          }
          this.inProgress = false;
        },
        err => {
          this.errorMsgService.handleServerErrors(err);
          this.inProgress = false;
        });
  }

  public applyFilter(searchCriteria: { key: string, value: string }): void {
    this.searchTerm[searchCriteria.key] = searchCriteria.value;
    this.paginator.pageIndex = 0;
    this.fetchTestTemplateFieldsListing();
  }

}
