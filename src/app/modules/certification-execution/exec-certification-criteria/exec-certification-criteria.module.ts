import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ExecCertificationCriteriaComponent} from './exec-certification-criteria.component';
import {CertificationCriteriaListComponent} from './components/certification-criteria-list/certification-criteria-list.component';
import {CertificationCriteriaViewComponent} from './components/certification-criteria-view/certification-criteria-view.component';
import {routing} from './exec-certification-criteria.routes';
import {SharedModule} from '../../../shared/shared.module';
import {CertificationCriteriaDetailListComponent} from './components/certification-criteria-detail-list/certification-criteria-detail-list.component';
import {CertificationCriteriaFieldValuesDetailListComponent} from './components/certification-criteria-field-values-detail-list/certification-criteria-field-values-detail-list.component';
import {CertificationCriteriaExecutionHistoryComponent} from './components/certification-criteria-execution-history/certification-criteria-execution-history.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    routing
  ],
  declarations: [
    ExecCertificationCriteriaComponent,
    CertificationCriteriaListComponent,
    CertificationCriteriaViewComponent,
    CertificationCriteriaDetailListComponent,
    CertificationCriteriaFieldValuesDetailListComponent,
    CertificationCriteriaExecutionHistoryComponent
  ],
  entryComponents: [
    CertificationCriteriaFieldValuesDetailListComponent,
    CertificationCriteriaExecutionHistoryComponent
  ]
})
export class ExecCertificationCriteriaModule {
}
