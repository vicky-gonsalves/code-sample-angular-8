import {Component, OnInit} from '@angular/core';
import {appAnimations} from '../../core/animations/animations';

@Component({
  selector: 'certification-execution',
  templateUrl: './certification-execution.component.html',
  animations: appAnimations
})
export class CertificationExecutionComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
