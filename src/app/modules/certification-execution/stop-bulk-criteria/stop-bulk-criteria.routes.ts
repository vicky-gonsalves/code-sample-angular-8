import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserDataGuard} from '../../../core/guards/user-data/user-data.guard';
import {StopBulkCriteriaComponent} from './stop-bulk-criteria.component';
import {StopBulkCriteriaEditorComponent} from './components/stop-bulk-criteria-editor/stop-bulk-criteria-editor.component';

export const routes: Routes = [
  {
    path: '',
    canActivate: [UserDataGuard],
    component: StopBulkCriteriaComponent,
    children: [
      {
        path: '',
        canActivateChild: [UserDataGuard],
        component: StopBulkCriteriaEditorComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'stop-bulk-criteria'}
      }
    ]
  },
  {path: '**', redirectTo: '', pathMatch: 'full'}
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);

