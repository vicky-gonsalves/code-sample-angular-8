import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {StopBulkCriteriaComponent} from './stop-bulk-criteria.component';
import {StopBulkCriteriaEditorComponent} from './components/stop-bulk-criteria-editor/stop-bulk-criteria-editor.component';
import {SharedModule} from '../../../shared/shared.module';
import {routing} from './stop-bulk-criteria.routes';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    routing
  ],
  declarations: [StopBulkCriteriaComponent, StopBulkCriteriaEditorComponent]
})
export class StopBulkCriteriaModule {
}
