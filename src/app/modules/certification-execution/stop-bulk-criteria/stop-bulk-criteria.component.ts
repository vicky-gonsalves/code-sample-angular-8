import { Component, OnInit } from '@angular/core';
import {appAnimations} from '../../../core/animations/animations';

@Component({
  selector: 'stop-bulk-criteria',
  templateUrl: './stop-bulk-criteria.component.html',
  styleUrls: ['./stop-bulk-criteria.component.scss'],
  animations: appAnimations
})
export class StopBulkCriteriaComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
