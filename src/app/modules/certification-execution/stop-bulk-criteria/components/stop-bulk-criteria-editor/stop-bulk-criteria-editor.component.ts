import {AfterViewInit, Component, OnInit} from '@angular/core';
import {appAnimations} from '../../../../../core/animations/animations';
import {NavItemService} from '../../../../../shared/services/nav-item/nav-item.service';
import {PageTitleService} from '../../../../../shared/services/page-title/page-title.service';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {Router} from '@angular/router';
import {PageHeaderService} from '../../../../../shared/services/page-header/page-header.service';
import {NavItemInterface} from '../../../../../interface/nav-item.interface';
import {CertCriteriaInterface} from '../../../../../interface/cert-criteria.interface';
import {Observable} from 'rxjs';
import {CertCriteria} from '../../../../../model/cert.criteria';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {CertCriteriaExecReqService} from '../../../../../shared/services/cert-criteria-exec-req/cert-criteria-exec-req.service';
import {debounceTime, distinctUntilChanged, startWith, switchMap} from 'rxjs/operators';
import {CertCriteriaService} from '../../../../../shared/services/cert-criteria/cert-criteria.service';

@Component({
  selector: 'stop-bulk-criteria-editor',
  templateUrl: './stop-bulk-criteria-editor.component.html',
  styleUrls: ['./stop-bulk-criteria-editor.component.scss'],
  animations: appAnimations
})
export class StopBulkCriteriaEditorComponent implements OnInit, AfterViewInit {

  private componentNavItem: NavItemInterface;
  public filteredCertCriteria: Observable<CertCriteria[]>;
  public stopBulkCriteriaForm: FormGroup;
  public inProgress: Boolean = false;
  public submitted: Boolean = false;
  public certCriteria: CertCriteria = new CertCriteria(
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null
  );

  constructor(public navItems: NavItemService,
              public pageTitle: PageTitleService,
              private errorMessageService: ErrorMessageService,
              private certCriteriaExecReqService: CertCriteriaExecReqService,
              private certCriteriaService: CertCriteriaService,
              private router: Router) {
  }

  get criteria_name_dropdown() {
    return this.stopBulkCriteriaForm.get('criteria_name_dropdown');
  }

  ngOnInit() {
    this.componentNavItem = this.navItems.getItemById('stop-bulk-criteria');
    if (this.componentNavItem) {
      // Set Page Title
      this.pageTitle.setTitle(this.componentNavItem.name);
    }

    // Set Page Header Details
    PageHeaderService.updatePageHeader({
      showImage: true,
      icon: 'pan_tool'
    });

    this.setupForm();
  }

  ngAfterViewInit() {
    this.initAutocomplete();
  }

  private setupForm(): void {
    this.stopBulkCriteriaForm = new FormGroup({
      'criteria_name_dropdown': new FormControl('', [Validators.required])
    });
  }

  private initAutocomplete(): void {
    this.filteredCertCriteria = this.criteria_name_dropdown.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        startWith(null),
        switchMap(
          (term: string | any) =>
            (typeof term === 'string' || term === '' || term === null) ?
              this.certCriteriaService.getCertCriteriasForAutocomplete({
                term: term,
                limit: 10,
                is_bulk: true
              }) : []
        )
      );
  }

  private setInProgress(): void {
    this.stopBulkCriteriaForm.disable();
    this.inProgress = true;
  }

  private setNotInProgress(): void {
    this.inProgress = false;
    this.stopBulkCriteriaForm.enable();
  }

  public certCriteriaDisplayFn(certCriteria?: CertCriteriaInterface): string | undefined {
    return certCriteria ? (certCriteria.criteria_name) : undefined;
  }

  public getErrorMessage(fieldName: string): string {
    return this.errorMessageService.getError(this[fieldName], fieldName);
  }

  private prepareFormFields(): CertCriteria {
    const formModel = this.stopBulkCriteriaForm.value;
    if (formModel.criteria_name_dropdown && !formModel.criteria_name_dropdown.criteria_name) {
      this.criteria_name_dropdown.setValue(null);
    }
    return {
      criteria_name: formModel.criteria_name_dropdown && formModel.criteria_name_dropdown.criteria_name ? formModel.criteria_name_dropdown.criteria_name : null
    };
  }

  public submitStopBulkCriteriaForm(): void {
    this.submitted = true;
    this.certCriteria = this.prepareFormFields();
    if (this.stopBulkCriteriaForm.valid) {
      this.setInProgress();
      this.certCriteriaExecReqService.stopBulkCriteria(this.certCriteria)
        .subscribe(response => {
            if (response && response.hasOwnProperty('data') && response.data) {
              this.errorMessageService.openSnackBar('Your request is successfully executed.', 5000, 'OK');
            }
            /*Form reset patch*/
            this.router.navigateByUrl('/RefreshComponent', {skipLocationChange: true}).then(() =>
              this.router.navigate(['execute-certification/stop-bulk-criteria']));
          },
          err => {
            this.setNotInProgress();
            this.errorMessageService.handleServerErrors(err, this.stopBulkCriteriaForm);
          });
    }
  }
}
