import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StopBulkCriteriaEditorComponent } from './stop-bulk-criteria-editor.component';

describe('StopBulkCriteriaEditorComponent', () => {
  let component: StopBulkCriteriaEditorComponent;
  let fixture: ComponentFixture<StopBulkCriteriaEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StopBulkCriteriaEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StopBulkCriteriaEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
