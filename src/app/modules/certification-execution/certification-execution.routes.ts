import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserDataGuard} from '../../core/guards/user-data/user-data.guard';

export const routes: Routes = [
  {
    path: '',
    canActivate: [UserDataGuard],
    children: [
      {
        path: 'exec-certification-criteria',
        canActivateChild: [UserDataGuard],
        loadChildren: () => import('./exec-certification-criteria/exec-certification-criteria.module').then(mod => mod.ExecCertificationCriteriaModule),
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'exec-certification-criteria'}
      },
      {
        path: 'stop-bulk-criteria',
        canActivateChild: [UserDataGuard],
        loadChildren: () => import('./stop-bulk-criteria/stop-bulk-criteria.module').then(mod => mod.StopBulkCriteriaModule),
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'stop-bulk-criteria'}
      }
    ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);

