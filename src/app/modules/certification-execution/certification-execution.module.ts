import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CertificationExecutionComponent} from './certification-execution.component';
import {routing} from './certification-execution.routes';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    routing
  ],
  declarations: [CertificationExecutionComponent]
})
export class CertificationExecutionModule {
}
