import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {appAnimations} from '../../../../../core/animations/animations';
import {CertCriteriaInterface} from '../../../../../interface/cert-criteria.interface';
import {NavItemInterface} from '../../../../../interface/nav-item.interface';
import {CertCriteriaService} from '../../../../../shared/services/cert-criteria/cert-criteria.service';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {NavItemService} from '../../../../../shared/services/nav-item/nav-item.service';
import {PageHeaderService} from '../../../../../shared/services/page-header/page-header.service';
import {PageTitleService} from '../../../../../shared/services/page-title/page-title.service';

@Component({
  selector: 'certification-criteria-list',
  templateUrl: './certification-criteria-list.component.html',
  styleUrls: ['./certification-criteria-list.component.scss'],
  animations: appAnimations
})
export class CertificationCriteriaListComponent implements OnInit {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  public searchTerm: {
    participant_id: string,
    criteria_name: string
  };
  public displayedColumns =
    ['criteria_name', 'criteria_type', 'criteria_description', 'participant_id', 'template_names', 'is_deactive', 'actions'];
  public dataSource: MatTableDataSource<CertCriteriaInterface>;
  public paginatorOptions = {
    count: 0,
    pageSize: 10,
    pageSizeOptions: [5, 10, 25, 50, 100],
    showFirstLastButtons: true
  };
  public inProgress: Boolean = true;
  private sortBy = '-createdAt';
  private pageSize = 10;
  private componentNavItem: NavItemInterface;

  constructor(private navItems: NavItemService,
              private certCriteriaService: CertCriteriaService,
              private errorMsgService: ErrorMessageService,
              private pageTitle: PageTitleService) {
    this.searchTerm = {
      participant_id: null,
      criteria_name: null
    };
  }

  ngOnInit() {
    // Get Navigation Component Name
    this.componentNavItem = this.navItems.getItemById('certification-criteria');
    if (this.componentNavItem) {
      // Set Page Title
      this.pageTitle.setTitle(this.componentNavItem.name);
    }

    // Set Page Header Details
    PageHeaderService.updatePageHeader({
      showImage: true,
      icon: 'class',
      actionButton: {
        title: 'Add New Certification Criteria',
        url: 'manage-certificate-criteria/certification-criteria/add'
      }
    });

    // Fetch CertCriterias
    this.fetchCertCriteriaListing();
  }

  public sortData(sort: Sort): void {
    const data = this.dataSource;
    if (!sort.active || sort.direction === '') {
      this.sortBy = '-createdAt';
    } else {
      this.sortBy = sort.direction === 'asc' ? sort.active : '-' + sort.active;
    }
    this.paginator.pageIndex = 0;
    this.fetchCertCriteriaListing();
  }

  public fetchCertCriteriaListing(pageEvent?: PageEvent): void {
    if (pageEvent) {
      this.pageSize = pageEvent.pageSize;
    }
    const options = {
      limit: (pageEvent ? (pageEvent.pageSize) : this.pageSize ? this.pageSize : this.paginatorOptions.pageSize),
      page: (pageEvent ? (pageEvent.pageIndex + 1) : 1),
      sort: [this.sortBy]
    };
    if (this.searchTerm.criteria_name) {
      options['criteria_name'] = this.searchTerm.criteria_name;
    }
    if (this.searchTerm.participant_id) {
      options['participant_id'] = this.searchTerm.participant_id;
    }
    this.inProgress = true;
    this.certCriteriaService.getCertCriterias(options)
      .subscribe(response => {
          if (response && response.data && response.data.rows && response.data.rows.length) {
            this.dataSource = new MatTableDataSource(response.data.rows);
            this.paginatorOptions.count = response.data.count;
            this.dataSource.sort = this.sort;
          } else {
            this.errorMsgService.openSnackBar(this.errorMsgService.getNonFormError('NO_DATA'));
          }
          this.inProgress = false;
        },
        err => {
          this.errorMsgService.handleServerErrors(err);
          this.inProgress = false;
        });
  }

  public applyFilter(searchCriteria: { key: string, value: string }): void {
    this.searchTerm[searchCriteria.key] = searchCriteria.value;
    this.paginator.pageIndex = 0;
    this.fetchCertCriteriaListing();
  }

  public getFirstTestTemplateName(templates): string {
    return templates[0];
  }

  public getOtherTestTemplateNames(templates): string | null {
    if (templates && templates.length > 1) {
      return templates.join(', ');
    }
    return null;
  }
}
