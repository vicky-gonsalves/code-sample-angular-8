import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CertificationCriteriaDetailListComponent} from './certification-criteria-detail-list.component';

describe('CertificationCriteriaDetailListComponent', () => {
  let component: CertificationCriteriaDetailListComponent;
  let fixture: ComponentFixture<CertificationCriteriaDetailListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CertificationCriteriaDetailListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertificationCriteriaDetailListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
