import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {appAnimations} from '../../../../../core/animations/animations';
import {TestTemplateInterface} from '../../../../../interface/test-template.interface';
import {CertCriteriaService} from '../../../../../shared/services/cert-criteria/cert-criteria.service';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {CertificationCriteriaFieldValuesDetailListComponent} from '../certification-criteria-field-values-detail-list/certification-criteria-field-values-detail-list.component';

@Component({
  selector: 'certification-criteria-detail-list',
  templateUrl: './certification-criteria-detail-list.component.html',
  styleUrls: ['./certification-criteria-detail-list.component.scss'],
  animations: appAnimations
})
export class CertificationCriteriaDetailListComponent implements OnInit {
  @Input() id: string;
  @Input() participantId: string;
  @Input() criteriaType: string;
  @Input() is_bulk?: Boolean;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  public searchTerm: {
    template_name: string,
    testcase_name: string,
    initiating_msg_type: string
  };
  public displayedColumns =
    ['template_name', 'template_type', 'testcase_name', 'testcase_description', 'initiating_msg_type', 'actions'];
  public dataSource: MatTableDataSource<TestTemplateInterface>;
  public paginatorOptions = {
    count: 0,
    pageSize: 10,
    pageSizeOptions: [5, 10, 25, 50, 100],
    showFirstLastButtons: true
  };
  public inProgress: Boolean = true;
  public pageEvent: any;
  private sortBy = '-_id';
  private pageSize = 10;

  constructor(private certCriteriaService: CertCriteriaService,
              private dialog: MatDialog,
              private errorMsgService: ErrorMessageService) {
    this.searchTerm = {
      template_name: null,
      testcase_name: null,
      initiating_msg_type: null,
    };
  }

  ngOnInit() {
    // Fetch Test Templates
    this.fetchCertCriteriaTestTemplatesListing();
  }

  public sortData(sort: Sort): void {
    const data = this.dataSource;
    if (!sort.active || sort.direction === '') {
      this.sortBy = '-_id';
    } else {
      this.sortBy = sort.direction === 'asc' ? sort.active : '-' + sort.active;
    }
    this.paginator.pageIndex = 0;
    this.fetchCertCriteriaTestTemplatesListing();
  }

  public fetchCertCriteriaTestTemplatesListing(_pageEvent?: PageEvent): void {
    if (_pageEvent) {
      this.pageEvent = _pageEvent;
      this.pageSize = _pageEvent.pageSize;
    }
    const options = {
      limit: (this.pageEvent ? (this.pageEvent.pageSize) : this.pageSize ? this.pageSize : this.paginatorOptions.pageSize),
      page: (this.pageEvent ? (this.pageEvent.pageIndex + 1) : 1),
      sort: [this.sortBy]
    };
    if (this.searchTerm.template_name) {
      options['template_name'] = this.searchTerm.template_name;
    }
    if (this.searchTerm.testcase_name) {
      options['testcase_name'] = this.searchTerm.testcase_name;
    }
    if (this.searchTerm.initiating_msg_type) {
      options['initiating_msg_type'] = this.searchTerm.initiating_msg_type;
    }
    this.inProgress = true;
    this.certCriteriaService.getCertCriteriaTestTemplates(options, this.id)
      .subscribe(response => {
          if (response && response.data && response.data.rows && response.data.rows.length) {
            const hasConnName = response.data.rows.filter(template => {
              return !!(template.hasOwnProperty('conn_name') && template.conn_name);
            });
            this.showHideFields(hasConnName.length > 0);
            this.dataSource = new MatTableDataSource(response.data.rows);
            this.paginatorOptions.count = response.data.count;
            this.dataSource.sort = this.sort;
          } else {
            this.errorMsgService.openSnackBar(this.errorMsgService.getNonFormError('NO_DATA'));
          }
          this.inProgress = false;
        },
        err => {
          this.errorMsgService.handleServerErrors(err);
          this.inProgress = false;
        });
  }

  public applyFilter(searchCriteria: { key: string, value: string }): void {
    this.searchTerm[searchCriteria.key] = searchCriteria.value;
    this.paginator.pageIndex = 0;
    this.fetchCertCriteriaTestTemplatesListing();
  }

  public showFieldMappings(id: string): void {
    const conf: MatDialogConfig = {
      autoFocus: false,
      closeOnNavigation: true,
      panelClass: 'panelClass',
      maxWidth: '1024px',
      data: {id: id}
    };
    this.dialog.open(CertificationCriteriaFieldValuesDetailListComponent, conf);
  }

  private showHideFields(hasConnName: Boolean) {
    if (hasConnName) {
      this.displayedColumns =
        ['template_name', 'template_type', 'testcase_name', 'testcase_description', 'initiating_msg_type', 'conn_name', 'actions'];
    }
    if (this.is_bulk) {
      this.displayedColumns =
        ['template_name', 'template_type', 'testcase_name', 'testcase_description', 'initiating_msg_type', 'weightage', 'actions'];
      if (hasConnName) {
        this.displayedColumns =
          ['template_name', 'template_type', 'testcase_name', 'testcase_description', 'initiating_msg_type', 'weightage', 'conn_name', 'actions'];
      }
    }
  }
}
