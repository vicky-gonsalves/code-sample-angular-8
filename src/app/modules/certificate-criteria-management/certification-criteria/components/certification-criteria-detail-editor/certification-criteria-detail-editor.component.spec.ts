import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CertificationCriteriaDetailEditorComponent} from './certification-criteria-detail-editor.component';

describe('CertificationCriteriaDetailEditorComponent', () => {
  let component: CertificationCriteriaDetailEditorComponent;
  let fixture: ComponentFixture<CertificationCriteriaDetailEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CertificationCriteriaDetailEditorComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertificationCriteriaDetailEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
