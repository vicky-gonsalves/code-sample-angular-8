import {AfterViewInit, Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {flatMap} from 'lodash';
import {Observable} from 'rxjs';
import {debounceTime, distinctUntilChanged, startWith, switchMap} from 'rxjs/internal/operators';
import {appAnimations} from '../../../../../core/animations/animations';
import {TestTemplateInterface} from '../../../../../interface/test-template.interface';
import {CertCriteria} from '../../../../../model/cert.criteria';
import {TestTemplate} from '../../../../../model/test-template.model';
import {CertCriteriaService} from '../../../../../shared/services/cert-criteria/cert-criteria.service';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {TestCaseService} from '../../../../../shared/services/test-case/test-case.service';
import {TestTemplateService} from '../../../../../shared/services/test-template/test-template.service';

@Component({
  selector: 'certification-criteria-detail-editor',
  templateUrl: './certification-criteria-detail-editor.component.html',
  styleUrls: ['./certification-criteria-detail-editor.component.scss'],
  animations: appAnimations
})
export class CertificationCriteriaDetailEditorComponent implements OnInit, AfterViewInit {
  public IS_EDIT: Boolean = false;
  public certCriteriaForm: FormGroup;
  public certCriteria: CertCriteria = new CertCriteria(
    null,
    null,
    null,
    null,
    null,
    false,
    null
  );
  public inProgress: Boolean = false;
  public submitted: Boolean = false;

  public displayTemplateNames = [];
  public filteredTemplateNames: Observable<TestTemplate[]>;

  constructor(private dialogRef: MatDialogRef<CertificationCriteriaDetailEditorComponent>,
              @Inject(MAT_DIALOG_DATA) private data: any,
              private errorMessageService: ErrorMessageService,
              private certCriteriaService: CertCriteriaService,
              private testCaseService: TestCaseService,
              private testTemplateService: TestTemplateService) {
  }

  get template_names() {
    return this.certCriteriaForm.get('template_names');
  }

  ngOnInit() {
    this.setupForm();
  }

  ngAfterViewInit() {
    this.initAutocomplete();
  }

  public getErrorMessage(fieldName: string): string {
    return this.errorMessageService.getError(this[fieldName], fieldName);
  }

  public submitCertCriteriaForm(): void {
    this.submitted = true;
    this.certCriteria = this.prepareFormFields();
    if (this.certCriteriaForm.valid) {
      this.setInProgress();
      this.certCriteriaService.updateCertCriteriaTemplateName(this.data.id, this.certCriteria)
        .subscribe(response => {
            if (response && response.hasOwnProperty('data') && response.data) {
              this.errorMessageService.openSnackBar('Certification Criteria successfully updated.', 5000, 'OK');
              this.dialogRef.close();
            }
            this.setNotInProgress();
          },
          err => {
            this.setNotInProgress();
            this.errorMessageService.handleServerErrors(err, this.certCriteriaForm);
          });
    }
  }

  public templateNamesDisplayFn(testTemplate?: TestTemplateInterface): string | undefined {
    return testTemplate ? (testTemplate.template_name) : undefined;
  }

  public onTemplateNamesChanged() {
    const formModel = this.certCriteriaForm.value;
    this.filteredTemplateNames = null;
    this.setTemplateNamesAutoComplete();
  }

  private initAutocomplete(): void {
    this.setTemplateNamesAutoComplete();
    this.setValue();
  }

  private setInProgress(): void {
    this.certCriteriaForm.disable();
    this.inProgress = true;
  }

  private setNotInProgress(): void {
    this.inProgress = false;
    this.certCriteriaForm.enable();
  }

  private setupForm(): void {
    this.certCriteriaForm = new FormGroup({
      'template_names': new FormControl('', [])
    });
    this.initExtraValidators();
  }

  private initExtraValidators(): void {
    this.template_names.valueChanges.subscribe(data => this.onTemplateNamesChanged());
  }

  private setTemplateNamesAutoComplete(): void {
    this.filteredTemplateNames = null;
    this.filteredTemplateNames = this.template_names.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        startWith(null),
        switchMap(
          (term: string | any) =>
            (typeof term === 'string' || term === '' || term === null) ?
              this.testTemplateService.getTestTemplatesForAutocomplete({
                  term: term,
                  limit: 10
                },
                this.data.criteriaType,
                this.data.participantId,
                flatMap(this.displayTemplateNames, 'template_name')) : [])
      );
  }

  private prepareFormFields(): any {
    const formModel = this.certCriteriaForm.value;
    return {
      old_template_name: this.data.testTemplate.template_name,
      new_template_name: formModel.template_names.template_name,
      _id: this.data.id
    };
  }

  private setValue(): void {
    setTimeout(() => {
      this.certCriteriaForm.patchValue({
        template_names: {template_name: this.data.testTemplate.template_name}
      });
    });
  }
}
