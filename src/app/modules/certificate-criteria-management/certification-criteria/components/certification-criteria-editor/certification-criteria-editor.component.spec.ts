import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CertificationCriteriaEditorComponent} from './certification-criteria-editor.component';

describe('CertificationCriteriaEditorComponent', () => {
  let component: CertificationCriteriaEditorComponent;
  let fixture: ComponentFixture<CertificationCriteriaEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CertificationCriteriaEditorComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertificationCriteriaEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
