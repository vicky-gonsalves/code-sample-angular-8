import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {AfterViewInit, Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {MatAutocompleteSelectedEvent} from '@angular/material/autocomplete';
import {MatChipInputEvent} from '@angular/material/chips';
import {ActivatedRoute, Router} from '@angular/router';
import {findIndex, flatMap} from 'lodash';
import {Observable} from 'rxjs';
import {debounceTime, distinctUntilChanged, startWith, switchMap} from 'rxjs/internal/operators';
import {appAnimations} from '../../../../../core/animations/animations';
import {NavItemInterface} from '../../../../../interface/nav-item.interface';
import {ParticipantInterface} from '../../../../../interface/participant.interface';
import {TestTemplateInterface} from '../../../../../interface/test-template.interface';
import {CertCriteria} from '../../../../../model/cert.criteria';
import {Participant} from '../../../../../model/participant.model';
import {TestTemplate} from '../../../../../model/test-template.model';
import {CertCriteriaService} from '../../../../../shared/services/cert-criteria/cert-criteria.service';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {NavItemService} from '../../../../../shared/services/nav-item/nav-item.service';
import {PageHeaderService} from '../../../../../shared/services/page-header/page-header.service';
import {PageTitleService} from '../../../../../shared/services/page-title/page-title.service';
import {ParticipantService} from '../../../../../shared/services/participant/participant.service';
import {TestCaseService} from '../../../../../shared/services/test-case/test-case.service';
import {TestTemplateService} from '../../../../../shared/services/test-template/test-template.service';

@Component({
  selector: 'certification-criteria-editor',
  templateUrl: './certification-criteria-editor.component.html',
  styleUrls: ['./certification-criteria-editor.component.scss'],
  animations: appAnimations
})
export class CertificationCriteriaEditorComponent implements OnInit, AfterViewInit {
  public IS_EDIT: Boolean = false;
  public certCriteriaForm: FormGroup;
  public certCriteria: CertCriteria = new CertCriteria(
    null,
    null,
    null,
    null,
    null,
    false,
    false,
    null,
    null,
    null
  );
  public inProgress: Boolean = false;
  public submitted: Boolean = false;
  public filteredParticipants: Observable<Participant[]>;
  public helpText: any;
  public addOnBlur: Boolean = true;
  public removable: Boolean = true;
  public separatorKeysCodes = [ENTER, COMMA];
  public displayTemplateNames = [];
  public filteredTemplateNames: Observable<TestTemplate[]>;
  private componentNavItem: NavItemInterface;
  private certCriteriaId: string;

  constructor(public navItems: NavItemService,
              public pageTitle: PageTitleService,
              private errorMessageService: ErrorMessageService,
              private certCriteriaService: CertCriteriaService,
              private participantService: ParticipantService,
              private testCaseService: TestCaseService,
              private testTemplateService: TestTemplateService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe(params => {
      if (params.hasOwnProperty('id') && params.id.length) {
        this.certCriteriaId = params.id;
        this.IS_EDIT = true;
      }
    });
  }

  get criteria_name() {
    return this.certCriteriaForm.get('criteria_name');
  }

  get criteria_type() {
    return this.certCriteriaForm.get('criteria_type');
  }

  get criteria_description() {
    return this.certCriteriaForm.get('criteria_description');
  }

  get participant_id() {
    return this.certCriteriaForm.get('participant_id');
  }

  get is_deactive() {
    return this.certCriteriaForm.get('is_deactive');
  }

  get is_bulk() {
    return this.certCriteriaForm.get('is_bulk');
  }

  get duration() {
    return this.certCriteriaForm.get('duration');
  }

  get iterations() {
    return this.certCriteriaForm.get('iterations');
  }

  get request_interval() {
    return this.certCriteriaForm.get('request_interval');
  }

  get template_names() {
    return this.certCriteriaForm.get('template_names');
  }

  get template_names_chips() {
    return this.certCriteriaForm.get('template_names_chips');
  }

  ngOnInit() {
    let id = 'certification-criteria-add';
    if (this.IS_EDIT) {
      id = 'certification-criteria-edit';
      PageHeaderService.updatePageHeader({backPath: 'manage-certificate-criteria/certification-criteria', subTitle: 'Loading...'});
    } else {
      PageHeaderService.updatePageHeader({backPath: 'manage-certificate-criteria/certification-criteria'});
    }
    this.componentNavItem = this.navItems.getItemById(id);
    if (this.componentNavItem) {
      this.pageTitle.setTitle(this.componentNavItem.name);
    }
    this.setupForm();

    if (this.certCriteriaId) {
      this.fetchCertCriteria(this.certCriteriaId);
    }
  }

  ngAfterViewInit() {
    this.initAutocomplete();
  }

  public getErrorMessage(fieldName: string): string {
    return this.errorMessageService.getError(this[fieldName], fieldName);
  }

  public submitCertCriteriaForm(): void {
    this.submitted = true;
    if (!this.IS_EDIT) {
      this.checkTemplateNamesValidity();
    }
    this.certCriteria = this.prepareFormFields();
    if (this.certCriteriaForm.valid) {
      this.setInProgress();
      if (this.IS_EDIT) {
        this.certCriteriaService.updateCertCriteria(this.certCriteria)
          .subscribe(response => {
              if (response && response.hasOwnProperty('data') && response.data) {
                this.setValue(response.data);
                this.errorMessageService.openSnackBar('Certification Criteria successfully updated.', 5000, 'OK');
              }
              this.setNotInProgress();
            },
            err => {
              this.setNotInProgress();
              this.errorMessageService.handleServerErrors(err, this.certCriteriaForm);
            });
      } else {
        this.certCriteriaService.addCertCriteria(this.certCriteria)
          .subscribe(response => {
              if (response && response.hasOwnProperty('data') && response.data) {
                this.router.navigate(['manage-certificate-criteria/certification-criteria/view/' + response.data._id]);
                this.errorMessageService.openSnackBar('Certification Criteria successfully added.', 5000, 'OK');
              }
              this.setNotInProgress();
            },
            err => {
              this.setNotInProgress();
              this.errorMessageService.handleServerErrors(err, this.certCriteriaForm);
            });
      }
    }
  }

  public participantDisplayFn(participant?: ParticipantInterface): string | undefined {
    return participant ? (participant.id) : undefined;
  }

  public templateNamesDisplayFn(testTemplate?: TestTemplateInterface): string | undefined {
    return testTemplate ? (testTemplate.template_name) : undefined;
  }

  public addTemplateName(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our item
    if (value && value.hasOwnProperty('template_name')) {
      this.displayTemplateNames.push(value);
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  public removeTemplateName(item: any): void {
    this.filteredTemplateNames = null;
    const index = findIndex(this.displayTemplateNames, item);
    if (index >= 0) {
      this.displayTemplateNames.splice(index, 1);
      this.onTemplateNamesChanged();
    }
    this.checkTemplateNamesValidity();
  }

  public addSelectTemplateName(event: MatAutocompleteSelectedEvent) {
    const option = event.option;
    const value = option.value;
    if (value && value.hasOwnProperty('template_name')) {
      this.displayTemplateNames.push(value);
    }
    this.checkTemplateNamesValidity();
  }

  public onParticipantChanged(value?: any) {
    const formModel = this.certCriteriaForm.value;
    this.filteredTemplateNames = null;
    this.displayTemplateNames.length = 0;
    this.template_names.setValue({
      template_name: null
    });
    if (formModel.criteria_type && value && value.id) {
      this.setTemplateNamesAutoComplete(formModel.criteria_type, value.id);
    }
  }

  public onTemplateNamesChanged() {
    const formModel = this.certCriteriaForm.value;
    this.filteredTemplateNames = null;
    if (formModel.criteria_type && formModel.participant_id && formModel.participant_id.id) {
      this.setTemplateNamesAutoComplete(formModel.criteria_type, formModel.participant_id.id);
    }
  }

  public onCriteriaChanged(selection: any) {
    const formModel = this.certCriteriaForm.value;
    this.filteredTemplateNames = null;
    this.displayTemplateNames.length = 0;
    this.template_names.patchValue({
      template_name: null
    });
    if (selection && selection.value && formModel.participant_id && formModel.participant_id.id) {
      this.setTemplateNamesAutoComplete(selection.value, formModel.participant_id.id);
    }
  }

  public isBulkChanged(flag: boolean) {
    if (!flag) {
      this.certCriteriaForm.patchValue({
        duration: null,
        iterations: null,
        request_interval: null,
      });
      this.duration.setErrors(null);
      this.iterations.setErrors(null);
      this.request_interval.setErrors(null);
    }
  }

  private fetchCertCriteria(id: string): void {
    this.certCriteriaService.getCertCriteria(id)
      .subscribe(response => {
          if (response && response.hasOwnProperty('data') && response.data) {
            this.setValue(response.data);
          }
          this.setNotInProgress();
        },
        err => {
          this.setNotInProgress();
          this.errorMessageService.handleServerErrors(err, this.certCriteriaForm);
        });
  }

  private initAutocomplete(): void {
    this.filteredParticipants = this.participant_id.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        startWith(null),
        switchMap(
          (term: string | any) =>
            (typeof term === 'string' || term === '' || term === null) ?
              this.participantService.getParticipantsForAutocomplete({
                term: term,
                limit: 10
              }) : [])
      );
  }

  private setInProgress(): void {
    this.certCriteriaForm.disable();
    this.inProgress = true;
  }

  private setNotInProgress(): void {
    this.inProgress = false;
    this.certCriteriaForm.enable();
  }

  private setupForm(): void {
    this.certCriteriaForm = new FormGroup({
      'criteria_name': new FormControl('', [
        Validators.required,
        Validators.pattern(/^[A-Za-z\d\-\s]+$/),
        Validators.minLength(1),
        Validators.maxLength(35)
      ]),
      'criteria_type': new FormControl('', [
        Validators.required
      ]),
      'criteria_description': new FormControl('', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(500)
      ]),
      'participant_id': new FormControl('', [
        Validators.required
      ]),
      'is_deactive': new FormControl('', []),
      'is_bulk': new FormControl(this.certCriteria.is_bulk, []),
      'duration': new FormControl('', []),
      'iterations': new FormControl('', []),
      'request_interval': new FormControl('', []),
      'template_names': new FormControl('', []),
      'template_names_chips': new FormControl('', [])
    }, [this.atLeastOneValidator(), this.requestIntervalValidator()]);
    this.initExtraValidators();
  }

  private atLeastOneValidator() {
    return (control: FormGroup): ValidationErrors | null => {
      const isBulkVal = control.get('is_bulk') ? control.get('is_bulk').value : null;
      const durationVal = control.get('duration') ? control.get('duration').value : null;
      const iterationsVal = control.get('iterations') ? control.get('iterations').value : null;
      if (isBulkVal && (!durationVal && !iterationsVal)) {
        this.duration.setErrors({atLeastOne: true});
        this.iterations.setErrors({atLeastOne: true});
        return {
          dummyError: true
        };
      } else if (isBulkVal && (isNaN(durationVal) || (!isNaN(durationVal) && durationVal < 1)) && !iterationsVal) {
        this.duration.setErrors({min: true});
        return {
          dummyError: true
        };
      } else if (isBulkVal && (isNaN(iterationsVal) || (!isNaN(iterationsVal) && iterationsVal < 1)) && !durationVal) {
        this.iterations.setErrors({min: true});
        return {
          dummyError: true
        };
      } else if (isBulkVal && iterationsVal && (isNaN(iterationsVal))) {
        this.iterations.setErrors({pattern: true});
        return {
          dummyError: true
        };
      } else if (isBulkVal && durationVal && (isNaN(durationVal))) {
        this.duration.setErrors({pattern: true});
        return {
          dummyError: true
        };
      } else if (control.get('is_bulk').value && ((control.get('duration') && control.get('duration').value) || (control.get('iterations') && control.get('iterations').value))) {
        this.duration.setErrors(null);
        this.iterations.setErrors(null);
      }
      return null;
    };
  }

  private requestIntervalValidator() {
    return (control: FormGroup): ValidationErrors | null => {
      const isBulkVal = control.get('is_bulk') ? control.get('is_bulk').value : null;
      const requestIntervalVal = control.get('request_interval') ? control.get('request_interval').value : null;
      if (isBulkVal && (requestIntervalVal === null || requestIntervalVal === '')) {
        this.request_interval.setErrors({required: true});
        return {
          dummyError: true
        };
      } else if (isBulkVal && (isNaN(requestIntervalVal))) {
        this.request_interval.setErrors({pattern: true});
        return {
          dummyError: true
        };
      } else if (isBulkVal && (requestIntervalVal < 0)) {
        this.request_interval.setErrors({min: true});
        return {
          dummyError: true
        };
      } else if (control.get('is_bulk').value && (control.get('request_interval').value)) {
        this.request_interval.setErrors(null);
      }

      return null;
    };
  }

  private initExtraValidators(): void {
    this.participant_id.valueChanges.subscribe(data => this.onParticipantChanged(data));
    this.template_names.valueChanges.subscribe(data => this.onTemplateNamesChanged());
  }

  private setTemplateNamesAutoComplete(criteria_type: string, participant_id: string): void {
    this.filteredTemplateNames = null;
    this.filteredTemplateNames = this.template_names.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        startWith(!this.template_names.value.length ? null : this.template_names.value),
        switchMap((term: string | null) =>
          (typeof term === 'string' || term === '' || term === null) ?
            this.testTemplateService.getTestTemplatesForAutocomplete({
              default: this.certCriteriaId || null,
              term: term,
              limit: 10
            }, criteria_type, participant_id, flatMap(this.displayTemplateNames, 'template_name')) : []
        ));
  }

  private prepareFormFields(): CertCriteria {
    const formModel = this.certCriteriaForm.value;
    if (formModel.participant_id && !formModel.participant_id.id) {
      this.participant_id.setValue(null);
    }
    let duration = parseInt(formModel.duration, 10);
    let iterations = parseInt(formModel.iterations, 10);
    let request_interval = parseInt(formModel.request_interval, 10);
    if (!formModel.is_bulk) {
      duration = null;
      iterations = null;
      request_interval = null;
    }
    if (this.IS_EDIT) {
      return {
        criteria_description: formModel.criteria_description,
        is_deactive: formModel.is_deactive,
        is_bulk: formModel.is_bulk,
        duration: duration > 0 ? duration : null,
        iterations: iterations > 0 ? iterations : null,
        request_interval: request_interval >= 0 ? request_interval : null,
        template_names: flatMap(this.displayTemplateNames, 'template_name'),
        _id: this.certCriteriaId
      };
    }
    return {
      criteria_name: formModel.criteria_name,
      criteria_type: formModel.criteria_type,
      criteria_description: formModel.criteria_description,
      is_bulk: formModel.is_bulk,
      duration: duration > 0 ? duration : null,
      iterations: iterations > 0 ? iterations : null,
      request_interval: request_interval >= 0 ? request_interval : null,
      participant_id: formModel.participant_id && formModel.participant_id.id ? formModel.participant_id.id : null,
      is_deactive: formModel.is_deactive,
      template_names: flatMap(this.displayTemplateNames, 'template_name')
    };
  }

  private setValue(data: CertCriteria): void {
    setTimeout(() => {
      this.certCriteriaForm.patchValue({
        criteria_name: data.criteria_name,
        criteria_type: data.criteria_type,
        criteria_description: data.criteria_description,
        is_bulk: data.is_bulk,
        duration: data.duration,
        iterations: data.iterations,
        request_interval: data.request_interval,
        participant_id: {id: data.participant_id},
        template_names_chips: data.template_names,
        is_deactive: data.is_deactive
      });
      this.displayTemplateNames = data.template_names.map(name => ({template_name: name}));
      this.onTemplateNamesChanged();
      PageHeaderService.updatePageHeader({backPath: 'manage-certificate-criteria/certification-criteria', subTitle: data.criteria_name});
    });
  }

  private checkTemplateNamesValidity() {
    if (!this.displayTemplateNames.length) {
      this.template_names_chips.updateValueAndValidity();
      this.template_names_chips.markAsDirty();
      this.template_names_chips.setErrors({required: true});
    } else {
      this.template_names_chips.updateValueAndValidity();
      this.template_names_chips.markAsPristine();
      this.template_names_chips.setErrors(null);
    }
  }

}
