import {Component, OnInit} from '@angular/core';
import {appAnimations} from '../../../../../core/animations/animations';
import {CertCriteriaInterface} from '../../../../../interface/cert-criteria.interface';
import {NavItemService} from '../../../../../shared/services/nav-item/nav-item.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NavItemInterface} from '../../../../../interface/nav-item.interface';
import {PageHeaderService} from '../../../../../shared/services/page-header/page-header.service';
import {CertCriteriaService} from '../../../../../shared/services/cert-criteria/cert-criteria.service';
import {HelpTextService} from '../../../../../core/services/help-text/help-text.service';
import {PageTitleService} from '../../../../../shared/services/page-title/page-title.service';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';

@Component({
  selector: 'certification-criteria-view',
  templateUrl: './certification-criteria-view.component.html',
  styleUrls: ['./certification-criteria-view.component.scss'],
  animations: appAnimations
})
export class CertificationCriteriaViewComponent implements OnInit {
  public certCriteriaId: string;
  public certCriteria: CertCriteriaInterface;
  public inProgress: Boolean = true;
  public helpText: any;
  private componentNavItem: NavItemInterface;

  constructor(public navItems: NavItemService,
              public pageTitle: PageTitleService,
              private errorMessageService: ErrorMessageService,
              private router: Router,
              private certCriteriaService: CertCriteriaService,
              private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe(params => {
      if (params.hasOwnProperty('id') && params.id.length) {
        this.certCriteriaId = params.id;
      }
    });
    HelpTextService.helpText.subscribe(helpText => {
      if (helpText.certCriteria) {
        this.helpText = helpText.certCriteria;
      }
    });
  }

  ngOnInit() {
    this.componentNavItem = this.navItems.getItemById('certification-criteria-view');
    if (this.componentNavItem) {
      this.pageTitle.setTitle(this.componentNavItem.name);
    }
    // Set Page Header Details
    PageHeaderService.updatePageHeader({
      backPath: 'manage-certificate-criteria/certification-criteria',
      subTitle: 'Loading...',
      actionButton: {
        title: 'Add New Certification Criteria',
        url: 'manage-certificate-criteria/certification-criteria/add'
      }
    });

    if (this.certCriteriaId) {
      this.fetchCertCriteria(this.certCriteriaId);
    }
  }

  private setInProgress(): void {
    this.inProgress = true;
  }

  private setNotInProgress(): void {
    this.inProgress = false;
  }

  private fetchCertCriteria(id: string): void {
    this.certCriteriaService.getCertCriteria(id)
      .subscribe(response => {
          if (response && response.hasOwnProperty('data') && response.data) {
            this.certCriteria = response.data;
            PageHeaderService.updatePageHeader({
              backPath: 'manage-certificate-criteria/certification-criteria', actionButton: {
                title: 'Add New Certification Criteria',
                url: 'manage-certificate-criteria/certification-criteria/add'
              },
              subTitle: this.certCriteria.criteria_name
            });
          }
          this.setNotInProgress();
        },
        err => {
          this.setNotInProgress();
          this.errorMessageService.handleServerErrors(err);
        });
  }
}
