import {Component, OnInit} from '@angular/core';
import {appAnimations} from '../../../core/animations/animations';

@Component({
  selector: 'certification-criteria',
  templateUrl: './certification-criteria.component.html',
  styleUrls: ['./certification-criteria.component.scss'],
  animations: appAnimations
})
export class CertificationCriteriaComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
