import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserDataGuard} from '../../../core/guards/user-data/user-data.guard';
import {CertificationCriteriaComponent} from './certification-criteria.component';
import {CertificationCriteriaListComponent} from './components/certification-criteria-list/certification-criteria-list.component';
import {CertificationCriteriaEditorComponent} from './components/certification-criteria-editor/certification-criteria-editor.component';
import {CertificationCriteriaViewComponent} from './components/certification-criteria-view/certification-criteria-view.component';

export const routes: Routes = [
  {
    path: '',
    canActivate: [UserDataGuard],
    component: CertificationCriteriaComponent,
    children: [
      {
        path: '',
        canActivateChild: [UserDataGuard],
        component: CertificationCriteriaListComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'certification-criteria'}
      },
      {
        path: 'add',
        canActivateChild: [UserDataGuard],
        component: CertificationCriteriaEditorComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'certification-criteria-add'}
      },
      {
        path: 'edit/:id',
        canActivateChild: [UserDataGuard],
        component: CertificationCriteriaEditorComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'certification-criteria-edit'}
      },
      {
        path: 'view/:id',
        canActivateChild: [UserDataGuard],
        component: CertificationCriteriaViewComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'certification-criteria-view'}
      }
    ]
  },
  {path: '**', redirectTo: '', pathMatch: 'full'}
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);

