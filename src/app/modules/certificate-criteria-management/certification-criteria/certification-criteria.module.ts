import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CertificationCriteriaComponent} from './certification-criteria.component';
import {SharedModule} from '../../../shared/shared.module';
import {routing} from './certification-criteria.routes';
import {CertificationCriteriaListComponent} from './components/certification-criteria-list/certification-criteria-list.component';
import {CertificationCriteriaViewComponent} from './components/certification-criteria-view/certification-criteria-view.component';
import {CertificationCriteriaEditorComponent} from './components/certification-criteria-editor/certification-criteria-editor.component';
import {CertificationCriteriaDetailListComponent} from './components/certification-criteria-detail-list/certification-criteria-detail-list.component';
import {CertificationCriteriaDetailEditorComponent} from './components/certification-criteria-detail-editor/certification-criteria-detail-editor.component';
import {CertificationCriteriaFieldValuesDetailListComponent} from './components/certification-criteria-field-values-detail-list/certification-criteria-field-values-detail-list.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    routing
  ],
  declarations: [
    CertificationCriteriaComponent,
    CertificationCriteriaListComponent,
    CertificationCriteriaViewComponent,
    CertificationCriteriaEditorComponent,
    CertificationCriteriaDetailListComponent,
    CertificationCriteriaDetailEditorComponent,
    CertificationCriteriaFieldValuesDetailListComponent
  ],
  entryComponents: [
    CertificationCriteriaDetailEditorComponent,
    CertificationCriteriaFieldValuesDetailListComponent
  ]
})
export class CertificationCriteriaModule {
}
