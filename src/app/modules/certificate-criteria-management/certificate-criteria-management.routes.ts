import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserDataGuard} from '../../core/guards/user-data/user-data.guard';

export const routes: Routes = [
  {
    path: '',
    canActivate: [UserDataGuard],
    children: [
      {
        path: 'certification-criteria',
        canActivateChild: [UserDataGuard],
        loadChildren: () => import('./certification-criteria/certification-criteria.module').then(mod => mod.CertificationCriteriaModule),
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'certification-criteria'}
      }
    ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);

