import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CertificateCriteriaManagementComponent} from './certificate-criteria-management.component';
import {SharedModule} from '../../shared/shared.module';
import {routing} from './certificate-criteria-management.routes';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    routing
  ],
  declarations: [CertificateCriteriaManagementComponent]
})
export class CertificateCriteriaManagementModule {
}
