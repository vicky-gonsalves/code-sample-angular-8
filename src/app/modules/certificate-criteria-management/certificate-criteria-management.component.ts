import {Component, OnInit} from '@angular/core';
import {appAnimations} from '../../core/animations/animations';

@Component({
  selector: 'certificate-criteria-management',
  templateUrl: './certificate-criteria-management.component.html',
  animations: appAnimations
})
export class CertificateCriteriaManagementComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
