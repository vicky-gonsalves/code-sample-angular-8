import {Component, OnInit} from '@angular/core';
import {appAnimations} from '../../core/animations/animations';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss'],
  animations: appAnimations
})
export class AccountComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
