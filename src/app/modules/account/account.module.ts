import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoginComponent} from './components/login/login.component';
import {AccountComponent} from './account.component';
import {routing} from './account.routes';
import {SharedModule} from '../../shared/shared.module';
import {ForgotPasswordComponent} from './components/forgot-password/forgot-password.component';
import {ResetPasswordComponent} from './components/reset-password/reset-password.component';
import {ActivateComponent} from './components/activate/activate.component';
import {UnlockComponent} from './components/unlock/unlock.component';
import {ChangePasswordComponent} from './components/change-password/change-password.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    routing
  ],
  declarations: [
    ChangePasswordComponent,
    UnlockComponent,
    ActivateComponent,
    LoginComponent,
    AccountComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent
  ]
})
export class AccountModule {
}
