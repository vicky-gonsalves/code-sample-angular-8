import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ForgotPasswordComponent} from './components/forgot-password/forgot-password.component';
import {LoginComponent} from './components/login/login.component';
import {ResetPasswordComponent} from './components/reset-password/reset-password.component';
import {ActivateComponent} from './components/activate/activate.component';
import {UnlockComponent} from './components/unlock/unlock.component';
import {UserDataGuard} from '../../core/guards/user-data/user-data.guard';
import {ChangePasswordComponent} from './components/change-password/change-password.component';

export const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    data: {
      hideSideNav: true,
      hideNavBar: true,
      hideHeader: true
    }
  },
  {
    path: 'forgot-password',
    component: ForgotPasswordComponent,
    data: {
      hideSideNav: true,
      hideNavBar: true,
      hideHeader: true
    }
  },
  {
    path: 'reset-password/:id/:token',
    component: ResetPasswordComponent,
    data: {
      hideSideNav: true,
      hideNavBar: true,
      hideHeader: true
    }
  },
  {
    path: 'activate/:id/:token',
    component: ActivateComponent,
    data: {
      hideSideNav: true,
      hideNavBar: true,
      hideHeader: true
    }
  },
  {
    path: 'unlock/:id/:token',
    component: UnlockComponent,
    data: {
      hideSideNav: true,
      hideNavBar: true,
      hideHeader: true
    }
  },
  {
    path: 'change-password',
    canActivate: [UserDataGuard],
    component: ChangePasswordComponent,
    data: {
      hideSideNav: true,
      hideNavBar: true,
      hideHeader: true
    }
  },
  {
    path: '**',
    redirectTo: 'login'
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);

