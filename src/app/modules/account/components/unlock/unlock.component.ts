import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthenticationService} from '../../../../core/services/authentication/authentication.service';
import {appAnimations} from '../../../../core/animations/animations';
import {ContactService} from '../../../../shared/services/contact/contact.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Contact} from '../../../../model/contact.model';
import {ProgressBarService} from '../../../../shared/services/progress-bar/progress-bar.service';
import {ErrorMessageService} from '../../../../shared/services/error-message/error-message.service';
import {MatchOtherValidatorService} from '../../../../shared/services/match-other-validator/match-other-validator.service';

@Component({
  selector: 'unlock',
  templateUrl: './unlock.component.html',
  styleUrls: ['./unlock.component.scss'],
  animations: appAnimations
})
export class UnlockComponent implements OnInit {
  public verified: Boolean = true;
  public valid: Boolean = true;
  public passwordChanged: Boolean = false;
  public setPassForm: FormGroup;
  public contact: Contact = new Contact();
  public submitted: Boolean = false;
  public inProgress: Boolean = false;
  private id: string;
  private token: string;

  constructor(private activatedRoute: ActivatedRoute,
              private contactService: ContactService,
              private matchOtherValidatorService: MatchOtherValidatorService,
              private router: Router,
              private errorMessageService: ErrorMessageService) {
  }

  get password() {
    return this.setPassForm.get('password');
  }

  get confirmPassword() {
    return this.setPassForm.get('confirmPassword');
  }

  ngOnInit() {
    AuthenticationService.unsetIdentity();
    ProgressBarService.updateLoading(true);
    this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
      this.token = params['token'];
      if (this.id && this.token) {
        this.contactService.verifyTempToken(this.id, {temporary_token: this.token}).subscribe(response => {
            this.verified = true;
            if (response && response.hasOwnProperty('data') &&
              response.data && response.data.hasOwnProperty('valid') &&
              response.data.valid) {
              this.valid = true;
            } else {
              this.valid = false;
            }
            ProgressBarService.updateLoading(false);
          },
          err => {
            this.verified = true;
            this.valid = false;
            ProgressBarService.updateLoading(false);
          });
      }
    });
    this.setPassForm = new FormGroup({
      'password': new FormControl(this.contact.password, [
        Validators.required,
        Validators.minLength(8),
        Validators.pattern(/^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{8,}$/)
      ]),
      'confirmPassword': new FormControl(this.contact.confirmPassword, [
        Validators.required,
        this.matchOtherValidatorService.validate('password')
      ])
    });
  }

  public submitSetPassForm(): void {
    this.submitted = true;
    this.contact = this.prepareFormFields();
    if (this.setPassForm.valid) {
      this.setInProgress();
      this.contactService.unlockContact(this.id, this.contact)
        .subscribe(response => {
            if (response && response.hasOwnProperty('data') &&
              response.data && response.data.hasOwnProperty('contact') && response.data.contact) {
              this.passwordChanged = true;
              this.errorMessageService.openSnackBar('Your account is unlocked', 5000, 'OK');
              setTimeout(() => {
                this.router.navigate(['/'], {replaceUrl: true});
              }, 100);
            } else {
              this.errorMessageService.openSnackBar(this.errorMessageService.getNonFormError('NO_VALID_RESPONSE'), 5000, 'OK');
            }
            this.setNotInProgress();
            this.submitted = false;
          },
          err => {
            this.setNotInProgress();
            this.errorMessageService.handleServerErrors(err, this.setPassForm);
            this.submitted = false;
          });
    }
  }

  public getErrorMessage(fieldName: string): string {
    return this.errorMessageService.getError(this[fieldName], fieldName);
  }

  private prepareFormFields(): Contact {
    const formModel = this.setPassForm.value;

    return {
      password: formModel.password,
      temporary_token: this.token
    };
  }

  private setInProgress(): void {
    this.setPassForm.disable();
    this.inProgress = true;
  }

  private setNotInProgress(): void {
    this.inProgress = false;
    this.setPassForm.enable();
  }

}
