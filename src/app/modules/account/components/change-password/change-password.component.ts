import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {appAnimations} from '../../../../core/animations/animations';
import {ErrorMessageService} from '../../../../shared/services/error-message/error-message.service';
import {ContactService} from '../../../../shared/services/contact/contact.service';
import {Contact} from '../../../../model/contact.model';
import {MatchOtherValidatorService} from '../../../../shared/services/match-other-validator/match-other-validator.service';
import {ContactInterface} from '../../../../interface/contact.interface';
import {IdentityService} from '../../../../core/services/identity/identity.service';
import {Subscription} from 'rxjs';
import {Identity} from '../../../../core/services/identity/identity';

@Component({
  selector: 'change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
  animations: appAnimations
})
export class ChangePasswordComponent implements OnInit {
  public identity: Identity;
  public identitySubscription: Subscription;
  public passwordChanged: Boolean = false;
  public changePassForm: FormGroup;
  public contact: Contact = new Contact();
  public submitted: Boolean = false;
  public inProgress: Boolean = false;

  constructor(private activatedRoute: ActivatedRoute,
              private contactService: ContactService,
              private matchOtherValidatorService: MatchOtherValidatorService,
              private router: Router,
              private errorMessageService: ErrorMessageService) {
  }

  get oldPassword() {
    return this.changePassForm.get('oldPassword');
  }

  get password() {
    return this.changePassForm.get('password');
  }

  get confirmPassword() {
    return this.changePassForm.get('confirmPassword');
  }

  ngOnInit() {
    this.identitySubscription = IdentityService.identity.subscribe((userData: ContactInterface) => {
      if (userData) {
        this.identity = userData;
      }
    });

    this.changePassForm = new FormGroup({
      'oldPassword': new FormControl(this.contact.oldPassword, [
        Validators.required
      ]),
      'password': new FormControl(this.contact.password, [
        Validators.required,
        Validators.minLength(8),
        Validators.pattern(/^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{8,}$/)
      ]),
      'confirmPassword': new FormControl(this.contact.confirmPassword, [
        Validators.required,
        this.matchOtherValidatorService.validate('password')
      ])
    });
  }

  public submitChangePassForm(): void {
    this.submitted = true;
    this.contact = this.prepareFormFields();
    if (this.changePassForm.valid) {
      this.setInProgress();
      this.contactService.changePassword(this.identity._id, this.contact)
        .subscribe(response => {
            if (response && response.hasOwnProperty('data') &&
              response.data && response.data.hasOwnProperty('contact') && response.data.contact) {
              this.passwordChanged = true;
              this.errorMessageService.openSnackBar('Password successfully changed', 5000, 'OK');
              setTimeout(() => {
                this.router.navigate(['/'], {replaceUrl: true});
              }, 100);
            } else {
              this.errorMessageService.openSnackBar(this.errorMessageService.getNonFormError('NO_VALID_RESPONSE'), 5000, 'OK');
            }
            this.setNotInProgress();
            this.submitted = false;
          },
          err => {
            this.setNotInProgress();
            this.errorMessageService.handleServerErrors(err, this.changePassForm);
            this.submitted = false;
          });
    }
  }

  public getErrorMessage(fieldName: string): string {
    return this.errorMessageService.getError(this[fieldName], fieldName);
  }

  private prepareFormFields(): Contact {
    const formModel = this.changePassForm.value;

    return {
      oldPassword: formModel.oldPassword,
      password: formModel.password
    };
  }

  private setInProgress(): void {
    this.changePassForm.disable();
    this.inProgress = true;
  }

  private setNotInProgress(): void {
    this.inProgress = false;
    this.changePassForm.enable();
  }

}
