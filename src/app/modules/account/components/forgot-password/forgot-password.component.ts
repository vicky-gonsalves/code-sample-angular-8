import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {appAnimations} from '../../../../core/animations/animations';
import {AuthenticationService} from '../../../../core/services/authentication/authentication.service';
import {ErrorMessageService} from '../../../../shared/services/error-message/error-message.service';
import {Contact} from '../../../../model/contact.model';
import {ContactService} from '../../../../shared/services/contact/contact.service';

@Component({
  selector: 'forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
  animations: appAnimations
})
export class ForgotPasswordComponent implements OnInit {

  public forgotPassForm: FormGroup;
  public contact: Contact = new Contact();
  public submitted: Boolean = false;
  public reset: Boolean = false;
  public inProgress: Boolean = false;

  constructor(private errorMsgService: ErrorMessageService,
              private contactService: ContactService,
              private router: Router) {
  }

  get email() {
    return this.forgotPassForm.get('email');
  }

  ngOnInit() {
    AuthenticationService.unsetIdentity();
    this.forgotPassForm = new FormGroup({
      'email': new FormControl(this.contact.id, [
        Validators.required,
        Validators.pattern(/^(\S+@\S+\.\S+)$/)
      ])
    });
  }

  public getErrorMessage(fieldName: string): string {
    return this.errorMsgService.getError(this[fieldName], fieldName);
  }

  public submitForgotPassForm(): void {
    this.submitted = true;
    this.contact = this.prepareFormFields();
    if (this.forgotPassForm.valid) {
      this.setInProgress();
      this.contactService.requestResetPassword(this.contact)
        .subscribe(response => {
            if (response && response.hasOwnProperty('data') &&
              response.data && response.data.hasOwnProperty('reset') && response.data.reset) {
              this.reset = true;
              this.submitted = false;
            }
            this.setNotInProgress();
          },
          err => {
            this.setNotInProgress();
            this.errorMsgService.handleServerErrors(err, this.forgotPassForm);
          });
    }
  }

  public resendLink(): void {
    this.reset = false;
    this.forgotPassForm.enable();
  }

  public cancel(): void {
    this.router.navigate(['/account/login'], {replaceUrl: true});
  }

  private setInProgress(): void {
    this.forgotPassForm.disable();
    this.inProgress = true;
  }

  private setNotInProgress(): void {
    this.inProgress = false;
    this.forgotPassForm.enable();
  }

  private prepareFormFields(): Contact {
    const formModel = this.forgotPassForm.value;

    return {
      id: formModel.email
    };
  }

}
