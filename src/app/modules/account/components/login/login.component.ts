import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {appAnimations} from '../../../../core/animations/animations';
import {AuthenticationService} from '../../../../core/services/authentication/authentication.service';
import {ErrorMessageService} from '../../../../shared/services/error-message/error-message.service';
import {Contact} from '../../../../model/contact.model';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: appAnimations
})
export class LoginComponent implements OnInit {

  public signinForm: FormGroup;
  public contact: Contact = new Contact();
  public submitted: Boolean = false;
  public inProgress: Boolean = false;

  constructor(private errorMessageService: ErrorMessageService,
              private authenticationService: AuthenticationService,
              private router: Router) {
  }

  get email() {
    return this.signinForm.get('email');
  }

  get password() {
    return this.signinForm.get('password');
  }

  ngOnInit() {
    AuthenticationService.unsetIdentity();
    this.signinForm = new FormGroup({
      'email': new FormControl(this.contact.id, [
        Validators.required,
        Validators.pattern(/^(\S+@\S+\.\S+)$/)
      ]),
      'password': new FormControl(this.contact.password, [
        Validators.required
      ])
    });
  }

  public getErrorMessage(fieldName: string): string {
    return this.errorMessageService.getError(this[fieldName], fieldName);
  }

  public submitSigninForm(): void {
    this.submitted = true;
    this.contact = this.prepareFormFields();
    if (this.signinForm.valid) {
      this.setInProgress();
      this.authenticationService.signIn(this.contact.id, this.contact.password)
        .subscribe(response => {
            if (response && response.hasOwnProperty('data') &&
              response.data && response.data.hasOwnProperty('contact') && response.data.contact) {
              if (this.authenticationService.redirectUrl) {
                this.router.navigate([this.authenticationService.redirectUrl], {replaceUrl: true});
                this.authenticationService.redirectUrl = null;
              } else {
                this.router.navigate(['/'], {replaceUrl: true});
              }
            } else {
              this.errorMessageService.openSnackBar(this.errorMessageService.getNonFormError('NO_VALID_RESPONSE'), 5000, 'OK');
              this.setNotInProgress();
            }
            this.submitted = false;
          },
          err => {
            this.setNotInProgress();
            this.errorMessageService.handleServerErrors(err, this.signinForm);
            this.submitted = false;
          });
    }
  }

  public resetPassword(): void {
    this.router.navigate(['/account/forgot-password']);
  }

  private setInProgress(): void {
    this.signinForm.disable();
    this.inProgress = true;
  }

  private setNotInProgress(): void {
    this.inProgress = false;
    this.signinForm.enable();
  }

  private prepareFormFields(): Contact {
    const formModel = this.signinForm.value;

    return {
      id: formModel.email,
      password: formModel.password
    };
  }

}
