import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserDataGuard} from '../../core/guards/user-data/user-data.guard';

export const routes: Routes = [
  {
    path: '',
    canActivate: [UserDataGuard],
    children: [
      {
        path: 'payment-transaction',
        canActivateChild: [UserDataGuard],
        loadChildren: () => import('./payment-transaction/payment-transaction.module').then(mod => mod.PaymentTransactionModule),
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'payment-transaction'}
      },
      {
        path: 'non-payment-transaction',
        canActivateChild: [UserDataGuard],
        loadChildren: () => import('./non-payment-transaction/non-payment-transaction.module').then(mod => mod.NonPaymentTransactionModule),
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'non-payment-transaction'}
      },
      {
        path: 'view-sen-message',
        canActivateChild: [UserDataGuard],
        loadChildren: () => import('./sen-message/sen-message.module').then(mod => mod.SenMessageModule),
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'view-sen-message'}
      },
      {
        path: 'view-admin-message',
        canActivateChild: [UserDataGuard],
        loadChildren: () => import('./admin-message/admin-message.module').then(mod => mod.AdminMessageModule),
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'view-admin-message'}
      }
    ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);

