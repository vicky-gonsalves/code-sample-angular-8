import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ViewPaymentTransactionComponent} from './view-payment-transaction.component';
import {routing} from './view-payment-transaction.routes';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    routing
  ],
  declarations: [ViewPaymentTransactionComponent]
})
export class ViewPaymentTransactionModule {
}
