import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserDataGuard} from '../../../core/guards/user-data/user-data.guard';
import {SearchFormComponent} from './components/search-form/search-form.component';
import {SearchResultComponent} from './components/search-result/search-result.component';
import {NonPaymentTransactionComponent} from './non-payment-transaction.component';

export const routes: Routes = [
  {
    path: '',
    canActivate: [UserDataGuard],
    component: NonPaymentTransactionComponent,
    children: [
      {
        path: '',
        canActivateChild: [UserDataGuard],
        component: SearchFormComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'non-payment-transaction'}
      },
      {
        path: 'search-results',
        canActivateChild: [UserDataGuard],
        component: SearchResultComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'non-payment-transaction-result'}
      }
    ]
  },
  {path: '**', redirectTo: '', pathMatch: 'full'}
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);

