import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NonPaymentTransactionComponent} from './non-payment-transaction.component';
import {SearchFormComponent} from './components/search-form/search-form.component';
import {SearchResultComponent} from './components/search-result/search-result.component';
import {TransactionDetailViewComponent} from './components/transaction-detail-view/transaction-detail-view.component';
import {PaymentLegListComponent} from './components/payment-leg-list/payment-leg-list.component';
import {PaymentXmlViewerComponent} from './components/payment-xml-viewer/payment-xml-viewer.component';
import {routing} from './non-payment-transaction.routes';
import {SharedModule} from '../../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    routing
  ],
  declarations: [
    NonPaymentTransactionComponent,
    SearchFormComponent,
    SearchResultComponent,
    TransactionDetailViewComponent,
    PaymentLegListComponent,
    PaymentXmlViewerComponent
  ],
  entryComponents: [
    TransactionDetailViewComponent,
    PaymentLegListComponent,
    PaymentXmlViewerComponent
  ]
})
export class NonPaymentTransactionModule {
}
