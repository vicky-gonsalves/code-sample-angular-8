import {HttpErrorResponse} from '@angular/common/http';
import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {Router} from '@angular/router';
import moment from 'moment-timezone';
import {Subscription} from 'rxjs';
import {appAnimations} from '../../../../../core/animations/animations';
import {Identity} from '../../../../../core/services/identity/identity';
import {IdentityService} from '../../../../../core/services/identity/identity.service';
import {NavItemInterface} from '../../../../../interface/nav-item.interface';
import {NonPaymentTransactionInterface} from '../../../../../interface/non-payment-transaction.interface';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {NavItemService} from '../../../../../shared/services/nav-item/nav-item.service';
import {NonPaymentTransactionService} from '../../../../../shared/services/non-payment-transaction/non-payment-transaction.service';
import {PageHeaderService} from '../../../../../shared/services/page-header/page-header.service';
import {PageTitleService} from '../../../../../shared/services/page-title/page-title.service';
import {QueryResultStoreNonPaymentService} from '../../../../../shared/services/query-result-store-non-payment/query-result-store-non-payment.service';
import {RefreshService} from '../../../../../shared/services/refresh/refresh.service';
import {UiConfigParameterService} from '../../../../../shared/services/ui-config-parameter/ui-config-parameter.service';
import {PaymentLegListComponent} from '../payment-leg-list/payment-leg-list.component';
import {TransactionDetailViewComponent} from '../transaction-detail-view/transaction-detail-view.component';

@Component({
  selector: 'search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.scss'],
  animations: appAnimations
})
export class SearchResultComponent implements OnInit, OnDestroy {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  public searchTerm: {
    id: string,
    creditor_routing_id: string,
    creditor_account_id: string,
    debtor_routing_id: string,
    debtor_account_id: string,
    org_msg_name: string
    org_instruction_id: string
  };
  public displayedColumns =
    ['actions', 'id', 'transaction_type', 'creation_tmstmp', 'creditor_participant_id', 'debtor_participant_id', 'org_msg_name', 'org_instruction_id', 'creditor_routing_id',
      'creditor_account_id', 'debtor_routing_id', 'debtor_account_id', 'amount', 'accept_reject_code', 'end_to_end_reference', 'reason_code'];
  public dataSource: MatTableDataSource<NonPaymentTransactionInterface>;
  public paginatorOptions = {
    count: 0,
    pageSize: 10,
    pageSizeOptions: [5, 10, 25, 50, 100],
    showFirstLastButtons: true
  };
  public inProgress: Boolean = true;
  public hasPermissionToViewMessageLegs: Boolean = false;
  public hasPermissionToViewTransactions: Boolean = false;
  private sortBy = '-creation_tmstmp';
  private pageSize = 10;
  private componentNavItem: NavItemInterface;
  private nonPaymentTransactionQuery: any;
  private storedResults: any;
  private identity: Identity;
  private _lastPageEvent: PageEvent;
  private _lastOptions: any;
  private refreshSubscription: Subscription;

  constructor(private queryResultStoreNonPaymentService: QueryResultStoreNonPaymentService,
              private router: Router,
              private navItems: NavItemService,
              private errorMessageService: ErrorMessageService,
              private uiConfigParameterService: UiConfigParameterService,
              private dialog: MatDialog,
              private nonPaymentTransactionService: NonPaymentTransactionService,
              private pageTitle: PageTitleService,
              private refreshService: RefreshService) {
    IdentityService.identity.subscribe(contactIdentity => {
      this.identity = contactIdentity;
      if (IdentityService.hasPrivileges(['viewMessageLegs'])) {
        this.hasPermissionToViewMessageLegs = true;
      }
      if (IdentityService.hasPrivileges(['viewTransactions'])) {
        this.hasPermissionToViewTransactions = true;
      }
    });

    this.refreshSubscription = this.refreshService.refreshObservable$.subscribe((res) => {
      if (res.hasOwnProperty('refresh') && res.refresh) {
        this.nonPaymentTransactionQuery = this.queryResultStoreNonPaymentService.getFinalQuery();
        this.fetchNonPaymentTransaction(this._lastPageEvent, this._lastOptions);
      }
    });

    this.searchTerm = {
      id: null,
      creditor_routing_id: null,
      creditor_account_id: null,
      debtor_routing_id: null,
      debtor_account_id: null,
      org_msg_name: null,
      org_instruction_id: null
    };
    const _storedResults = this.queryResultStoreNonPaymentService.getResults();
    this.nonPaymentTransactionQuery = this.queryResultStoreNonPaymentService.getFinalQuery();
    if (!this.nonPaymentTransactionQuery && !_storedResults) {
      this.router.navigate(['/view-payment-transactions/non-payment-transaction']);
    } else {
      this.storedResults = _storedResults.rows.map(d => {
        d.amount = d.amount ? JSON.parse(d.amount).$numberDecimal : null;
        return d;
      });
      this.dataSource = new MatTableDataSource(this.storedResults);
      this.paginatorOptions.count = _storedResults.count;
      this.dataSource.sort = this.sort;
    }
  }

  ngOnInit() {
    // Get Navigation Component Name
    this.componentNavItem = this.navItems.getItemById('non-payment-transaction-result');
    if (this.componentNavItem) {
      // Set Page Title
      this.pageTitle.setTitle(this.componentNavItem.name);
    }

    // Set Page Header Details
    PageHeaderService.updatePageHeader({
      backPath: 'view-payment-transactions/non-payment-transaction',
      refreshButton: true
    });

    // Fetch Payment Related Transaction
    this.fetchNonPaymentTransaction();
  }

  ngOnDestroy() {
    this.refreshSubscription.unsubscribe();
  }

  public sortData(sort: Sort): void {
    const data = this.dataSource;
    if (!sort.active || sort.direction === '') {
      this.sortBy = '-creation_tmstmp';
    } else {
      this.sortBy = sort.direction === 'asc' ? sort.active : '-' + sort.active;
    }
    this.paginator.pageIndex = 0;
    this.fetchNonPaymentTransaction();
  }

  public fetchNonPaymentTransaction(pageEvent?: PageEvent, lastOptions?: any): void {
    if (pageEvent) {
      this.pageSize = pageEvent.pageSize;
      this._lastPageEvent = pageEvent;
    }
    let options = null;
    if (lastOptions) {
      options = lastOptions;
    } else {
      options = {
        limit: (pageEvent ? (pageEvent.pageSize) : this.pageSize ? this.pageSize : this.paginatorOptions.pageSize),
        page: (pageEvent ? (pageEvent.pageIndex + 1) : 1),
        sort: [this.sortBy]
      };
      if (this.searchTerm.id) {
        options['id'] = this.searchTerm.id;
      }
      if (this.searchTerm.creditor_routing_id) {
        options['creditor_routing_id'] = this.searchTerm.creditor_routing_id;
      }
      if (this.searchTerm.creditor_account_id) {
        options['creditor_account_id'] = this.searchTerm.creditor_account_id;
      }
      if (this.searchTerm.debtor_routing_id) {
        options['debtor_routing_id'] = this.searchTerm.debtor_routing_id;
      }
      if (this.searchTerm.debtor_account_id) {
        options['debtor_account_id'] = this.searchTerm.debtor_account_id;
      }
      if (this.searchTerm.org_msg_name) {
        options['org_msg_name'] = this.searchTerm.org_msg_name;
      }
      if (this.searchTerm.org_instruction_id) {
        options['org_instruction_id'] = this.searchTerm.org_instruction_id;
      }
      this._lastOptions = options;
    }
    this.inProgress = true;
    this.nonPaymentTransactionService.getNonPaymentTransactions(options, this.nonPaymentTransactionQuery)
      .subscribe(response => {
          if (response && response.data && response.data.rows && response.data.rows.length) {
            response.data.rows.forEach(row => {
              row.creation_tmstmp = this.convertTimezone(row.creation_tmstmp);
            });
            this.dataSource = new MatTableDataSource(response.data.rows.map(d => {
              d.amount = d.amount ? JSON.parse(d.amount).$numberDecimal : null;
              return d;
            }));
            this.paginatorOptions.count = response.data.count;
            this.dataSource.sort = this.sort;
          } else {
            this.errorMessageService.openSnackBar(this.errorMessageService.getNonFormError('NO_DATA'));
          }
          this.inProgress = false;
        },
        (err: HttpErrorResponse) => {
          this.errorMessageService.handleServerErrors(err);
          this.inProgress = false;
        });
  }

  public applyFilter(searchCriteria: { key: string, value: string }): void {
    this.searchTerm[searchCriteria.key] = searchCriteria.value;
    this.paginator.pageIndex = 0;
    this.fetchNonPaymentTransaction();
  }

  public convertTimezone(date: Date): moment {
    return this.uiConfigParameterService.convertTimezone(date);
  }

  public viewTransactionDetail(nonPaymentTransaction?: NonPaymentTransactionInterface): void {
    const conf: MatDialogConfig = {
      autoFocus: false,
      closeOnNavigation: true,
      panelClass: 'panelClass',
      maxWidth: '1200px',
      data: nonPaymentTransaction
    };
    this.dialog.open(TransactionDetailViewComponent, conf);
  }


  public viewMessageLegDetail(nonPaymentTransaction?: NonPaymentTransactionInterface): void {
    const conf: MatDialogConfig = {
      autoFocus: false,
      closeOnNavigation: true,
      panelClass: 'panelClass',
      maxWidth: '1400px',
      data: nonPaymentTransaction
    };
    this.dialog.open(PaymentLegListComponent, conf);
  }
}
