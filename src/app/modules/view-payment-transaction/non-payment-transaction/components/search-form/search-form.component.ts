import {HttpErrorResponse} from '@angular/common/http';
import {AfterViewInit, Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {PageEvent} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {Router} from '@angular/router';
import * as moment from 'moment-timezone';
import {Observable} from 'rxjs';
import {debounceTime, distinctUntilChanged, startWith, switchMap} from 'rxjs/internal/operators';
import {appAnimations} from '../../../../../core/animations/animations';
import {Identity} from '../../../../../core/services/identity/identity';
import {IdentityService} from '../../../../../core/services/identity/identity.service';
import {NavItemInterface} from '../../../../../interface/nav-item.interface';
import {NonPaymentTransactionInterface} from '../../../../../interface/non-payment-transaction.interface';
import {ParticipantRoutingDetailInterface} from '../../../../../interface/participant-routing-detail.interface';
import {ParticipantInterface} from '../../../../../interface/participant.interface';
import {TransactionTypeInterface} from '../../../../../interface/transaction-type.interface';
import {ParticipantRoutingDetail} from '../../../../../model/participant-routing-detail.model';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {InstructionIdValidatorService} from '../../../../../shared/services/instruction-id-validator/instruction-id-validator.service';
import {NavItemService} from '../../../../../shared/services/nav-item/nav-item.service';
import {NonPaymentTransactionService} from '../../../../../shared/services/non-payment-transaction/non-payment-transaction.service';
import {PageHeaderService} from '../../../../../shared/services/page-header/page-header.service';
import {PageTitleService} from '../../../../../shared/services/page-title/page-title.service';
import {ParticipantRoutingDetailService} from '../../../../../shared/services/participant-routing-detail/participant-routing-detail.service';
import {ParticipantService} from '../../../../../shared/services/participant/participant.service';
import {QueryResultStoreNonPaymentService} from '../../../../../shared/services/query-result-store-non-payment/query-result-store-non-payment.service';
import {QueryResultStoreService} from '../../../../../shared/services/query-result-store/query-result-store.service';
import {TransactionTypeService} from '../../../../../shared/services/transaction-type/transaction-type.service';
import {UiConfigParameterService} from '../../../../../shared/services/ui-config-parameter/ui-config-parameter.service';

@Component({
  selector: 'search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss'],
  animations: appAnimations
})
export class SearchFormComponent implements OnInit, AfterViewInit {
  public nonPaymentTransactionForm: FormGroup;
  public filteredTransactionTypes: Observable<TransactionTypeInterface[]>;
  public filteredCreditorParticipants: Observable<ParticipantInterface[]>;
  public filteredDebtorParticipants: Observable<ParticipantInterface[]>;
  public filteredCreditorParticipantRoutings: Observable<ParticipantRoutingDetail[]>;
  public filteredDebtorParticipantRoutings: Observable<ParticipantRoutingDetail[]>;
  public nonPaymentTransactionQuery: NonPaymentTransactionInterface;
  public inProgress: Boolean = false;
  public submitted: Boolean = false;
  public dataSource: MatTableDataSource<TransactionTypeInterface>;
  public frmTime = moment().startOf('day').toISOString();
  public toTime = moment().startOf('day').add(1, 'days').toISOString();
  public paginatorOptions = {
    count: 0,
    pageSize: 10,
    pageSizeOptions: [5, 10, 25, 50, 100],
    showFirstLastButtons: true
  };
  public isParticipantContact: Boolean;
  private componentNavItem: NavItemInterface;
  private sortBy = '-creation_tmstmp';
  private pageSize = 10;
  private identity: Identity;
  private dateRange: number;
  private storedQuery: any;
  private storedFinalQuery: any;


  constructor(public navItems: NavItemService,
              public pageTitle: PageTitleService,
              private errorMessageService: ErrorMessageService,
              private nonPaymentTransactionService: NonPaymentTransactionService,
              private instructionIdValidatorService: InstructionIdValidatorService,
              private participantService: ParticipantService,
              private uiConfigParameterService: UiConfigParameterService,
              private participantRoutingDetailService: ParticipantRoutingDetailService,
              private transactionTypeService: TransactionTypeService,
              private queryResultStoreService: QueryResultStoreNonPaymentService,
              private queryResultStoreService2: QueryResultStoreService,
              private router: Router) {
    this.dateRange = parseFloat(this.uiConfigParameterService.getLocalConfig('DATE_RANGE'));
    IdentityService.identity.subscribe(contactIdentity => {
      this.identity = contactIdentity;
      this.isParticipantContact = this.identity && this.identity.contactProfile_id && this.identity.contactProfile_id === 'participant';
    });
    this.queryResultStoreService2.setQuery(null);
    this.storedQuery = this.queryResultStoreService.getQuery();
    this.storedFinalQuery = this.queryResultStoreService.getFinalQuery();
    if (this.storedFinalQuery) {
      this.frmTime = this.storedFinalQuery.frmTimestamp;
      this.toTime = this.storedFinalQuery.toTimestamp;
    }
  }

  get transaction_type() {
    return this.nonPaymentTransactionForm.get('transaction_type');
  }

  get instruction_id() {
    return this.nonPaymentTransactionForm.get('instruction_id');
  }

  get transaction_create_frm_date() {
    return this.nonPaymentTransactionForm.get('transaction_create_frm_date');
  }

  get transaction_create_frm_time() {
    return this.nonPaymentTransactionForm.get('transaction_create_frm_time');
  }

  get transaction_create_to_date() {
    return this.nonPaymentTransactionForm.get('transaction_create_to_date');
  }

  get transaction_create_to_time() {
    return this.nonPaymentTransactionForm.get('transaction_create_to_time');
  }

  get role() {
    return this.nonPaymentTransactionForm.get('role');
  }

  get creditor_participant_id() {
    return this.nonPaymentTransactionForm.get('creditor_participant_id');
  }

  get debtor_participant_id() {
    return this.nonPaymentTransactionForm.get('debtor_participant_id');
  }

  get creditor_routing_id() {
    return this.nonPaymentTransactionForm.get('creditor_routing_id');
  }

  get debtor_routing_id() {
    return this.nonPaymentTransactionForm.get('debtor_routing_id');
  }

  get creditor_account_id() {
    return this.nonPaymentTransactionForm.get('creditor_account_id');
  }

  get debtor_account_id() {
    return this.nonPaymentTransactionForm.get('debtor_account_id');
  }

  ngOnInit() {
    this.componentNavItem = this.navItems.getItemById('non-payment-transaction');
    if (this.componentNavItem) {
      this.pageTitle.setTitle(this.componentNavItem.name);
    }

    // Set Page Header Details
    PageHeaderService.updatePageHeader({
      showImage: true,
      icon: 'compare_arrows'
    });
    this.setupForm();
  }

  ngAfterViewInit() {
    this.initAutocomplete();
    setTimeout(() => {
      if (!this.storedQuery) {
        if (this.isParticipantContact) {
          const initialRole = 'Creditor';
          this.nonPaymentTransactionForm.patchValue({
            role: initialRole,
            transaction_create_frm_date: moment().startOf('day').format(),
            transaction_create_to_date: moment().startOf('day').add(1, 'days').format(),
            transaction_create_frm_time: this.frmTime,
            transaction_create_to_time: moment(this.toTime).format()
          });
          this.roleChanged({value: initialRole});
        } else {
          this.nonPaymentTransactionForm.patchValue({
            transaction_create_frm_date: moment().startOf('day').format(),
            transaction_create_to_date: moment().startOf('day').add(1, 'days').format(),
            transaction_create_frm_time: this.frmTime,
            transaction_create_to_time: moment(this.toTime).format()
          });
        }
      } else {
        this.nonPaymentTransactionForm.patchValue(this.storedQuery);
      }
    }, 10);
  }

  public transactionTypeDisplayFn(transactionType?: TransactionTypeInterface): string | undefined {
    return transactionType ? (transactionType.id) : undefined;
  }

  public participantDisplayFn(participantRoutingDetailOwner?: ParticipantInterface): string | undefined {
    return participantRoutingDetailOwner ? (participantRoutingDetailOwner.id) : undefined;
  }

  public participantRoutingDetailDisplayFn(participantRoutingDetail?: ParticipantRoutingDetailInterface): string | undefined {
    return participantRoutingDetail ? (participantRoutingDetail.id) : undefined;
  }

  public getErrorMessage(fieldName: string): string {
    if (fieldName === 'transaction_create_to_time' || fieldName === 'transaction_create_to_date' && this[fieldName].errors.hasOwnProperty('dateRange') && this[fieldName].errors['dateRange']) {
      return 'Transaction creation date/time range should be within ' + this.dateRange + ' hours';
    }
    return this.errorMessageService.getError(this[fieldName], fieldName);
  }

  public submitNonPaymentTransactionForm(pageEvent?: PageEvent): void {
    if (pageEvent) {
      this.pageSize = pageEvent.pageSize;
    }
    const options = {
      limit: (pageEvent ? (pageEvent.pageSize) : this.pageSize ? this.pageSize : this.paginatorOptions.pageSize),
      page: (pageEvent ? (pageEvent.pageIndex + 1) : 1),
      sort: [this.sortBy]
    };
    this.submitted = true;
    this.validateDateRange();
    this.nonPaymentTransactionQuery = this.prepareFormFields();
    if (this.nonPaymentTransactionForm.valid) {
      this.setInProgress();
      this.nonPaymentTransactionService.getNonPaymentTransactions(options, this.nonPaymentTransactionQuery)
        .subscribe(response => {
            if (response && response.hasOwnProperty('data') && response.data) {
              if (response.data.rows && response.data.rows.length) {
                response.data.rows.forEach(row => {
                  row.creation_tmstmp = this.convertTimezone(row.creation_tmstmp);
                });
                this.queryResultStoreService.setResults(response.data);
                this.router.navigate(['/view-payment-transactions/non-payment-transaction/search-results']);
              } else {
                this.errorMessageService.openSnackBar('No search results found', 5000);
              }
            }
            this.setNotInProgress();
          },
          (err: HttpErrorResponse) => {
            this.setNotInProgress();
            this.errorMessageService.handleServerErrors(err, this.nonPaymentTransactionForm);
          });
    }
  }

  public roleChanged(role): void {
    if (role.value === 'Creditor') {
      this.nonPaymentTransactionForm.patchValue({
        creditor_participant_id: {id: this.identity.participant_id},
        debtor_participant_id: {id: null}
      });
    } else {
      this.nonPaymentTransactionForm.patchValue({
        creditor_participant_id: {id: null},
        debtor_participant_id: {id: this.identity.participant_id}
      });
    }
  }

  public frmDateChanged(date) {
    const changedDate = moment(date.value);
    this.frmTime = moment(this.transaction_create_frm_time.value).date(changedDate.date()).month(changedDate.month()).year(changedDate.year());
    this.nonPaymentTransactionForm.patchValue({
      transaction_create_frm_time: this.frmTime
    });
    this.validateDateRange();
  }

  public toDateChanged(date) {
    const changedDate = moment(date.value);
    this.toTime = moment(this.transaction_create_to_time.value).date(changedDate.date()).month(changedDate.month()).year(changedDate.year());
    this.nonPaymentTransactionForm.patchValue({
      transaction_create_to_time: this.toTime
    });
    this.validateDateRange();
  }

  public frmTimeChanged() {
    const frmDate = moment(this.transaction_create_frm_date.value);
    this.frmTime = moment(this.transaction_create_frm_time.value).date(frmDate.date()).month(frmDate.month()).year(frmDate.year());
    this.validateDateRange();
  }

  public toTimeChanged() {
    const toDate = moment(this.transaction_create_to_date.value);
    this.toTime = moment(this.transaction_create_to_time.value).date(toDate.date()).month(toDate.month()).year(toDate.year());
    this.validateDateRange();
  }

  public validateDateRange() {
    setTimeout(() => {
      const formModel = this.nonPaymentTransactionForm.value;
      this.dateRange = parseFloat(this.uiConfigParameterService.getLocalConfig('DATE_RANGE'));
      if (formModel.transaction_create_frm_date && formModel.transaction_create_to_date &&
        this.toTime && moment(this.frmTime).isValid() && moment(this.toTime).isValid()) {
        if (moment(this.toTime).diff(this.frmTime, 'hours') > this.dateRange) {
          this.transaction_create_to_date.setErrors({'dateRange': true});
          this.transaction_create_to_time.setErrors({'dateRange': true});
        } else if (moment(this.toTime).diff(this.frmTime, 'seconds') < 0) {
          this.transaction_create_frm_date.setErrors({'dateRangeLower': true});
          this.transaction_create_frm_time.setErrors({'dateRangeLower': true});
        } else {
          this.transaction_create_frm_date.setErrors({'dateRangeLower': null});
          this.transaction_create_frm_time.setErrors({'dateRangeLower': null});
          this.transaction_create_to_date.setErrors({'dateRange': null});
          this.transaction_create_to_time.setErrors({'dateRange': null});
          this.transaction_create_frm_date.clearValidators();
          this.transaction_create_frm_time.clearValidators();
          this.transaction_create_to_date.clearValidators();
          this.transaction_create_to_time.clearValidators();
          this.transaction_create_frm_date.updateValueAndValidity();
          this.transaction_create_frm_time.updateValueAndValidity();
          this.transaction_create_to_date.updateValueAndValidity();
          this.transaction_create_to_time.updateValueAndValidity();
        }
      }
    });
  }

  public convertTimezone(date: Date): moment {
    return this.uiConfigParameterService.convertTimezone(date);
  }

  public notHasInstructionId(): Boolean {
    return (!this.nonPaymentTransactionForm.value.instruction_id ||
      (this.nonPaymentTransactionForm.value.instruction_id && !this.nonPaymentTransactionForm.value.instruction_id.length));
  }

  public resetForm(): void {
    this.queryResultStoreService.setFinalQuery(null);
    setTimeout(() => {
      if (this.isParticipantContact) {
        const initialRole = 'Creditor';
        this.nonPaymentTransactionForm.patchValue({
          role: initialRole,
          transaction_create_frm_date: moment().startOf('day').format(),
          transaction_create_to_date: moment().startOf('day').add(1, 'days').format(),
          transaction_create_frm_time: moment().startOf('day').format(),
          transaction_create_to_time: moment().startOf('day').add(1, 'days').format()
        });
        this.roleChanged({value: initialRole});
      } else {
        this.nonPaymentTransactionForm.patchValue({
          transaction_create_frm_date: moment().startOf('day').format(),
          transaction_create_to_date: moment().startOf('day').add(1, 'days').format(),
          transaction_create_frm_time: moment().startOf('day').format(),
          transaction_create_to_time: moment().startOf('day').add(1, 'days').format()
        });
      }
    });
  }

  private initAutocomplete(): void {
    this.filteredTransactionTypes = this.transaction_type.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        startWith(null),
        switchMap(
          (term: string | any) =>
            (typeof term === 'string' || term === '' || term === null) ?
              this.transactionTypeService.getTransactionTypesForAutocomplete({
                term: term,
                limit: 10
              }, 'NONPAYMENT')
              : []
        )
      );

    this.filteredCreditorParticipants = this.creditor_participant_id.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        startWith(null),
        switchMap(
          (term: string | any) =>
            (typeof term === 'string' || term === '' || term === null) ?
              this.participantService.getParticipantsForAutocomplete({
                term: term,
                limit: 15
              }) : []
        )
      );
    this.filteredDebtorParticipants = this.debtor_participant_id.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        startWith(null),
        switchMap(
          (term: string | any) =>
            (typeof term === 'string' || term === '' || term === null) ?
              this.participantService.getParticipantsForAutocomplete({
                term: term,
                limit: 15
              }) : []
        )
      );
  }

  private setInProgress(): void {
    // this.nonPaymentTransactionForm.disable();
    this.inProgress = true;
  }

  private setNotInProgress(): void {
    this.inProgress = false;
    // this.nonPaymentTransactionForm.enable();
  }

  private setupForm(): void {
    this.nonPaymentTransactionForm = new FormGroup({
      'transaction_type': new FormControl('', [
        Validators.required
      ]),
      'instruction_id': new FormControl('', [
        Validators.minLength(8),
        this.instructionIdValidatorService.validate()
      ]),
      'transaction_create_frm_date': new FormControl('', [
        Validators.required
      ]),
      'transaction_create_frm_time': new FormControl('', [
        Validators.required
      ]),
      'transaction_create_to_date': new FormControl('', [
        Validators.required
      ]),
      'transaction_create_to_time': new FormControl('', [
        Validators.required
      ]),
      'role': new FormControl('', []),
      'creditor_participant_id': new FormControl('', []),
      'debtor_participant_id': new FormControl('', []),
      'creditor_routing_id': new FormControl('', []),
      'creditor_account_id': new FormControl('', [
        Validators.pattern(/^[A-Za-z0-9-]+$/),
        Validators.minLength(1),
        Validators.maxLength(35),
      ]),
      'debtor_routing_id': new FormControl('', []),
      'debtor_account_id': new FormControl('', [
        Validators.pattern(/^[A-Za-z0-9-]+$/),
        Validators.minLength(1),
        Validators.maxLength(35),
      ]),
    });
    this.initExtraValidators();
  }

  private initExtraValidators(): void {
    this.creditor_participant_id.valueChanges.subscribe(data => this.onCreditorParticipantChanged(data));
    this.debtor_participant_id.valueChanges.subscribe(data => this.onDebtorParticipantChanged(data));
    this.instruction_id.valueChanges.subscribe(data => this.onInstructionIdChanged(data));
  }

  private onInstructionIdChanged(value: string): void {
    if (value) {
      this.nonPaymentTransactionForm.controls['transaction_create_frm_date'].setValidators(null);
      this.nonPaymentTransactionForm.controls['transaction_create_frm_time'].setValidators(null);
      this.nonPaymentTransactionForm.controls['transaction_create_to_date'].setValidators(null);
      this.nonPaymentTransactionForm.controls['transaction_create_to_time'].setValidators(null);
      this.nonPaymentTransactionForm.controls['debtor_participant_id'].setValidators(null);
      this.nonPaymentTransactionForm.controls['creditor_participant_id'].setValidators(null);
      if (this.isParticipantContact) {
        this.nonPaymentTransactionForm.controls['role'].setValidators(null);
        this.nonPaymentTransactionForm.controls['role'].updateValueAndValidity();
      }
      this.nonPaymentTransactionForm.controls['transaction_create_frm_date'].updateValueAndValidity();
      this.nonPaymentTransactionForm.controls['transaction_create_frm_time'].updateValueAndValidity();
      this.nonPaymentTransactionForm.controls['transaction_create_to_date'].updateValueAndValidity();
      this.nonPaymentTransactionForm.controls['transaction_create_to_time'].updateValueAndValidity();
      this.nonPaymentTransactionForm.controls['debtor_participant_id'].updateValueAndValidity();
      this.nonPaymentTransactionForm.controls['creditor_participant_id'].updateValueAndValidity();
    } else {
      this.nonPaymentTransactionForm.controls['transaction_create_frm_date'].setValidators([Validators.required]);
      this.nonPaymentTransactionForm.controls['transaction_create_frm_time'].setValidators([Validators.required]);
      this.nonPaymentTransactionForm.controls['transaction_create_to_date'].setValidators([Validators.required]);
      this.nonPaymentTransactionForm.controls['transaction_create_to_time'].setValidators([Validators.required]);
      this.nonPaymentTransactionForm.controls['debtor_participant_id'].setValidators([]);
      this.nonPaymentTransactionForm.controls['creditor_participant_id'].setValidators([]);
      if (this.isParticipantContact) {
        this.nonPaymentTransactionForm.controls['role'].setValidators([Validators.required]);
        this.nonPaymentTransactionForm.controls['role'].updateValueAndValidity();
      }
      this.nonPaymentTransactionForm.controls['transaction_create_frm_date'].updateValueAndValidity();
      this.nonPaymentTransactionForm.controls['transaction_create_frm_time'].updateValueAndValidity();
      this.nonPaymentTransactionForm.controls['transaction_create_to_date'].updateValueAndValidity();
      this.nonPaymentTransactionForm.controls['transaction_create_to_time'].updateValueAndValidity();
      this.nonPaymentTransactionForm.controls['debtor_participant_id'].updateValueAndValidity();
      this.nonPaymentTransactionForm.controls['creditor_participant_id'].updateValueAndValidity();
    }
  }

  private onCreditorParticipantChanged(value: any): void {
    this.filteredCreditorParticipantRoutings = null;
    this.creditor_routing_id.setValue(null);
    if (value && value.id) {
      this.filteredCreditorParticipantRoutings = this.creditor_routing_id.valueChanges
        .pipe(
          debounceTime(100),
          distinctUntilChanged(),
          startWith(null),
          switchMap(
            (term: string | any) =>
              (typeof term === 'string' || term === '' || term === null) ?
                this.participantRoutingDetailService.getParticipantsRoutingDetailsForAutocomplete({
                    term: term,
                    limit: 10
                  },
                  this.nonPaymentTransactionForm.value.creditor_participant_id &&
                  this.nonPaymentTransactionForm.value.creditor_participant_id.id ?
                    this.nonPaymentTransactionForm.value.creditor_participant_id.id : null) : []
          )
        );
    }
  }

  private onDebtorParticipantChanged(value: any) {
    this.filteredDebtorParticipantRoutings = null;
    this.debtor_routing_id.setValue(null);
    if (value && value.id) {
      this.filteredDebtorParticipantRoutings = this.debtor_routing_id.valueChanges
        .pipe(
          debounceTime(100),
          distinctUntilChanged(),
          startWith(null),
          switchMap(
            (term: string | any) =>
              (typeof term === 'string' || term === '' || term === null) ?
                this.participantRoutingDetailService.getParticipantsRoutingDetailsForAutocomplete({
                    term: term,
                    limit: 10
                  },
                  this.nonPaymentTransactionForm.value.debtor_participant_id &&
                  this.nonPaymentTransactionForm.value.debtor_participant_id.id ?
                    this.nonPaymentTransactionForm.value.debtor_participant_id.id : null) : []
          )
        );
    }
  }

  private prepareFormFields(): NonPaymentTransactionInterface {
    const formModel = this.nonPaymentTransactionForm.value;
    this.queryResultStoreService.setQuery(formModel);
    if (formModel.transaction_type && !formModel.transaction_type.hasOwnProperty('id')) {
      this.transaction_type.setValue(null);
      this.transaction_type.setErrors({'invalidSelection': true});
    }
    if (formModel.creditor_participant_id && !formModel.creditor_participant_id.hasOwnProperty('id')) {
      this.creditor_participant_id.setValue(null);
      this.creditor_participant_id.setErrors({'invalidSelection': true});
    }
    if (formModel.debtor_participant_id && !formModel.debtor_participant_id.hasOwnProperty('id')) {
      this.debtor_participant_id.setValue(null);
      this.debtor_participant_id.setErrors({'invalidSelection': true});
    }
    if (formModel.creditor_routing_id && !formModel.creditor_routing_id.hasOwnProperty('id')) {
      this.creditor_routing_id.setValue(null);
      this.creditor_routing_id.setErrors({'invalidSelection': true});
    }
    if (formModel.debtor_routing_id && !formModel.debtor_routing_id.hasOwnProperty('id')) {
      this.debtor_routing_id.setValue(null);
      this.debtor_routing_id.setErrors({'invalidSelection': true});
    }
    const query: NonPaymentTransactionInterface = {
      transaction_type: formModel.transaction_type && formModel.transaction_type.id ? formModel.transaction_type.id : null
    };
    if (this.notHasInstructionId()) {
      if (formModel.creditor_participant_id && formModel.creditor_participant_id.id) {
        query.creditor_participant_id = formModel.creditor_participant_id.id;
      }
      if (formModel.debtor_participant_id && formModel.debtor_participant_id.id) {
        query.debtor_participant_id = formModel.debtor_participant_id.id;
      }
      if (formModel.creditor_routing_id && formModel.creditor_routing_id.id) {
        query.creditor_routing_id = formModel.creditor_routing_id.id;
      }
      if (formModel.debtor_routing_id && formModel.debtor_routing_id.id) {
        query.debtor_routing_id = formModel.debtor_routing_id.id;
      }
      if (formModel.creditor_account_id) {
        query.creditor_account_id = formModel.creditor_account_id;
      }
      if (formModel.debtor_account_id) {
        query.debtor_account_id = formModel.debtor_account_id;
      }
    } else {
      if (formModel.instruction_id) {
        query.id = formModel.instruction_id;
      }
    }
    if (formModel.instruction_id && formModel.instruction_id.length && formModel.instruction_id.length >= 8) {
      let crDateTime;
      if (formModel.instruction_id.match(/^[\d]/)) {
        crDateTime = moment(formModel.instruction_id.substring(0, 8), 'YYYYMMDD', true);
      } else if (formModel.instruction_id.match(/^M\d/)) {
        crDateTime = moment(formModel.instruction_id.substring(1, 9), 'YYYYMMDD', true);
      }
      if (moment(new Date(crDateTime)).isValid()) {
        query.frmTimestamp = moment(crDateTime.utcOffset('+00:00', true).startOf('day')).subtract(1, 'days').toISOString();
        query.toTimestamp = moment(crDateTime.utcOffset('+00:00', true).startOf('day')).add(1, 'days').toISOString();
        this.instruction_id.clearValidators();
        this.instruction_id.updateValueAndValidity();
      } else {
        this.instruction_id.setErrors({invalid: true});
      }
    } else if ((!formModel.instruction_id || (formModel.instruction_id && !formModel.instruction_id.length)) &&
      this.transaction_create_frm_date.valid && this.transaction_create_to_date.valid &&
      this.transaction_create_frm_time.valid && this.transaction_create_to_time.valid) {
      query.frmTimestamp = moment(this.transaction_create_frm_time.value).format('YYYY-MM-DDTHH:mm:ss');
      query.toTimestamp = moment(this.transaction_create_to_time.value).format('YYYY-MM-DDTHH:mm:ss');
    } else if (formModel.instruction_id && formModel.instruction_id.length && !query.frmTimestamp && !query.toTimestamp) {
      this.instruction_id.setErrors({'format': true});
    }
    this.queryResultStoreService.setFinalQuery(query);
    return query;
  }
}
