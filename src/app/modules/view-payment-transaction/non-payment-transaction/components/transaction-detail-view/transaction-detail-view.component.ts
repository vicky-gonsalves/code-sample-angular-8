import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogConfig} from '@angular/material/dialog';
import moment from 'moment-timezone';
import {Identity} from '../../../../../core/services/identity/identity';
import {IdentityService} from '../../../../../core/services/identity/identity.service';
import {NonPaymentTransactionInterface} from '../../../../../interface/non-payment-transaction.interface';
import {UiConfigParameterService} from '../../../../../shared/services/ui-config-parameter/ui-config-parameter.service';
import {PaymentLegListComponent} from '../payment-leg-list/payment-leg-list.component';

@Component({
  selector: 'transaction-detail-view',
  templateUrl: './transaction-detail-view.component.html',
  styleUrls: ['./transaction-detail-view.component.scss']
})
export class TransactionDetailViewComponent implements OnInit {
  public nonPaymentTransaction: NonPaymentTransactionInterface;
  public identity: Identity;
  public hasPermissionToViewMessageLegs: Boolean = true;
  public inProgress: Boolean = false;

  constructor(@Inject(MAT_DIALOG_DATA) private data: any,
              private dialog: MatDialog,
              private uiConfigParameterService: UiConfigParameterService) {
    IdentityService.identity.subscribe(contactIdentity => {
      this.identity = contactIdentity;
      this.hasPermissionToViewMessageLegs = IdentityService.hasPrivileges(['viewMessageLegs']);
    });
    this.nonPaymentTransaction = data;
  }

  ngOnInit() {
  }

  public convertTimezone(date: Date): moment {
    return this.uiConfigParameterService.convertTimezone(date);
  }

  public viewMessageLegDetail(): void {
    const conf: MatDialogConfig = {
      autoFocus: false,
      closeOnNavigation: true,
      panelClass: 'panelClass',
      maxWidth: '1400px',
      data: this.nonPaymentTransaction
    };
    this.dialog.open(PaymentLegListComponent, conf);
  }
}
