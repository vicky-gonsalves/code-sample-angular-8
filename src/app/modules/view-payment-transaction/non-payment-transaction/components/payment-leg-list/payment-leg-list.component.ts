import {Component, Inject, Input, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogConfig, MatDialogRef} from '@angular/material/dialog';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import moment from 'moment-timezone';
import {ClhNonPaymentLegInterface} from '../../../../../interface/clh-non-payment-leg.interface';
import {ClhNonPaymentLegDataService} from '../../../../../shared/services/clh-non-payment-leg-data/clh-non-payment-leg-data.service';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {UiConfigParameterService} from '../../../../../shared/services/ui-config-parameter/ui-config-parameter.service';
import {PaymentXmlViewerComponent} from '../payment-xml-viewer/payment-xml-viewer.component';

@Component({
  selector: 'payment-leg-list',
  templateUrl: './payment-leg-list.component.html',
  styleUrls: ['./payment-leg-list.component.scss']
})
export class PaymentLegListComponent implements OnInit {
  @Input() msgType: string;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  public displayedColumns =
    ['id', 'message_identifier', 'leg_name', 'connection_id', 'msg_set_version', 'send_receive_tmstmp', 'leg_xml_data', 'actions'];
  public dataSource: MatTableDataSource<ClhNonPaymentLegInterface>;
  public paginatorOptions = {
    count: 0,
    pageSize: 10,
    pageSizeOptions: [5, 10, 25, 50, 100],
    showFirstLastButtons: true
  };
  public inProgress: Boolean = true;
  public pageEvent: any;
  private sortBy = 'send_receive_tmstmp';
  private pageSize = 10;

  constructor(private dialogRef: MatDialogRef<PaymentLegListComponent>,
              @Inject(MAT_DIALOG_DATA) private data: any,
              private clhNonPaymentLegDataService: ClhNonPaymentLegDataService,
              private uiConfigParameterService: UiConfigParameterService,
              private dialog: MatDialog,
              private errorMsgService: ErrorMessageService) {
  }

  ngOnInit() {
    // Fetch Payment Leg Listing
    this.fetchNonPaymentLegListing();
  }

  public sortData(sort: Sort): void {
    const data = this.dataSource;
    if (!sort.active || sort.direction === '') {
      this.sortBy = 'send_receive_tmstmp';
    } else {
      this.sortBy = sort.direction === 'asc' ? sort.active : '-' + sort.active;
    }
    this.paginator.pageIndex = 0;
    this.fetchNonPaymentLegListing();
  }

  public fetchNonPaymentLegListing(_pageEvent?: PageEvent): void {
    if (_pageEvent) {
      this.pageEvent = _pageEvent;
      this.pageSize = _pageEvent.pageSize;
    }
    const options = {
      limit: (this.pageEvent ? (this.pageEvent.pageSize) : this.pageSize ? this.pageSize : this.paginatorOptions.pageSize),
      page: (this.pageEvent ? (this.pageEvent.pageIndex + 1) : 1),
      sort: [this.sortBy]
    };
    if (this.data.transaction_type) {
      options['transaction_type'] = this.data.transaction_type;
    }
    this.inProgress = true;
    this.clhNonPaymentLegDataService.getClhNonPaymentLegData(options, this.data.id)
      .subscribe(response => {
          if (response && response.data && response.data.rows && response.data.rows.length) {
            response.data.rows.forEach(row => {
              row.send_receive_tmstmp = this.convertTimezone(row.send_receive_tmstmp);
            });
            this.dataSource = new MatTableDataSource(response.data.rows);
            this.paginatorOptions.count = response.data.count;
            this.dataSource.sort = this.sort;
          } else {
            this.errorMsgService.openSnackBar(this.errorMsgService.getNonFormError('NO_DATA'));
          }
          this.inProgress = false;
        },
        err => {
          this.errorMsgService.handleServerErrors(err);
          this.inProgress = false;
        });
  }

  public convertTimezone(date: Date): moment {
    return this.uiConfigParameterService.convertTimezone(date);
  }

  public viewXmlData(data: ClhNonPaymentLegInterface): void {
    const conf: MatDialogConfig = {
      autoFocus: false,
      closeOnNavigation: true,
      panelClass: 'panelClass',
      maxWidth: '1200px',
      data: {xmlData: data.leg_xml_data}
    };
    this.dialog.open(PaymentXmlViewerComponent, conf);
  }
}
