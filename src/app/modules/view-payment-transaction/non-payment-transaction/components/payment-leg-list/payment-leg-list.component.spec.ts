import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PaymentLegListComponent} from './payment-leg-list.component';

describe('NonPaymentLegListComponent', () => {
  let component: PaymentLegListComponent;
  let fixture: ComponentFixture<PaymentLegListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PaymentLegListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentLegListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
