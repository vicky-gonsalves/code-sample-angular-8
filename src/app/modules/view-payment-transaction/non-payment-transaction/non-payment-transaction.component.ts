import {Component, OnDestroy, OnInit} from '@angular/core';
import {appAnimations} from '../../../core/animations/animations';
import {QueryResultStoreNonPaymentService} from '../../../shared/services/query-result-store-non-payment/query-result-store-non-payment.service';

@Component({
  selector: 'non-payment-transaction',
  templateUrl: './non-payment-transaction.component.html',
  styleUrls: ['./non-payment-transaction.component.scss'],
  animations: appAnimations
})
export class NonPaymentTransactionComponent implements OnInit, OnDestroy {

  constructor(private queryResultStoreNonPaymentService: QueryResultStoreNonPaymentService) {
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.queryResultStoreNonPaymentService.setQuery(null);
    this.queryResultStoreNonPaymentService.setFinalQuery(null);
    this.queryResultStoreNonPaymentService.setResults(null);
  }

}
