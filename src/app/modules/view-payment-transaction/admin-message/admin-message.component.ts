import {Component, OnDestroy, OnInit} from '@angular/core';
import {QueryResultStoreAdminMessageService} from '../../../shared/services/query-result-store-admin-message/query-result-store-admin-message.service';

@Component({
  selector: 'admin-message',
  templateUrl: './admin-message.component.html',
  styleUrls: ['./admin-message.component.scss']
})
export class AdminMessageComponent implements OnInit, OnDestroy {

  constructor(private queryResultStoreAdminMessageService: QueryResultStoreAdminMessageService) {
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.queryResultStoreAdminMessageService.setQuery(null);
    this.queryResultStoreAdminMessageService.setFinalQuery(null);
    this.queryResultStoreAdminMessageService.setResults(null);
  }

}
