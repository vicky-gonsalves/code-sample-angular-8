import {HttpErrorResponse} from '@angular/common/http';
import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {Router} from '@angular/router';
import moment from 'moment-timezone';
import {Subscription} from 'rxjs';
import {appAnimations} from '../../../../../core/animations/animations';
import {Identity} from '../../../../../core/services/identity/identity';
import {IdentityService} from '../../../../../core/services/identity/identity.service';
import {AdminMessageInterface} from '../../../../../interface/admin-message.interface';
import {NavItemInterface} from '../../../../../interface/nav-item.interface';
import {XmlViewerComponent} from '../../../../../shared/components/xml-viewer/xml-viewer.component';
import {AdminMessageService} from '../../../../../shared/services/admin-message/admin-message.service';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {NavItemService} from '../../../../../shared/services/nav-item/nav-item.service';
import {PageHeaderService} from '../../../../../shared/services/page-header/page-header.service';
import {PageTitleService} from '../../../../../shared/services/page-title/page-title.service';
import {QueryResultStoreAdminMessageService} from '../../../../../shared/services/query-result-store-admin-message/query-result-store-admin-message.service';
import {RefreshService} from '../../../../../shared/services/refresh/refresh.service';
import {UiConfigParameterService} from '../../../../../shared/services/ui-config-parameter/ui-config-parameter.service';

@Component({
  selector: 'search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.scss'],
  animations: appAnimations
})
export class SearchResultComponent implements OnInit, OnDestroy {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  public searchTerm: {
    message_type: string,
    participant_id: string
  };
  public displayedColumns =
    ['actions', 'id', 'creation_tmstmp', 'message_type', 'instruction_id', 'res_msg_id', 'status', 'reason_code', 'participant_id'];
  public dataSource: MatTableDataSource<AdminMessageInterface>;
  public paginatorOptions = {
    count: 0,
    pageSize: 10,
    pageSizeOptions: [5, 10, 25, 50, 100],
    showFirstLastButtons: true
  };
  public inProgress: Boolean = true;
  private sortBy = '-creation_tmstmp';
  private pageSize = 10;
  private componentNavItem: NavItemInterface;
  private adminMessageQuery: any;
  private storedResults: any;
  private identity: Identity;
  private _lastPageEvent: PageEvent;
  private _lastOptions: any;
  private refreshSubscription: Subscription;

  constructor(private queryResultStoreAdminMessageService: QueryResultStoreAdminMessageService,
              private router: Router,
              private navItems: NavItemService,
              private errorMessageService: ErrorMessageService,
              private uiConfigParameterService: UiConfigParameterService,
              private dialog: MatDialog,
              private adminMessageService: AdminMessageService,
              private pageTitle: PageTitleService,
              private refreshService: RefreshService) {
    this.refreshSubscription = this.refreshService.refreshObservable$.subscribe((res) => {
      if (res.hasOwnProperty('refresh') && res.refresh) {
        this.adminMessageQuery = this.queryResultStoreAdminMessageService.getFinalQuery();
        this.fetchAdminMessage(this._lastPageEvent, this._lastOptions);
      }
    });

    IdentityService.identity.subscribe(contactIdentity => {
      this.identity = contactIdentity;
    });
    this.searchTerm = {
      message_type: null,
      participant_id: null
    };
    const _storedResults = this.queryResultStoreAdminMessageService.getResults();
    this.adminMessageQuery = this.queryResultStoreAdminMessageService.getFinalQuery();
    if (!this.adminMessageQuery && !_storedResults) {
      this.router.navigate(['/view-payment-transactions/view-admin-message']);
    } else {
      this.storedResults = _storedResults.rows;
      this.dataSource = new MatTableDataSource(this.storedResults);
      this.paginatorOptions.count = _storedResults.count;
      this.dataSource.sort = this.sort;
    }
  }

  ngOnInit() {
    // Get Navigation Component Name
    this.componentNavItem = this.navItems.getItemById('admin-message-result');
    if (this.componentNavItem) {
      // Set Page Title
      this.pageTitle.setTitle(this.componentNavItem.name);
    }

    // Set Page Header Details
    PageHeaderService.updatePageHeader({
      backPath: 'view-payment-transactions/view-admin-message',
      refreshButton: true
    });
  }

  ngOnDestroy() {
    this.refreshSubscription.unsubscribe();
  }

  public sortData(sort: Sort): void {
    const data = this.dataSource;
    if (!sort.active || sort.direction === '') {
      this.sortBy = '-creation_tmstmp';
    } else {
      this.sortBy = sort.direction === 'asc' ? sort.active : '-' + sort.active;
    }
    this.paginator.pageIndex = 0;
    this.fetchAdminMessage();
  }

  public fetchAdminMessage(pageEvent?: PageEvent, lastOptions?: any): void {
    if (pageEvent) {
      this.pageSize = pageEvent.pageSize;
      this._lastPageEvent = pageEvent;
    }
    let options = null;
    if (lastOptions) {
      options = lastOptions;
    } else {
      options = {
        limit: (pageEvent ? (pageEvent.pageSize) : this.pageSize ? this.pageSize : this.paginatorOptions.pageSize),
        page: (pageEvent ? (pageEvent.pageIndex + 1) : 1),
        sort: [this.sortBy]
      };
      if (this.searchTerm.message_type) {
        options['message_type'] = this.searchTerm.message_type;
      }
      if (this.searchTerm.participant_id) {
        options['participant_id'] = this.searchTerm.participant_id;
      }
      this._lastOptions = options;
    }
    this.inProgress = true;
    this.adminMessageService.getAdminMessage(options, this.adminMessageQuery)
      .subscribe(response => {
          if (response && response.data && response.data.rows && response.data.rows.length) {
            response.data.rows.forEach(row => {
              row.creation_tmstmp = this.convertTimezone(row.creation_tmstmp);
            });
            this.dataSource = response.data.rows;
            this.paginatorOptions.count = response.data.count;
            this.dataSource.sort = this.sort;
          } else {
            this.errorMessageService.openSnackBar(this.errorMessageService.getNonFormError('NO_DATA'));
          }
          this.inProgress = false;
        },
        (err: HttpErrorResponse) => {
          this.errorMessageService.handleServerErrors(err);
          this.inProgress = false;
        });
  }

  public applyFilter(searchCriteria: { key: string, value: string }): void {
    this.searchTerm[searchCriteria.key] = searchCriteria.value;
    this.paginator.pageIndex = 0;
    this.fetchAdminMessage();
  }

  public convertTimezone(date: Date): moment {
    return this.uiConfigParameterService.convertTimezone(date);
  }

  public viewRequestMessage(adminMessage?: AdminMessageInterface): void {
    const conf: MatDialogConfig = {
      autoFocus: false,
      closeOnNavigation: true,
      panelClass: 'panelClass',
      maxWidth: '1200px',
      data: {xmlData: adminMessage.request_message}
    };
    this.dialog.open(XmlViewerComponent, conf);
  }

  public viewResponseMessage(adminMessage?: AdminMessageInterface): void {
    const conf: MatDialogConfig = {
      autoFocus: false,
      closeOnNavigation: true,
      panelClass: 'panelClass',
      maxWidth: '1200px',
      data: {xmlData: adminMessage.response_message}
    };
    this.dialog.open(XmlViewerComponent, conf);
  }
}
