import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdminMessageComponent} from './admin-message.component';
import {SearchFormComponent} from './components/search-form/search-form.component';
import {SearchResultComponent} from './components/search-result/search-result.component';
import {SharedModule} from '../../../shared/shared.module';
import {routing} from './admin-message.routes';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    routing
  ],
  declarations: [AdminMessageComponent, SearchFormComponent, SearchResultComponent]
})
export class AdminMessageModule {
}
