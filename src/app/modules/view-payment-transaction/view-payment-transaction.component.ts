import {Component, OnInit} from '@angular/core';
import {appAnimations} from '../../core/animations/animations';

@Component({
  selector: 'view-payment-transaction',
  templateUrl: './view-payment-transaction.component.html',
  animations: appAnimations
})
export class ViewPaymentTransactionComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
