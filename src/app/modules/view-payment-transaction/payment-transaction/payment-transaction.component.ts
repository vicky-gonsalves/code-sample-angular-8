import {Component, OnDestroy, OnInit} from '@angular/core';
import {appAnimations} from '../../../core/animations/animations';
import {QueryResultStoreService} from '../../../shared/services/query-result-store/query-result-store.service';

@Component({
  selector: 'payment-transaction',
  templateUrl: './payment-transaction.component.html',
  styleUrls: ['./payment-transaction.component.scss'],
  animations: appAnimations
})
export class PaymentTransactionComponent implements OnInit, OnDestroy {

  constructor(private queryResultStoreService: QueryResultStoreService) {
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.queryResultStoreService.setQuery(null);
    this.queryResultStoreService.setFinalQuery(null);
    this.queryResultStoreService.setResults(null);
  }

}
