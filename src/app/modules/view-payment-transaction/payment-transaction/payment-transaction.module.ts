import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PaymentTransactionComponent} from './payment-transaction.component';
import {SearchFormComponent} from './components/search-form/search-form.component';
import {routing} from './payment-transaction.routes';
import {SharedModule} from '../../../shared/shared.module';
import {SearchResultComponent} from './components/search-result/search-result.component';
import {TransactionDetailViewComponent} from './components/transaction-detail-view/transaction-detail-view.component';
import {PaymentLegListComponent} from './components/payment-leg-list/payment-leg-list.component';
import {PaymentXmlViewerComponent} from './components/payment-xml-viewer/payment-xml-viewer.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    routing
  ],
  declarations: [
    PaymentTransactionComponent,
    SearchFormComponent,
    SearchResultComponent,
    TransactionDetailViewComponent,
    PaymentLegListComponent,
    PaymentXmlViewerComponent
  ],
  entryComponents: [
    TransactionDetailViewComponent,
    PaymentLegListComponent,
    PaymentXmlViewerComponent
  ]
})
export class PaymentTransactionModule {
}
