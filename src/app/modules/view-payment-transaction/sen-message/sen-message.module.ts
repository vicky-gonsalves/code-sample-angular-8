import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SenMessageComponent} from './sen-message.component';
import {SearchFormComponent} from './components/search-form/search-form.component';
import {SearchResultComponent} from './components/search-result/search-result.component';
import {SharedModule} from '../../../shared/shared.module';
import {routing} from './sen-message.routes';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    routing
  ],
  declarations: [SenMessageComponent, SearchFormComponent, SearchResultComponent]
})
export class SenMessageModule {
}
