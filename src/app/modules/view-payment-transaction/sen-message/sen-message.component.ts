import {Component, OnDestroy, OnInit} from '@angular/core';
import {appAnimations} from '../../../core/animations/animations';
import {QueryResultStoreSenMessageService} from '../../../shared/services/query-result-store-sen-message/query-result-store-sen-message.service';

@Component({
  selector: 'sen-messgae',
  templateUrl: './sen-message.component.html',
  styleUrls: ['./sen-message.component.scss'],
  animations: appAnimations
})
export class SenMessageComponent implements OnInit, OnDestroy {

  constructor(private queryResultStoreSenMessageService: QueryResultStoreSenMessageService) {
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.queryResultStoreSenMessageService.setQuery(null);
    this.queryResultStoreSenMessageService.setFinalQuery(null);
    this.queryResultStoreSenMessageService.setResults(null);
  }
}
