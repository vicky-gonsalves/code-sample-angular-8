import {HttpErrorResponse} from '@angular/common/http';
import {AfterViewInit, Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {PageEvent} from '@angular/material/paginator';
import {Router} from '@angular/router';
import moment from 'moment-timezone';
import {appAnimations} from '../../../../../core/animations/animations';
import {ClhSenMsgInterface} from '../../../../../interface/clh-sen-msg.interface';
import {NavItemInterface} from '../../../../../interface/nav-item.interface';
import {ClhSenMsgService} from '../../../../../shared/services/clh-sen-msg/clh-sen-msg.service';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {NavItemService} from '../../../../../shared/services/nav-item/nav-item.service';
import {PageHeaderService} from '../../../../../shared/services/page-header/page-header.service';
import {PageTitleService} from '../../../../../shared/services/page-title/page-title.service';
import {ParticipantService} from '../../../../../shared/services/participant/participant.service';
import {QueryResultStoreSenMessageService} from '../../../../../shared/services/query-result-store-sen-message/query-result-store-sen-message.service';
import {UiConfigParameterService} from '../../../../../shared/services/ui-config-parameter/ui-config-parameter.service';

@Component({
  selector: 'search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss'],
  animations: appAnimations
})
export class SearchFormComponent implements OnInit, AfterViewInit {

  public accountSearchForm: FormGroup;
  public inProgress: Boolean = false;
  public submitted: Boolean = false;
  public isParticipantContact: Boolean;
  public paginatorOptions = {
    count: 0,
    pageSize: 10,
    pageSizeOptions: [5, 10, 25, 50, 100],
    showFirstLastButtons: true
  };
  private componentNavItem: NavItemInterface;
  private dateRange: number;
  private snapshot_frm_date: moment;
  private snapshot_to_date: moment;
  private sortBy = '-event_tmstmp';
  private pageSize = 10;

  private storedQuery: any;
  private storedFinalQuery: any;

  constructor(private navItems: NavItemService,
              private errorMessageService: ErrorMessageService,
              private pageTitle: PageTitleService,
              private participantService: ParticipantService,
              private uiConfigParameterService: UiConfigParameterService,
              private queryResultStoreSenMessageService: QueryResultStoreSenMessageService,
              private clhSenMsgService: ClhSenMsgService,
              private router: Router) {
    this.dateRange = parseFloat(this.uiConfigParameterService.getLocalConfig('SEN_MESSAGE_DATE_RANGE'));
    this.queryResultStoreSenMessageService.setQuery(null);
    this.storedQuery = this.queryResultStoreSenMessageService.getQuery();
  }

  get frm_date() {
    return this.accountSearchForm.get('frm_date');
  }

  get to_date() {
    return this.accountSearchForm.get('to_date');
  }

  ngOnInit() {
    this.componentNavItem = this.navItems.getItemById('view-sen-message');
    if (this.componentNavItem) {
      this.pageTitle.setTitle(this.componentNavItem.name);
    }

    // Set Page Header Details
    PageHeaderService.updatePageHeader({
      showImage: true,
      icon: 'receipt'
    });

    this.setupForm();
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.storedFinalQuery = this.queryResultStoreSenMessageService.getFinalQuery();
      if (this.storedFinalQuery) {
        this.accountSearchForm.patchValue({
          frm_date: this.storedFinalQuery.frmTimestamp,
          to_date: this.storedFinalQuery.toTimestamp
        });
        this.snapshot_frm_date = this.storedFinalQuery.frmTimestamp;
        this.snapshot_to_date = this.storedFinalQuery.toTimestamp;
        this.frm_date.markAsTouched();
        this.frm_date.updateValueAndValidity();
        this.to_date.markAsTouched();
        this.to_date.updateValueAndValidity();
      } else {
        this.snapshot_frm_date = moment().startOf('day');
        this.snapshot_to_date = moment().startOf('day').add(1, 'days');
        this.accountSearchForm.patchValue({
          frm_date: this.snapshot_frm_date.format(),
          to_date: this.snapshot_to_date.format()
        });
      }
    }, 10);
  }

  public frmDateChanged(date) {
    this.snapshot_frm_date = moment(date.value);
    this.validateDateRange();
  }

  public toDateChanged(date) {
    this.snapshot_to_date = moment(date.value);
    this.validateDateRange();
  }

  public validateDateRange() {
    if (this.snapshot_frm_date && this.snapshot_to_date) {
      setTimeout(() => {
        if (moment(this.snapshot_to_date).diff(moment(this.snapshot_frm_date), 'days') > this.dateRange) {
          this.to_date.setErrors({'dateRange': true});
        } else if (moment(this.snapshot_to_date).diff(moment(this.snapshot_frm_date), 'days') < 0) {
          this.frm_date.setErrors({'dateRangeLower': true});
        } else {
          this.frm_date.setErrors({'dateRangeLower': null});
          this.to_date.setErrors({'dateRange': null});
          this.frm_date.clearValidators();
          this.to_date.clearValidators();
          this.frm_date.updateValueAndValidity();
          this.to_date.updateValueAndValidity();
        }
      });
    }
  }

  public getErrorMessage(fieldName: string): string {
    if (fieldName === 'to_date' && this[fieldName].errors.hasOwnProperty('dateRange') && this[fieldName].errors['dateRange']) {
      return 'From/To date range should be within ' + this.dateRange + ' days';
    }
    return this.errorMessageService.getError(this[fieldName], fieldName);
  }

  public submitAccountSearchForm(pageEvent?: PageEvent): void {
    if (pageEvent) {
      this.pageSize = pageEvent.pageSize;
    }
    const options = {
      limit: (pageEvent ? (pageEvent.pageSize) : this.pageSize ? this.pageSize : this.paginatorOptions.pageSize),
      page: (pageEvent ? (pageEvent.pageIndex + 1) : 1),
      sort: [this.sortBy]
    };
    this.submitted = true;
    const query = this.prepareFormFields();
    if (this.accountSearchForm.valid) {
      this.setInProgress();
      this.clhSenMsgService.getClhSenMsg(options, query)
        .subscribe(response => {
            if (response && response.hasOwnProperty('data') && response.data) {
              if (response.data.rows && response.data.rows.length) {
                response.data.rows.forEach(row => {
                  row.event_tmstmp = this.convertTimezone(row.event_tmstmp);
                });
                this.queryResultStoreSenMessageService.setResults(response.data);
                this.router.navigate(['/view-payment-transactions/view-sen-message/search-results']);
              } else {
                this.errorMessageService.openSnackBar('No search results found', 5000);
              }
            }
            this.setNotInProgress();
          },
          (err: HttpErrorResponse) => {
            this.setNotInProgress();
            this.errorMessageService.handleServerErrors(err, this.accountSearchForm);
          });
    }
  }

  public convertTimezone(date: Date): moment {
    return this.uiConfigParameterService.convertTimezone(date);
  }

  public resetForm(): void {
    this.queryResultStoreSenMessageService.setFinalQuery(null);
    setTimeout(() => {
      this.accountSearchForm.patchValue({
        frm_date: moment().startOf('day').format(),
        to_date: moment().startOf('day').add(1, 'days').format()
      });
    }, 10);
  }

  private setInProgress(): void {
    this.inProgress = true;
  }

  private setNotInProgress(): void {
    this.inProgress = false;
  }

  private setupForm(): void {
    this.accountSearchForm = new FormGroup({
      'frm_date': new FormControl('', [
        Validators.required
      ]),
      'to_date': new FormControl('', [
        Validators.required
      ])
    });
  }

  private prepareFormFields(): ClhSenMsgInterface {
    const query: ClhSenMsgInterface = {};
    query.frmTimestamp = moment(this.frm_date.value).format('YYYY-MM-DDTHH:mm:ss');
    query.toTimestamp = moment(this.to_date.value).format('YYYY-MM-DDTHH:mm:ss');
    this.queryResultStoreSenMessageService.setFinalQuery(query);
    return query;
  }
}

