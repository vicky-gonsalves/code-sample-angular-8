import {HttpErrorResponse} from '@angular/common/http';
import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {Router} from '@angular/router';
import moment from 'moment-timezone';
import {Subscription} from 'rxjs';
import {appAnimations} from '../../../../../core/animations/animations';
import {Identity} from '../../../../../core/services/identity/identity';
import {IdentityService} from '../../../../../core/services/identity/identity.service';
import {ClhSenMsgInterface} from '../../../../../interface/clh-sen-msg.interface';
import {NavItemInterface} from '../../../../../interface/nav-item.interface';
import {XmlViewerComponent} from '../../../../../shared/components/xml-viewer/xml-viewer.component';
import {ClhSenMsgService} from '../../../../../shared/services/clh-sen-msg/clh-sen-msg.service';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {NavItemService} from '../../../../../shared/services/nav-item/nav-item.service';
import {PageHeaderService} from '../../../../../shared/services/page-header/page-header.service';
import {PageTitleService} from '../../../../../shared/services/page-title/page-title.service';
import {QueryResultStoreSenMessageService} from '../../../../../shared/services/query-result-store-sen-message/query-result-store-sen-message.service';
import {RefreshService} from '../../../../../shared/services/refresh/refresh.service';
import {UiConfigParameterService} from '../../../../../shared/services/ui-config-parameter/ui-config-parameter.service';

@Component({
  selector: 'search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.scss'],
  animations: appAnimations
})
export class SearchResultComponent implements OnInit, OnDestroy {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  public searchTerm: {
    event_code: string,
    receiver_participant_id: string
  };
  public displayedColumns =
    ['actions', 'event_code', 'event_tmstmp', 'event_description', 'receiver_participant_id'];
  public dataSource: MatTableDataSource<ClhSenMsgInterface>;
  public paginatorOptions = {
    count: 0,
    pageSize: 10,
    pageSizeOptions: [5, 10, 25, 50, 100],
    showFirstLastButtons: true
  };
  public inProgress: Boolean = true;
  private sortBy = '-event_tmstmp';
  private pageSize = 10;
  private componentNavItem: NavItemInterface;
  private clhSenMessageQuery: any;
  private storedResults: any;
  private identity: Identity;
  private _lastPageEvent: PageEvent;
  private _lastOptions: any;
  private refreshSubscription: Subscription;

  constructor(private queryResultStoreSenMessageService: QueryResultStoreSenMessageService,
              private router: Router,
              private navItems: NavItemService,
              private errorMessageService: ErrorMessageService,
              private uiConfigParameterService: UiConfigParameterService,
              private dialog: MatDialog,
              private clhSenMsgService: ClhSenMsgService,
              private pageTitle: PageTitleService,
              private refreshService: RefreshService) {
    this.refreshSubscription = this.refreshService.refreshObservable$.subscribe((res) => {
      if (res.hasOwnProperty('refresh') && res.refresh) {
        this.clhSenMessageQuery = this.queryResultStoreSenMessageService.getFinalQuery();
        this.fetchClhSenMsg(this._lastPageEvent, this._lastOptions);
      }
    });

    IdentityService.identity.subscribe(contactIdentity => {
      this.identity = contactIdentity;
    });
    this.searchTerm = {
      event_code: null,
      receiver_participant_id: null
    };
    const _storedResults = this.queryResultStoreSenMessageService.getResults();
    this.clhSenMessageQuery = this.queryResultStoreSenMessageService.getFinalQuery();
    if (!this.clhSenMessageQuery && !_storedResults) {
      this.router.navigate(['/view-payment-transactions/view-sen-message']);
    } else {
      this.storedResults = _storedResults.rows;
      this.dataSource = new MatTableDataSource(this.storedResults);
      this.paginatorOptions.count = _storedResults.count;
      this.dataSource.sort = this.sort;
    }
  }

  ngOnInit() {
    // Get Navigation Component Name
    this.componentNavItem = this.navItems.getItemById('sen-message-result');
    if (this.componentNavItem) {
      // Set Page Title
      this.pageTitle.setTitle(this.componentNavItem.name);
    }

    // Set Page Header Details
    PageHeaderService.updatePageHeader({
      backPath: 'view-payment-transactions/view-sen-message',
      refreshButton: true
    });
  }

  ngOnDestroy() {
    this.refreshSubscription.unsubscribe();
  }

  public sortData(sort: Sort): void {
    const data = this.dataSource;
    if (!sort.active || sort.direction === '') {
      this.sortBy = '-event_tmstmp';
    } else {
      this.sortBy = sort.direction === 'asc' ? sort.active : '-' + sort.active;
    }
    this.paginator.pageIndex = 0;
    this.fetchClhSenMsg();
  }

  public fetchClhSenMsg(pageEvent?: PageEvent, lastOptions?: any): void {
    if (pageEvent) {
      this.pageSize = pageEvent.pageSize;
      this._lastPageEvent = pageEvent;
    }
    let options = null;
    if (lastOptions) {
      options = lastOptions;
    } else {
      options = {
        limit: (pageEvent ? (pageEvent.pageSize) : this.pageSize ? this.pageSize : this.paginatorOptions.pageSize),
        page: (pageEvent ? (pageEvent.pageIndex + 1) : 1),
        sort: [this.sortBy]
      };
      if (this.searchTerm.event_code) {
        options['event_code'] = this.searchTerm.event_code;
      }
      if (this.searchTerm.receiver_participant_id) {
        options['receiver_participant_id'] = this.searchTerm.receiver_participant_id;
      }
      this._lastOptions = options;
    }
    this.inProgress = true;
    this.clhSenMsgService.getClhSenMsg(options, this.clhSenMessageQuery)
      .subscribe(response => {
          if (response && response.data && response.data.rows && response.data.rows.length) {
            response.data.rows.forEach(row => {
              row.event_tmstmp = this.convertTimezone(row.event_tmstmp);
            });
            this.dataSource = response.data.rows;
            this.paginatorOptions.count = response.data.count;
            this.dataSource.sort = this.sort;
          } else {
            this.errorMessageService.openSnackBar(this.errorMessageService.getNonFormError('NO_DATA'));
          }
          this.inProgress = false;
        },
        (err: HttpErrorResponse) => {
          this.errorMessageService.handleServerErrors(err);
          this.inProgress = false;
        });
  }

  public applyFilter(searchCriteria: { key: string, value: string }): void {
    this.searchTerm[searchCriteria.key] = searchCriteria.value;
    this.paginator.pageIndex = 0;
    this.fetchClhSenMsg();
  }

  public convertTimezone(date: Date): moment {
    return this.uiConfigParameterService.convertTimezone(date);
  }

  public viewXmlMessage(clhSenMsg?: ClhSenMsgInterface): void {
    const conf: MatDialogConfig = {
      autoFocus: false,
      closeOnNavigation: true,
      panelClass: 'panelClass',
      maxWidth: '1200px',
      data: {xmlData: clhSenMsg.event_xml_message}
    };
    this.dialog.open(XmlViewerComponent, conf);
  }
}
