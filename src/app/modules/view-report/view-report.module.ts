import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ViewReportComponent} from './view-report.component';
import {routing} from './view-report.routes';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    routing
  ],
  declarations: [ViewReportComponent]
})
export class ViewReportModule {
}
