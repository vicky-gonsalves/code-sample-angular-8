import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserDataGuard} from '../../core/guards/user-data/user-data.guard';

export const routes: Routes = [
  {
    path: '',
    canActivate: [UserDataGuard],
    children: [
      {
        path: 'certification-criteria-report',
        canActivateChild: [UserDataGuard],
        loadChildren: () => import('./certificate-criteria-report/certificate-criteria-report.module').then(mod => mod.CertificateCriteriaReportModule),
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'certification-criteria-report'}
      },
      {
        path: 'bulk-execution-report',
        canActivateChild: [UserDataGuard],
        loadChildren: () => import('./bulk-criteria-report/bulk-criteria-report.module').then(mod => mod.BulkCriteriaReportModule),
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'bulk-criteria-report'}
      }
    ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);

