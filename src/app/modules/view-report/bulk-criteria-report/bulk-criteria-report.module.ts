import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BulkCriteriaReportComponent} from './bulk-criteria-report.component';
import {SharedModule} from '../../../shared/shared.module';
import {routing} from './bulk-criteria-report.routes';
import {SearchFormComponent} from './components/search-form/search-form.component';
import {SearchResultComponent} from './components/search-result/search-result.component';
import {CountSummaryViewComponent} from './components/count-summary-view/count-summary-view.component';
import {BulkCertCriteriaExecSummaryListComponent} from './components/bulk-cert-criteria-exec-summary-list/bulk-cert-criteria-exec-summary-list.component';
import {AccountSummaryViewComponent} from './components/account-summary-view/account-summary-view.component';
import {BulkCertCriteriaSmtSummaryListComponent} from './components/bulk-cert-criteria-smt-summary-list/bulk-cert-criteria-smt-summary-list.component';
import {LegSummaryViewComponent} from './components/leg-summary-view/leg-summary-view.component';
import {BulkPerfSummaryListComponent} from './components/bulk-perf-summary-list/bulk-perf-summary-list.component';
import {SampleMaxTimeTransactionViewComponent} from './components/sample-max-time-transaction-view/sample-max-time-transaction-view.component';
import {SampleMaxTimeTransactionLegDetailViewComponent} from './components/sample-max-time-transaction-leg-detail-view/sample-max-time-transaction-leg-detail-view.component';
import {TransactionSummaryViewComponent} from './components/transaction-summary-view/transaction-summary-view.component';
import {BulkTpsSummaryListComponent} from './components/bulk-tps-summary-list/bulk-tps-summary-list.component';
import {SampleMinTpsTransactionViewComponent} from './components/sample-min-tps-transaction-view/sample-min-tps-transaction-view.component';
import {SampleMinTpsTransactionLegDetailViewComponent} from './components/sample-min-tps-transaction-leg-detail-view/sample-min-tps-transaction-leg-detail-view.component';
import {TpsGraphViewComponent} from './components/tps-graph-view/tps-graph-view.component';
import {TpsGraphComponent} from './components/tps-graph/tps-graph.component';
import {NvD3Module} from 'ng2-nvd3';
import 'nvd3';
import {NouisliderModule} from 'ng2-nouislider';
import {BulkTpsDetailsListComponent} from './components/bulk-tps-details-list/bulk-tps-details-list.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    routing,
    NvD3Module,
    NouisliderModule
  ],
  declarations: [
    BulkCriteriaReportComponent,
    SearchFormComponent,
    SearchResultComponent,
    CountSummaryViewComponent,
    BulkCertCriteriaExecSummaryListComponent,
    AccountSummaryViewComponent,
    BulkCertCriteriaSmtSummaryListComponent,
    LegSummaryViewComponent,
    BulkPerfSummaryListComponent,
    SampleMaxTimeTransactionViewComponent,
    SampleMaxTimeTransactionLegDetailViewComponent,
    TransactionSummaryViewComponent,
    BulkTpsSummaryListComponent,
    SampleMinTpsTransactionViewComponent,
    SampleMinTpsTransactionLegDetailViewComponent,
    TpsGraphViewComponent,
    TpsGraphComponent,
    BulkTpsDetailsListComponent
  ],
  entryComponents: [
    CountSummaryViewComponent,
    AccountSummaryViewComponent,
    LegSummaryViewComponent,
    SampleMaxTimeTransactionViewComponent,
    SampleMaxTimeTransactionLegDetailViewComponent,
    TransactionSummaryViewComponent,
    SampleMinTpsTransactionViewComponent,
    TpsGraphViewComponent,
    BulkTpsDetailsListComponent
  ]
})
export class BulkCriteriaReportModule {
}
