import {Component, OnDestroy, OnInit} from '@angular/core';
import {appAnimations} from '../../../core/animations/animations';
import {QueryResultStoreBulkExecutionService} from '../../../shared/services/query-result-store-bulk-execution/query-result-store-bulk-execution.service';

@Component({
  selector: 'bulk-criteria-report',
  templateUrl: './bulk-criteria-report.component.html',
  styleUrls: ['./bulk-criteria-report.component.scss'],
  animations: appAnimations
})
export class BulkCriteriaReportComponent implements OnInit, OnDestroy {

  constructor(private queryResultStoreBulkExecutionService: QueryResultStoreBulkExecutionService) {
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.queryResultStoreBulkExecutionService.setQuery(null);
    this.queryResultStoreBulkExecutionService.setFinalQuery(null);
    this.queryResultStoreBulkExecutionService.setResults(null);
  }
}
