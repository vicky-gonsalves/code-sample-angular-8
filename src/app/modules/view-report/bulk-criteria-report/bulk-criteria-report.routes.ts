import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserDataGuard} from '../../../core/guards/user-data/user-data.guard';
import {BulkCriteriaReportComponent} from './bulk-criteria-report.component';
import {SearchFormComponent} from './components/search-form/search-form.component';
import {SearchResultComponent} from './components/search-result/search-result.component';

export const routes: Routes = [
  {
    path: '',
    canActivate: [UserDataGuard],
    component: BulkCriteriaReportComponent,
    children: [
      {
        path: '',
        canActivateChild: [UserDataGuard],
        component: SearchFormComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'bulk-criteria-report'}
      },
      {
        path: 'search-results',
        canActivateChild: [UserDataGuard],
        component: SearchResultComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'bulk-execution-result'}
      }
    ]
  },
  {path: '**', redirectTo: '', pathMatch: 'full'}
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);

