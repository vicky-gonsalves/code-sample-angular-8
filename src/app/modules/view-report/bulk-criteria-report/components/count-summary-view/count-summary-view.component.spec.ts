import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CountSummaryViewComponent } from './count-summary-view.component';

describe('CountSummaryViewComponent', () => {
  let component: CountSummaryViewComponent;
  let fixture: ComponentFixture<CountSummaryViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CountSummaryViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CountSummaryViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
