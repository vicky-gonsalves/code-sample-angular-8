import {HttpErrorResponse} from '@angular/common/http';
import {AfterViewInit, Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {PageEvent} from '@angular/material/paginator';
import {Router} from '@angular/router';
import moment from 'moment-timezone';
import {appAnimations} from '../../../../../core/animations/animations';
import {NavItemInterface} from '../../../../../interface/nav-item.interface';
import {CertCriteriaExecReqService} from '../../../../../shared/services/cert-criteria-exec-req/cert-criteria-exec-req.service';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {NavItemService} from '../../../../../shared/services/nav-item/nav-item.service';
import {PageHeaderService} from '../../../../../shared/services/page-header/page-header.service';
import {PageTitleService} from '../../../../../shared/services/page-title/page-title.service';
import {QueryResultStoreBulkExecutionService} from '../../../../../shared/services/query-result-store-bulk-execution/query-result-store-bulk-execution.service';
import {UiConfigParameterService} from '../../../../../shared/services/ui-config-parameter/ui-config-parameter.service';

@Component({
  selector: 'search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss'],
  animations: appAnimations
})
export class SearchFormComponent implements OnInit, AfterViewInit {
  public bulkCriteriaSearchForm: FormGroup;
  public inProgress: Boolean = false;
  public submitted: Boolean = false;
  public paginatorOptions = {
    count: 0,
    pageSize: 10,
    pageSizeOptions: [5, 10, 25, 50, 100],
    showFirstLastButtons: true
  };
  private componentNavItem: NavItemInterface;
  private dateRange: number;
  private execution_frm_date: moment;
  private execution_to_date: moment;
  private sortBy = '-createdAt';
  private pageSize = 10;

  private storedQuery: any;
  private storedFinalQuery: any;

  constructor(private navItems: NavItemService,
              private errorMessageService: ErrorMessageService,
              private pageTitle: PageTitleService,
              private uiConfigParameterService: UiConfigParameterService,
              private queryResultStoreBulkExecutionService: QueryResultStoreBulkExecutionService,
              private certCriteriaExecReqService: CertCriteriaExecReqService,
              private router: Router) {
    this.dateRange = parseFloat(this.uiConfigParameterService.getLocalConfig('BULK_CRITERIA_SEARCH_DATE_RANGE'));
    this.queryResultStoreBulkExecutionService.setQuery(null);
    this.storedQuery = this.queryResultStoreBulkExecutionService.getQuery();
  }

  get frm_date() {
    return this.bulkCriteriaSearchForm.get('frm_date');
  }

  get to_date() {
    return this.bulkCriteriaSearchForm.get('to_date');
  }

  ngOnInit() {
    this.componentNavItem = this.navItems.getItemById('bulk-criteria-report');
    if (this.componentNavItem) {
      this.pageTitle.setTitle(this.componentNavItem.name);
    }

    // Set Page Header Details
    PageHeaderService.updatePageHeader({
      showImage: true,
      icon: 'collections'
    });

    this.setupForm();
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.storedFinalQuery = this.queryResultStoreBulkExecutionService.getFinalQuery();
      if (this.storedFinalQuery) {
        this.bulkCriteriaSearchForm.patchValue({
          frm_date: this.storedFinalQuery.frmTimestamp,
          to_date: this.storedFinalQuery.toTimestamp
        });
        this.execution_frm_date = this.storedFinalQuery.frmTimestamp;
        this.execution_to_date = this.storedFinalQuery.toTimestamp;
        this.frm_date.markAsTouched();
        this.frm_date.updateValueAndValidity();
        this.to_date.markAsTouched();
        this.to_date.updateValueAndValidity();
      } else {
        this.execution_frm_date = moment().startOf('day');
        this.execution_to_date = moment().startOf('day').add(1, 'days');
        this.bulkCriteriaSearchForm.patchValue({
          frm_date: this.execution_frm_date.format(),
          to_date: this.execution_to_date.format()
        });
      }
    }, 10);
  }

  public frmDateChanged(date) {
    this.execution_frm_date = moment(date.value);
    this.validateDateRange();
  }

  public toDateChanged(date) {
    this.execution_to_date = moment(date.value);
    this.validateDateRange();
  }

  public validateDateRange() {
    if (this.execution_frm_date && this.execution_to_date) {
      setTimeout(() => {
        if (moment(this.execution_to_date).diff(moment(this.execution_frm_date), 'days') > this.dateRange) {
          this.to_date.setErrors({'dateRange': true});
        } else if (moment(this.execution_to_date).diff(moment(this.execution_frm_date), 'days') < 0) {
          this.frm_date.setErrors({'dateRangeLower': true});
        } else {
          this.frm_date.setErrors({'dateRangeLower': null});
          this.to_date.setErrors({'dateRange': null});
          this.frm_date.clearValidators();
          this.to_date.clearValidators();
          this.frm_date.updateValueAndValidity();
          this.to_date.updateValueAndValidity();
        }
      });
    }
  }

  public getErrorMessage(fieldName: string): string {
    if (fieldName === 'to_date' && this[fieldName].errors.hasOwnProperty('dateRange') && this[fieldName].errors['dateRange']) {
      return 'Execution date range should be within ' + this.dateRange + ' days';
    }
    return this.errorMessageService.getError(this[fieldName], fieldName);
  }

  public submitBulkCriteriaSearchForm(pageEvent?: PageEvent): void {
    if (pageEvent) {
      this.pageSize = pageEvent.pageSize;
    }
    const options = {
      limit: (pageEvent ? (pageEvent.pageSize) : this.pageSize ? this.pageSize : this.paginatorOptions.pageSize),
      page: (pageEvent ? (pageEvent.pageIndex + 1) : 1),
      sort: [this.sortBy]
    };
    this.submitted = true;
    const query = this.prepareFormFields();
    if (this.bulkCriteriaSearchForm.valid) {
      this.setInProgress();
      this.certCriteriaExecReqService.getBulkExecution(options, query)
        .subscribe(response => {
            if (response && response.hasOwnProperty('data') && response.data) {
              if (response.data.rows && response.data.rows.length) {
                response.data.rows.forEach(row => {
                  row.exe_requested_datetime = this.convertTimezone(row.exe_requested_datetime);
                  row.exe_end_datetime = this.convertTimezone(row.exe_end_datetime);
                  row.created_at = this.convertTimezone(row.created_at);
                });
                this.queryResultStoreBulkExecutionService.setResults(response.data);
                this.router.navigate(['/view-report/bulk-execution-report/search-results']);
              } else {
                this.errorMessageService.openSnackBar('No search results found', 5000);
              }
            }
            this.setNotInProgress();
          },
          (err: HttpErrorResponse) => {
            this.setNotInProgress();
            this.errorMessageService.handleServerErrors(err, this.bulkCriteriaSearchForm);
          });
    }
  }

  public convertTimezone(date: Date): moment {
    return this.uiConfigParameterService.convertTimezone(date);
  }

  public resetForm(): void {
    this.queryResultStoreBulkExecutionService.setFinalQuery(null);
    setTimeout(() => {
      this.bulkCriteriaSearchForm.patchValue({
        frm_date: moment().startOf('day').format(),
        to_date: moment().startOf('day').add(1, 'days').format()
      });
    }, 10);
  }

  private setInProgress(): void {
    this.inProgress = true;
  }

  private setNotInProgress(): void {
    this.inProgress = false;
  }

  private setupForm(): void {
    this.bulkCriteriaSearchForm = new FormGroup({
      'frm_date': new FormControl('', [
        Validators.required
      ]),
      'to_date': new FormControl('', [
        Validators.required
      ])
    });
  }

  private prepareFormFields(): any {
    const query: any = {};
    query.frmTimestamp = moment(this.frm_date.value).format('YYYY-MM-DDTHH:mm:ss');
    query.toTimestamp = moment(this.to_date.value).format('YYYY-MM-DDTHH:mm:ss');
    this.queryResultStoreBulkExecutionService.setFinalQuery(query);
    return query;
  }
}
