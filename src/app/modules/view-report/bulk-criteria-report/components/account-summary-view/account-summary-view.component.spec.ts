import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountSummaryViewComponent } from './account-summary-view.component';

describe('AccountSummaryViewComponent', () => {
  let component: AccountSummaryViewComponent;
  let fixture: ComponentFixture<AccountSummaryViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountSummaryViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountSummaryViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
