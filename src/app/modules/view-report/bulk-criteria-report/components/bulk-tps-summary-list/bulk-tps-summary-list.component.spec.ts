import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BulkTpsSummaryListComponent } from './bulk-tps-summary-list.component';

describe('BulkTpsSummaryListComponent', () => {
  let component: BulkTpsSummaryListComponent;
  let fixture: ComponentFixture<BulkTpsSummaryListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BulkTpsSummaryListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BulkTpsSummaryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
