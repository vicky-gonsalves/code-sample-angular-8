import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {appAnimations} from '../../../../../core/animations/animations';
import {BulkExecutionInterface} from '../../../../../interface/bulk-execution.interface';
import {BulkTpsSummaryInterface} from '../../../../../interface/bulk-tps-summary.interface';
import {BulkTpsSummaryService} from '../../../../../shared/services/bulk-tps-summary/bulk-tps-summary.service';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {SampleMinTpsTransactionViewComponent} from '../sample-min-tps-transaction-view/sample-min-tps-transaction-view.component';
import {TpsGraphViewComponent} from '../tps-graph-view/tps-graph-view.component';

@Component({
  selector: 'bulk-tps-summary-list',
  templateUrl: './bulk-tps-summary-list.component.html',
  styleUrls: ['./bulk-tps-summary-list.component.scss'],
  animations: appAnimations
})
export class BulkTpsSummaryListComponent implements OnInit {
  @Input() bulkExecutionData: BulkExecutionInterface;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  public displayedColumns = ['transaction_type', 'min_total', 'avg_total', 'max_total', 'actions'];
  public dataSource: MatTableDataSource<BulkTpsSummaryInterface>;
  public paginatorOptions = {
    count: 0,
    pageSize: 10,
    pageSizeOptions: [5, 10, 25, 50, 100],
    showFirstLastButtons: true
  };
  public inProgress: Boolean = true;
  public pageEvent: any;
  private sortBy = '-transaction_type';
  private pageSize = 10;

  constructor(private bulkTpsSummaryService: BulkTpsSummaryService,
              private dialog: MatDialog,
              private errorMsgService: ErrorMessageService) {
  }

  ngOnInit() {
    this.fetchBulkTpsSummaryListing();
  }

  public sortData(sort: Sort): void {
    const data = this.dataSource;
    if (!sort.active || sort.direction === '') {
      this.sortBy = '-transaction_type';
    } else {
      this.sortBy = sort.direction === 'asc' ? sort.active : '-' + sort.active;
    }
    this.paginator.pageIndex = 0;
    this.fetchBulkTpsSummaryListing();
  }

  public fetchBulkTpsSummaryListing(_pageEvent?: PageEvent): void {
    if (_pageEvent) {
      this.pageEvent = _pageEvent;
      this.pageSize = _pageEvent.pageSize;
    }
    const options = {
      limit: (this.pageEvent ? (this.pageEvent.pageSize) : this.pageSize ? this.pageSize : this.paginatorOptions.pageSize),
      page: (this.pageEvent ? (this.pageEvent.pageIndex + 1) : 1),
      // sort: [this.sortBy]
    };
    this.inProgress = true;
    this.bulkTpsSummaryService.getBulkTPSSummary(options, this.bulkExecutionData.run_id)
      .subscribe(response => {
          if (response && response.data && response.data.rows && response.data.rows.length) {
            this.dataSource = new MatTableDataSource(response.data.rows);
            this.paginatorOptions.count = response.data.count;
            // this.dataSource.sort = this.sort;
            /*TODO Sort is fixed at api level*/
          }
          this.inProgress = false;
        },
        err => {
          this.errorMsgService.handleServerErrors(err);
          this.inProgress = false;
        });
  }

  public viewSampleMinTPSTransaction(bulkTpsSummary?: BulkTpsSummaryInterface): void {
    const conf: MatDialogConfig = {
      autoFocus: false,
      closeOnNavigation: true,
      panelClass: 'panelClass',
      maxWidth: '1400px',
      data: {bulkTpsSummary, bulkExecutionData: this.bulkExecutionData}
    };
    this.dialog.open(SampleMinTpsTransactionViewComponent, conf);
  }

  public viewTPSGraph(bulkTpsSummary?: BulkTpsSummaryInterface): void {
    const conf: MatDialogConfig = {
      autoFocus: false,
      closeOnNavigation: true,
      backdropClass: 'clearBackdrop',
      panelClass: 'panelClass',
      maxWidth: '1400px',
      data: {bulkTpsSummary, bulkExecutionData: this.bulkExecutionData}
    };
    this.dialog.open(TpsGraphViewComponent, conf);
  }
}
