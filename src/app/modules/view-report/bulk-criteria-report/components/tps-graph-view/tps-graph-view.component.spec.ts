import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TpsGraphViewComponent } from './tps-graph-view.component';

describe('TpsGraphViewComponent', () => {
  let component: TpsGraphViewComponent;
  let fixture: ComponentFixture<TpsGraphViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TpsGraphViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TpsGraphViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
