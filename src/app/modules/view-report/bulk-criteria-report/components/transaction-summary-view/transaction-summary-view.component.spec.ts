import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionSummaryViewComponent } from './transaction-summary-view.component';

describe('TransactionSummaryViewComponent', () => {
  let component: TransactionSummaryViewComponent;
  let fixture: ComponentFixture<TransactionSummaryViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransactionSummaryViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionSummaryViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
