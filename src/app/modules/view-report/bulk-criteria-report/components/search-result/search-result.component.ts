import {HttpErrorResponse} from '@angular/common/http';
import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {Router} from '@angular/router';
import moment from 'moment-timezone';
import {Subscription} from 'rxjs';
import {appAnimations} from '../../../../../core/animations/animations';
import {Identity} from '../../../../../core/services/identity/identity';
import {IdentityService} from '../../../../../core/services/identity/identity.service';
import {BulkExecutionInterface} from '../../../../../interface/bulk-execution.interface';
import {NavItemInterface} from '../../../../../interface/nav-item.interface';
import {CertCriteriaExecReqService} from '../../../../../shared/services/cert-criteria-exec-req/cert-criteria-exec-req.service';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {NavItemService} from '../../../../../shared/services/nav-item/nav-item.service';
import {PageHeaderService} from '../../../../../shared/services/page-header/page-header.service';
import {PageTitleService} from '../../../../../shared/services/page-title/page-title.service';
import {QueryResultStoreBulkExecutionService} from '../../../../../shared/services/query-result-store-bulk-execution/query-result-store-bulk-execution.service';
import {RefreshService} from '../../../../../shared/services/refresh/refresh.service';
import {UiConfigParameterService} from '../../../../../shared/services/ui-config-parameter/ui-config-parameter.service';
import {AccountSummaryViewComponent} from '../account-summary-view/account-summary-view.component';
import {CountSummaryViewComponent} from '../count-summary-view/count-summary-view.component';
import {LegSummaryViewComponent} from '../leg-summary-view/leg-summary-view.component';
import {TransactionSummaryViewComponent} from '../transaction-summary-view/transaction-summary-view.component';

@Component({
  selector: 'search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.scss'],
  animations: appAnimations
})
export class SearchResultComponent implements OnInit, OnDestroy {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  public searchTerm: {};
  public displayedColumns =
    ['actions', 'run_id', 'criteria_name', 'criteria_description', 'status', 'template_names', 'exe_requested_datetime', 'exe_end_datetime', 'rpt_aggrn_status'];
  public dataSource: MatTableDataSource<BulkExecutionInterface>;
  public paginatorOptions = {
    count: 0,
    pageSize: 10,
    pageSizeOptions: [5, 10, 25, 50, 100],
    showFirstLastButtons: true
  };
  public inProgress: Boolean = true;
  private sortBy = '-createdAt';
  private pageSize = 10;
  private componentNavItem: NavItemInterface;
  private bulkExecutionQuery: any;
  private storedResults: any;
  private identity: Identity;
  private _lastPageEvent: PageEvent;
  private _lastOptions: any;
  private refreshSubscription: Subscription;

  constructor(private queryResultStoreBulkExecutionService: QueryResultStoreBulkExecutionService,
              private router: Router,
              private navItems: NavItemService,
              private errorMessageService: ErrorMessageService,
              private uiConfigParameterService: UiConfigParameterService,
              private dialog: MatDialog,
              private certCriteriaExecReqService: CertCriteriaExecReqService,
              private pageTitle: PageTitleService,
              private refreshService: RefreshService) {
    this.refreshSubscription = this.refreshService.refreshObservable$.subscribe((res) => {
      if (res.hasOwnProperty('refresh') && res.refresh) {
        this.bulkExecutionQuery = this.queryResultStoreBulkExecutionService.getFinalQuery();
        this.fetchBulkExecution(this._lastPageEvent, this._lastOptions);
      }
    });

    IdentityService.identity.subscribe(contactIdentity => {
      this.identity = contactIdentity;
    });
    this.searchTerm = {};
    const _storedResults = this.queryResultStoreBulkExecutionService.getResults();
    this.bulkExecutionQuery = this.queryResultStoreBulkExecutionService.getFinalQuery();
    if (!this.bulkExecutionQuery && !_storedResults) {
      this.router.navigate(['/view-report/bulk-execution-report']);
    } else {
      this.storedResults = _storedResults.rows;
      this.dataSource = new MatTableDataSource(this.storedResults);
      this.paginatorOptions.count = _storedResults.count;
      this.dataSource.sort = this.sort;
    }
  }

  ngOnInit() {
    // Get Navigation Component Name
    this.componentNavItem = this.navItems.getItemById('bulk-execution-result');
    if (this.componentNavItem) {
      // Set Page Title
      this.pageTitle.setTitle(this.componentNavItem.name);
    }

    // Set Page Header Details
    PageHeaderService.updatePageHeader({
      backPath: '/view-report/bulk-execution-report',
      refreshButton: true
    });
  }

  ngOnDestroy() {
    this.refreshSubscription.unsubscribe();
  }

  public sortData(sort: Sort): void {
    const data = this.dataSource;
    if (!sort.active || sort.direction === '') {
      this.sortBy = '-createdAt';
    } else {
      this.sortBy = sort.direction === 'asc' ? sort.active : '-' + sort.active;
    }
    this.paginator.pageIndex = 0;
    this.fetchBulkExecution();
  }

  public fetchBulkExecution(pageEvent?: PageEvent, lastOptions?: any): void {
    if (pageEvent) {
      this.pageSize = pageEvent.pageSize;
      this._lastPageEvent = pageEvent;
    }
    let options = null;
    if (lastOptions) {
      options = lastOptions;
    } else {
      options = {
        limit: (pageEvent ? (pageEvent.pageSize) : this.pageSize ? this.pageSize : this.paginatorOptions.pageSize),
        page: (pageEvent ? (pageEvent.pageIndex + 1) : 1),
        sort: [this.sortBy]
      };
      this._lastOptions = options;
    }
    this.inProgress = true;
    this.certCriteriaExecReqService.getBulkExecution(options, this.bulkExecutionQuery)
      .subscribe(response => {
          if (response && response.data && response.data.rows && response.data.rows.length) {
            response.data.rows.forEach(row => {
              row.exe_requested_datetime = this.convertTimezone(row.exe_requested_datetime);
              row.exe_end_datetime = this.convertTimezone(row.exe_end_datetime);
              row.created_at = this.convertTimezone(row.created_at);
            });
            this.dataSource = new MatTableDataSource(response.data.rows);
            this.paginatorOptions.count = response.data.count;
            this.dataSource.sort = this.sort;
          } else {
            this.errorMessageService.openSnackBar(this.errorMessageService.getNonFormError('NO_DATA'));
          }
          this.inProgress = false;
        },
        (err: HttpErrorResponse) => {
          this.errorMessageService.handleServerErrors(err);
          this.inProgress = false;
        });
  }

  public applyFilter(searchCriteria: { key: string, value: string }): void {
    this.searchTerm[searchCriteria.key] = searchCriteria.value;
    this.paginator.pageIndex = 0;
    this.fetchBulkExecution();
  }

  public convertTimezone(date: Date): moment {
    if (date) {
      return this.uiConfigParameterService.convertTimezone(date);
    }
    return null;
  }

  public viewCountSummary(bulkExecutionData?: BulkExecutionInterface): void {
    const conf: MatDialogConfig = {
      autoFocus: false,
      closeOnNavigation: true,
      panelClass: 'panelClass',
      maxWidth: '1400px',
      data: bulkExecutionData
    };
    this.dialog.open(CountSummaryViewComponent, conf);
  }

  public viewAccountSummary(bulkExecutionData?: BulkExecutionInterface): void {
    const conf: MatDialogConfig = {
      autoFocus: false,
      closeOnNavigation: true,
      panelClass: 'panelClass',
      maxWidth: '1400px',
      data: bulkExecutionData
    };
    this.dialog.open(AccountSummaryViewComponent, conf);
  }

  public viewLegSummary(bulkExecutionData?: BulkExecutionInterface): void {
    const conf: MatDialogConfig = {
      autoFocus: false,
      closeOnNavigation: true,
      panelClass: 'panelClass',
      maxWidth: '1400px',
      data: bulkExecutionData
    };
    this.dialog.open(LegSummaryViewComponent, conf);
  }

  public viewTransactionSummary(bulkExecutionData?: BulkExecutionInterface): void {
    const conf: MatDialogConfig = {
      autoFocus: false,
      closeOnNavigation: true,
      panelClass: 'panelClass',
      maxWidth: '1400px',
      data: bulkExecutionData
    };
    this.dialog.open(TransactionSummaryViewComponent, conf);
  }

  public getFirstTestTemplateName(templates): string {
    return templates[0];
  }

  public getOtherTestTemplateNames(templates): string | null {
    if (templates && templates.length > 1) {
      return templates.join(', ');
    }
    return null;
  }
}
