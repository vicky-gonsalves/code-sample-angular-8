import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BulkTpsDetailsListComponent } from './bulk-tps-details-list.component';

describe('BulkTpsDetailsListComponent', () => {
  let component: BulkTpsDetailsListComponent;
  let fixture: ComponentFixture<BulkTpsDetailsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BulkTpsDetailsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BulkTpsDetailsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
