import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import moment from 'moment-timezone';
import {appAnimations} from '../../../../../core/animations/animations';
import {BulkTpsDetailInterface} from '../../../../../interface/bulk-tps-detail.interface';
import {BulkTpsDetailService} from '../../../../../shared/services/bulk-tps-detail/bulk-tps-detail.service';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {UiConfigParameterService} from '../../../../../shared/services/ui-config-parameter/ui-config-parameter.service';

@Component({
  selector: 'bulk-tps-details-list',
  templateUrl: './bulk-tps-details-list.component.html',
  styleUrls: ['./bulk-tps-details-list.component.scss'],
  animations: appAnimations
})
export class BulkTpsDetailsListComponent implements OnInit {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  public displayedColumns = ['instruction_id', 'leg1_tmstmp', 'leg2_tmstmp', 'leg3_tmstmp', 'leg4_tmstmp'];
  public dataSource: MatTableDataSource<BulkTpsDetailInterface>;
  public paginatorOptions = {
    count: 0,
    pageSize: 10,
    pageSizeOptions: [5, 10, 25, 50, 100],
    showFirstLastButtons: true
  };
  public inProgress: Boolean = true;
  public pageEvent: any;
  private sortBy = '-instruction_id';
  private pageSize = 10;
  private runId: string;
  private transactionType: string;
  private leg_tmstmp: Date;

  constructor(private dialogRef: MatDialogRef<BulkTpsDetailsListComponent>,
              @Inject(MAT_DIALOG_DATA) private data: any,
              private bulkTpsDetailService: BulkTpsDetailService,
              private uiConfigParameterService: UiConfigParameterService,
              private errorMsgService: ErrorMessageService) {
    this.runId = data.runId;
    this.transactionType = data.transactionType;
    this.leg_tmstmp = data.leg_tmstmp;
  }

  ngOnInit() {
    this.fetchBulkTpsDetailListing();
  }

  public sortData(sort: Sort): void {
    const data = this.dataSource;
    if (!sort.active || sort.direction === '') {
      this.sortBy = '-instruction_id';
    } else {
      this.sortBy = sort.direction === 'asc' ? sort.active : '-' + sort.active;
    }
    this.paginator.pageIndex = 0;
    this.fetchBulkTpsDetailListing();
  }

  public fetchBulkTpsDetailListing(_pageEvent?: PageEvent): void {
    if (_pageEvent) {
      this.pageEvent = _pageEvent;
      this.pageSize = _pageEvent.pageSize;
    }
    const options = {
      limit: (this.pageEvent ? (this.pageEvent.pageSize) : this.pageSize ? this.pageSize : this.paginatorOptions.pageSize),
      page: (this.pageEvent ? (this.pageEvent.pageIndex + 1) : 1),
      sort: [this.sortBy]
    };
    this.inProgress = true;
    this.bulkTpsDetailService.getBulkTPSDetailList(options, {
      run_id: this.runId,
      transaction_type: this.transactionType,
      leg_tmstmp: this.leg_tmstmp
    })
      .subscribe(response => {
          if (response && response.data && response.data.rows && response.data.rows.length) {
            response.data.rows.forEach(row => {
              row.leg1_tmstmp = this.convertTimezoneWithMillis(row.leg1_tmstmp);
              row.leg2_tmstmp = this.convertTimezoneWithMillis(row.leg2_tmstmp);
              row.leg3_tmstmp = this.convertTimezoneWithMillis(row.leg3_tmstmp);
              row.leg4_tmstmp = this.convertTimezoneWithMillis(row.leg4_tmstmp);
            });
            this.dataSource = new MatTableDataSource(response.data.rows);
            this.paginatorOptions.count = response.data.count;
            this.dataSource.sort = this.sort;
          }
          this.inProgress = false;
        },
        err => {
          this.errorMsgService.handleServerErrors(err);
          this.inProgress = false;
        });
  }

  public convertTimezoneWithMillis(date: Date): moment {
    if (date) {
      return this.uiConfigParameterService.convertTimezoneWithMillis(date);
    }
    return null;
  }
}
