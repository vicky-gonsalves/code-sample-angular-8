import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BulkPerfSummaryListComponent } from './bulk-perf-summary-list.component';

describe('BulkPerfSummaryListComponent', () => {
  let component: BulkPerfSummaryListComponent;
  let fixture: ComponentFixture<BulkPerfSummaryListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BulkPerfSummaryListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BulkPerfSummaryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
