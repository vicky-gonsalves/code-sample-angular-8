import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {appAnimations} from '../../../../../core/animations/animations';
import {BulkExecutionInterface} from '../../../../../interface/bulk-execution.interface';
import {BulkPerfSummaryInterface} from '../../../../../interface/bulk-perf-summary.interface';
import {BulkPerfSummaryService} from '../../../../../shared/services/bulk-perf-summary/bulk-perf-summary.service';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {SampleMaxTimeTransactionViewComponent} from '../sample-max-time-transaction-view/sample-max-time-transaction-view.component';

@Component({
  selector: 'bulk-perf-summary-list',
  templateUrl: './bulk-perf-summary-list.component.html',
  styleUrls: ['./bulk-perf-summary-list.component.scss'],
  animations: appAnimations
})
export class BulkPerfSummaryListComponent implements OnInit {
  @Input() bulkExecutionData: BulkExecutionInterface;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  public displayedColumns = ['transaction_type', 'min_leg4_leg1', 'avg_leg4_leg1', 'max_leg4_leg1', 'actions'];
  public dataSource: MatTableDataSource<BulkPerfSummaryInterface>;
  public paginatorOptions = {
    count: 0,
    pageSize: 10,
    pageSizeOptions: [5, 10, 25, 50, 100],
    showFirstLastButtons: true
  };
  public inProgress: Boolean = true;
  public pageEvent: any;
  private sortBy = '-date_time';
  private pageSize = 10;

  constructor(private bulkPerfSummaryService: BulkPerfSummaryService,
              private dialog: MatDialog,
              private errorMsgService: ErrorMessageService) {
  }

  ngOnInit() {
    this.fetchBulkPerfSummaryListing();
  }

  public sortData(sort: Sort): void {
    const data = this.dataSource;
    if (!sort.active || sort.direction === '') {
      this.sortBy = '-date_time';
    } else {
      this.sortBy = sort.direction === 'asc' ? sort.active : '-' + sort.active;
    }
    this.paginator.pageIndex = 0;
    this.fetchBulkPerfSummaryListing();
  }

  public fetchBulkPerfSummaryListing(_pageEvent?: PageEvent): void {
    if (_pageEvent) {
      this.pageEvent = _pageEvent;
      this.pageSize = _pageEvent.pageSize;
    }
    const options = {
      limit: (this.pageEvent ? (this.pageEvent.pageSize) : this.pageSize ? this.pageSize : this.paginatorOptions.pageSize),
      page: (this.pageEvent ? (this.pageEvent.pageIndex + 1) : 1),
      // sort: [this.sortBy]
    };
    this.inProgress = true;
    this.bulkPerfSummaryService.getBulkPerfSummary(options, this.bulkExecutionData.run_id)
      .subscribe(response => {
          if (response && response.data && response.data.rows && response.data.rows.length) {
            this.dataSource = new MatTableDataSource(response.data.rows);
            this.paginatorOptions.count = response.data.count;
            // this.dataSource.sort = this.sort;
            /*TODO Sort is fixed at api level*/
          }
          this.inProgress = false;
        },
        err => {
          this.errorMsgService.handleServerErrors(err);
          this.inProgress = false;
        });
  }

  public viewSampleMaxTimeTransaction(bulkPerfSummary?: BulkPerfSummaryInterface): void {
    const conf: MatDialogConfig = {
      autoFocus: false,
      closeOnNavigation: true,
      panelClass: 'panelClass',
      maxWidth: '1400px',
      data: {bulkPerfSummary, bulkExecutionData: this.bulkExecutionData}
    };
    this.dialog.open(SampleMaxTimeTransactionViewComponent, conf);
  }
}
