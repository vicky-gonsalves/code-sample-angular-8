import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SampleMinTpsTransactionViewComponent } from './sample-min-tps-transaction-view.component';

describe('SampleMinTpsTransactionViewComponent', () => {
  let component: SampleMinTpsTransactionViewComponent;
  let fixture: ComponentFixture<SampleMinTpsTransactionViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SampleMinTpsTransactionViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SampleMinTpsTransactionViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
