import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {appAnimations} from '../../../../../core/animations/animations';
import {BulkExecutionInterface} from '../../../../../interface/bulk-execution.interface';
import {BulkTpsSummaryInterface} from '../../../../../interface/bulk-tps-summary.interface';

@Component({
  selector: 'sample-min-tps-transaction-view',
  templateUrl: './sample-min-tps-transaction-view.component.html',
  styleUrls: ['./sample-min-tps-transaction-view.component.scss'],
  animations: appAnimations
})
export class SampleMinTpsTransactionViewComponent implements OnInit {

  public bulkExecutionData: BulkExecutionInterface;
  public bulkTpsSummaryData: BulkTpsSummaryInterface;
  public inProgress: Boolean = false;

  constructor(@Inject(MAT_DIALOG_DATA) private data: any) {
    this.bulkExecutionData = data.bulkExecutionData;
    this.bulkTpsSummaryData = data.bulkTpsSummary;
  }

  ngOnInit() {
  }
}
