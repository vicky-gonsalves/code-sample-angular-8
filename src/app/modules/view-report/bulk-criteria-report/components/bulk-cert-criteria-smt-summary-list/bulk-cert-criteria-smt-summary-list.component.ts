import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {appAnimations} from '../../../../../core/animations/animations';
import {BulkCertCriteriaSmtSummaryInterface} from '../../../../../interface/bulk-cert-criteria-smt-summary.interface';
import {BulkCertCriteriaSmtSummaryService} from '../../../../../shared/services/bulk-cert-criteria-smt-summary/bulk-cert-criteria-smt-summary.service';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';

@Component({
  selector: 'bulk-cert-criteria-smt-summary-list',
  templateUrl: './bulk-cert-criteria-smt-summary-list.component.html',
  styleUrls: ['./bulk-cert-criteria-smt-summary-list.component.scss'],
  animations: appAnimations
})
export class BulkCertCriteriaSmtSummaryListComponent implements OnInit {
  @Input() runId: string;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  public displayedColumns = ['smt_cycle_id', 'account_id', 'accept_reject_code', 'reason_code', 'in_out', 'count', 'amount'];
  public dataSource: MatTableDataSource<BulkCertCriteriaSmtSummaryInterface>;
  public paginatorOptions = {
    count: 0,
    pageSize: 10,
    pageSizeOptions: [5, 10, 25, 50, 100],
    showFirstLastButtons: true
  };
  public inProgress: Boolean = true;
  public pageEvent: any;
  private sortBy = '-date_time';
  private pageSize = 10;

  constructor(private bulkCertCriteriaSmtSummaryService: BulkCertCriteriaSmtSummaryService,
              private errorMsgService: ErrorMessageService) {
  }

  ngOnInit() {
    this.fetchBulkCertCriteriaSmtSummaryListing();
  }

  public sortData(sort: Sort): void {
    const data = this.dataSource;
    if (!sort.active || sort.direction === '') {
      this.sortBy = '-date_time';
    } else {
      this.sortBy = sort.direction === 'asc' ? sort.active : '-' + sort.active;
    }
    this.paginator.pageIndex = 0;
    this.fetchBulkCertCriteriaSmtSummaryListing();
  }

  public fetchBulkCertCriteriaSmtSummaryListing(_pageEvent?: PageEvent): void {
    if (_pageEvent) {
      this.pageEvent = _pageEvent;
      this.pageSize = _pageEvent.pageSize;
    }
    const options = {
      limit: (this.pageEvent ? (this.pageEvent.pageSize) : this.pageSize ? this.pageSize : this.paginatorOptions.pageSize),
      page: (this.pageEvent ? (this.pageEvent.pageIndex + 1) : 1),
      // sort: [this.sortBy]
    };
    this.inProgress = true;
    this.bulkCertCriteriaSmtSummaryService.getBulkCertCriteriaSmtSummary(options, this.runId)
      .subscribe(response => {
          if (response && response.data && response.data.rows && response.data.rows.length) {
            this.dataSource = new MatTableDataSource(response.data.rows);
            this.paginatorOptions.count = response.data.count;
            // this.dataSource.sort = this.sort;
            /*TODO Sort is fixed at api level*/
          }
          this.inProgress = false;
        },
        err => {
          this.errorMsgService.handleServerErrors(err);
          this.inProgress = false;
        });
  }
}
