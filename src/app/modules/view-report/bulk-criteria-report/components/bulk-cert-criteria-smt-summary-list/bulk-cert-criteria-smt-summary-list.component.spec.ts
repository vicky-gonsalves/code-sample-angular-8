import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BulkCertCriteriaSmtSummaryListComponent } from './bulk-cert-criteria-smt-summary-list.component';

describe('BulkCertCriteriaSmtSummaryListComponent', () => {
  let component: BulkCertCriteriaSmtSummaryListComponent;
  let fixture: ComponentFixture<BulkCertCriteriaSmtSummaryListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BulkCertCriteriaSmtSummaryListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BulkCertCriteriaSmtSummaryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
