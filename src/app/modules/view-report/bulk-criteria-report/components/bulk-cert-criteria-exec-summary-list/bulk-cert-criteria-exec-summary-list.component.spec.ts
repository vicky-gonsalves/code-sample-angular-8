import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BulkCertCriteriaExecSummaryListComponent } from './bulk-cert-criteria-exec-summary-list.component';

describe('BulkCertCriteriaExecSummaryListComponent', () => {
  let component: BulkCertCriteriaExecSummaryListComponent;
  let fixture: ComponentFixture<BulkCertCriteriaExecSummaryListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BulkCertCriteriaExecSummaryListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BulkCertCriteriaExecSummaryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
