import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {appAnimations} from '../../../../../core/animations/animations';
import {BulkCertCriteriaExecSummaryInterface} from '../../../../../interface/bulk-cert-criteria-exec-summary.interface';
import {BulkCertCriteriaExecSummaryListService} from '../../../../../shared/services/bulk-cert-criteria-exec-summary-list/bulk-cert-criteria-exec-summary-list.service';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';

@Component({
  selector: 'bulk-cert-criteria-exec-summary-list',
  templateUrl: './bulk-cert-criteria-exec-summary-list.component.html',
  styleUrls: ['./bulk-cert-criteria-exec-summary-list.component.scss'],
  animations: appAnimations
})
export class BulkCertCriteriaExecSummaryListComponent implements OnInit {
  @Input() runId: string;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  public displayedColumns = ['transaction_type', 'msg_type', 'template_name', 'template_description', 'weightage', 'accept_reject_code', 'reason_code', 'count'];
  public dataSource: MatTableDataSource<BulkCertCriteriaExecSummaryInterface>;
  public paginatorOptions = {
    count: 0,
    pageSize: 10,
    pageSizeOptions: [5, 10, 25, 50, 100],
    showFirstLastButtons: true
  };
  public inProgress: Boolean = true;
  public pageEvent: any;
  private sortBy = '-date_time';
  private pageSize = 10;

  constructor(private bulkCertCriteriaExecSummaryListService: BulkCertCriteriaExecSummaryListService,
              private errorMsgService: ErrorMessageService) {
  }

  ngOnInit() {
    this.fetchBulkCertCriteriaExecSummaryListing();
  }

  public sortData(sort: Sort): void {
    const data = this.dataSource;
    if (!sort.active || sort.direction === '') {
      this.sortBy = '-date_time';
    } else {
      this.sortBy = sort.direction === 'asc' ? sort.active : '-' + sort.active;
    }
    this.paginator.pageIndex = 0;
    this.fetchBulkCertCriteriaExecSummaryListing();
  }

  public fetchBulkCertCriteriaExecSummaryListing(_pageEvent?: PageEvent): void {
    if (_pageEvent) {
      this.pageEvent = _pageEvent;
      this.pageSize = _pageEvent.pageSize;
    }
    const options = {
      limit: (this.pageEvent ? (this.pageEvent.pageSize) : this.pageSize ? this.pageSize : this.paginatorOptions.pageSize),
      page: (this.pageEvent ? (this.pageEvent.pageIndex + 1) : 1),
      // sort: [this.sortBy]
    };
    this.inProgress = true;
    this.bulkCertCriteriaExecSummaryListService.getBulkCertCriteriaExecSummary(options, this.runId)
      .subscribe(response => {
          if (response && response.data && response.data.rows && response.data.rows.length) {
            this.dataSource = new MatTableDataSource(response.data.rows);
            this.paginatorOptions.count = response.data.count;
            // this.dataSource.sort = this.sort;
            /*TODO Sort is fixed at api level*/
          }
          this.inProgress = false;
        },
        err => {
          this.errorMsgService.handleServerErrors(err);
          this.inProgress = false;
        });
  }
}
