import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LegSummaryViewComponent } from './leg-summary-view.component';

describe('LegSummaryViewComponent', () => {
  let component: LegSummaryViewComponent;
  let fixture: ComponentFixture<LegSummaryViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LegSummaryViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LegSummaryViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
