import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {appAnimations} from '../../../../../core/animations/animations';
import {BulkExecutionInterface} from '../../../../../interface/bulk-execution.interface';

@Component({
  selector: 'leg-summary-view',
  templateUrl: './leg-summary-view.component.html',
  styleUrls: ['./leg-summary-view.component.scss'],
  animations: appAnimations
})
export class LegSummaryViewComponent implements OnInit {
  public bulkExecutionData: BulkExecutionInterface;
  public inProgress: Boolean = false;

  constructor(@Inject(MAT_DIALOG_DATA) private data: any) {
    this.bulkExecutionData = data;
  }

  ngOnInit() {
  }
}
