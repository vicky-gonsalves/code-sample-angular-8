import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {appAnimations} from '../../../../../core/animations/animations';
import {BulkPerfSummaryMaxRecordInterface} from '../../../../../interface/bulk-perf-summary-max-record.interface';
import {BulkPerfSummaryService} from '../../../../../shared/services/bulk-perf-summary/bulk-perf-summary.service';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';

@Component({
  selector: 'sample-max-time-transaction-leg-detail-view',
  templateUrl: './sample-max-time-transaction-leg-detail-view.component.html',
  styleUrls: ['./sample-max-time-transaction-leg-detail-view.component.scss'],
  animations: appAnimations
})
export class SampleMaxTimeTransactionLegDetailViewComponent implements OnInit {
  @Input() runId: string;
  @Input() transactionType: string;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  public displayedColumns = ['instruction_id', 'leg2_leg1', 'leg3_leg2', 'leg3_leg4'];
  public dataSource: MatTableDataSource<BulkPerfSummaryMaxRecordInterface>;
  public paginatorOptions = {
    count: 0,
    pageSize: 10,
    pageSizeOptions: [5, 10, 25, 50, 100],
    showFirstLastButtons: true
  };
  public inProgress: Boolean = true;
  public pageEvent: any;
  private sortBy = '-instruction_id';
  private pageSize = 10;

  constructor(private bulkPerfSummaryService: BulkPerfSummaryService,
              private errorMsgService: ErrorMessageService) {
  }

  ngOnInit() {
    this.fetchBulkPerfSummaryMaxRecordListing();
  }

  public sortData(sort: Sort): void {
    const data = this.dataSource;
    if (!sort.active || sort.direction === '') {
      this.sortBy = '-instruction_id';
    } else {
      this.sortBy = sort.direction === 'asc' ? sort.active : '-' + sort.active;
    }
    this.paginator.pageIndex = 0;
    this.fetchBulkPerfSummaryMaxRecordListing();
  }

  public fetchBulkPerfSummaryMaxRecordListing(_pageEvent?: PageEvent): void {
    if (_pageEvent) {
      this.pageEvent = _pageEvent;
      this.pageSize = _pageEvent.pageSize;
    }
    const options = {
      limit: (this.pageEvent ? (this.pageEvent.pageSize) : this.pageSize ? this.pageSize : this.paginatorOptions.pageSize),
      page: (this.pageEvent ? (this.pageEvent.pageIndex + 1) : 1),
      sort: [this.sortBy]
    };
    this.inProgress = true;
    this.bulkPerfSummaryService.getBulkPerfSummaryMaxRecords(options, this.runId, this.transactionType)
      .subscribe(response => {
          if (response && response.data && response.data.rows && response.data.rows.length) {
            this.dataSource = new MatTableDataSource(response.data.rows);
            this.paginatorOptions.count = response.data.count;
            this.dataSource.sort = this.sort;
          }
          this.inProgress = false;
        },
        err => {
          this.errorMsgService.handleServerErrors(err);
          this.inProgress = false;
        });
  }
}
