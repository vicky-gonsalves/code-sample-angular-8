import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SampleMaxTimeTransactionLegDetailViewComponent } from './sample-max-time-transaction-leg-detail-view.component';

describe('SampleMaxTimeTransactionLegDetailViewComponent', () => {
  let component: SampleMaxTimeTransactionLegDetailViewComponent;
  let fixture: ComponentFixture<SampleMaxTimeTransactionLegDetailViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SampleMaxTimeTransactionLegDetailViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SampleMaxTimeTransactionLegDetailViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
