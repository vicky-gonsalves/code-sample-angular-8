import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SampleMinTpsTransactionLegDetailViewComponent } from './sample-min-tps-transaction-leg-detail-view.component';

describe('SampleMinTpsTransactionLegDetailViewComponent', () => {
  let component: SampleMinTpsTransactionLegDetailViewComponent;
  let fixture: ComponentFixture<SampleMinTpsTransactionLegDetailViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SampleMinTpsTransactionLegDetailViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SampleMinTpsTransactionLegDetailViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
