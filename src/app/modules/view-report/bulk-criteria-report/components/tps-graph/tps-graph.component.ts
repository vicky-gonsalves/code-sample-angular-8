import {Component, Input, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import * as d3 from 'd3';
import moment from 'moment-timezone';
import {appAnimations} from '../../../../../core/animations/animations';
import {BulkTpsDetailInterface} from '../../../../../interface/bulk-tps-detail.interface';
import {BulkTpsDetailService} from '../../../../../shared/services/bulk-tps-detail/bulk-tps-detail.service';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {UiConfigParameterService} from '../../../../../shared/services/ui-config-parameter/ui-config-parameter.service';
import {BulkTpsDetailsListComponent} from '../bulk-tps-details-list/bulk-tps-details-list.component';

@Component({
  selector: 'tps-graph',
  templateUrl: './tps-graph.component.html',
  styleUrls: ['./tps-graph.component.scss'],
  animations: appAnimations,
  encapsulation: ViewEncapsulation.None
})
export class TpsGraphComponent implements OnInit, OnDestroy {
  @Input() runId: string;
  @Input() rangeStart: Date;
  @Input() rangeEnd: Date;
  @Input() transactionType: string;

  public options: any;
  public data: any;
  public point: BulkTpsDetailInterface;
  public inProgress: Boolean = true;
  public rangeConfig: any;
  public range: any;
  private limit: number;


  constructor(private uiConfigParameterService: UiConfigParameterService,
              private dialog: MatDialog,
              private errorMsgService: ErrorMessageService,
              private bulkTpsDetailService: BulkTpsDetailService) {
    this.limit = parseInt(uiConfigParameterService.getLocalConfig('GRAPH_MAX_SELECTION_LIMIT'), 10) * 1000;
  }

  ngOnInit() {
    this.range = [this.timestamp(this.rangeStart), this.getMaxLimit()];
    this.rangeConfig = {
      behaviour: 'drag',
      connect: true,
      start: [this.timestamp(this.rangeStart), this.timestamp(this.rangeEnd)],
      limit: this.limit,
      range: {
        min: this.timestamp(this.rangeStart),
        max: this.timestamp(this.rangeEnd)
      },
      step: 1000,
      tooltips: [false, false],
      // pips: {
      //   mode: 'steps',
      //   density: 5
      // }
      format: {
        to: (str) => {
          return this.formatDate(str);
        },
        from: Number
      }
    };
    this.fetchBulkTpsDetails();
    this.setStackedAreaChartOptions();
  }

  public formatDate(str) {
    return this.convertTimezone(new Date(str));
  }

  setStackedAreaChartOptions() {
    this.options = {
      chart: {
        type: 'lineWithFocusChart',
        height: 450,
        margin: {
          top: 20,
          right: 40,
          bottom: 30,
          left: 40
        },
        // interpolate: 'basis',
        useInteractiveGuideline: true,
        duration: 0,
        x: function (d) {
          return d.leg_tmstmp;
        },
        y: function (d) {
          return d.total;
        },
        xAxis: {
          axisLabel: 'Time',
          tickFormat: (d) => {
            return this.convertTimezone(new Date(d)).format('HH:mm:ss');
          },
          // rotateLabels: -45
        },
        x2Axis: {
          tickFormat: (d) => {
            return this.convertTimezone(new Date(d)).format('HH:mm:ss');
          }
        },
        yAxis: {
          axisLabel: 'Count',
          tickFormat: (d) => {
            return d3.format(',f')(d);
          }
        },
        y2Axis: {
          tickFormat: (d) => {
            return d3.format(',f')(d);
          }
        },
        lines: {
          dispatch: {
            elementClick: (e) => {
              this.point = e[0].point;
              this.viewTPSDetailsList();
            },
            elementMouseover: (e) => {
            },
            elementMouseout: (e) => {
            },
            renderEnd: (e) => {
            }
          }
        }
      }
    };
  }

  public applyFilter() {
    this.fetchBulkTpsDetails();
  }

  public convertTimezone(date: Date): moment {
    return this.uiConfigParameterService.convertTimezone(date);
  }

  public viewTPSDetailsList(): void {
    if (this.point.leg_type && this.point.leg_type === 'LEG4') {
      const conf: MatDialogConfig = {
        autoFocus: false,
        closeOnNavigation: true,
        panelClass: 'panelClass',
        maxWidth: '1400px',
        data: {leg_tmstmp: this.point.leg_tmstmp, runId: this.runId, transactionType: this.transactionType}
      };
      this.dialog.open(BulkTpsDetailsListComponent, conf);
    } else {
      this.errorMsgService.openSnackBar(this.errorMsgService.getNonFormError('NOT_ALLOWED_FOR_INPUT'));
    }
  }

  ngOnDestroy() {
    const element = document.querySelector('.nvtooltip');
    if (element) {
      element.parentNode.removeChild(element);
    }
  }

  private getMaxLimit() {
    const dateTime1 = moment(this.timestamp(this.rangeEnd));
    const dateTime2 = moment(this.timestamp(this.rangeStart)).add(this.limit, 'milliseconds');
    if (dateTime2 > dateTime1) {
      return dateTime1;
    }
    return dateTime2;
  }

  private timestamp(str) {
    return new Date(str).getTime();
  }

  private fetchBulkTpsDetails(): void {
    let frmTimestamp, toTimestamp;

    if (this.range && this.range.length > 0) {
      frmTimestamp = moment(this.range[0]).toISOString();
      toTimestamp = moment(this.range[1]).toISOString();
    }
    this.inProgress = true;
    this.bulkTpsDetailService.getBulkTPSDetails({
      run_id: this.runId,
      transaction_type: this.transactionType,
      frmTimestamp: frmTimestamp,
      toTimestamp: toTimestamp
    }).subscribe(response => {
        if (response && response.data && response.data.rows && response.data.rows.length) {
          response.data.rows.forEach(row => {
            row.leg_tmstmp = this.convertTimezone(row.leg_tmstmp);
          });
          const graph1Data = response.data.rows.filter(_data => _data.leg_type === 'LEG4');
          const graph2Data = response.data.rows.filter(_data => _data.leg_type === 'LEG1');
          this.data = [
            {
              'key': 'Output',
              'values': graph1Data,
              'type': 'line',
              'yAxis': 0,
              'color': '#cc8621',
              'area': false
            },
            {
              'key': 'Input',
              'values': graph2Data,
              'type': 'line',
              'yAxis': 0,
              'color': '#44ae57',
              'area': false
            }];
        }
        this.inProgress = false;
      },
      err => {
        this.errorMsgService.handleServerErrors(err);
        this.inProgress = false;
      });
  }

}
