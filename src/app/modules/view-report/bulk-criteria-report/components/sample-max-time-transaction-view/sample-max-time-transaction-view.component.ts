import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material/dialog';
import {appAnimations} from '../../../../../core/animations/animations';
import {BulkExecutionInterface} from '../../../../../interface/bulk-execution.interface';
import {BulkPerfSummaryInterface} from '../../../../../interface/bulk-perf-summary.interface';

@Component({
  selector: 'sample-max-time-transaction-view',
  templateUrl: './sample-max-time-transaction-view.component.html',
  styleUrls: ['./sample-max-time-transaction-view.component.scss'],
  animations: appAnimations
})
export class SampleMaxTimeTransactionViewComponent implements OnInit {

  public bulkExecutionData: BulkExecutionInterface;
  public bulkPerfSummaryData: BulkPerfSummaryInterface;
  public inProgress: Boolean = false;

  constructor(@Inject(MAT_DIALOG_DATA) private data: any,
              private dialog: MatDialog) {
    this.bulkExecutionData = data.bulkExecutionData;
    this.bulkPerfSummaryData = data.bulkPerfSummary;
  }

  ngOnInit() {
  }
}
