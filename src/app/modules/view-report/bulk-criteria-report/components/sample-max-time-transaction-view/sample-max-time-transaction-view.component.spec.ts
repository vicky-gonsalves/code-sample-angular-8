import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SampleMaxTimeTransactionViewComponent } from './sample-max-time-transaction-view.component';

describe('SampleMaxTimeTransactionViewComponent', () => {
  let component: SampleMaxTimeTransactionViewComponent;
  let fixture: ComponentFixture<SampleMaxTimeTransactionViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SampleMaxTimeTransactionViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SampleMaxTimeTransactionViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
