import {Component, OnInit} from '@angular/core';
import {appAnimations} from '../../core/animations/animations';

@Component({
  selector: 'view-report',
  templateUrl: './view-report.component.html',
  animations: appAnimations
})
export class ViewReportComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
