import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserDataGuard} from '../../../core/guards/user-data/user-data.guard';
import {CertificationCriteriaListComponent} from './components/certification-criteria-list/certification-criteria-list.component';
import {CertificationCriteriaViewComponent} from './components/certification-criteria-view/certification-criteria-view.component';
import {CertificateCriteriaReportComponent} from './certificate-criteria-report.component';

export const routes: Routes = [
  {
    path: '',
    canActivate: [UserDataGuard],
    component: CertificateCriteriaReportComponent,
    children: [
      {
        path: '',
        canActivateChild: [UserDataGuard],
        component: CertificationCriteriaListComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'certification-criteria-report'}
      },
      {
        path: 'view/:id',
        canActivateChild: [UserDataGuard],
        component: CertificationCriteriaViewComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'certification-criteria-report-view'}
      }
    ]
  },
  {path: '**', redirectTo: '', pathMatch: 'full'}
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);

