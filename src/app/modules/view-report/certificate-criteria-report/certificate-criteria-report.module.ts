import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CertificateCriteriaReportComponent} from './certificate-criteria-report.component';
import {CertificationCriteriaListComponent} from './components/certification-criteria-list/certification-criteria-list.component';
import {routing} from './certificate-criteria-report.routes';
import {SharedModule} from '../../../shared/shared.module';
import {CertificationCriteriaViewComponent} from './components/certification-criteria-view/certification-criteria-view.component';
import {CertificationCriteriaRepOutDetailListComponent} from './components/certification-criteria-rep-out-detail-list/certification-criteria-rep-out-detail-list.component';
import {CertificationCriteriaRepDtlOutListComponent} from './components/certification-criteria-rep-dtl-out-list/certification-criteria-rep-dtl-out-list.component';
import {CertificationCriteriaValidationListComponent} from './components/certification-criteria-validation-list/certification-criteria-validation-list.component';
import {CertificationCriteriaFieldDetailsListComponent} from './components/certification-criteria-field-details-list/certification-criteria-field-details-list.component';
import {ViewPaymentTransactionsComponent} from './components/view-payment-transactions/view-payment-transactions.component';
import {NonPaymentLegListComponent} from './components/payment-related-transactions/non-payment-leg-list/non-payment-leg-list.component';
import {PaymentXmlViewerComponent} from './components/payment-transaction/payment-xml-viewer/payment-xml-viewer.component';
import {NonPaymentXmlViewerComponent} from './components/payment-related-transactions/non-payment-xml-viewer/non-payment-xml-viewer.component';
import {TransactionDetailViewComponent} from './components/payment-transaction/transaction-detail-view/transaction-detail-view.component';
import {NonTransactionDetailViewComponent} from './components/payment-related-transactions/non-transaction-detail-view/non-transaction-detail-view.component';
import {PaymentLegListComponent} from './components/payment-transaction/payment-leg-list/payment-leg-list.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    routing
  ],
  declarations: [
    CertificateCriteriaReportComponent,
    CertificationCriteriaListComponent,
    CertificationCriteriaViewComponent,
    CertificationCriteriaRepOutDetailListComponent,
    CertificationCriteriaRepDtlOutListComponent,
    CertificationCriteriaValidationListComponent,
    CertificationCriteriaFieldDetailsListComponent,
    ViewPaymentTransactionsComponent,
    PaymentLegListComponent,
    NonPaymentLegListComponent,
    PaymentXmlViewerComponent,
    NonPaymentXmlViewerComponent,
    TransactionDetailViewComponent,
    NonTransactionDetailViewComponent
  ],
  entryComponents: [
    CertificationCriteriaRepDtlOutListComponent,
    CertificationCriteriaValidationListComponent,
    CertificationCriteriaFieldDetailsListComponent,
    ViewPaymentTransactionsComponent,
    PaymentLegListComponent,
    NonPaymentLegListComponent,
    PaymentXmlViewerComponent,
    NonPaymentXmlViewerComponent,
    NonTransactionDetailViewComponent,
    TransactionDetailViewComponent
  ]
})
export class CertificateCriteriaReportModule {
}
