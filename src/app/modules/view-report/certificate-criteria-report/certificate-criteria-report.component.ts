import {Component, OnInit} from '@angular/core';
import {appAnimations} from '../../../core/animations/animations';

@Component({
  selector: 'certificate-criteria-report',
  templateUrl: './certificate-criteria-report.component.html',
  styleUrls: ['./certificate-criteria-report.component.scss'],
  animations: appAnimations
})
export class CertificateCriteriaReportComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
