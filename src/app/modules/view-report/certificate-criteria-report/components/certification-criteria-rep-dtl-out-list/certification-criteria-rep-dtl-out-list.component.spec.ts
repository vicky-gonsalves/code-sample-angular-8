import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CertificationCriteriaRepDtlOutListComponent} from './certification-criteria-rep-dtl-out-list.component';

describe('CertificationCriteriaRepDtlOutListComponent', () => {
  let component: CertificationCriteriaRepDtlOutListComponent;
  let fixture: ComponentFixture<CertificationCriteriaRepDtlOutListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CertificationCriteriaRepDtlOutListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertificationCriteriaRepDtlOutListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
