import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogConfig, MatDialogRef} from '@angular/material/dialog';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import moment from 'moment-timezone';
import {CertCriteriaRepDtlOutInterface} from '../../../../../interface/cert-criteria-rep-dtl-out.interface';
import {CertCriteriaRepOutInterface, CertCriteriaTemplateResultsInterface} from '../../../../../interface/cert-criteria-rep-out.interface';
import {CertCriteriaRepDtlOutService} from '../../../../../shared/services/cert-criteria-rep-dtl-out/cert-criteria-rep-dtl-out.service';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {UiConfigParameterService} from '../../../../../shared/services/ui-config-parameter/ui-config-parameter.service';
import {CertificationCriteriaFieldValuesDetailListComponent} from '../../../../certification-execution/exec-certification-criteria/components/certification-criteria-field-values-detail-list/certification-criteria-field-values-detail-list.component';
import {CertificationCriteriaValidationListComponent} from '../certification-criteria-validation-list/certification-criteria-validation-list.component';
import {ViewPaymentTransactionsComponent} from '../view-payment-transactions/view-payment-transactions.component';

@Component({
  selector: 'certification-criteria-rep-dtl-out-list',
  templateUrl: './certification-criteria-rep-dtl-out-list.component.html',
  styleUrls: ['./certification-criteria-rep-dtl-out-list.component.scss']
})
export class CertificationCriteriaRepDtlOutListComponent implements OnInit {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  public displayedColumns =
    ['instruction_id', 'initiating_msg_type', 'msg_rcvd_dttm', 'status', 'status_update_dttm', 'actions'];
  public dataSource: MatTableDataSource<CertCriteriaRepOutInterface>;
  public paginatorOptions = {
    count: 0,
    pageSize: 10,
    pageSizeOptions: [5, 10, 25, 50, 100],
    showFirstLastButtons: true
  };
  public inProgress: Boolean = true;
  public pageEvent: any;
  public certCriteriaRepOut: CertCriteriaRepOutInterface;
  public template: CertCriteriaTemplateResultsInterface;
  private sortBy = '-status_update_dttm';
  private pageSize = 10;

  constructor(private dialogRef: MatDialogRef<CertificationCriteriaFieldValuesDetailListComponent>,
              @Inject(MAT_DIALOG_DATA) private data: any,
              private dialog: MatDialog,
              private certCriteriaRepDtlOutService: CertCriteriaRepDtlOutService,
              private uiConfigParameterService: UiConfigParameterService,
              private errorMsgService: ErrorMessageService) {
    this.certCriteriaRepOut = this.data.certCriteriaRepOut;
    this.template = this.data.template;
  }

  ngOnInit() {
    this.fetchCertCriteriaRepDtlOut();
  }

  public sortData(sort: Sort): void {
    if (!sort.active || sort.direction === '') {
      this.sortBy = '-status_update_dttm';
    } else {
      this.sortBy = sort.direction === 'asc' ? sort.active : '-' + sort.active;
    }
    this.paginator.pageIndex = 0;
    this.fetchCertCriteriaRepDtlOut();
  }

  public fetchCertCriteriaRepDtlOut(_pageEvent?: PageEvent): void {
    if (_pageEvent) {
      this.pageEvent = _pageEvent;
      this.pageSize = _pageEvent.pageSize;
    }
    const options = {
      limit: (this.pageEvent ? (this.pageEvent.pageSize) : this.pageSize ? this.pageSize : this.paginatorOptions.pageSize),
      page: (this.pageEvent ? (this.pageEvent.pageIndex + 1) : 1),
      sort: [this.sortBy]
    };

    this.inProgress = true;
    this.certCriteriaRepDtlOutService.getCertCriteriaRepDtlOutList(options,
      this.certCriteriaRepOut.participant_id,
      this.certCriteriaRepOut.criteria_name,
      this.template.template_name)
      .subscribe(response => {
          if (response && response.data && response.data.rows && response.data.rows.length) {
            response.data.rows.forEach(row => {
              row.msg_rcvd_dttm = this.convertTimezone(row.msg_rcvd_dttm);
              row.status_update_dttm = this.convertTimezone(row.status_update_dttm);
            });
            this.dataSource = new MatTableDataSource(response.data.rows);
            this.paginatorOptions.count = response.data.count;
            this.dataSource.sort = this.sort;
          } else {
            this.errorMsgService.openSnackBar(this.errorMsgService.getNonFormError('NO_DATA'));
          }
          this.inProgress = false;
        },
        err => {
          this.errorMsgService.handleServerErrors(err);
          this.inProgress = false;
        });
  }

  public convertTimezone(date: Date): moment {
    return this.uiConfigParameterService.convertTimezone(date);
  }

  public validationDetails(certCriteriaRepDtlOut: CertCriteriaRepDtlOutInterface): void {
    const conf: MatDialogConfig = {
      autoFocus: false,
      closeOnNavigation: true,
      panelClass: 'panelClass',
      maxWidth: '1400px',
      backdropClass: 'transparent-backdrop',
      data: {template: this.template, certCriteriaRepDtlOut: certCriteriaRepDtlOut, certCriteriaRepOut: this.certCriteriaRepOut}
    };
    this.dialog.open(CertificationCriteriaValidationListComponent, conf);
  }

  public viewPaymentTransactions(certCriteriaRepDtlOut: CertCriteriaRepDtlOutInterface): void {
    const conf: MatDialogConfig = {
      autoFocus: false,
      closeOnNavigation: true,
      panelClass: 'panelClass',
      maxWidth: '1800px',
      backdropClass: 'transparent-backdrop',
      data: {
        ...this.certCriteriaRepOut,
        ...this.template,
        ...certCriteriaRepDtlOut
      }
    };
    this.dialog.open(ViewPaymentTransactionsComponent, conf);
  }

}
