import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewPaymentTransactionsComponent } from './view-payment-transactions.component';

describe('ViewPaymentTransactionsComponent', () => {
  let component: ViewPaymentTransactionsComponent;
  let fixture: ComponentFixture<ViewPaymentTransactionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewPaymentTransactionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewPaymentTransactionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
