import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import moment from 'moment-timezone';
import {Identity} from '../../../../../core/services/identity/identity';
import {IdentityService} from '../../../../../core/services/identity/identity.service';
import {CertCriteriaRepDtlOutInterface} from '../../../../../interface/cert-criteria-rep-dtl-out.interface';
import {CertCriteriaRepOutInterface, CertCriteriaTemplateResultsInterface} from '../../../../../interface/cert-criteria-rep-out.interface';
import {NonPaymentTransactionInterface} from '../../../../../interface/non-payment-transaction.interface';
import {PaymentTransactionInterface} from '../../../../../interface/payment-transaction.interface';
import {CertCriteriaExecLogService} from '../../../../../shared/services/cert-criteria-exec-log/cert-criteria-exec-log.service';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {TransactionTypeService} from '../../../../../shared/services/transaction-type/transaction-type.service';
import {UiConfigParameterService} from '../../../../../shared/services/ui-config-parameter/ui-config-parameter.service';
import {NonPaymentLegListComponent} from '../payment-related-transactions/non-payment-leg-list/non-payment-leg-list.component';
import {NonTransactionDetailViewComponent} from '../payment-related-transactions/non-transaction-detail-view/non-transaction-detail-view.component';
import {PaymentLegListComponent} from '../payment-transaction/payment-leg-list/payment-leg-list.component';
import {TransactionDetailViewComponent} from '../payment-transaction/transaction-detail-view/transaction-detail-view.component';

@Component({
  selector: 'view-payment-transactions',
  templateUrl: './view-payment-transactions.component.html',
  styleUrls: ['./view-payment-transactions.component.scss']
})
export class ViewPaymentTransactionsComponent implements OnInit {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  public displayedColumns =
    ['id', 'transaction_type', 'creation_tmstmp', 'creditor_participant_id',
      'debtor_participant_id', 'sender_participant_id', 'receiver_participant_id', 'creditor_routing_id',
      'creditor_account_id', 'debtor_routing_id', 'debtor_account_id', 'amount', 'accept_reject_code', 'actions'];
  public dataSource: MatTableDataSource<CertCriteriaRepOutInterface>;
  public paginatorOptions = {
    count: 0,
    pageSize: 10,
    pageSizeOptions: [5, 10, 25, 50, 100],
    showFirstLastButtons: true
  };
  public hasPermissionToViewMessageLegs: Boolean = false;
  public hasPermissionToViewTransactions: Boolean = false;
  public inProgress: Boolean = true;
  public pageEvent: any;
  public transaction: any;
  public certCriteriaRepDtlOut: CertCriteriaRepDtlOutInterface;
  public template: CertCriteriaTemplateResultsInterface;
  private sortBy = '-creation_tmstmp';
  private pageSize = 10;
  private identity: Identity;

  constructor(@Inject(MAT_DIALOG_DATA) private data: any,
              private certCriteriaExecLogService: CertCriteriaExecLogService,
              private transactionTypeService: TransactionTypeService,
              private dialog: MatDialog,
              private uiConfigParameterService: UiConfigParameterService,
              private errorMsgService: ErrorMessageService) {
    IdentityService.identity.subscribe(contactIdentity => {
      this.identity = contactIdentity;
      if (IdentityService.hasPrivileges(['viewMessageLegs'])) {
        this.hasPermissionToViewMessageLegs = true;
      }
      if (IdentityService.hasPrivileges(['viewTransactions'])) {
        this.hasPermissionToViewTransactions = true;
      }
    });
    this.transaction = this.data;
  }

  ngOnInit() {
    this.fetchPaymentTransaction();
  }

  public sortData(sort: Sort): void {
    if (!sort.active || sort.direction === '') {
      this.sortBy = '-_id';
    } else {
      this.sortBy = sort.direction === 'asc' ? sort.active : '-' + sort.active;
    }
    this.paginator.pageIndex = 0;
    this.fetchPaymentTransaction();
  }

  public fetchPaymentTransaction(_pageEvent?: PageEvent): void {
    if (_pageEvent) {
      this.pageEvent = _pageEvent;
      this.pageSize = _pageEvent.pageSize;
    }
    const options = {
      limit: (this.pageEvent ? (this.pageEvent.pageSize) : this.pageSize ? this.pageSize : this.paginatorOptions.pageSize),
      page: (this.pageEvent ? (this.pageEvent.pageIndex + 1) : 1),
      sort: [this.sortBy]
    };

    this.inProgress = true;
    this.certCriteriaExecLogService.getTransactions(options, {
      participant_id: this.transaction.participant_id,
      criteria_name: this.transaction.criteria_name,
      template_name: this.transaction.template_name,
      initiating_msg_type: this.transaction.initiating_msg_type,
      instruction_id: this.transaction.instruction_id
    })
      .subscribe(response => {
          if (response && response.data && response.data.rows && response.data.rows.length) {
            response.data.rows.forEach(row => {
              row.creation_tmstmp = this.convertTimezone(row.creation_tmstmp);
            });
            const storedResults = response.data.rows.map(d => {
              d.amount = d.amount ? d.amount.$numberDecimal : null;
              return d;
            });
            this.dataSource = new MatTableDataSource(storedResults);
            this.paginatorOptions.count = response.data.count;
            this.dataSource.sort = this.sort;
          } else {
            this.errorMsgService.openSnackBar(this.errorMsgService.getNonFormError('NO_DATA'));
          }
          this.inProgress = false;
        },
        err => {
          this.errorMsgService.handleServerErrors(err);
          this.inProgress = false;
        });
  }

  public viewTransactionDetails(transaction: PaymentTransactionInterface | NonPaymentTransactionInterface): void {
    const conf: MatDialogConfig = {
      autoFocus: false,
      closeOnNavigation: true,
      panelClass: 'panelClass',
      maxWidth: '1400px',
      backdropClass: 'transparent-backdrop',
      data: transaction
    };
    this.transactionTypeService.getType({id: transaction.transaction_type}).subscribe(response => {
        if (response && response.data) {
          if (response.data.transaction_type === 'PAYMENT') {
            this.dialog.open(TransactionDetailViewComponent, conf);
          } else {
            this.dialog.open(NonTransactionDetailViewComponent, conf);
          }
        } else {
          this.errorMsgService.openSnackBar(this.errorMsgService.getNonFormError('NO_DATA'));
        }
        this.inProgress = false;
      },
      err => {
        this.errorMsgService.handleServerErrors(err);
        this.inProgress = false;
      });
  }

  public viewMessageLegDetail(transaction?: PaymentTransactionInterface | NonPaymentTransactionInterface): void {
    const conf: MatDialogConfig = {
      autoFocus: false,
      closeOnNavigation: true,
      panelClass: 'panelClass',
      maxWidth: '1400px',
      data: transaction
    };
    this.transactionTypeService.getType({id: transaction.transaction_type}).subscribe(response => {
        if (response && response.data) {
          if (response.data.transaction_type === 'PAYMENT') {
            this.dialog.open(PaymentLegListComponent, conf);
          } else {
            this.dialog.open(NonPaymentLegListComponent, conf);
          }
        } else {
          this.errorMsgService.openSnackBar(this.errorMsgService.getNonFormError('NO_DATA'));
        }
        this.inProgress = false;
      },
      err => {
        this.errorMsgService.handleServerErrors(err);
        this.inProgress = false;
      });
  }

  public convertTimezone(date: Date): moment {
    return this.uiConfigParameterService.convertTimezone(date);
  }

}
