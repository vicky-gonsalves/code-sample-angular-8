import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CertificationCriteriaFieldDetailsListComponent} from './certification-criteria-field-details-list.component';

describe('CertificationCriteriaFieldDetailsListComponent', () => {
  let component: CertificationCriteriaFieldDetailsListComponent;
  let fixture: ComponentFixture<CertificationCriteriaFieldDetailsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CertificationCriteriaFieldDetailsListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertificationCriteriaFieldDetailsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
