import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import moment from 'moment-timezone';
import {Subscription} from 'rxjs';
import {appAnimations} from '../../../../../core/animations/animations';
import {CertCriteriaInterface} from '../../../../../interface/cert-criteria.interface';
import {NavItemInterface} from '../../../../../interface/nav-item.interface';
import {CertCriteriaRepOutService} from '../../../../../shared/services/cert-criteria-rep-out/cert-criteria-rep-out.service';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {NavItemService} from '../../../../../shared/services/nav-item/nav-item.service';
import {PageHeaderService} from '../../../../../shared/services/page-header/page-header.service';
import {PageTitleService} from '../../../../../shared/services/page-title/page-title.service';
import {RefreshService} from '../../../../../shared/services/refresh/refresh.service';
import {UiConfigParameterService} from '../../../../../shared/services/ui-config-parameter/ui-config-parameter.service';

@Component({
  selector: 'certification-criteria-list',
  templateUrl: './certification-criteria-list.component.html',
  styleUrls: ['./certification-criteria-list.component.scss'],
  animations: appAnimations
})
export class CertificationCriteriaListComponent implements OnInit, OnDestroy {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  public searchTerm: {
    participant_id: string,
    criteria_name: string
  };
  public displayedColumns =
    ['actions', 'criteria_name', 'criteria_type', 'criteria_description', 'criteria_status', 'last_update_dttm', 'participant_id'];
  public dataSource: MatTableDataSource<CertCriteriaInterface>;
  public paginatorOptions = {
    count: 0,
    pageSize: 10,
    pageSizeOptions: [5, 10, 25, 50, 100],
    showFirstLastButtons: true
  };
  public inProgress: Boolean = true;
  private sortBy = '-last_update_dttm';
  private pageSize = 10;
  private componentNavItem: NavItemInterface;
  private _lastPageEvent: PageEvent;
  private _lastOptions: any;
  private refreshSubscription: Subscription;

  constructor(private navItems: NavItemService,
              private certCriteriaRepOutService: CertCriteriaRepOutService,
              private uiConfigParameterService: UiConfigParameterService,
              private errorMsgService: ErrorMessageService,
              private pageTitle: PageTitleService,
              private refreshService: RefreshService) {
    this.searchTerm = {
      participant_id: null,
      criteria_name: null
    };

    this.refreshSubscription = this.refreshService.refreshObservable$.subscribe((res) => {
      if (res.hasOwnProperty('refresh') && res.refresh) {
        this.fetchCertCriteriaRepOutListing(this._lastPageEvent, this._lastOptions);
      }
    });

  }

  ngOnInit() {
    // Get Navigation Component Name
    this.componentNavItem = this.navItems.getItemById('certification-criteria-report');
    if (this.componentNavItem) {
      // Set Page Title
      this.pageTitle.setTitle(this.componentNavItem.name);
    }

    // Set Page Header Details
    PageHeaderService.updatePageHeader({
      showImage: true,
      icon: 'report',
      refreshButton: true
    });

    // Fetch CertCriterias Rep Out
    this.fetchCertCriteriaRepOutListing();
  }

  ngOnDestroy() {
    this.refreshSubscription.unsubscribe();
  }

  public sortData(sort: Sort): void {
    const data = this.dataSource;
    if (!sort.active || sort.direction === '') {
      this.sortBy = '-last_update_dttm';
    } else {
      this.sortBy = sort.direction === 'asc' ? sort.active : '-' + sort.active;
    }
    this.paginator.pageIndex = 0;
    this.fetchCertCriteriaRepOutListing();
  }

  public fetchCertCriteriaRepOutListing(pageEvent?: PageEvent, lastOptions?: any): void {
    if (pageEvent) {
      this.pageSize = pageEvent.pageSize;
      this._lastPageEvent = pageEvent;
    }
    let options = null;
    if (lastOptions) {
      options = lastOptions;
    } else {
      options = {
        limit: (pageEvent ? (pageEvent.pageSize) : this.pageSize ? this.pageSize : this.paginatorOptions.pageSize),
        page: (pageEvent ? (pageEvent.pageIndex + 1) : 1),
        sort: [this.sortBy]
      };
      if (this.searchTerm.criteria_name) {
        options['criteria_name'] = this.searchTerm.criteria_name;
      }
      if (this.searchTerm.participant_id) {
        options['participant_id'] = this.searchTerm.participant_id;
      }
      this._lastOptions = options;
    }
    this.inProgress = true;
    this.certCriteriaRepOutService.getCertCriteriaRepOutList(options)
      .subscribe(response => {
          if (response && response.data && response.data.rows && response.data.rows.length) {
            response.data.rows.forEach(row => {
              row.last_update_dttm = this.convertTimezone(row.last_update_dttm);
            });
            this.dataSource = new MatTableDataSource(response.data.rows);
            this.paginatorOptions.count = response.data.count;
            this.dataSource.sort = this.sort;
          } else {
            this.errorMsgService.openSnackBar(this.errorMsgService.getNonFormError('NO_DATA'));
          }
          this.inProgress = false;
        },
        err => {
          this.errorMsgService.handleServerErrors(err);
          this.inProgress = false;
        });
  }

  public applyFilter(searchCriteria: { key: string, value: string }): void {
    this.searchTerm[searchCriteria.key] = searchCriteria.value;
    this.paginator.pageIndex = 0;
    this.fetchCertCriteriaRepOutListing();
  }

  public getFirstTestTemplateName(templates): string {
    return templates[0];
  }

  public getOtherTestTemplateNames(templates): string | null {
    if (templates && templates.length > 1) {
      return templates.join(', ');
    }
    return null;
  }

  public convertTimezone(date: Date): moment {
    return this.uiConfigParameterService.convertTimezone(date);
  }
}
