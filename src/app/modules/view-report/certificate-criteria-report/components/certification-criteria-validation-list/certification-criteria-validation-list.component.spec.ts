import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CertificationCriteriaValidationListComponent} from './certification-criteria-validation-list.component';

describe('CertificationCriteriaValidationListComponent', () => {
  let component: CertificationCriteriaValidationListComponent;
  let fixture: ComponentFixture<CertificationCriteriaValidationListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CertificationCriteriaValidationListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertificationCriteriaValidationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
