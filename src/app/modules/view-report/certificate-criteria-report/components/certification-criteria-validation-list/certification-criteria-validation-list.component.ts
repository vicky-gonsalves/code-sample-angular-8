import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogConfig, MatDialogRef} from '@angular/material/dialog';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import moment from 'moment-timezone';
import {
  CertCriteriaRepDtlOutInterface,
  CertCriteriaRepDtlOutValCriteriaResultsInterface
} from '../../../../../interface/cert-criteria-rep-dtl-out.interface';
import {CertCriteriaRepOutInterface, CertCriteriaTemplateResultsInterface} from '../../../../../interface/cert-criteria-rep-out.interface';
import {CertCriteriaRepDtlOutService} from '../../../../../shared/services/cert-criteria-rep-dtl-out/cert-criteria-rep-dtl-out.service';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {UiConfigParameterService} from '../../../../../shared/services/ui-config-parameter/ui-config-parameter.service';
import {CertificationCriteriaFieldValuesDetailListComponent} from '../../../../certification-execution/exec-certification-criteria/components/certification-criteria-field-values-detail-list/certification-criteria-field-values-detail-list.component';
import {CertificationCriteriaFieldDetailsListComponent} from '../certification-criteria-field-details-list/certification-criteria-field-details-list.component';

@Component({
  selector: 'certification-criteria-validation-list',
  templateUrl: './certification-criteria-validation-list.component.html',
  styleUrls: ['./certification-criteria-validation-list.component.scss']
})
export class CertificationCriteriaValidationListComponent implements OnInit {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  public displayedColumns =
    ['validation_name', 'source_msg_type', 'source_msg_leg', 'target_msg_type',
      'target_msg_leg', 'validation_status', 'reason_code', 'actions'];
  public dataSource: MatTableDataSource<CertCriteriaRepOutInterface>;
  public paginatorOptions = {
    count: 0,
    pageSize: 10,
    pageSizeOptions: [5, 10, 25, 50, 100],
    showFirstLastButtons: true
  };
  public inProgress: Boolean = true;
  public pageEvent: any;
  public certCriteriaRepOut: CertCriteriaRepOutInterface;
  public certCriteriaRepDtlOut: CertCriteriaRepDtlOutInterface;
  public template: CertCriteriaTemplateResultsInterface;
  private sortBy = '-createdAt';
  private pageSize = 10;

  constructor(private dialogRef: MatDialogRef<CertificationCriteriaFieldValuesDetailListComponent>,
              @Inject(MAT_DIALOG_DATA) private data: any,
              private certCriteriaRepDtlOutService: CertCriteriaRepDtlOutService,
              private dialog: MatDialog,
              private uiConfigParameterService: UiConfigParameterService,
              private errorMsgService: ErrorMessageService) {
    this.certCriteriaRepOut = this.data.certCriteriaRepOut;
    this.certCriteriaRepDtlOut = this.data.certCriteriaRepDtlOut;
    this.template = this.data.template;
  }

  ngOnInit() {
    this.fetchCertCriteriaRepDtlOutValidation();
  }

  public sortData(sort: Sort): void {
    if (!sort.active || sort.direction === '') {
      this.sortBy = '-_id';
    } else {
      this.sortBy = sort.direction === 'asc' ? sort.active : '-' + sort.active;
    }
    this.paginator.pageIndex = 0;
    this.fetchCertCriteriaRepDtlOutValidation();
  }

  public fetchCertCriteriaRepDtlOutValidation(_pageEvent?: PageEvent): void {
    if (_pageEvent) {
      this.pageEvent = _pageEvent;
      this.pageSize = _pageEvent.pageSize;
    }
    const options = {
      limit: (this.pageEvent ? (this.pageEvent.pageSize) : this.pageSize ? this.pageSize : this.paginatorOptions.pageSize),
      page: (this.pageEvent ? (this.pageEvent.pageIndex + 1) : 1),
      sort: [this.sortBy]
    };

    this.inProgress = true;
    this.certCriteriaRepDtlOutService.getCertCriteriaRepDtlOutValidationList(options, this.certCriteriaRepDtlOut._id)
      .subscribe(response => {
          if (response && response.data && response.data.rows && response.data.rows.length) {
            this.dataSource = new MatTableDataSource(response.data.rows);
            this.paginatorOptions.count = response.data.count;
            this.dataSource.sort = this.sort;
          } else {
            this.errorMsgService.openSnackBar(this.errorMsgService.getNonFormError('NO_DATA'));
          }
          this.inProgress = false;
        },
        err => {
          this.errorMsgService.handleServerErrors(err);
          this.inProgress = false;
        });
  }

  public fieldDetails(validation: CertCriteriaRepDtlOutValCriteriaResultsInterface): void {
    const conf: MatDialogConfig = {
      autoFocus: false,
      closeOnNavigation: true,
      panelClass: 'panelClass',
      maxWidth: '1400px',
      backdropClass: 'transparent-backdrop',
      data: {
        validation: validation,
        template: this.template,
        certCriteriaRepDtlOut: this.certCriteriaRepDtlOut,
        certCriteriaRepOut: this.certCriteriaRepOut
      }
    };
    this.dialog.open(CertificationCriteriaFieldDetailsListComponent, conf);
  }

  public convertTimezone(date: Date): moment {
    return this.uiConfigParameterService.convertTimezone(date);
  }

}
