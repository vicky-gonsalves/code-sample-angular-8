import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'payment-xml-viewer',
  templateUrl: './payment-xml-viewer.component.html',
  styleUrls: ['./payment-xml-viewer.component.scss']
})
export class PaymentXmlViewerComponent implements OnInit {
  public inProgress: Boolean = false;
  public xmlData: string;

  constructor(@Inject(MAT_DIALOG_DATA) private data: any) {
    this.xmlData = data.xmlData;
  }

  ngOnInit() {
  }

}
