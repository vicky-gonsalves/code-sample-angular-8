import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PaymentXmlViewerComponent} from './payment-xml-viewer.component';

describe('PaymentXmlViewerComponent', () => {
  let component: PaymentXmlViewerComponent;
  let fixture: ComponentFixture<PaymentXmlViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PaymentXmlViewerComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentXmlViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
