import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'payment-xml-viewer',
  templateUrl: './non-payment-xml-viewer.component.html',
  styleUrls: ['./non-payment-xml-viewer.component.scss']
})
export class NonPaymentXmlViewerComponent implements OnInit {
  public inProgress: Boolean = false;
  public xmlData: string;

  constructor(@Inject(MAT_DIALOG_DATA) private data: any) {
    this.xmlData = data.xmlData;
  }

  ngOnInit() {
  }

}
