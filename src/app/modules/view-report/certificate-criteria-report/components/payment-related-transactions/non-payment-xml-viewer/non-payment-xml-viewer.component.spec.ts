import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NonPaymentXmlViewerComponent} from './non-payment-xml-viewer.component';

describe('NonPaymentXmlViewerComponent', () => {
  let component: NonPaymentXmlViewerComponent;
  let fixture: ComponentFixture<NonPaymentXmlViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NonPaymentXmlViewerComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NonPaymentXmlViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
