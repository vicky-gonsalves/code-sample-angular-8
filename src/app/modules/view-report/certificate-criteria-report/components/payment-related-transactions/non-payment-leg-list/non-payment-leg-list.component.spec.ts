import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NonPaymentLegListComponent} from './non-payment-leg-list.component';

describe('NonPaymentLegListComponent', () => {
  let component: NonPaymentLegListComponent;
  let fixture: ComponentFixture<NonPaymentLegListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NonPaymentLegListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NonPaymentLegListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
