import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NonTransactionDetailViewComponent} from './non-transaction-detail-view.component';

describe('NonTransactionDetailViewComponent', () => {
  let component: NonTransactionDetailViewComponent;
  let fixture: ComponentFixture<NonTransactionDetailViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NonTransactionDetailViewComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NonTransactionDetailViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
