import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import moment from 'moment-timezone';
import {appAnimations} from '../../../../../core/animations/animations';
import {CertCriteriaRepOutInterface, CertCriteriaTemplateResultsInterface} from '../../../../../interface/cert-criteria-rep-out.interface';
import {TestTemplateInterface} from '../../../../../interface/test-template.interface';
import {CertCriteriaRepOutService} from '../../../../../shared/services/cert-criteria-rep-out/cert-criteria-rep-out.service';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {UiConfigParameterService} from '../../../../../shared/services/ui-config-parameter/ui-config-parameter.service';
import {CertificationCriteriaRepDtlOutListComponent} from '../certification-criteria-rep-dtl-out-list/certification-criteria-rep-dtl-out-list.component';


@Component({
  selector: 'certification-criteria-rep-out-detail-list',
  templateUrl: './certification-criteria-rep-out-detail-list.component.html',
  styleUrls: ['./certification-criteria-rep-out-detail-list.component.scss'],
  animations: appAnimations
})
export class CertificationCriteriaRepOutDetailListComponent implements OnInit {
  @Input() id: string;
  @Input() certCriteriaRepOut: CertCriteriaRepOutInterface;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  public searchTerm: {
    template_name: string,
    testcase_name: string,
    initiating_msg_type: string
  };
  public displayedColumns =
    ['template_name', 'testcase_description', 'initiating_msg_type', 'template_status', 'status_update_dttm', 'actions'];
  public dataSource: MatTableDataSource<TestTemplateInterface>;
  public paginatorOptions = {
    count: 0,
    pageSize: 10,
    pageSizeOptions: [5, 10, 25, 50, 100],
    showFirstLastButtons: true
  };
  public inProgress: Boolean = true;
  public pageEvent: any;
  private sortBy = '-status_update_dttm';
  private pageSize = 10;

  constructor(private certCriteriaRepOutService: CertCriteriaRepOutService,
              private dialog: MatDialog,
              private uiConfigParameterService: UiConfigParameterService,
              private errorMsgService: ErrorMessageService) {
    this.searchTerm = {
      template_name: null,
      testcase_name: null,
      initiating_msg_type: null,
    };
  }

  ngOnInit() {
    // Fetch Test Templates
    this.fetchCertCriteriaRepOutTestTemplatesListing();
  }

  public sortData(sort: Sort): void {
    const data = this.dataSource;
    if (!sort.active || sort.direction === '') {
      this.sortBy = '-status_update_dttm';
    } else {
      this.sortBy = sort.direction === 'asc' ? sort.active : '-' + sort.active;
    }
    this.paginator.pageIndex = 0;
    this.fetchCertCriteriaRepOutTestTemplatesListing();
  }

  public fetchCertCriteriaRepOutTestTemplatesListing(_pageEvent?: PageEvent): void {
    if (_pageEvent) {
      this.pageEvent = _pageEvent;
      this.pageSize = _pageEvent.pageSize;
    }
    const options = {
      limit: (this.pageEvent ? (this.pageEvent.pageSize) : this.pageSize ? this.pageSize : this.paginatorOptions.pageSize),
      page: (this.pageEvent ? (this.pageEvent.pageIndex + 1) : 1),
      sort: [this.sortBy]
    };
    if (this.searchTerm.template_name) {
      options['template_name'] = this.searchTerm.template_name;
    }
    if (this.searchTerm.testcase_name) {
      options['testcase_name'] = this.searchTerm.testcase_name;
    }
    if (this.searchTerm.initiating_msg_type) {
      options['initiating_msg_type'] = this.searchTerm.initiating_msg_type;
    }
    this.inProgress = true;
    this.certCriteriaRepOutService.getCertCriteriaRepOutTemplates(options, this.id)
      .subscribe(response => {
          if (response && response.data && response.data.rows && response.data.rows.length) {
            response.data.rows.forEach(row => {
              row.status_update_dttm = this.convertTimezone(row.status_update_dttm);
            });
            this.dataSource = new MatTableDataSource(response.data.rows);
            this.paginatorOptions.count = response.data.count;
            this.dataSource.sort = this.sort;
          } else {
            this.errorMsgService.openSnackBar(this.errorMsgService.getNonFormError('NO_DATA'));
          }
          this.inProgress = false;
        },
        err => {
          this.errorMsgService.handleServerErrors(err);
          this.inProgress = false;
        });
  }

  public applyFilter(searchCriteria: { key: string, value: string }): void {
    this.searchTerm[searchCriteria.key] = searchCriteria.value;
    this.paginator.pageIndex = 0;
    this.fetchCertCriteriaRepOutTestTemplatesListing();
  }

  public runDetails(template: CertCriteriaTemplateResultsInterface): void {
    const conf: MatDialogConfig = {
      autoFocus: false,
      closeOnNavigation: true,
      panelClass: 'panelClass',
      maxWidth: '1400px',
      data: {template: template, certCriteriaRepOut: this.certCriteriaRepOut}
    };
    this.dialog.open(CertificationCriteriaRepDtlOutListComponent, conf);
  }

  public convertTimezone(date: Date): moment {
    return this.uiConfigParameterService.convertTimezone(date);
  }
}
