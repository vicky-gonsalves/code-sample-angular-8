import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CertificationCriteriaRepOutDetailListComponent} from './certification-criteria-rep-out-detail-list.component';

describe('CertificationCriteriaRepOutDetailListComponent', () => {
  let component: CertificationCriteriaRepOutDetailListComponent;
  let fixture: ComponentFixture<CertificationCriteriaRepOutDetailListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CertificationCriteriaRepOutDetailListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertificationCriteriaRepOutDetailListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
