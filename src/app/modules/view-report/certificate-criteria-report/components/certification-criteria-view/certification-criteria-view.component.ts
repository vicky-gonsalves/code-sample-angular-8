import {Component, OnInit} from '@angular/core';
import {appAnimations} from '../../../../../core/animations/animations';
import {NavItemService} from '../../../../../shared/services/nav-item/nav-item.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {PageTitleService} from '../../../../../shared/services/page-title/page-title.service';
import {PageHeaderService} from '../../../../../shared/services/page-header/page-header.service';
import {NavItemInterface} from '../../../../../interface/nav-item.interface';
import {CertCriteriaRepOutService} from '../../../../../shared/services/cert-criteria-rep-out/cert-criteria-rep-out.service';
import {CertCriteriaRepOutInterface} from '../../../../../interface/cert-criteria-rep-out.interface';
import moment from 'moment-timezone';
import {UiConfigParameterService} from '../../../../../shared/services/ui-config-parameter/ui-config-parameter.service';

@Component({
  selector: 'certification-criteria-view',
  templateUrl: './certification-criteria-view.component.html',
  styleUrls: ['./certification-criteria-view.component.scss'],
  animations: appAnimations
})
export class CertificationCriteriaViewComponent implements OnInit {
  public certCriteriaRepOutId: string;
  public certCriteriaRepOut: CertCriteriaRepOutInterface;
  public inProgress: Boolean = true;
  private componentNavItem: NavItemInterface;

  constructor(public navItems: NavItemService,
              public pageTitle: PageTitleService,
              private errorMessageService: ErrorMessageService,
              private router: Router,
              private certCriteriaRepOutService: CertCriteriaRepOutService,
              private uiConfigParameterService: UiConfigParameterService,
              private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe(params => {
      if (params.hasOwnProperty('id') && params.id.length) {
        this.certCriteriaRepOutId = params.id;
      }
    });
  }

  ngOnInit() {
    this.componentNavItem = this.navItems.getItemById('certification-criteria-report-view');
    if (this.componentNavItem) {
      this.pageTitle.setTitle(this.componentNavItem.name);
    }
    // Set Page Header Details
    PageHeaderService.updatePageHeader({
      backPath: 'view-report/certification-criteria-report',
      subTitle: 'Loading...'
    });

    if (this.certCriteriaRepOutId) {
      this.fetchCertCriteriaRepOut(this.certCriteriaRepOutId);
    }
  }

  public convertTimezone(date: Date): moment {
    return this.uiConfigParameterService.convertTimezone(date);
  }

  private setInProgress(): void {
    this.inProgress = true;
  }

  private setNotInProgress(): void {
    this.inProgress = false;
  }

  private fetchCertCriteriaRepOut(id: string): void {
    this.certCriteriaRepOutService.getCertCriteriaRepOut(id)
      .subscribe(response => {
          if (response && response.hasOwnProperty('data') && response.data) {
            response.data.last_update_dttm = this.convertTimezone(response.data.last_update_dttm);
            this.certCriteriaRepOut = response.data;
            PageHeaderService.updatePageHeader({
              backPath: 'view-report/certification-criteria-report',
              subTitle: this.certCriteriaRepOut.criteria_name
            });
          }
          this.setNotInProgress();
        },
        err => {
          this.setNotInProgress();
          this.router.navigate(['view-report/certification-criteria-report']);
          this.errorMessageService.handleServerErrors(err);
        });
  }
}
