import {Component, OnDestroy, OnInit} from '@angular/core';
import {appAnimations} from '../../../core/animations/animations';
import {QueryResultStoreSettlementSnapshotService} from '../../../shared/services/query-result-store-settlement-snapshot/query-result-store-settlement-snapshot.service';

@Component({
  selector: 'view-settlement-snapshot',
  templateUrl: './view-settlement-snapshot.component.html',
  styleUrls: ['./view-settlement-snapshot.component.scss'],
  animations: appAnimations
})
export class ViewSettlementSnapshotComponent implements OnInit, OnDestroy {

  constructor(private queryResultStoreSettlementSnapshotService: QueryResultStoreSettlementSnapshotService) {
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.queryResultStoreSettlementSnapshotService.setQuery(null);
    this.queryResultStoreSettlementSnapshotService.setFinalQuery(null);
    this.queryResultStoreSettlementSnapshotService.setResults(null);
  }

}
