import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserDataGuard} from '../../../core/guards/user-data/user-data.guard';
import {SearchFormComponent} from './components/search-form/search-form.component';
import {ViewSettlementSnapshotComponent} from './view-settlement-snapshot.component';
import {SearchResultComponent} from './components/search-result/search-result.component';

export const routes: Routes = [
  {
    path: '',
    canActivate: [UserDataGuard],
    component: ViewSettlementSnapshotComponent,
    children: [
      {
        path: '',
        canActivateChild: [UserDataGuard],
        component: SearchFormComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'view-settlement-snapshot'}
      },
      {
        path: 'search-results',
        canActivateChild: [UserDataGuard],
        component: SearchResultComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'settlement-snapshot-result'}
      }
    ]
  },
  {path: '**', redirectTo: '', pathMatch: 'full'}
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);

