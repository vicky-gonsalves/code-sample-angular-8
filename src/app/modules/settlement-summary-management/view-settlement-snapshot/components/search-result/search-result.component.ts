import {HttpErrorResponse} from '@angular/common/http';
import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {Router} from '@angular/router';
import moment from 'moment-timezone';
import {Subscription} from 'rxjs';
import {appAnimations} from '../../../../../core/animations/animations';
import {Identity} from '../../../../../core/services/identity/identity';
import {IdentityService} from '../../../../../core/services/identity/identity.service';
import {NavItemInterface} from '../../../../../interface/nav-item.interface';
import {SettlementSnapshotInterface} from '../../../../../interface/settlement-snapshot.interface';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {NavItemService} from '../../../../../shared/services/nav-item/nav-item.service';
import {PageHeaderService} from '../../../../../shared/services/page-header/page-header.service';
import {PageTitleService} from '../../../../../shared/services/page-title/page-title.service';
import {QueryResultStoreSettlementSnapshotService} from '../../../../../shared/services/query-result-store-settlement-snapshot/query-result-store-settlement-snapshot.service';
import {RefreshService} from '../../../../../shared/services/refresh/refresh.service';
import {SettlementSnapshotService} from '../../../../../shared/services/settlement-snapshot/settlement-snapshot.service';
import {UiConfigParameterService} from '../../../../../shared/services/ui-config-parameter/ui-config-parameter.service';
import {SnapshotDetailViewComponent} from '../snapshot-detail-view/snapshot-detail-view.component';

@Component({
  selector: 'search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.scss'],
  animations: appAnimations
})
export class SearchResultComponent implements OnInit, OnDestroy {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  public searchTerm: {
    id: string,
    account_id: string,
    participant_id: string
  };
  public displayedColumns =
    ['actions', 'creation_tmstmp', 'id', 'cycle_start_dttm', 'account_id', 'account_type', 'tracking_account_id', 'participant_id', 'initial_cap', 'aval_balance', 'account_position'];
  public dataSource: MatTableDataSource<SettlementSnapshotInterface>;
  public paginatorOptions = {
    count: 0,
    pageSize: 10,
    pageSizeOptions: [5, 10, 25, 50, 100],
    showFirstLastButtons: true
  };
  public inProgress: Boolean = true;
  private sortBy = '-creation_tmstmp';
  private pageSize = 10;
  private componentNavItem: NavItemInterface;
  private settlementSnapshotQuery: any;
  private storedResults: any;
  private identity: Identity;
  private _lastPageEvent: PageEvent;
  private _lastOptions: any;
  private refreshSubscription: Subscription;

  constructor(private queryResultStoreSettlementSnapshotService: QueryResultStoreSettlementSnapshotService,
              private router: Router,
              private navItems: NavItemService,
              private errorMessageService: ErrorMessageService,
              private uiConfigParameterService: UiConfigParameterService,
              private dialog: MatDialog,
              private settlementSnapshotService: SettlementSnapshotService,
              private pageTitle: PageTitleService,
              private refreshService: RefreshService) {
    this.refreshSubscription = this.refreshService.refreshObservable$.subscribe((res) => {
      if (res.hasOwnProperty('refresh') && res.refresh) {
        this.settlementSnapshotQuery = this.queryResultStoreSettlementSnapshotService.getFinalQuery();
        this.fetchSettlementSnapshot(this._lastPageEvent, this._lastOptions);
      }
    });

    IdentityService.identity.subscribe(contactIdentity => {
      this.identity = contactIdentity;
    });
    this.searchTerm = {
      id: null,
      account_id: null,
      participant_id: null
    };
    const _storedResults = this.queryResultStoreSettlementSnapshotService.getResults();
    this.settlementSnapshotQuery = this.queryResultStoreSettlementSnapshotService.getFinalQuery();
    if (!this.settlementSnapshotQuery && !_storedResults) {
      this.router.navigate(['/manage-settlement-summary/view-settlement-snapshot']);
    } else {
      this.storedResults = _storedResults.rows.map(d => {
        d.initial_cap = d.initial_cap ? JSON.parse(d.initial_cap).$numberDecimal : null;
        d.aval_balance = d.aval_balance ? JSON.parse(d.aval_balance).$numberDecimal : null;
        d.account_position = d.account_position ? JSON.parse(d.account_position).$numberDecimal : null;
        d.outward_accepted_value = d.outward_accepted_value ? JSON.parse(d.outward_accepted_value).$numberDecimal : null;
        d.outward_rejected_value = d.outward_rejected_value ? JSON.parse(d.outward_rejected_value).$numberDecimal : null;
        d.inward_accepted_value = d.inward_accepted_value ? JSON.parse(d.inward_accepted_value).$numberDecimal : null;
        d.inward_rejected_value = d.inward_rejected_value ? JSON.parse(d.inward_rejected_value).$numberDecimal : null;
        return d;
      });
      this.dataSource = new MatTableDataSource(this.storedResults);
      this.paginatorOptions.count = _storedResults.count;
      this.dataSource.sort = this.sort;
    }
  }

  ngOnInit() {
    // Get Navigation Component Name
    this.componentNavItem = this.navItems.getItemById('settlement-snapshot-result');
    if (this.componentNavItem) {
      // Set Page Title
      this.pageTitle.setTitle(this.componentNavItem.name);
    }

    // Set Page Header Details
    PageHeaderService.updatePageHeader({
      backPath: 'manage-settlement-summary/view-settlement-snapshot',
      refreshButton: true
    });
  }

  ngOnDestroy() {
    this.refreshSubscription.unsubscribe();
  }

  public sortData(sort: Sort): void {
    const data = this.dataSource;
    if (!sort.active || sort.direction === '') {
      this.sortBy = '-creation_tmstmp';
    } else {
      this.sortBy = sort.direction === 'asc' ? sort.active : '-' + sort.active;
    }
    this.paginator.pageIndex = 0;
    this.fetchSettlementSnapshot();
  }

  public fetchSettlementSnapshot(pageEvent?: PageEvent, lastOptions?: any): void {
    if (pageEvent) {
      this.pageSize = pageEvent.pageSize;
      this._lastPageEvent = pageEvent;
    }
    let options = null;
    if (lastOptions) {
      options = lastOptions;
    } else {
      options = {
        limit: (pageEvent ? (pageEvent.pageSize) : this.pageSize ? this.pageSize : this.paginatorOptions.pageSize),
        page: (pageEvent ? (pageEvent.pageIndex + 1) : 1),
        sort: [this.sortBy]
      };
      if (this.searchTerm.id) {
        options['id'] = this.searchTerm.id;
      }
      if (this.searchTerm.account_id) {
        options['account_id'] = this.searchTerm.account_id;
      }
      if (this.searchTerm.participant_id) {
        options['participant_id'] = this.searchTerm.participant_id;
      }
      this._lastOptions = options;
    }
    this.inProgress = true;
    this.settlementSnapshotService.getSettlementSnapshots(options, this.settlementSnapshotQuery)
      .subscribe(response => {
          if (response && response.data && response.data.rows && response.data.rows.length) {
            response.data.rows.forEach(row => {
              row.creation_tmstmp = this.convertTimezone(row.creation_tmstmp);
              row.cycle_start_dttm = this.convertTimezone(row.cycle_start_dttm);
            });
            this.dataSource = new MatTableDataSource(response.data.rows.map(d => {
              d.initial_cap = d.initial_cap ? JSON.parse(d.initial_cap).$numberDecimal : null;
              d.aval_balance = d.aval_balance ? JSON.parse(d.aval_balance).$numberDecimal : null;
              d.account_position = d.account_position ? JSON.parse(d.account_position).$numberDecimal : null;
              d.outward_accepted_value = d.outward_accepted_value ? JSON.parse(d.outward_accepted_value).$numberDecimal : null;
              d.outward_rejected_value = d.outward_rejected_value ? JSON.parse(d.outward_rejected_value).$numberDecimal : null;
              d.inward_accepted_value = d.inward_accepted_value ? JSON.parse(d.inward_accepted_value).$numberDecimal : null;
              d.inward_rejected_value = d.inward_rejected_value ? JSON.parse(d.inward_rejected_value).$numberDecimal : null;
              return d;
            }));
            this.paginatorOptions.count = response.data.count;
            this.dataSource.sort = this.sort;
          } else {
            this.errorMessageService.openSnackBar(this.errorMessageService.getNonFormError('NO_DATA'));
          }
          this.inProgress = false;
        },
        (err: HttpErrorResponse) => {
          this.errorMessageService.handleServerErrors(err);
          this.inProgress = false;
        });
  }

  public applyFilter(searchCriteria: { key: string, value: string }): void {
    this.searchTerm[searchCriteria.key] = searchCriteria.value;
    this.paginator.pageIndex = 0;
    this.fetchSettlementSnapshot();
  }

  public convertTimezone(date: Date): moment {
    return this.uiConfigParameterService.convertTimezone(date);
  }

  public viewTransactionDetail(settlementSnapshot?: SettlementSnapshotInterface): void {
    const conf: MatDialogConfig = {
      autoFocus: false,
      closeOnNavigation: true,
      panelClass: 'panelClass',
      maxWidth: '1400px',
      data: settlementSnapshot
    };
    this.dialog.open(SnapshotDetailViewComponent, conf);
  }
}
