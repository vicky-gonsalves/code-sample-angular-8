import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SnapshotDetailViewComponent} from './snapshot-detail-view.component';

describe('SnapshotDetailViewComponent', () => {
  let component: SnapshotDetailViewComponent;
  let fixture: ComponentFixture<SnapshotDetailViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SnapshotDetailViewComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SnapshotDetailViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
