import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import moment from 'moment-timezone';
import {Identity} from '../../../../../core/services/identity/identity';
import {IdentityService} from '../../../../../core/services/identity/identity.service';
import {SettlementSnapshotInterface} from '../../../../../interface/settlement-snapshot.interface';
import {UiConfigParameterService} from '../../../../../shared/services/ui-config-parameter/ui-config-parameter.service';

@Component({
  selector: 'transaction-detail-view',
  templateUrl: './snapshot-detail-view.component.html',
  styleUrls: ['./snapshot-detail-view.component.scss']
})
export class SnapshotDetailViewComponent implements OnInit {
  public settlementSnapshot: SettlementSnapshotInterface;
  public identity: Identity;
  public hasPermissionToViewMessageLegs: Boolean = true;
  public inProgress: Boolean = false;

  constructor(@Inject(MAT_DIALOG_DATA) private data: any,
              private uiConfigParameterService: UiConfigParameterService) {
    IdentityService.identity.subscribe(contactIdentity => {
      this.identity = contactIdentity;
      this.hasPermissionToViewMessageLegs = IdentityService.hasPrivileges(['viewMessageLegs']);
    });
    this.settlementSnapshot = data;
  }

  ngOnInit() {
  }

  public convertTimezone(date: Date): moment {
    return this.uiConfigParameterService.convertTimezone(date);
  }
}
