import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SettlementSnapshotCreditDetailListComponent} from './settlement-snapshot-credit-detail-list.component';

describe('SettlementSnapshotCreditDetailListComponent', () => {
  let component: SettlementSnapshotCreditDetailListComponent;
  let fixture: ComponentFixture<SettlementSnapshotCreditDetailListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SettlementSnapshotCreditDetailListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettlementSnapshotCreditDetailListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
