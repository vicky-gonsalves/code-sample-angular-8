import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SettlementSnapshotDebitDetailListComponent} from './settlement-snapshot-debit-detail-list.component';

describe('SettlementSnapshotDebitDetailListComponent', () => {
  let component: SettlementSnapshotDebitDetailListComponent;
  let fixture: ComponentFixture<SettlementSnapshotDebitDetailListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SettlementSnapshotDebitDetailListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettlementSnapshotDebitDetailListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
