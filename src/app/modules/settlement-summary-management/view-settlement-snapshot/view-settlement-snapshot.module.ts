import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ViewSettlementSnapshotComponent} from './view-settlement-snapshot.component';
import {SearchFormComponent} from './components/search-form/search-form.component';
import {SharedModule} from '../../../shared/shared.module';
import {routing} from './view-settlement-snapshot.routes';
import {SnapshotDetailViewComponent} from './components/snapshot-detail-view/snapshot-detail-view.component';
import {SearchResultComponent} from './components/search-result/search-result.component';
import {SettlementSnapshotCreditDetailListComponent} from './components/settlement-snapshot-credit-detail-list/settlement-snapshot-credit-detail-list.component';
import {SettlementSnapshotDebitDetailListComponent} from './components/settlement-snapshot-debit-detail-list/settlement-snapshot-debit-detail-list.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    routing
  ],
  declarations: [ViewSettlementSnapshotComponent,
    SearchFormComponent,
    SearchResultComponent,
    SnapshotDetailViewComponent,
    SettlementSnapshotCreditDetailListComponent,
    SettlementSnapshotDebitDetailListComponent],
  entryComponents: [SnapshotDetailViewComponent]
})
export class ViewSettlementSnapshotModule {
}
