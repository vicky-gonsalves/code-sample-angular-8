import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SettlementSummaryManagementComponent} from './settlement-summary-management.component';
import {routing} from './settlement-summary-management.routes';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    routing
  ],
  declarations: [SettlementSummaryManagementComponent]
})
export class SettlementSummaryManagementModule {
}
