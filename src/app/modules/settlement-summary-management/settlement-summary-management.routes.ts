import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserDataGuard} from '../../core/guards/user-data/user-data.guard';

export const routes: Routes = [
  {
    path: '',
    canActivate: [UserDataGuard],
    children: [
      {
        path: 'settlement-summary',
        canActivateChild: [UserDataGuard],
        loadChildren: () => import('./settlement-summary/settlement-summary.module').then(mod => mod.SettlementSummaryModule),
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'settlement-summary'}
      },
      {
        path: 'credit-debit-settlement-account',
        canActivateChild: [UserDataGuard],
        loadChildren: () => import('./credit-debit-settlement-account/credit-debit-settlement-account.module').then(mod => mod.CreditDebitSettlementAccountModule),
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'credit-debit-settlement-account'}
      },
      {
        path: 'snapshot-settlement-position',
        canActivateChild: [UserDataGuard],
        loadChildren: () => import('./snapshot-settlement-position/snapshot-settlement-position.module').then(mod => mod.SnapshotSettlementPositionModule),
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'snapshot-settlement-position'}
      },
      {
        path: 'view-settlement-snapshot',
        canActivateChild: [UserDataGuard],
        loadChildren: () => import('./view-settlement-snapshot/view-settlement-snapshot.module').then(mod => mod.ViewSettlementSnapshotModule),
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'view-settlement-snapshot'}
      }
    ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);

