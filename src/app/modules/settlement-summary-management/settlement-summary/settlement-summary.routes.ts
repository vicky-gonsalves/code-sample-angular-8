import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserDataGuard} from '../../../core/guards/user-data/user-data.guard';
import {SettlementSummaryComponent} from './settlement-summary.component';
import {SettlementSummaryListComponent} from './components/settlement-summary-list/settlement-summary-list.component';
import {SettlementSummaryViewComponent} from './components/settlement-summary-view/settlement-summary-view.component';
import {SearchFormComponent} from './components/search-form/search-form.component';

export const routes: Routes = [
  {
    path: '',
    canActivate: [UserDataGuard],
    component: SettlementSummaryComponent,
    children: [
      {
        path: '',
        canActivateChild: [UserDataGuard],
        component: SearchFormComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'settlement-summary'}
      },
      {
        path: 'search-results',
        canActivateChild: [UserDataGuard],
        component: SettlementSummaryListComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'settlement-summary'}
      },
      {
        path: 'view/:id',
        canActivateChild: [UserDataGuard],
        component: SettlementSummaryViewComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'settlement-summary-view'}
      }
    ]
  },
  {path: '**', redirectTo: '', pathMatch: 'full'}
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);

