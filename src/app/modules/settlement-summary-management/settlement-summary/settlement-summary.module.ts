import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SettlementSummaryComponent} from './settlement-summary.component';
import {SettlementSummaryListComponent} from './components/settlement-summary-list/settlement-summary-list.component';
import {SettlementSummaryViewComponent} from './components/settlement-summary-view/settlement-summary-view.component';
import {routing} from './settlement-summary.routes';
import {SharedModule} from '../../../shared/shared.module';
import {SettlementSummaryCreditDetailListComponent} from './components/settlement-summary-credit-detail-list/settlement-summary-credit-detail-list.component';
import {SettlementSummaryDebitDetailListComponent} from './components/settlement-summary-debit-detail-list/settlement-summary-debit-detail-list.component';
import {SearchFormComponent} from './components/search-form/search-form.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    routing
  ],
  declarations: [
    SettlementSummaryComponent,
    SettlementSummaryListComponent,
    SettlementSummaryViewComponent,
    SettlementSummaryCreditDetailListComponent,
    SettlementSummaryDebitDetailListComponent,
    SearchFormComponent
  ],
  entryComponents: [SettlementSummaryViewComponent]
})
export class SettlementSummaryModule {
}
