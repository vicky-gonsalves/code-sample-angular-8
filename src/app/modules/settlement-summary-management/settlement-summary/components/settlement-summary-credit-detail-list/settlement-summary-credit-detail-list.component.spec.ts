import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SettlementSummaryCreditDetailListComponent} from './settlement-summary-credit-detail-list.component';

describe('SettlementSummaryCreditDetailListComponent', () => {
  let component: SettlementSummaryCreditDetailListComponent;
  let fixture: ComponentFixture<SettlementSummaryCreditDetailListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SettlementSummaryCreditDetailListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettlementSummaryCreditDetailListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
