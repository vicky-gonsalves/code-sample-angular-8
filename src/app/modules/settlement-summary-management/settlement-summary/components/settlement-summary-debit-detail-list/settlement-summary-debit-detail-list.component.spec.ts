import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SettlementSummaryDebitDetailListComponent} from './settlement-summary-debit-detail-list.component';

describe('SettlementSummaryDebitDetailListComponent', () => {
  let component: SettlementSummaryDebitDetailListComponent;
  let fixture: ComponentFixture<SettlementSummaryDebitDetailListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SettlementSummaryDebitDetailListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettlementSummaryDebitDetailListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
