import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import moment from 'moment-timezone';
import {SettlementSummaryAccountsInterface, SettlementSummaryInterface} from '../../../../../interface/settlement-summary.interface';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {SettlementSummaryService} from '../../../../../shared/services/settlement-summary/settlement-summary.service';
import {UiConfigParameterService} from '../../../../../shared/services/ui-config-parameter/ui-config-parameter.service';

@Component({
  selector: 'settlement-summary-debit-detail-list',
  templateUrl: './settlement-summary-debit-detail-list.component.html',
  styleUrls: ['./settlement-summary-debit-detail-list.component.scss']
})
export class SettlementSummaryDebitDetailListComponent implements OnInit {
  @Input() id: string;
  @Input() settlementSummary: SettlementSummaryInterface;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  public displayedColumns =
    ['date_time', 'amount'];
  public dataSource: MatTableDataSource<SettlementSummaryAccountsInterface>;
  public paginatorOptions = {
    count: 0,
    pageSize: 10,
    pageSizeOptions: [5, 10, 25, 50, 100],
    showFirstLastButtons: true
  };
  public inProgress: Boolean = true;
  public pageEvent: any;
  private sortBy = '-date_time';
  private pageSize = 10;

  constructor(private settlementSummaryService: SettlementSummaryService,
              private uiConfigParameterService: UiConfigParameterService,
              private errorMsgService: ErrorMessageService) {
  }

  ngOnInit() {
    // Fetch Debit List
    this.fetchSettlementSummaryDebitListing();
  }

  public sortData(sort: Sort): void {
    const data = this.dataSource;
    if (!sort.active || sort.direction === '') {
      this.sortBy = '-date_time';
    } else {
      this.sortBy = sort.direction === 'asc' ? sort.active : '-' + sort.active;
    }
    this.paginator.pageIndex = 0;
    this.fetchSettlementSummaryDebitListing();
  }

  public fetchSettlementSummaryDebitListing(_pageEvent?: PageEvent): void {
    if (_pageEvent) {
      this.pageEvent = _pageEvent;
      this.pageSize = _pageEvent.pageSize;
    }
    const options = {
      limit: (this.pageEvent ? (this.pageEvent.pageSize) : this.pageSize ? this.pageSize : this.paginatorOptions.pageSize),
      page: (this.pageEvent ? (this.pageEvent.pageIndex + 1) : 1),
      sort: [this.sortBy]
    };
    this.inProgress = true;
    this.settlementSummaryService.getSettlementSummaryDebitList(options, this.id)
      .subscribe(response => {
          if (response && response.data && response.data.rows && response.data.rows.length) {
            response.data.rows.forEach(row => {
              row.date_time = this.convertTimezone(row.date_time);
            });
            const source = response.data.rows.map(row => {
              row.amount = row.amount.$numberDecimal;
              return row;
            });
            this.dataSource = new MatTableDataSource(source);
            this.paginatorOptions.count = response.data.count;
            this.dataSource.sort = this.sort;
          }
          this.inProgress = false;
        },
        err => {
          this.errorMsgService.handleServerErrors(err);
          this.inProgress = false;
        });
  }

  public convertTimezone(date: Date): moment {
    return this.uiConfigParameterService.convertTimezone(date);
  }
}
