import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SettlementSummaryListComponent} from './settlement-summary-list.component';

describe('SettlementSummaryListComponent', () => {
  let component: SettlementSummaryListComponent;
  let fixture: ComponentFixture<SettlementSummaryListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SettlementSummaryListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettlementSummaryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
