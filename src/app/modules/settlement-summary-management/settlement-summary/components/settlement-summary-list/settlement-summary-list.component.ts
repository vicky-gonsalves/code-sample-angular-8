import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {Router} from '@angular/router';
import moment from 'moment-timezone';
import {Subscription} from 'rxjs';
import {appAnimations} from '../../../../../core/animations/animations';
import {NavItemInterface} from '../../../../../interface/nav-item.interface';
import {SettlementSummaryInterface} from '../../../../../interface/settlement-summary.interface';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {NavItemService} from '../../../../../shared/services/nav-item/nav-item.service';
import {PageHeaderService} from '../../../../../shared/services/page-header/page-header.service';
import {PageTitleService} from '../../../../../shared/services/page-title/page-title.service';
import {QueryResultStoreSettlementSummaryService} from '../../../../../shared/services/query-result-store-settlement-summary/query-result-store-settlement-summary.service';
import {RefreshService} from '../../../../../shared/services/refresh/refresh.service';
import {SettlementSummaryService} from '../../../../../shared/services/settlement-summary/settlement-summary.service';
import {UiConfigParameterService} from '../../../../../shared/services/ui-config-parameter/ui-config-parameter.service';
import {SettlementSummaryViewComponent} from '../settlement-summary-view/settlement-summary-view.component';

@Component({
  selector: 'settlement-summary-list',
  templateUrl: './settlement-summary-list.component.html',
  styleUrls: ['./settlement-summary-list.component.scss'],
  animations: appAnimations
})
export class SettlementSummaryListComponent implements OnInit, OnDestroy {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  public searchTerm: {
    id: string,
    account_id: string,
    participant_id: string
  };
  public displayedColumns =
    ['actions', 'id', 'cycle_start_dttm', 'cycle_end_dttm', 'account_id', 'account_type', 'tracking_account_id', 'participant_id', 'initial_cap', 'aval_balance', 'account_position'];
  public dataSource: MatTableDataSource<SettlementSummaryInterface>;
  public paginatorOptions = {
    count: 0,
    pageSize: 10,
    pageSizeOptions: [5, 10, 25, 50, 100],
    showFirstLastButtons: true
  };
  public inProgress: Boolean = true;
  private sortBy = '-id';
  private pageSize = 10;
  private componentNavItem: NavItemInterface;
  private settlementSummaryQuery: any;
  private storedResults: any;
  private _lastPageEvent: PageEvent;
  private _lastOptions: any;
  private refreshSubscription: Subscription;

  constructor(private navItems: NavItemService,
              private settlementSummaryService: SettlementSummaryService,
              private uiConfigParameterService: UiConfigParameterService,
              private queryResultStoreSettlementSummaryService: QueryResultStoreSettlementSummaryService,
              private errorMsgService: ErrorMessageService,
              private router: Router,
              private dialog: MatDialog,
              private pageTitle: PageTitleService,
              private refreshService: RefreshService) {
    this.refreshSubscription = this.refreshService.refreshObservable$.subscribe((res) => {
      if (res.hasOwnProperty('refresh') && res.refresh) {
        this.settlementSummaryQuery = this.queryResultStoreSettlementSummaryService.getFinalQuery();
        this.fetchSettlementSummaryListing(this._lastPageEvent, this._lastOptions);
      }
    });

    this.searchTerm = {
      id: null,
      account_id: null,
      participant_id: null
    };
    const _storedResults = this.queryResultStoreSettlementSummaryService.getResults();
    this.settlementSummaryQuery = this.queryResultStoreSettlementSummaryService.getFinalQuery();
    if (!this.settlementSummaryQuery && !_storedResults) {
      this.router.navigate(['/manage-settlement-summary/settlement-summary']);
    } else {
      this.storedResults = _storedResults.rows.map(d => {
        d.initial_cap = d.initial_cap ? JSON.parse(d.initial_cap).$numberDecimal : null;
        d.effective_cap = d.effective_cap ? JSON.parse(d.effective_cap).$numberDecimal : null;
        d.account_position = d.account_position ? JSON.parse(d.account_position).$numberDecimal : null;
        d.aval_balance = d.aval_balance ? JSON.parse(d.aval_balance).$numberDecimal : null;
        d.outward_accepted_value = d.outward_accepted_value ? JSON.parse(d.outward_accepted_value).$numberDecimal : null;
        d.outward_rejected_value = d.outward_rejected_value ? JSON.parse(d.outward_rejected_value).$numberDecimal : null;
        d.inward_accepted_value = d.inward_accepted_value ? JSON.parse(d.inward_accepted_value).$numberDecimal : null;
        d.inward_rejected_value = d.inward_rejected_value ? JSON.parse(d.inward_rejected_value).$numberDecimal : null;
        return d;
      });
      this.dataSource = new MatTableDataSource(this.storedResults);
      this.paginatorOptions.count = _storedResults.count;
      this.dataSource.sort = this.sort;
    }
  }

  ngOnInit() {
    // Get Navigation Component Name
    this.componentNavItem = this.navItems.getItemById('settlement-summary-result');
    if (this.componentNavItem) {
      // Set Page Title
      this.pageTitle.setTitle(this.componentNavItem.name);
    }

    // Set Page Header Details
    PageHeaderService.updatePageHeader({
      backPath: 'manage-settlement-summary/settlement-summary',
      refreshButton: true
    });
  }

  ngOnDestroy() {
    this.refreshSubscription.unsubscribe();
  }

  public sortData(sort: Sort): void {
    const data = this.dataSource;
    if (!sort.active || sort.direction === '') {
      this.sortBy = '-id';
    } else {
      this.sortBy = sort.direction === 'asc' ? sort.active : '-' + sort.active;
    }
    this.paginator.pageIndex = 0;
    this.fetchSettlementSummaryListing();
  }

  public fetchSettlementSummaryListing(pageEvent?: PageEvent, lastOptions?: any): void {
    if (pageEvent) {
      this.pageSize = pageEvent.pageSize;
      this._lastPageEvent = pageEvent;
    }
    let options = null;
    if (lastOptions) {
      options = lastOptions;
    } else {
      options = {
        limit: (pageEvent ? (pageEvent.pageSize) : this.pageSize ? this.pageSize : this.paginatorOptions.pageSize),
        page: (pageEvent ? (pageEvent.pageIndex + 1) : 1),
        sort: [this.sortBy]
      };
      if (this.searchTerm.id) {
        options['id'] = this.searchTerm.id;
      }
      if (this.searchTerm.account_id) {
        options['account_id'] = this.searchTerm.account_id;
      }
      if (this.searchTerm.participant_id) {
        options['participant_id'] = this.searchTerm.participant_id;
      }
      this._lastOptions = options;
    }
    this.inProgress = true;
    this.settlementSummaryService.getSettlementSummaries(options, this.settlementSummaryQuery)
      .subscribe(response => {
          if (response && response.data && response.data.rows && response.data.rows.length) {
            response.data.rows.forEach(row => {
              row.cycle_start_dttm = this.convertTimezone(row.cycle_start_dttm);
              row.cycle_end_dttm = this.convertTimezone(row.cycle_end_dttm);
            });
            const source = response.data.rows.map(row => {
              row.initial_cap = row.initial_cap ? JSON.parse(row.initial_cap).$numberDecimal : null;
              row.effective_cap = row.effective_cap ? JSON.parse(row.effective_cap).$numberDecimal : null;
              row.account_position = row.account_position ? JSON.parse(row.account_position).$numberDecimal : null;
              row.aval_balance = row.aval_balance ? JSON.parse(row.aval_balance).$numberDecimal : null;
              row.outward_accepted_value = row.outward_accepted_value ? JSON.parse(row.outward_accepted_value).$numberDecimal : null;
              row.outward_rejected_value = row.outward_rejected_value ? JSON.parse(row.outward_rejected_value).$numberDecimal : null;
              row.inward_accepted_value = row.inward_accepted_value ? JSON.parse(row.inward_accepted_value).$numberDecimal : null;
              row.inward_rejected_value = row.inward_rejected_value ? JSON.parse(row.inward_rejected_value).$numberDecimal : null;
              return row;
            });
            this.dataSource = new MatTableDataSource(source);
            this.paginatorOptions.count = response.data.count;
            this.dataSource.sort = this.sort;
          } else {
            this.errorMsgService.openSnackBar(this.errorMsgService.getNonFormError('NO_DATA'));
          }
          this.inProgress = false;
        },
        err => {
          this.errorMsgService.handleServerErrors(err);
          this.inProgress = false;
        });
  }

  public viewDetail(settlementSummary: SettlementSummaryInterface): void {
    const conf: MatDialogConfig = {
      autoFocus: false,
      closeOnNavigation: true,
      panelClass: 'panelClass',
      maxWidth: '1400px',
      data: settlementSummary
    };
    this.dialog.open(SettlementSummaryViewComponent, conf);
  }

  public applyFilter(searchCriteria: { key: string, value: string }): void {
    this.searchTerm[searchCriteria.key] = searchCriteria.value;
    this.paginator.pageIndex = 0;
    this.fetchSettlementSummaryListing();
  }

  public convertTimezone(date: Date): moment {
    return this.uiConfigParameterService.convertTimezone(date);
  }
}
