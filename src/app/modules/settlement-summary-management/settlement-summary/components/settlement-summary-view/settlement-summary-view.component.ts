import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import moment from 'moment-timezone';
import {appAnimations} from '../../../../../core/animations/animations';
import {SettlementSummaryInterface} from '../../../../../interface/settlement-summary.interface';
import {UiConfigParameterService} from '../../../../../shared/services/ui-config-parameter/ui-config-parameter.service';

@Component({
  selector: 'settlement-summary-view',
  templateUrl: './settlement-summary-view.component.html',
  styleUrls: ['./settlement-summary-view.component.scss'],
  animations: appAnimations
})
export class SettlementSummaryViewComponent implements OnInit {
  public settlementSummary: SettlementSummaryInterface;
  public inProgress: Boolean = true;

  constructor(@Inject(MAT_DIALOG_DATA) private data: any,
              private uiConfigParameterService: UiConfigParameterService) {
    this.settlementSummary = data;
  }

  ngOnInit() {
  }

  public convertTimezone(date: Date): moment {
    return this.uiConfigParameterService.convertTimezone(date);
  }
}
