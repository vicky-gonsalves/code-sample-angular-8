import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SettlementSummaryViewComponent} from './settlement-summary-view.component';

describe('SettlementSummaryViewComponent', () => {
  let component: SettlementSummaryViewComponent;
  let fixture: ComponentFixture<SettlementSummaryViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SettlementSummaryViewComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettlementSummaryViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
