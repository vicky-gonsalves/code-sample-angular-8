import {Component, OnDestroy, OnInit} from '@angular/core';
import {appAnimations} from '../../../core/animations/animations';
import {QueryResultStoreSettlementSummaryService} from '../../../shared/services/query-result-store-settlement-summary/query-result-store-settlement-summary.service';

@Component({
  selector: 'settlement-summary',
  templateUrl: './settlement-summary.component.html',
  styleUrls: ['./settlement-summary.component.scss'],
  animations: appAnimations
})
export class SettlementSummaryComponent implements OnInit, OnDestroy {

  constructor(private queryResultStoreSettlementSummaryService: QueryResultStoreSettlementSummaryService) {
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.queryResultStoreSettlementSummaryService.setQuery(null);
    this.queryResultStoreSettlementSummaryService.setFinalQuery(null);
    this.queryResultStoreSettlementSummaryService.setResults(null);
  }

}
