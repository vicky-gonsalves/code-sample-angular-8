import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CreditDebitSettlementAccountComponent} from './credit-debit-settlement-account.component';
import {SearchFormComponent} from './components/search-form/search-form.component';
import {SharedModule} from '../../../shared/shared.module';
import {routing} from './credit-debit-settlement-account.routes';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    routing
  ],
  declarations: [CreditDebitSettlementAccountComponent,
    SearchFormComponent]
})
export class CreditDebitSettlementAccountModule {
}
