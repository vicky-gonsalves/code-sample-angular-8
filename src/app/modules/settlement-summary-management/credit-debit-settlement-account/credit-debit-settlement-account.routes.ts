import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserDataGuard} from '../../../core/guards/user-data/user-data.guard';
import {CreditDebitSettlementAccountComponent} from './credit-debit-settlement-account.component';
import {SearchFormComponent} from './components/search-form/search-form.component';

export const routes: Routes = [
  {
    path: '',
    canActivate: [UserDataGuard],
    component: CreditDebitSettlementAccountComponent,
    children: [
      {
        path: '',
        canActivateChild: [UserDataGuard],
        component: SearchFormComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'credit-debit-settlement-account'}
      }
    ]
  },
  {path: '**', redirectTo: '', pathMatch: 'full'}
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);

