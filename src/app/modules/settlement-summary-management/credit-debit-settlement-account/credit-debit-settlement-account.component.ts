import {Component, OnInit} from '@angular/core';
import {appAnimations} from '../../../core/animations/animations';

@Component({
  selector: 'credit-debit-settlement-account',
  templateUrl: './credit-debit-settlement-account.component.html',
  styleUrls: ['./credit-debit-settlement-account.component.scss'],
  animations: appAnimations
})
export class CreditDebitSettlementAccountComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
