import {HttpErrorResponse} from '@angular/common/http';
import {AfterViewInit, Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {Observable} from 'rxjs';
import {debounceTime, distinctUntilChanged, startWith, switchMap} from 'rxjs/operators';
import {appAnimations} from '../../../../../core/animations/animations';
import {Identity} from '../../../../../core/services/identity/identity';
import {IdentityService} from '../../../../../core/services/identity/identity.service';
import {NavItemInterface} from '../../../../../interface/nav-item.interface';
import {ParticipantInterface} from '../../../../../interface/participant.interface';
import {DataViewerComponent} from '../../../../../shared/components/data-viewer/data-viewer.component';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {NavItemService} from '../../../../../shared/services/nav-item/nav-item.service';
import {PageHeaderService} from '../../../../../shared/services/page-header/page-header.service';
import {PageTitleService} from '../../../../../shared/services/page-title/page-title.service';
import {ParticipantService} from '../../../../../shared/services/participant/participant.service';
import {SettlementAccountService} from '../../../../../shared/services/settlement-account/settlement-account.service';

@Component({
  selector: 'search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss'],
  animations: appAnimations
})
export class SearchFormComponent implements OnInit, AfterViewInit {

  public accountSearchForm: FormGroup;
  public filteredParticipants: Observable<ParticipantInterface[]>;
  public inProgress: Boolean = false;
  public submitted: Boolean = false;
  public isParticipantContact: Boolean;
  private componentNavItem: NavItemInterface;
  private identity: Identity;

  constructor(private navItems: NavItemService,
              private errorMessageService: ErrorMessageService,
              private pageTitle: PageTitleService,
              private participantService: ParticipantService,
              private settlementAccountService: SettlementAccountService,
              private dialog: MatDialog) {
    IdentityService.identity.subscribe(contactIdentity => {
      this.identity = contactIdentity;
      this.isParticipantContact = this.identity && this.identity.contactProfile_id && this.identity.contactProfile_id === 'participant';
    });
  }

  get participant_id() {
    return this.accountSearchForm.get('participant_id');
  }

  get op_type() {
    return this.accountSearchForm.get('op_type');
  }

  get amount() {
    return this.accountSearchForm.get('amount');
  }

  ngOnInit() {
    this.componentNavItem = this.navItems.getItemById('credit-debit-settlement-account');
    if (this.componentNavItem) {
      this.pageTitle.setTitle(this.componentNavItem.name);
    }

    // Set Page Header Details
    PageHeaderService.updatePageHeader({
      showImage: true,
      icon: 'account_balance'
    });

    this.setupForm();
  }

  ngAfterViewInit() {
    this.initAutocomplete();
    this.setForm();
  }

  public setForm(): void {
    setTimeout(() => {
      if (this.isParticipantContact) {
        this.accountSearchForm.patchValue({
          contactProfile_id: {id: this.identity.contactProfile_id},
          participant_id: {id: this.identity.participant_id},
        });
      }
      this.accountSearchForm.patchValue({
        op_type: 'Credit'
      });
    });
  }

  public resetForm(formDirective): void {
    formDirective.resetForm();
    setTimeout(() => {
      if (this.isParticipantContact) {
        this.accountSearchForm.patchValue({
          contactProfile_id: {id: this.identity.contactProfile_id},
          participant_id: {id: this.identity.participant_id},
          op_type: 'Credit',
          amount: null
        });
      } else {
        this.accountSearchForm.patchValue({
          participant_id: null,
          op_type: 'Credit',
          amount: null
        });
      }
      this.submitted = false;
    });
  }


  public participantDisplayFn(participantRoutingDetailOwner?: ParticipantInterface): string | undefined {
    return participantRoutingDetailOwner ? (participantRoutingDetailOwner.id) : undefined;
  }

  public getErrorMessage(fieldName: string): string {
    return this.errorMessageService.getError(this[fieldName], fieldName);
  }

  public submitAccountSearchForm(formDirective): void {
    this.submitted = true;
    const query = this.prepareFormFields();
    if (this.accountSearchForm.valid) {
      this.setInProgress();
      this.settlementAccountService.getCreditDebitSettlementAccount(query)
        .subscribe(response => {
            const conf: MatDialogConfig = {
              autoFocus: false,
              closeOnNavigation: true,
              panelClass: 'panelClass',
              maxWidth: '1200px',
              data: {response: (response && response.data ? response.data : response)}
            };
            const dialogRef = this.dialog.open(DataViewerComponent, conf);
            dialogRef.afterClosed().subscribe(result => {
              this.resetForm(formDirective);

            });
            this.setNotInProgress();
          },
          (err: HttpErrorResponse) => {
            this.setNotInProgress();
            this.errorMessageService.handleServerErrors(err, this.accountSearchForm);
          });
    }
  }

  private initAutocomplete(): void {
    this.filteredParticipants = this.participant_id.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        startWith(null),
        switchMap(
          (term: string | any) =>
            (typeof term === 'string' || term === '' || term === null) ?
              this.participantService.getParticipantsForAutocomplete({
                term: term,
                limit: 10
              }) : []
        )
      );
  }

  private setInProgress(): void {
    this.inProgress = true;
  }

  private setNotInProgress(): void {
    this.inProgress = false;
  }

  private setupForm(): void {
    this.accountSearchForm = new FormGroup({
      'participant_id': new FormControl('', [
        Validators.required
      ]),
      'op_type': new FormControl('', [
        Validators.required
      ]),
      'amount': new FormControl('', [
        Validators.required,
        Validators.pattern(/^[-]?\d+(\.\d{1,2})?$/)
      ]),
    });
  }

  private prepareFormFields(): any {
    const formModel = this.accountSearchForm.value;
    if (formModel.participant_id && !formModel.participant_id.id) {
      this.participant_id.setValue(null);
    }

    return {
      participant_id: formModel.participant_id && formModel.participant_id.id ? formModel.participant_id.id : null,
      op_type: formModel.op_type,
      amount: formModel.amount
    };
  }


}
