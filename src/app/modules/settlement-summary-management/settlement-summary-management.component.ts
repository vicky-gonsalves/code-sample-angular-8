import {Component, OnInit} from '@angular/core';
import {appAnimations} from '../../core/animations/animations';

@Component({
  selector: 'settlement-summary-management',
  templateUrl: './settlement-summary-management.component.html',
  animations: appAnimations
})
export class SettlementSummaryManagementComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
