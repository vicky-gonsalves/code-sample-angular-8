import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SnapshotSettlementPositionComponent} from './snapshot-settlement-position.component';
import {SearchFormComponent} from './components/search-form/search-form.component';
import {SharedModule} from '../../../shared/shared.module';
import {routing} from './snapshot-settlement-position.routes';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    routing
  ],
  declarations: [SnapshotSettlementPositionComponent,
    SearchFormComponent]
})
export class SnapshotSettlementPositionModule {
}
