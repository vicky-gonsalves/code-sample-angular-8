import {Component, OnInit} from '@angular/core';
import {appAnimations} from '../../../core/animations/animations';

@Component({
  selector: 'snapshot-settlement-position',
  templateUrl: './snapshot-settlement-position.component.html',
  styleUrls: ['./snapshot-settlement-position.component.scss'],
  animations: appAnimations
})
export class SnapshotSettlementPositionComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
