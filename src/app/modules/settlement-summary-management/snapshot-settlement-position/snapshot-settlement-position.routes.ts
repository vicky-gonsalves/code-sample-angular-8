import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserDataGuard} from '../../../core/guards/user-data/user-data.guard';
import {SearchFormComponent} from './components/search-form/search-form.component';
import {SnapshotSettlementPositionComponent} from './snapshot-settlement-position.component';

export const routes: Routes = [
  {
    path: '',
    canActivate: [UserDataGuard],
    component: SnapshotSettlementPositionComponent,
    children: [
      {
        path: '',
        canActivateChild: [UserDataGuard],
        component: SearchFormComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'snapshot-settlement-position'}
      }
    ]
  },
  {path: '**', redirectTo: '', pathMatch: 'full'}
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);

