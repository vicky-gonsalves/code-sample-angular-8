import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserDataGuard} from '../../core/guards/user-data/user-data.guard';

export const routes: Routes = [
  {
    path: '',
    canActivate: [UserDataGuard],
    children: [
      {
        path: 'contacts',
        canActivateChild: [UserDataGuard],
        loadChildren: () => import('./contact/contact.module').then(mod => mod.ContactModule),
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'contacts'}
      }
    ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);

