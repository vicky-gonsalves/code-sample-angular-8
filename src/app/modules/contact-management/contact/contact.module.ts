import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ContactComponent} from './contact.component';
import {ContactListComponent} from './components/contact-list/contact-list.component';
import {ContactEditorComponent} from './components/contact-editor/contact-editor.component';
import {SharedModule} from '../../../shared/shared.module';
import {routing} from './contact.routes';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    routing
  ],
  declarations: [
    ContactComponent,
    ContactListComponent,
    ContactEditorComponent
  ]
})
export class ContactModule {
}
