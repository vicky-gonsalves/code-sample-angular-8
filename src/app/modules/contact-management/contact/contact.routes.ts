import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ContactComponent} from './contact.component';
import {ContactListComponent} from './components/contact-list/contact-list.component';
import {ContactEditorComponent} from './components/contact-editor/contact-editor.component';
import {UserDataGuard} from '../../../core/guards/user-data/user-data.guard';

export const routes: Routes = [
  {
    path: '',
    canActivate: [UserDataGuard],
    component: ContactComponent,
    children: [
      {
        path: '',
        canActivateChild: [UserDataGuard],
        component: ContactListComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'contact'}
      },
      {
        path: 'add',
        canActivateChild: [UserDataGuard],
        component: ContactEditorComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'contact-add'}
      },
      {
        path: 'edit/:id',
        canActivateChild: [UserDataGuard],
        component: ContactEditorComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'contact-edit'}
      }
    ]
  },
  {path: '**', redirectTo: '', pathMatch: 'full'}
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);

