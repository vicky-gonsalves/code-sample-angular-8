import {AfterViewInit, Component, OnInit} from '@angular/core';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ContactService} from '../../../../../shared/services/contact/contact.service';
import {Contact} from '../../../../../model/contact.model';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {debounceTime, distinctUntilChanged, startWith, switchMap} from 'rxjs/internal/operators';
import {PageHeaderService} from '../../../../../shared/services/page-header/page-header.service';
import {NavItemInterface} from '../../../../../interface/nav-item.interface';
import {PageTitleService} from '../../../../../shared/services/page-title/page-title.service';
import {NavItemService} from '../../../../../shared/services/nav-item/nav-item.service';
import {appAnimations} from '../../../../../core/animations/animations';
import {ContactProfile} from '../../../../../model/contact-profile.model';
import {ContactProfileInterface} from '../../../../../interface/contact-profile.interface';
import {ContactProfileService} from '../../../../../shared/services/contact-profile/contact-profile.service';
import {ContactRoleInterface} from '../../../../../interface/contact-role.interface';
import {ContactRole} from '../../../../../model/contact-role.model';
import {ContactRoleService} from '../../../../../shared/services/contact-role/contact-role.service';
import {Participant} from '../../../../../model/participant.model';
import {ParticipantInterface} from '../../../../../interface/participant.interface';
import {ParticipantService} from '../../../../../shared/services/participant/participant.service';
import {ClhPlatformInterface} from '../../../../../interface/clh-platform.interface';
import {ClhPlatform} from '../../../../../model/clh-platform.model';
import {ClhPlatformService} from '../../../../../shared/services/clh-platform/clh-platform.service';
import {IdentityService} from '../../../../../core/services/identity/identity.service';
import {Identity} from '../../../../../core/services/identity/identity';
import {HelpTextService} from '../../../../../core/services/help-text/help-text.service';
import {UiConfigService} from '../../../../../core/services/ui-config/ui-config.service';
import {UiConfig} from '../../../../../core/services/ui-config/ui-config';
import moment from 'moment-timezone';
import {UiConfigParameterService} from '../../../../../shared/services/ui-config-parameter/ui-config-parameter.service';

@Component({
  selector: 'contact-editor',
  templateUrl: './contact-editor.component.html',
  styleUrls: ['./contact-editor.component.scss'],
  animations: appAnimations
})
export class ContactEditorComponent implements OnInit, AfterViewInit {
  public IS_EDIT: Boolean = false;
  public IS_PARTICIPANT_CONTACT: Boolean = false;
  public helpText: any;
  public contactForm: FormGroup;
  public contact: Contact = new Contact(
    null,
    null,
    null,
    null,
    null,
    null,
    false
  );
  public inProgress: Boolean = false;
  public submitted: Boolean = false;
  public filteredContactProfiles: Observable<ContactProfile[]>;
  public filteredContactRoles: Observable<ContactRole[]>;
  public filteredParticipants: Observable<Participant[]>;
  public filteredClhPlatforms: Observable<ClhPlatform[]>;
  private componentNavItem: NavItemInterface;
  private contactId: string;
  private identity: Identity;
  private uiConfig: UiConfig;

  constructor(public navItems: NavItemService,
              public pageTitle: PageTitleService,
              private errorMessageService: ErrorMessageService,
              private contactService: ContactService,
              private contactProfileService: ContactProfileService,
              private contactRoleService: ContactRoleService,
              private participantService: ParticipantService,
              private clhPlatformService: ClhPlatformService,
              private router: Router,
              private uiConfigParameterService: UiConfigParameterService,
              private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe(params => {
      if (params.hasOwnProperty('id') && params.id.length) {
        this.contactId = params.id;
        this.IS_EDIT = true;
      }
    });
    UiConfigService.uiConfig.subscribe(config => {
      this.uiConfig = config;
    });
    IdentityService.identity.subscribe(contactIdentity => {
      this.identity = contactIdentity;
      this.IS_PARTICIPANT_CONTACT = this.identity && this.identity.contactProfile_id &&
        this.identity.contactProfile_id === 'participant';
    });
    HelpTextService.helpText.subscribe(helpText => {
      if (helpText.contact) {
        this.helpText = helpText.contact;
      }
    });
  }

  get id() {
    return this.contactForm.get('id');
  }

  get firstName() {
    return this.contactForm.get('firstName');
  }

  get lastName() {
    return this.contactForm.get('lastName');
  }

  get contactProfile_id() {
    return this.contactForm.get('contactProfile_id');
  }

  get role_id() {
    return this.contactForm.get('role_id');
  }

  get participant_id() {
    return this.contactForm.get('participant_id');
  }

  get is_deactive() {
    return this.contactForm.get('is_deactive');
  }

  ngOnInit() {
    let id = 'contact-add';
    if (this.IS_EDIT) {
      id = 'contact-edit';
      PageHeaderService.updatePageHeader({backPath: 'manage-contact/contacts', subTitle: 'Loading...'});
    } else {
      PageHeaderService.updatePageHeader({backPath: 'manage-contact/contacts'});
    }
    this.componentNavItem = this.navItems.getItemById(id);
    if (this.componentNavItem) {
      this.pageTitle.setTitle(this.componentNavItem.name);
    }

    this.setupForm();

    if (this.contactId) {
      this.fetchContact(this.contactId);
    }
  }

  ngAfterViewInit() {
    this.initAutocomplete();
    setTimeout(() => {
      if (this.IS_PARTICIPANT_CONTACT) {
        this.contactForm.patchValue({
          contactProfile_id: {id: this.identity.contactProfile_id},
          participant_id: {id: this.identity.participant_id},
        });
      }
    });
  }

  public getErrorMessage(fieldName: string): string {
    return this.errorMessageService.getError(this[fieldName], fieldName);
  }

  public submitContactForm(): void {
    this.submitted = true;
    this.contact = this.prepareFormFields();
    if (this.contactForm.valid) {
      this.setInProgress();
      if (this.IS_EDIT) {
        this.contactService.updateContact(this.contact)
          .subscribe(response => {
              if (response && response.hasOwnProperty('data') && response.data) {
                this.setValue(response.data);
                this.errorMessageService.openSnackBar('Contact successfully updated.', 5000, 'OK');
                if (this.contactId === this.identity._id) {
                  window.location.reload();
                }
              }
              this.setNotInProgress();
              this.submitted = false;
            },
            err => {
              this.setNotInProgress();
              this.errorMessageService.handleServerErrors(err, this.contactForm);
              this.submitted = false;
            });
      } else {
        this.contactService.addContact(this.contact)
          .subscribe(response => {
              if (response && response.hasOwnProperty('data') && response.data) {
                this.router.navigate(['manage-contact/contacts']);
                this.errorMessageService.openSnackBar('Contact successfully added.', 5000, 'OK');
              }
              this.setNotInProgress();
            },
            err => {
              this.setNotInProgress();
              this.errorMessageService.handleServerErrors(err, this.contactForm);
              this.submitted = false;
              if (err && err.hasOwnProperty('forceSuccess') && err.forceSuccess) {
                this.router.navigate(['manage-contact/contacts']);
              }
            });
      }
    } else {
      this.submitted = false;
    }
  }

  public contactProfileDisplayFn(contactProfile?: ContactProfileInterface): string | undefined {
    return contactProfile ? (contactProfile.id) : undefined;
  }

  public contactRoleDisplayFn(contactRole?: ContactRoleInterface): string | undefined {
    return contactRole ? (contactRole.id) : undefined;
  }

  public participantDisplayFn(participant?: ParticipantInterface): string | undefined {
    return participant ? (participant.id) : undefined;
  }

  public clhPlatformDisplayFn(clhPlatform?: ClhPlatformInterface): string | undefined {
    return clhPlatform ? (clhPlatform.id) : undefined;
  }

  public unlockAccount(): void {
    if (this.IS_EDIT && this.contact.is_locked && this.contact.is_activation_complete && !this.contact.is_deactive) {
      this.setInProgress();
      this.contactService.sendUnlockToken(this.contact)
        .subscribe(response => {
            if (response && response.hasOwnProperty('data') && response.data) {
              this.setValue(response.data);
              this.errorMessageService.openSnackBar('User has been emailed containing link to unlock the account.', 5000, 'OK');
            }
            this.setNotInProgress();
            this.submitted = false;
          },
          err => {
            this.setNotInProgress();
            this.errorMessageService.handleServerErrors(err, this.contactForm);
            this.submitted = false;
          });
    }
  }

  public sendActivationToken(): void {
    if (this.IS_EDIT && !this.contact.is_activation_complete && !this.contact.is_locked && !this.contact.is_deactive) {
      this.setInProgress();
      this.contactService.sendActivationToken(this.contact)
        .subscribe(response => {
            if (response && response.hasOwnProperty('data') && response.data) {
              this.setValue(response.data);
              this.errorMessageService.openSnackBar('User has been emailed containing link to activate the account.', 5000, 'OK');
            }
            this.setNotInProgress();
            this.submitted = false;
          },
          err => {
            this.setNotInProgress();
            this.errorMessageService.handleServerErrors(err, this.contactForm);
            this.submitted = false;
          });
    }
  }

  public convertTimezone(date: Date): moment {
    return this.uiConfigParameterService.convertTimezone(date);
  }

  private fetchContact(id: string): void {
    this.setInProgress();
    this.contactService.getContact(id)
      .subscribe(response => {
          if (response && response.hasOwnProperty('data') && response.data) {
            if (!this.IS_PARTICIPANT_CONTACT || (this.IS_PARTICIPANT_CONTACT && this.identity.participant_id &&
              this.identity.contactProfile_id === response.data.contactProfile_id &&
              this.identity.participant_id === response.data.participant_id)) {
              this.setValue(response.data);
            } else {
              this.errorMessageService.openSnackBar(this.errorMessageService.getNonFormError('NO_EDIT_ACCESS'), 5000, 'OK');
              this.router.navigate(['/manage-contact/contacts']);
            }
          }
          this.setNotInProgress();
        },
        err => {
          this.setNotInProgress();
          this.errorMessageService.handleServerErrors(err, this.contactForm);
          this.router.navigate(['/']);
        });
  }

  private initAutocomplete(): void {
    this.filteredContactProfiles = this.contactProfile_id.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        startWith(null),
        switchMap(
          (term: string | any) =>
            (typeof term === 'string' || term === '' || term === null) ?
              this.contactProfileService.getContactProfilesForAutocomplete({
                term: term,
                limit: 10
              })
              : []
        )
      );
  }

  private setInProgress(): void {
    this.contactForm.disable();
    this.inProgress = true;
  }

  private setNotInProgress(): void {
    this.inProgress = false;
    this.contactForm.enable();
  }

  private setupForm(): void {
    this.contactForm = new FormGroup({
      'id': new FormControl(this.contact.id, [
        Validators.required,
        Validators.email
      ]),
      'firstName': new FormControl(this.contact.firstName, [
        Validators.required,
        Validators.pattern(/^[A-Za-z]+$/),
        Validators.minLength(1),
        Validators.maxLength(100),
      ]),
      'lastName': new FormControl(this.contact.lastName, [
        Validators.required,
        Validators.pattern(/^[A-Za-z]+$/),
        Validators.minLength(1),
        Validators.maxLength(100),
      ]),
      'contactProfile_id': new FormControl(this.contact.contactProfile_id, [
        Validators.required
      ]),
      'role_id': new FormControl(this.contact.role_id, [
        Validators.required
      ]),
      'participant_id': new FormControl(this.contact.participant_id, [
        Validators.required
      ]),
      'is_deactive': new FormControl(this.contact.is_deactive, [
        Validators.required
      ])
    });
    this.initExtraValidators();
  }

  private initExtraValidators(): void {
    this.contactProfile_id.valueChanges.subscribe(data => this.onContactProfileIdChanged(data));
  }

  private onContactProfileIdChanged(value: any) {
    if (!this.submitted) {
      this.filteredContactRoles = null;
      this.filteredParticipants = null;
      this.filteredClhPlatforms = null;
      this.role_id.setValue({
        id: null,
        description: null
      });
      this.participant_id.setValue({
        id: null,
        name: null,
        routing_id: null
      });
      if (value && value.id) {
        this.filteredContactRoles = this.role_id.valueChanges
          .pipe(
            debounceTime(100),
            distinctUntilChanged(),
            startWith(null),
            switchMap(
              (term: string | any) =>
                (typeof term === 'string' || term === '' || term === null) ?
                  this.contactRoleService.getContactRolesForAutocomplete({
                    profile_id: value && value.id,
                    term: term,
                    limit: 10
                  }) : []
            )
          );
        if (value && value.id === 'participant') {
          this.filteredParticipants = this.participant_id.valueChanges
            .pipe(
              debounceTime(100),
              distinctUntilChanged(),
              startWith(null),
              switchMap(
                (term: string | any) =>
                  (typeof term === 'string' || term === '' || term === null) ?
                    this.participantService.getParticipantsForAutocomplete({
                      term: term,
                      limit: 10
                    })
                    : []
              )
            );
        } else if (value && value.id === 'platform') {
          this.filteredClhPlatforms = this.participant_id.valueChanges
            .pipe(
              debounceTime(100),
              distinctUntilChanged(),
              startWith(null),
              switchMap(
                (term: string | any) =>
                  (typeof term === 'string' || term === '' || term === null) ?
                    this.clhPlatformService.getClhPlatformsForAutocomplete({
                      term: term,
                      limit: 10
                    })
                    : []
              )
            );
        }
      }
    }
  }

  private prepareFormFields(): Contact {
    const formModel = this.contactForm.value;
    if (formModel.contactProfile_id && !formModel.contactProfile_id.id) {
      this.contactProfile_id.setValue(null);
    }
    if (formModel.role_id && !formModel.role_id.id) {
      this.role_id.setValue(null);
    }
    if (formModel.participant_id && !formModel.participant_id.id) {
      this.participant_id.setValue(null);
    }
    return {
      id: formModel.id,
      firstName: formModel.firstName,
      lastName: formModel.lastName,
      contactProfile_id: formModel.contactProfile_id && formModel.contactProfile_id.id ? formModel.contactProfile_id.id : null,
      role_id: formModel.role_id && formModel.role_id.id ? formModel.role_id.id : null,
      participant_id: formModel.participant_id && formModel.participant_id.id ? formModel.participant_id.id : null,
      is_deactive: formModel.is_deactive,
      _id: this.contact && this.contact._id ? this.contact._id : null
    };
  }

  private setValue(data: Contact): void {
    PageHeaderService.updatePageHeader({backPath: 'manage-contact/contacts', subTitle: data.id});
    setTimeout(() => { // setTimeout patch for autocomplete when editing
      this.contactForm.patchValue({
        id: data.id,
        firstName: data.firstName,
        lastName: data.lastName,
        contactProfile_id: {id: data.contactProfile_id},
        role_id: {id: data.role_id},
        participant_id: {id: data.participant_id},
        is_deactive: data.is_deactive,
      });
      this.contact = data;
      this.contact.last_tmptoken_gen_date = this.convertTimezone(this.contact.last_tmptoken_gen_date);
    });
  }

}
