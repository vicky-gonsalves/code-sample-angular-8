import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {appAnimations} from '../../../../../core/animations/animations';
import {ContactInterface} from '../../../../../interface/contact.interface';
import {NavItemInterface} from '../../../../../interface/nav-item.interface';
import {ContactService} from '../../../../../shared/services/contact/contact.service';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {NavItemService} from '../../../../../shared/services/nav-item/nav-item.service';
import {PageHeaderService} from '../../../../../shared/services/page-header/page-header.service';
import {PageTitleService} from '../../../../../shared/services/page-title/page-title.service';

@Component({
  selector: 'contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.scss'],
  animations: appAnimations
})
export class ContactListComponent implements OnInit {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  public searchTerm: {
    id: string,
    firstName: string
    lastName: string,
    participant_id: string
  };
  public displayedColumns =
    ['id', 'firstName', 'lastName', 'contactProfile_id', 'role_id', 'participant_id', 'is_deactive', 'actions'];
  public dataSource: MatTableDataSource<ContactInterface>;
  public paginatorOptions = {
    count: 0,
    pageSize: 10,
    pageSizeOptions: [5, 10, 25, 50, 100],
    showFirstLastButtons: true
  };
  public inProgress: Boolean = true;
  private sortBy = '-createdAt';
  private pageSize = 10;
  private componentNavItem: NavItemInterface;

  constructor(private navItems: NavItemService,
              private contactService: ContactService,
              private errorMsgService: ErrorMessageService,
              private pageTitle: PageTitleService) {
    this.searchTerm = {
      id: null,
      firstName: null,
      lastName: null,
      participant_id: null
    };
  }

  ngOnInit() {
    // Get Navigation Component Name
    this.componentNavItem = this.navItems.getItemById('contact');
    if (this.componentNavItem) {
      // Set Page Title
      this.pageTitle.setTitle(this.componentNavItem.name);
    }

    // Set Page Header Details
    PageHeaderService.updatePageHeader({
      showImage: true,
      icon: 'recent_actors',
      actionButton: {
        title: 'Add new contact',
        url: 'manage-contact/contacts/add'
      }
    });

    // Fetch Contacts
    this.fetchContactListing();
  }

  public sortData(sort: Sort): void {
    const data = this.dataSource;
    if (!sort.active || sort.direction === '') {
      this.sortBy = '-createdAt';
    } else {
      this.sortBy = sort.direction === 'asc' ? sort.active : '-' + sort.active;
    }
    this.paginator.pageIndex = 0;
    this.fetchContactListing();
  }

  public fetchContactListing(pageEvent?: PageEvent): void {
    if (pageEvent) {
      this.pageSize = pageEvent.pageSize;
    }
    const options = {
      limit: (pageEvent ? (pageEvent.pageSize) : this.pageSize ? this.pageSize : this.paginatorOptions.pageSize),
      page: (pageEvent ? (pageEvent.pageIndex + 1) : 1),
      sort: [this.sortBy]
    };
    if (this.searchTerm.id) {
      options['id'] = this.searchTerm.id;
    }
    if (this.searchTerm.firstName) {
      options['firstName'] = this.searchTerm.firstName;
    }
    if (this.searchTerm.lastName) {
      options['lastName'] = this.searchTerm.lastName;
    }
    if (this.searchTerm.participant_id) {
      options['participant_id'] = this.searchTerm.participant_id;
    }
    this.inProgress = true;
    this.contactService.getContacts(options)
      .subscribe(response => {
          if (response && response.data && response.data.rows && response.data.rows.length) {
            this.dataSource = new MatTableDataSource(response.data.rows);
            this.paginatorOptions.count = response.data.count;
            this.dataSource.sort = this.sort;
          } else {
            this.errorMsgService.openSnackBar(this.errorMsgService.getNonFormError('NO_DATA'));
          }
          this.inProgress = false;
        },
        err => {
          this.errorMsgService.handleServerErrors(err);
          this.inProgress = false;
        });
  }

  public applyFilter(searchCriteria: { key: string, value: string }): void {
    this.searchTerm[searchCriteria.key] = searchCriteria.value;
    this.paginator.pageIndex = 0;
    this.fetchContactListing();
  }
}
