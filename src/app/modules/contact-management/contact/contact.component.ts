import {Component, OnInit} from '@angular/core';
import {appAnimations} from '../../../core/animations/animations';

@Component({
  selector: 'contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
  animations: appAnimations
})
export class ContactComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
