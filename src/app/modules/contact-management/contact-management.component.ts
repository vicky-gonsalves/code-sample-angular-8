import {Component, OnInit} from '@angular/core';
import {appAnimations} from '../../core/animations/animations';

@Component({
  selector: 'contact-management',
  templateUrl: './contact-management.component.html',
  animations: appAnimations
})
export class ContactManagementComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
