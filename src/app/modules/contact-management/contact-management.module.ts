import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ContactManagementComponent} from './contact-management.component';
import {SharedModule} from '../../shared/shared.module';
import {routing} from './contact-management.routes';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    routing
  ],
  declarations: [ContactManagementComponent]
})
export class ContactManagementModule {
}
