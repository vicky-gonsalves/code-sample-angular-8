import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ParticipantManagementComponent} from './participant-management.component';
import {SharedModule} from '../../shared/shared.module';
import {routing} from './participant-management.routes';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    routing
  ],
  declarations: [ParticipantManagementComponent]
})
export class ParticipantManagementModule {
}
