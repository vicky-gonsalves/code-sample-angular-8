import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserDataGuard} from '../../core/guards/user-data/user-data.guard';

export const routes: Routes = [
  {
    path: '',
    canActivate: [UserDataGuard],
    children: [
      {
        path: 'participants',
        canActivateChild: [UserDataGuard],
        loadChildren: () => import('./participant/participant.module').then(mod => mod.ParticipantModule),
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'participant'}
      }
    ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);

