import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../../../shared/shared.module';
import {routing} from './participant.routes';
import {ParticipantComponent} from './participant.component';
import {ParticipantEditorComponent} from './components/participant-editor/participant-editor.component';
import {ParticipantListComponent} from './components/participant-list/participant-list.component';
import {ViewTimeoutOffsetComponent} from './components/view-timeout-offset/view-timeout-offset.component';
import {ViewTimeoutOffsetDetailListComponent} from './components/view-timeout-offset-detail-list/view-timeout-offset-detail-list.component';
import {ViewTimeoutOffsetDetailEditorComponent} from './components/view-timeout-offset-detail-editor/view-timeout-offset-detail-editor.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    routing
  ],
  declarations: [
    ParticipantComponent,
    ParticipantEditorComponent,
    ParticipantListComponent,
    ViewTimeoutOffsetComponent,
    ViewTimeoutOffsetDetailListComponent,
    ViewTimeoutOffsetDetailEditorComponent
  ],
  entryComponents: [ViewTimeoutOffsetDetailEditorComponent]
})
export class ParticipantModule {
}
