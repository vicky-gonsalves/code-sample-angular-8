import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {flatMap} from 'lodash';
import {appAnimations} from '../../../../../core/animations/animations';
import {NavItemInterface} from '../../../../../interface/nav-item.interface';
import {ParticipantInterface} from '../../../../../interface/participant.interface';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {NavItemService} from '../../../../../shared/services/nav-item/nav-item.service';
import {PageHeaderService} from '../../../../../shared/services/page-header/page-header.service';
import {PageTitleService} from '../../../../../shared/services/page-title/page-title.service';
import {ParticipantService} from '../../../../../shared/services/participant/participant.service';

@Component({
  selector: 'participant-list',
  templateUrl: './participant-list.component.html',
  styleUrls: ['./participant-list.component.scss'],
  animations: appAnimations
})
export class ParticipantListComponent implements OnInit {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  public searchTerm: {
    id: string,
    short_id: string,
    name: string,
    connections: string,
    settlementAccount: string,
    privileges: string
  };
  public displayedColumns =
    ['actions', 'id', 'short_id', 'name', 'connections', 'settlementAccount', 'privileges', 'initial_signon', 'is_suspended', 'effective_date',
      'is_deactive'];
  public dataSource: MatTableDataSource<ParticipantInterface>;
  public paginatorOptions = {
    count: 0,
    pageSize: 10,
    pageSizeOptions: [5, 10, 25, 50, 100],
    showFirstLastButtons: true
  };
  public inProgress: Boolean = true;
  private sortBy = '-createdAt';
  private pageSize = 10;
  private componentNavItem: NavItemInterface;

  constructor(private navItems: NavItemService,
              private participantService: ParticipantService,
              private errorMsgService: ErrorMessageService,
              private pageTitle: PageTitleService) {
    this.searchTerm = {
      id: null,
      short_id: null,
      name: null,
      connections: null,
      settlementAccount: null,
      privileges: null
    };
  }

  ngOnInit() {
    // Get Navigation Component Name
    this.componentNavItem = this.navItems.getItemById('participant');
    if (this.componentNavItem) {
      // Set Page Title
      this.pageTitle.setTitle(this.componentNavItem.name);
    }

    // Set Page Header Details
    PageHeaderService.updatePageHeader({
      showImage: true,
      icon: 'group',
      actionButton: {
        title: 'Add new participant',
        url: 'manage-participant/participants/add'
      }
    });

    // Fetch Participants
    this.fetchParticipantListing();
  }

  public sortData(sort: Sort): void {
    const data = this.dataSource;
    if (!sort.active || sort.direction === '') {
      this.sortBy = '-createdAt';
    } else {
      this.sortBy = sort.direction === 'asc' ? sort.active : '-' + sort.active;
    }
    this.paginator.pageIndex = 0;
    this.fetchParticipantListing();
  }

  public fetchParticipantListing(pageEvent?: PageEvent): void {
    if (pageEvent) {
      this.pageSize = pageEvent.pageSize;
    }
    const options = {
      limit: (pageEvent ? (pageEvent.pageSize) : this.pageSize ? this.pageSize : this.paginatorOptions.pageSize),
      page: (pageEvent ? (pageEvent.pageIndex + 1) : 1),
      sort: [this.sortBy]
    };
    if (this.searchTerm.id) {
      options['id'] = this.searchTerm.id;
    }
    if (this.searchTerm.short_id) {
      options['short_id'] = this.searchTerm.short_id;
    }
    if (this.searchTerm.name) {
      options['name'] = this.searchTerm.name;
    }
    if (this.searchTerm.connections) {
      options['connections'] = this.searchTerm.connections;
    }
    if (this.searchTerm.settlementAccount) {
      options['settlementAccount'] = this.searchTerm.settlementAccount;
    }
    if (this.searchTerm.privileges) {
      options['privileges'] = this.searchTerm.privileges;
    }
    this.inProgress = true;
    this.participantService.getParticipants(options)
      .subscribe(response => {
          if (response && response.data && response.data.rows && response.data.rows.length) {
            this.dataSource = new MatTableDataSource(response.data.rows);
            this.paginatorOptions.count = response.data.count;
            this.dataSource.sort = this.sort;
          } else {
            this.errorMsgService.openSnackBar(this.errorMsgService.getNonFormError('NO_DATA'));
          }
          this.inProgress = false;
        },
        err => {
          this.errorMsgService.handleServerErrors(err);
          this.inProgress = false;
        });
  }

  public applyFilter(searchCriteria: { key: string, value: string }): void {
    this.searchTerm[searchCriteria.key] = searchCriteria.value;
    this.paginator.pageIndex = 0;
    this.fetchParticipantListing();
  }

  public getDefaultConnectionId(connections: any): any {
    return connections.filter(conn => conn.default_flag === true)[0].connection_id;
  }

  public getOtherConnectionId(connections: any): any {
    return flatMap(connections.filter(conn => conn.default_flag === false), 'connection_id').join(', ');
  }
}
