import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {appAnimations} from '../../../../../core/animations/animations';
import {ParticipantMsgInfoInterface} from '../../../../../interface/participant-msg-info.interface';
import {ConfirmDialogComponent} from '../../../../../shared/components/dialogs/confirm-dialog/confirm-dialog.component';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {ParticipantMsgInfoService} from '../../../../../shared/services/participant-msg-info/participant-msg-info.service';
import {ViewTimeoutOffsetDetailEditorComponent} from '../view-timeout-offset-detail-editor/view-timeout-offset-detail-editor.component';

@Component({
  selector: 'view-timeout-offset-detail-list',
  templateUrl: './view-timeout-offset-detail-list.component.html',
  styleUrls: ['./view-timeout-offset-detail-list.component.scss'],
  animations: appAnimations
})
export class ViewTimeoutOffsetDetailListComponent implements OnInit {
  @Input() participantId: string;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  public displayedColumns = ['msg_type', 'timeout_offset', 'actions'];
  public dataSource: MatTableDataSource<ParticipantMsgInfoInterface>;
  public paginatorOptions = {
    count: 0,
    pageSize: 10,
    pageSizeOptions: [5, 10, 25, 50, 100],
    showFirstLastButtons: true
  };
  public inProgress: Boolean = true;
  public pageEvent: any;
  private sortBy = '-_id';
  private pageSize = 10;

  constructor(private errorMsgService: ErrorMessageService,
              private dialog: MatDialog,
              private participantMsgInfoService: ParticipantMsgInfoService) {
  }


  ngOnInit() {
    this.fetchParticipantMsgInfoListing();
  }

  public fetchParticipantMsgInfoListing(_pageEvent?: PageEvent): void {
    if (_pageEvent) {
      this.pageEvent = _pageEvent;
      this.pageSize = _pageEvent.pageSize;
    }
    const options = {
      limit: (this.pageEvent ? (this.pageEvent.pageSize) : this.pageSize ? this.pageSize : this.paginatorOptions.pageSize),
      page: (this.pageEvent ? (this.pageEvent.pageIndex + 1) : 1),
      sort: [this.sortBy]
    };
    this.inProgress = true;
    this.participantMsgInfoService.getParticipantMsgInfo(options, this.participantId)
      .subscribe(response => {
          if (response && response.data && response.data.rows && response.data.rows.length) {
            this.dataSource = new MatTableDataSource(response.data.rows);
            this.paginatorOptions.count = response.data.count;
          }
          this.inProgress = false;
        },
        err => {
          this.errorMsgService.handleServerErrors(err);
          this.inProgress = false;
        });
  }


  public addDetail(): void {
    this.openEditorDialog();
  }

  public editDetail(participantMsgInfo: ParticipantMsgInfoInterface): void {
    this.openEditorDialog(participantMsgInfo);
  }

  public deleteDetail(participantMsgInfo: ParticipantMsgInfoInterface): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '250px',
      disableClose: true,
      closeOnNavigation: true,
      data: {title: 'Confirm Delete', text: 'Do you want to delete this Timeout Offset?', okButtonText: 'Yes', cancelButtonText: 'No'}
    });

    dialogRef.afterClosed().subscribe(response => {
      if (response) {
        this.participantMsgInfoService.deleteParticipantMsgInfo(participantMsgInfo)
          .subscribe(() => {
              this.dataSource = null;
              this.fetchParticipantMsgInfoListing();
              this.errorMsgService.openSnackBar('Timeout Offset successfully deleted.', 5000, 'OK');
              this.inProgress = false;
            },
            err => {
              this.errorMsgService.handleServerErrors(err);
              this.inProgress = false;
            });
      }
    });
  }

  private openEditorDialog(participantMsgInfo?: ParticipantMsgInfoInterface): void {
    const conf: MatDialogConfig = {
      autoFocus: false,
      disableClose: true,
      closeOnNavigation: true,
      panelClass: 'panelClass',
      maxWidth: '800px',
      data: {id: this.participantId}
    };
    if (participantMsgInfo) {
      conf.data['participantMsgInfo'] = participantMsgInfo;
    }
    const dialogRef = this.dialog.open(ViewTimeoutOffsetDetailEditorComponent, conf);
    dialogRef.afterClosed().subscribe(result => {
      if (result && result.success && result.data) {
        this.fetchParticipantMsgInfoListing();
      }
    });
  }

}
