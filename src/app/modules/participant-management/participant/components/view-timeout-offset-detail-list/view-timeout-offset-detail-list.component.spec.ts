import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewTimeoutOffsetDetailListComponent } from './view-timeout-offset-detail-list.component';

describe('ViewTimeoutOffsetDetailListComponent', () => {
  let component: ViewTimeoutOffsetDetailListComponent;
  let fixture: ComponentFixture<ViewTimeoutOffsetDetailListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewTimeoutOffsetDetailListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewTimeoutOffsetDetailListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
