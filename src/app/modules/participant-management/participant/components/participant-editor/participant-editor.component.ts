import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {AfterViewInit, Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatAutocompleteSelectedEvent} from '@angular/material/autocomplete';
import {MatChipInputEvent} from '@angular/material/chips';
import {ActivatedRoute, Router} from '@angular/router';
import {findIndex, flatMap} from 'lodash';
import * as moment from 'moment-timezone';
import {Observable} from 'rxjs';
import {debounceTime, distinctUntilChanged, startWith, switchMap} from 'rxjs/internal/operators';
import {appAnimations} from '../../../../../core/animations/animations';
import {HelpTextService} from '../../../../../core/services/help-text/help-text.service';
import {ConnectionInterface} from '../../../../../interface/connection.interface';
import {NavItemInterface} from '../../../../../interface/nav-item.interface';
import {ParticipantPrivilegeInterface} from '../../../../../interface/participant-privilege.interface';
import {SettlementAccountInterface} from '../../../../../interface/settlement-account.interface';
import {Connection} from '../../../../../model/connection.model';
import {ParticipantPrivilege} from '../../../../../model/participant-privilege.model';
import {Participant} from '../../../../../model/participant.model';
import {SettlementAccount} from '../../../../../model/settlement-account.model';
import {ConnectionService} from '../../../../../shared/services/connection/connection.service';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {NavItemService} from '../../../../../shared/services/nav-item/nav-item.service';
import {PageHeaderService} from '../../../../../shared/services/page-header/page-header.service';
import {PageTitleService} from '../../../../../shared/services/page-title/page-title.service';
import {ParticipantPrivilegeService} from '../../../../../shared/services/participant-privilege/participant-privilege.service';
import {ParticipantService} from '../../../../../shared/services/participant/participant.service';
import {SettlementAccountService} from '../../../../../shared/services/settlement-account/settlement-account.service';

@Component({
  selector: 'participant-editor',
  templateUrl: './participant-editor.component.html',
  styleUrls: ['./participant-editor.component.scss'],
  animations: appAnimations
})
export class ParticipantEditorComponent implements OnInit, AfterViewInit {
  public IS_EDIT: Boolean = false;
  public effectiveDateDisabled: Boolean = false;
  public participantForm: FormGroup;
  public participant: Participant = new Participant(
    null,
    null,
    null,
    null,
    null,
    null,
    false,
    false,
    null,
    false,
    null,
    null,
    null
  );
  public addOnBlur: Boolean = true;
  public removable: Boolean = true;
  public selectable: Boolean = true;
  public visible: Boolean = true;
  public separatorKeysCodes = [ENTER, COMMA];
  public displayOtherConnectionIds = [];
  public displayParticipantPrivileges = [];
  public inProgress: Boolean = false;
  public submitted: Boolean = false;
  public minDate = moment().utcOffset('+00:00', true).startOf('day').toISOString();
  public filteredDefaultConnections: Observable<Connection[]>;
  public filteredOtherConnections: Observable<Connection[]>;
  public filteredSettlementAccounts: Observable<SettlementAccount[]>;
  public filteredPrivileges: Observable<ParticipantPrivilege[]>;
  public helpText: any;
  private componentNavItem: NavItemInterface;
  private participantId: string;

  constructor(public navItems: NavItemService,
              public pageTitle: PageTitleService,
              private errorMessageService: ErrorMessageService,
              private participantService: ParticipantService,
              private participantPrivilegeService: ParticipantPrivilegeService,
              private connectionService: ConnectionService,
              private settlementAccountService: SettlementAccountService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe(params => {
      if (params.hasOwnProperty('id') && params.id.length) {
        this.participantId = params.id;
        this.IS_EDIT = true;
      }
    });
    HelpTextService.helpText.subscribe(helpText => {
      if (helpText.participant) {
        this.helpText = helpText.participant;
      }
    });
  }

  get id() {
    return this.participantForm.get('id');
  }

  get short_id() {
    return this.participantForm.get('short_id');
  }

  get name() {
    return this.participantForm.get('name');
  }

  get default_connection_id() {
    return this.participantForm.get('default_connection_id');
  }

  get other_connection_id() {
    return this.participantForm.get('other_connection_id');
  }

  get settlementAccount() {
    return this.participantForm.get('settlementAccount');
  }

  get privileges() {
    return this.participantForm.get('privileges');
  }

  get privilegesChips() {
    return this.participantForm.get('privilegesChips');
  }

  get initial_signon() {
    return this.participantForm.get('initial_signon');
  }

  get is_suspended() {
    return this.participantForm.get('is_suspended');
  }

  get effective_date() {
    return this.participantForm.get('effective_date');
  }

  get is_deactive() {
    return this.participantForm.get('is_deactive');
  }

  ngOnInit() {
    let id = 'participant-add';
    if (this.IS_EDIT) {
      id = 'participant-edit';
      PageHeaderService.updatePageHeader({backPath: 'manage-participant/participants', subTitle: 'Loading...'});
    } else {
      PageHeaderService.updatePageHeader({backPath: 'manage-participant/participants'});
    }
    this.componentNavItem = this.navItems.getItemById(id);
    if (this.componentNavItem) {
      this.pageTitle.setTitle(this.componentNavItem.name);
    }

    this.setupForm();

    if (this.participantId) {
      this.fetchParticipant(this.participantId);
    }
  }

  ngAfterViewInit() {
    this.initAutocomplete();
  }

  public submitParticipantForm(): void {
    this.submitted = true;
    this.checkPrivilegeValidity();
    this.participant = this.prepareFormFields();
    if (this.participantForm.valid) {
      this.setInProgress();
      if (this.IS_EDIT) {
        this.participantService.updateParticipant(this.participant)
          .subscribe(response => {
              if (response && response.hasOwnProperty('data') && response.data) {
                this.setValue(response.data);
                this.router.navigate(['manage-participant/participants']);
                this.errorMessageService.openSnackBar('Participant successfully updated.', 5000, 'OK');
              }
              this.setNotInProgress();
            },
            err => {
              this.setNotInProgress();
              this.errorMessageService.handleServerErrors(err, this.participantForm);
            });
      } else {
        this.participantService.addParticipant(this.participant)
          .subscribe(response => {
              if (response && response.hasOwnProperty('data') && response.data) {
                this.router.navigate(['manage-participant/participants']);
                this.errorMessageService.openSnackBar('Participant successfully added.', 5000, 'OK');
              }
              this.setNotInProgress();
            },
            err => {
              this.setNotInProgress();
              this.errorMessageService.handleServerErrors(err, this.participantForm);
            });
      }
    }
  }

  public getErrorMessage(fieldName: string): string {
    return this.errorMessageService.getError(this[fieldName], fieldName);
  }

  public defaultConnectionDisplayFn(connection?: ConnectionInterface): string | undefined {
    return connection ? (connection.id) : undefined;
  }

  public otherConnectionDisplayFn(connection?: ConnectionInterface): string | undefined {
    return connection ? (connection.id) : undefined;
  }

  public settlementAccountDisplayFn(settlementAccount?: SettlementAccountInterface): string | undefined {
    return settlementAccount ? (settlementAccount.id) : undefined;
  }

  public privilegesDisplayFn(participantPrivilege?: ParticipantPrivilegeInterface): string | undefined {
    return participantPrivilege ? (participantPrivilege.id) : undefined;
  }

  public addConnection(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our item
    if (value && value.hasOwnProperty('id')) {
      this.displayOtherConnectionIds.push(value);
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  public addPrivilege(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our item
    if (value && value.hasOwnProperty('id')) {
      this.displayParticipantPrivileges.push(value);
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  public removeConnection(item: any): void {
    this.filteredOtherConnections = null;
    this.setOtherConnectionsAutoComplete(this.participantForm.value.default_connection_id);
    const index = findIndex(this.displayOtherConnectionIds, item);
    if (index >= 0) {
      this.displayOtherConnectionIds.splice(index, 1);
    }
  }

  public removePrivilege(item: any): void {
    this.filteredPrivileges = null;
    this.setPrivilegeAutoComplete();
    const index = findIndex(this.displayParticipantPrivileges, item);
    if (index >= 0) {
      this.displayParticipantPrivileges.splice(index, 1);
    }
    this.checkPrivilegeValidity();
  }

  public addSelectConnection(event: MatAutocompleteSelectedEvent) {
    const option = event.option;
    const value = option.value;
    if (value && value.hasOwnProperty('id')) {
      this.displayOtherConnectionIds.push(value);
    }
  }

  public addSelectPrivilege(event: MatAutocompleteSelectedEvent) {
    const option = event.option;
    const value = option.value;
    if (value && value.hasOwnProperty('id')) {
      this.displayParticipantPrivileges.push(value);
    }
    this.checkPrivilegeValidity();
  }

  private fetchParticipant(id: string): void {
    this.participantService.getParticipant(id)
      .subscribe(response => {
          if (response && response.hasOwnProperty('data') && response.data) {
            this.setValue(response.data);
          }
          this.setNotInProgress();
        },
        err => {
          this.setNotInProgress();
          this.errorMessageService.handleServerErrors(err, this.participantForm);
        });
  }

  private initAutocomplete(): void {
    this.filteredDefaultConnections = this.default_connection_id.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        startWith(null),
        switchMap(
          (term: string | any) =>
            (typeof term === 'string' || term === '' || term === null) ?
              this.connectionService.getDefaultConnectionsForAutocomplete({
                term: term,
                limit: 10
              }) : [])
      );
    this.filteredSettlementAccounts = this.settlementAccount.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        startWith(null),
        switchMap(
          (term: string | any) =>
            (typeof term === 'string' || term === '' || term === null) ?
              this.settlementAccountService.getSettlementAccountsForAutocomplete({
                term: term,
                limit: 10,
                exists: true
              }, this.participant && this.participant.settlementAccount ? this.participant.settlementAccount : null) : [])
      );
    this.setPrivilegeAutoComplete();
  }

  private setPrivilegeAutoComplete(): void {
    this.filteredPrivileges = this.privileges.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        startWith(!this.privileges.value.length ? null : this.privileges.value),
        switchMap(
          (term: string | any) =>
            (typeof term === 'string' || term === '' || term === null) ?
              this.participantPrivilegeService.getParticipantPrivilegesForAutocomplete({
                term: term,
                limit: 10
              }, flatMap(this.displayParticipantPrivileges, 'id')) : []
        )
      );
  }

  private setOtherConnectionsAutoComplete(value?: any): void {
    this.filteredOtherConnections = null;
    this.filteredOtherConnections = this.other_connection_id.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        startWith(!this.other_connection_id.value.length ? null : this.other_connection_id.value),
        switchMap(
          (term: string | any) =>
            (typeof term === 'string' || term === '' || term === null) ?
              this.connectionService.getOtherConnectionsForAutocomplete({
                default: value && value.id,
                term: term,
                limit: 10
              }, flatMap(this.displayOtherConnectionIds, 'id')) : []
        )
      );
  }

  private setInProgress(): void {
    this.participantForm.disable();
    this.inProgress = true;
  }

  private setNotInProgress(): void {
    this.inProgress = false;
    this.participantForm.enable();
  }

  private setupForm(): void {
    this.participantForm = new FormGroup({
      'id': new FormControl(this.participant.id, [
        Validators.required,
        Validators.pattern(/^[A-Za-z\d]+$/),
        Validators.minLength(1),
        Validators.maxLength(35),
      ]),
      'short_id': new FormControl(this.participant.short_id, [
        Validators.required,
        Validators.pattern(/^[A-Za-z\d]+$/),
        Validators.minLength(1),
        Validators.maxLength(15),
      ]),
      'name': new FormControl(this.participant.name, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(100),
      ]),
      'default_connection_id': new FormControl(this.participant.default_connection_id, [
        Validators.required
      ]),
      'other_connection_id': new FormControl('', []),
      'other_connection_id_chips': new FormControl('', []),
      'settlementAccount': new FormControl(this.participant.settlementAccount, [
        Validators.required
      ]),
      'privileges': new FormControl('', []),
      'privilegesChips': new FormControl('', []),
      'initial_signon': new FormControl(this.participant.initial_signon, [
        Validators.required
      ]),
      'is_suspended': new FormControl(this.participant.is_suspended, [
        Validators.required
      ]),
      'effective_date': new FormControl(this.participant.effective_date, [
        Validators.required
      ]),
      'is_deactive': new FormControl(this.participant.is_deactive, [
        Validators.required
      ])
    });
    this.initExtraValidators();
  }

  private initExtraValidators(): void {
    this.default_connection_id.valueChanges.subscribe(data => this.onDefaultConnectionIdChanged(data));
    this.other_connection_id.valueChanges.subscribe(data => this.onOtherConnectionChanged(data)); // Hack to get always latest values
    this.privileges.valueChanges.subscribe(data => this.onPrivilegeChanged(data)); // Hack to get always latest values
  }

  private onOtherConnectionChanged(value: any) {
    this.filteredOtherConnections = null;
    this.setOtherConnectionsAutoComplete(this.participantForm.value.default_connection_id);
  }

  private onPrivilegeChanged(value: any) {
    this.filteredPrivileges = null;
    this.setPrivilegeAutoComplete();
  }

  private onDefaultConnectionIdChanged(value: any) {
    this.filteredOtherConnections = null;
    this.displayOtherConnectionIds.length = 0;
    this.other_connection_id.setValue({
      id: null,
      owner_id: null
    });
    if (value && value.id) {
      this.setOtherConnectionsAutoComplete(value);
    }
  }

  private prepareConnections(defaultConnectionId): { connection_id: string, default_flag: Boolean }[] {
    const connections = [{connection_id: defaultConnectionId.id, default_flag: true}];
    this.displayOtherConnectionIds.forEach(conn => {
      connections.push({connection_id: conn.id, default_flag: false});
    });
    return connections;
  }

  private prepareFormFields(): Participant {
    const formModel = this.participantForm.value;
    if (formModel.settlementAccount && !formModel.settlementAccount.id) {
      this.settlementAccount.setValue(null);
    }
    if (formModel.default_connection_id && !formModel.default_connection_id.id) {
      this.default_connection_id.setValue(null);
    }
    if (formModel.effective_date) {
      formModel.effective_date = moment(formModel.effective_date).utcOffset('+00:00', true).startOf('day').format();
    }
    return {
      id: formModel.id,
      short_id: formModel.short_id,
      name: formModel.name,
      connections: formModel.default_connection_id && formModel.default_connection_id.id
        ? this.prepareConnections(formModel.default_connection_id) : [],
      settlementAccount: formModel.settlementAccount && formModel.settlementAccount.id ? formModel.settlementAccount.id : null,
      privileges: flatMap(this.displayParticipantPrivileges, 'id'),
      initial_signon: formModel.initial_signon,
      is_suspended: formModel.is_suspended,
      effective_date: formModel.effective_date,
      is_deactive: formModel.is_deactive,
      _id: this.participant && this.participant._id ? this.participant._id : null
    };
  }

  private setValue(data: Participant): void {
    setTimeout(() => {
      const defaultConnectionId = data.connections.filter(conn => conn.default_flag === true)[0];
      this.participantForm.patchValue({
        id: data.id,
        short_id: data.short_id,
        name: data.name,
        default_connection_id: {id: defaultConnectionId.connection_id},
        other_connection_id_chips: data.connections.filter(conn => conn.default_flag === false),
        settlementAccount: {id: data.settlementAccount},
        privileges: {id: data.privileges},
        initial_signon: data.initial_signon,
        is_suspended: data.is_suspended,
        effective_date: data.effective_date,
        is_deactive: data.is_deactive,
      });
      this.participant = data;
      this.displayOtherConnectionIds = data.connections.filter(conn => conn.default_flag === false).map(id => ({id: id.connection_id}));
      this.displayParticipantPrivileges = data.privileges.map(priv => ({id: priv}));
      const effective_date = moment(this.participant.effective_date);
      if (moment(this.minDate) > effective_date) {
        this.minDate = effective_date;
      }
      if (moment(this.participant.effective_date) <= moment().utcOffset('+00:00', true).startOf('day')) {
        this.effectiveDateDisabled = true;
      }
    });
    PageHeaderService.updatePageHeader({
      backPath: 'manage-participant/participants',
      subTitle: this.participant.id,
      actionButton: {
        title: 'View Timeout Offset',
        url: `manage-participant/participants/view-timeout-offset/${this.participantId}`
      }
    });
  }

  private checkPrivilegeValidity() {
    if (!this.displayParticipantPrivileges.length) {
      this.privilegesChips.updateValueAndValidity();
      this.privilegesChips.markAsDirty();
      this.privilegesChips.setErrors({required: true});
    } else {
      this.privilegesChips.updateValueAndValidity();
      this.privilegesChips.markAsPristine();
      this.privilegesChips.setErrors(null);
    }
  }
}
