import {Component, OnInit} from '@angular/core';
import {appAnimations} from '../../../../../core/animations/animations';
import {NavItemInterface} from '../../../../../interface/nav-item.interface';
import {NavItemService} from '../../../../../shared/services/nav-item/nav-item.service';
import {PageTitleService} from '../../../../../shared/services/page-title/page-title.service';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Participant} from '../../../../../model/participant.model';
import {PageHeaderService} from '../../../../../shared/services/page-header/page-header.service';
import {ParticipantService} from '../../../../../shared/services/participant/participant.service';

@Component({
  selector: 'view-timeout-offset',
  templateUrl: './view-timeout-offset.component.html',
  styleUrls: ['./view-timeout-offset.component.scss'],
  animations: appAnimations
})
export class ViewTimeoutOffsetComponent implements OnInit {
  public inProgress: Boolean = true;
  private componentNavItem: NavItemInterface;
  private participantId: string;
  public participant: Participant;

  constructor(public navItems: NavItemService,
              public pageTitle: PageTitleService,
              private errorMessageService: ErrorMessageService,
              private participantService: ParticipantService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe(params => {
      if (params.hasOwnProperty('id') && params.id.length) {
        this.participantId = params.id;
      }
    });
  }

  ngOnInit() {
    this.componentNavItem = this.navItems.getItemById('view-timeout-offset');
    if (this.componentNavItem) {
      this.pageTitle.setTitle(this.componentNavItem.name);
    }
    // Set Page Header Details
    PageHeaderService.updatePageHeader({
      backPath: 'manage-participant/participants/edit/' + this.participantId,
      subTitle: 'Loading...'
    });

    if (this.participantId) {
      this.fetchParticipant(this.participantId);
    }

  }

  private setInProgress(): void {
    this.inProgress = true;
  }

  private setNotInProgress(): void {
    this.inProgress = false;
  }

  private fetchParticipant(id: string): void {
    this.participantService.getParticipant(id)
      .subscribe(response => {
          if (response && response.hasOwnProperty('data') && response.data) {
            this.participant = response.data;
            this.participant.default_connection_id = response.data.connections.filter(conn => conn.default_flag === true);
            setTimeout(() => {
              PageHeaderService.updatePageHeader({
                backPath: 'manage-participant/participants/edit/' + this.participantId,
                subTitle: this.participant.id
              });
            });
          }
          this.setNotInProgress();
        },
        err => {
          this.setNotInProgress();
          this.errorMessageService.handleServerErrors(err);
        });
  }

}
