import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewTimeoutOffsetComponent } from './view-timeout-offset.component';

describe('ViewTimeoutOffsetComponent', () => {
  let component: ViewTimeoutOffsetComponent;
  let fixture: ComponentFixture<ViewTimeoutOffsetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewTimeoutOffsetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewTimeoutOffsetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
