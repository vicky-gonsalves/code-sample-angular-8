import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewTimeoutOffsetDetailEditorComponent } from './view-timeout-offset-detail-editor.component';

describe('ViewTimeoutOffsetDetailEditorComponent', () => {
  let component: ViewTimeoutOffsetDetailEditorComponent;
  let fixture: ComponentFixture<ViewTimeoutOffsetDetailEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewTimeoutOffsetDetailEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewTimeoutOffsetDetailEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
