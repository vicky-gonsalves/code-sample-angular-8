import {AfterViewInit, Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Observable} from 'rxjs';
import {debounceTime, distinctUntilChanged, startWith, switchMap} from 'rxjs/operators';
import {appAnimations} from '../../../../../core/animations/animations';
import {HelpTextService} from '../../../../../core/services/help-text/help-text.service';
import {MessageTypeInterface} from '../../../../../interface/message-type.interface';
import {ParticipantMsgInfo} from '../../../../../model/participant-msg-info.model';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {MessageTypeService} from '../../../../../shared/services/message-type/message-type.service';
import {ParticipantMsgInfoService} from '../../../../../shared/services/participant-msg-info/participant-msg-info.service';

@Component({
  selector: 'view-timeout-offset-detail-editor',
  templateUrl: './view-timeout-offset-detail-editor.component.html',
  styleUrls: ['./view-timeout-offset-detail-editor.component.scss'],
  animations: appAnimations
})
export class ViewTimeoutOffsetDetailEditorComponent implements OnInit, AfterViewInit {
  public IS_EDIT: Boolean = false;
  public participantMsgInfoForm: FormGroup;
  public participantMsgInfo: ParticipantMsgInfo = new ParticipantMsgInfo(
    null,
    null,
    null,
    null
  );
  public inProgress: Boolean = false;
  public submitted: Boolean = false;
  public filteredMsgType: Observable<MessageTypeInterface[]>;
  public helpText: any;

  constructor(private dialogRef: MatDialogRef<ParticipantMsgInfo>,
              @Inject(MAT_DIALOG_DATA) private data: any,
              private messageTypeService: MessageTypeService,
              private participantMsgInfoService: ParticipantMsgInfoService,
              private errorMessageService: ErrorMessageService) {
    if (data.hasOwnProperty('participantMsgInfo') && data.participantMsgInfo) {
      this.IS_EDIT = true;
    }
    HelpTextService.helpText.subscribe(helpText => {
      if (helpText.testTemplate) {
        this.helpText = helpText.testTemplate;
      }
    });
  }

  get msg_type() {
    return this.participantMsgInfoForm.get('msg_type');
  }

  get timeout_offset() {
    return this.participantMsgInfoForm.get('timeout_offset');
  }

  ngOnInit() {
    this.setupForm();
  }

  ngAfterViewInit() {
    this.initAutocomplete();
  }

  public getErrorMessage(fieldName: string): string {
    return this.errorMessageService.getError(this[fieldName], fieldName);
  }

  public submitParticipantMsgInfoForm(): void {
    this.submitted = true;
    this.participantMsgInfo = this.prepareFormFields();
    if (this.participantMsgInfoForm.valid) {
      this.setInProgress();
      if (this.IS_EDIT) {
        this.participantMsgInfoService.updateParticipantMsgInfo(this.participantMsgInfo)
          .subscribe(response => {
              if (response && response.hasOwnProperty('data') && response.data) {
                this.errorMessageService.openSnackBar('Timeout Offset successfully updated.', 5000, 'OK');
                this.dialogRef.close({success: true, data: response.data});
              }
              this.setNotInProgress();
            },
            err => {
              this.setNotInProgress();
              this.errorMessageService.handleServerErrors(err, this.participantMsgInfoForm);
            });
      } else {
        this.participantMsgInfoService.addParticipantMsgInfo(this.participantMsgInfo)
          .subscribe(response => {
              if (response && response.hasOwnProperty('data') && response.data) {
                this.errorMessageService.openSnackBar('Timeout Offset successfully added.', 5000, 'OK');
                this.dialogRef.close({success: true, data: response.data});
              }
              this.setNotInProgress();
            },
            err => {
              this.setNotInProgress();
              this.errorMessageService.handleServerErrors(err, this.participantMsgInfoForm);
            });
      }
    }
  }

  public msgTypeDisplayFn(msgType?: MessageTypeInterface): string | undefined {
    return msgType ? (msgType.msg_type) : undefined;
  }

  public onNoClick(): void {
    this.dialogRef.close();
  }

  private initAutocomplete(): void {
    this.filteredMsgType = this.msg_type.valueChanges
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
        startWith(null),
        switchMap(
          (term: string | any) =>
            (typeof term === 'string' || term === '' || term === null) ?
              this.messageTypeService.getMsgTypesForAutocomplete({
                term: term,
                limit: 10
              }, this.data.id) : [])
      );
    if (this.IS_EDIT) {
      this.setValues(this.data.participantMsgInfo);
    }
  }

  private setInProgress(): void {
    this.participantMsgInfoForm.disable();
    this.inProgress = true;
  }

  private setNotInProgress(): void {
    this.inProgress = false;
    this.participantMsgInfoForm.enable();
  }

  private setupForm(): void {
    this.participantMsgInfoForm = new FormGroup({
      'msg_type': new FormControl('', [
        Validators.required
      ]),
      'timeout_offset': new FormControl('', [
        Validators.required,
        Validators.pattern(/^[\d]+$/),
        Validators.min(0)
      ])
    });
  }

  private prepareFormFields(): ParticipantMsgInfo {
    const formModel = this.participantMsgInfoForm.value;
    if (formModel.msg_type && !formModel.msg_type.msg_type) {
      this.msg_type.setValue(null);
    }
    return {
      id: this.data.id,
      msg_type: formModel.msg_type && formModel.msg_type.msg_type ? formModel.msg_type.msg_type : null,
      timeout_offset: formModel.timeout_offset
    };
  }

  private setValues(participantMsgInfo: ParticipantMsgInfo): void {
    setTimeout(() => {
      this.participantMsgInfoForm.patchValue({
        msg_type: {msg_type: participantMsgInfo.msg_type},
        timeout_offset: participantMsgInfo.timeout_offset
      });
    }, 100);
  }

}
