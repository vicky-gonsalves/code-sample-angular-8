import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserDataGuard} from '../../../core/guards/user-data/user-data.guard';
import {ParticipantEditorComponent} from './components/participant-editor/participant-editor.component';
import {ParticipantListComponent} from './components/participant-list/participant-list.component';
import {ParticipantComponent} from './participant.component';
import {ViewTimeoutOffsetComponent} from './components/view-timeout-offset/view-timeout-offset.component';

export const routes: Routes = [
  {
    path: '',
    canActivate: [UserDataGuard],
    component: ParticipantComponent,
    children: [
      {
        path: '',
        canActivateChild: [UserDataGuard],
        component: ParticipantListComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'participant'}
      },
      {
        path: 'add',
        canActivateChild: [UserDataGuard],
        component: ParticipantEditorComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'participant-add'}
      },
      {
        path: 'edit/:id',
        canActivateChild: [UserDataGuard],
        component: ParticipantEditorComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'participant-edit'}
      },
      {
        path: 'view-timeout-offset/:id',
        canActivateChild: [UserDataGuard],
        component: ViewTimeoutOffsetComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'view-timeout-offset'}
      }
    ]
  },
  {path: '**', redirectTo: '', pathMatch: 'full'}
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);

