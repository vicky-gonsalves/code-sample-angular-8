import {Component, OnInit} from '@angular/core';
import {appAnimations} from '../../../core/animations/animations';

@Component({
  selector: 'participant',
  templateUrl: './participant.component.html',
  styleUrls: ['./participant.component.scss'],
  animations: appAnimations
})
export class ParticipantComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {

  }

}
