import {Component, OnInit} from '@angular/core';
import {appAnimations} from '../../core/animations/animations';

@Component({
  selector: 'participant-management',
  templateUrl: './participant-management.component.html',
  animations: appAnimations
})
export class ParticipantManagementComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
