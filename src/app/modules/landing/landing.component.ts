import {Component, OnInit} from '@angular/core';
import {appAnimations} from '../../core/animations/animations';
import {NavItemInterface} from '../../interface/nav-item.interface';
import {NavItemService} from '../../shared/services/nav-item/nav-item.service';
import {PageHeaderService} from '../../shared/services/page-header/page-header.service';
import {PageTitleService} from '../../shared/services/page-title/page-title.service';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss'],
  animations: appAnimations
})
export class LandingComponent implements OnInit {
  components: NavItemInterface[];
  componentNavItem: NavItemInterface;

  constructor(public navItems: NavItemService,
              public pageTitle: PageTitleService) {
    this.components = this.navItems.getAllComponents();
  }

  ngOnInit() {
    this.componentNavItem = this.navItems.getItemById('dashboard');
    if (this.componentNavItem) {
      this.pageTitle.setTitle(this.componentNavItem.name);
    }
    PageHeaderService.updatePageHeader({showImage: true, icon: 'dashboard'});
  }
}
