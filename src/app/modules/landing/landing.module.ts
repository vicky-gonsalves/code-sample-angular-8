import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LandingComponent} from './landing.component';
import {SharedModule} from '../../shared/shared.module';
import {routing} from './landing.routes';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    routing
  ],
  declarations: [LandingComponent]
})
export class LandingModule {
}
