import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LandingComponent} from './landing.component';
import {UserDataGuard} from '../../core/guards/user-data/user-data.guard';

export const routes: Routes = [
  {
    path: '',
    canActivate: [UserDataGuard],
    component: LandingComponent,
    data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'dashboard'}
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);

