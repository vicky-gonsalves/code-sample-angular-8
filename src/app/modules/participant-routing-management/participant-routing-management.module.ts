import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ParticipantRoutingManagementComponent} from './participant-routing-management.component';
import {SharedModule} from '../../shared/shared.module';
import {routing} from './participant-routing-management.routes';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    routing
  ],
  declarations: [
    ParticipantRoutingManagementComponent
  ]
})
export class ParticipantRoutingManagementModule {
}
