import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserDataGuard} from '../../core/guards/user-data/user-data.guard';

export const routes: Routes = [
  {
    path: '',
    canActivate: [UserDataGuard],
    children: [
      {
        path: 'participant-routing-details',
        canActivateChild: [UserDataGuard],
        loadChildren: () => import('./participant-routing/participant-routing.module').then(mod => mod.ParticipantRoutingModule),
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'participant-routing-detail'}
      }
    ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);

