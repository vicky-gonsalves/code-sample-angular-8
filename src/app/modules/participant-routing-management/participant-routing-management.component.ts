import {Component, OnInit} from '@angular/core';
import {appAnimations} from '../../core/animations/animations';

@Component({
  selector: 'participant-routing-management',
  templateUrl: './participant-routing-management.component.html',
  animations: appAnimations
})
export class ParticipantRoutingManagementComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
