import { Component, OnInit } from '@angular/core';
import {appAnimations} from '../../../core/animations/animations';

@Component({
  selector: 'participant-routing',
  templateUrl: './participant-routing.component.html',
  styleUrls: ['./participant-routing.component.scss'],
  animations: appAnimations
})
export class ParticipantRoutingComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
