import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ParticipantRoutingEditorComponent} from './components/participant-routing-editor/participant-routing-editor.component';
import {ParticipantRoutingComponent} from './participant-routing.component';
import {ParticipantRoutingListComponent} from './components/participant-routing-list/participant-routing-list.component';
import {SharedModule} from '../../../shared/shared.module';
import {routing} from './participant-routing.routes';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    routing
  ],
  declarations: [
    ParticipantRoutingComponent,
    ParticipantRoutingEditorComponent,
    ParticipantRoutingListComponent
  ]
})
export class ParticipantRoutingModule {
}
