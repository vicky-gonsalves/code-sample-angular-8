import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserDataGuard} from '../../../core/guards/user-data/user-data.guard';
import {ParticipantRoutingListComponent} from './components/participant-routing-list/participant-routing-list.component';
import {ParticipantRoutingEditorComponent} from './components/participant-routing-editor/participant-routing-editor.component';
import {ParticipantRoutingComponent} from './participant-routing.component';

export const routes: Routes = [
  {
    path: '',
    canActivate: [UserDataGuard],
    component: ParticipantRoutingComponent,
    children: [
      {
        path: '',
        canActivateChild: [UserDataGuard],
        component: ParticipantRoutingListComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'participant-routing-detail'}
      },
      {
        path: 'add',
        canActivateChild: [UserDataGuard],
        component: ParticipantRoutingEditorComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'participant-routing-detail-add'}
      },
      {
        path: 'edit/:id',
        canActivateChild: [UserDataGuard],
        component: ParticipantRoutingEditorComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'participant-routing-detail-edit'}
      }
    ]
  },
  {path: '**', redirectTo: '', pathMatch: 'full'}
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);

