import {AfterViewInit, Component, OnInit} from '@angular/core';
import {appAnimations} from '../../../../../core/animations/animations';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {debounceTime, distinctUntilChanged, startWith, switchMap} from 'rxjs/internal/operators';
import {PageHeaderService} from '../../../../../shared/services/page-header/page-header.service';
import {Observable} from 'rxjs';
import {PageTitleService} from '../../../../../shared/services/page-title/page-title.service';
import {NavItemInterface} from '../../../../../interface/nav-item.interface';
import {NavItemService} from '../../../../../shared/services/nav-item/nav-item.service';
import * as moment from 'moment-timezone';
import {ParticipantRoutingDetail} from '../../../../../model/participant-routing-detail.model';
import {Participant} from '../../../../../model/participant.model';
import {ParticipantRoutingDetailService} from '../../../../../shared/services/participant-routing-detail/participant-routing-detail.service';
import {ParticipantService} from '../../../../../shared/services/participant/participant.service';
import {ParticipantInterface} from '../../../../../interface/participant.interface';
import {HelpText} from '../../../../../core/services/help-text/help-text';
import {HelpTextService} from '../../../../../core/services/help-text/help-text.service';

@Component({
  selector: 'participant-routing-editor',
  templateUrl: './participant-routing-editor.component.html',
  styleUrls: ['./participant-routing-editor.component.scss'],
  animations: appAnimations
})
export class ParticipantRoutingEditorComponent implements OnInit, AfterViewInit {
  public IS_EDIT: Boolean = false;
  public helpText: any;
  public effectiveDateDisabled: Boolean = false;
  public participantRoutingDetailForm: FormGroup;
  public participantRoutingDetail: ParticipantRoutingDetail = new ParticipantRoutingDetail(
    null,
    null,
    null,
    false
  );
  public inProgress: Boolean = false;
  public submitted: Boolean = false;
  public minDate = moment().utcOffset('+00:00', true).startOf('day').toISOString();
  public filteredParticipants: Observable<Participant[]>;
  private componentNavItem: NavItemInterface;
  private participantRoutingDetailId: string;

  constructor(public navItems: NavItemService,
              public pageTitle: PageTitleService,
              private errorMessageService: ErrorMessageService,
              private participantRoutingDetailService: ParticipantRoutingDetailService,
              private participantService: ParticipantService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe(params => {
      if (params.hasOwnProperty('id') && params.id.length) {
        this.participantRoutingDetailId = params.id;
        this.IS_EDIT = true;
      }
    });
    HelpTextService.helpText.subscribe(helpText => {
      if (helpText.participantRoutingDetails) {
        this.helpText = helpText.participantRoutingDetails;
      }
    });
  }

  get id() {
    return this.participantRoutingDetailForm.get('id');
  }

  get participant_id() {
    return this.participantRoutingDetailForm.get('participant_id');
  }

  get effective_date() {
    return this.participantRoutingDetailForm.get('effective_date');
  }

  get is_deactive() {
    return this.participantRoutingDetailForm.get('is_deactive');
  }

  ngOnInit() {
    let id = 'participant-routing-detail-add';
    if (this.IS_EDIT) {
      id = 'participant-routing-detail-edit';
      PageHeaderService.updatePageHeader({backPath: 'manage-participant-routing/participant-routing-details', subTitle: 'Loading...'});
    } else {
      PageHeaderService.updatePageHeader({backPath: 'manage-participant-routing/participant-routing-details'});
    }
    this.componentNavItem = this.navItems.getItemById(id);
    if (this.componentNavItem) {
      this.pageTitle.setTitle(this.componentNavItem.name);
    }

    this.setupForm();

    if (this.participantRoutingDetailId) {
      this.fetchParticipantRoutingDetail(this.participantRoutingDetailId);
    }
  }

  ngAfterViewInit() {
    this.initAutocomplete();
  }

  public getErrorMessage(fieldName: string): string {
    return this.errorMessageService.getError(this[fieldName], fieldName);
  }

  public submitParticipantRoutingDetailForm(): void {
    this.submitted = true;
    this.participantRoutingDetail = this.prepareFormFields();
    if (this.participantRoutingDetailForm.valid) {
      this.setInProgress();
      if (this.IS_EDIT) {
        this.participantRoutingDetailService.updateParticipantRoutingDetail(this.participantRoutingDetail)
          .subscribe(response => {
              if (response && response.hasOwnProperty('data') && response.data) {
                this.setValue(response.data);
                this.errorMessageService.openSnackBar('Participant Routing Detail successfully updated.', 5000, 'OK');
              }
              this.setNotInProgress();
            },
            err => {
              this.setNotInProgress();
              this.errorMessageService.handleServerErrors(err, this.participantRoutingDetailForm);
            });
      } else {
        this.participantRoutingDetailService.addParticipantRoutingDetail(this.participantRoutingDetail)
          .subscribe(response => {
              if (response && response.hasOwnProperty('data') && response.data) {
                this.router.navigate(['manage-participant-routing/participant-routing-details']);
                this.errorMessageService.openSnackBar('Participant Routing Detail successfully added.', 5000, 'OK');
              }
              this.setNotInProgress();
            },
            err => {
              this.setNotInProgress();
              this.errorMessageService.handleServerErrors(err, this.participantRoutingDetailForm);
            });
      }
    }
  }

  public participantDisplayFn(participantRoutingDetailOwner?: ParticipantInterface): string | undefined {
    return participantRoutingDetailOwner ? (participantRoutingDetailOwner.id) : undefined;
  }

  private fetchParticipantRoutingDetail(id: string): void {
    this.participantRoutingDetailService.getParticipantRoutingDetail(id)
      .subscribe(response => {
          if (response && response.hasOwnProperty('data') && response.data) {
            this.setValue(response.data);
          }
          this.setNotInProgress();
        },
        err => {
          this.setNotInProgress();
          this.errorMessageService.handleServerErrors(err, this.participantRoutingDetailForm);
        });
  }

  private initAutocomplete(): void {
    this.filteredParticipants = this.participant_id.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        startWith(null),
        switchMap(
          (term: string | any) =>
            (typeof term === 'string' || term === '' || term === null) ?
              this.participantService.getParticipantsForAutocomplete({
                term: term,
                limit: 10
              }) : []
        )
      );
  }

  private setInProgress(): void {
    this.participantRoutingDetailForm.disable();
    this.inProgress = true;
  }

  private setNotInProgress(): void {
    this.inProgress = false;
    this.participantRoutingDetailForm.enable();
  }

  private setupForm(): void {
    this.participantRoutingDetailForm = new FormGroup({
      'id': new FormControl(this.participantRoutingDetail.id, [
        Validators.required,
        Validators.pattern(/^[A-Za-z\d-]+$/),
        Validators.minLength(1),
        Validators.maxLength(35),
      ]),
      'participant_id': new FormControl(this.participantRoutingDetail.participant_id, [
        Validators.required
      ]),
      'effective_date': new FormControl(this.participantRoutingDetail.effective_date, [
        Validators.required
      ]),
      'is_deactive': new FormControl(this.participantRoutingDetail.is_deactive, [
        Validators.required
      ])
    });
  }

  private prepareFormFields(): ParticipantRoutingDetail {
    const formModel = this.participantRoutingDetailForm.value;
    if (formModel.participant_id && !formModel.participant_id.id) {
      this.participant_id.setValue(null);
    }
    if (formModel.effective_date) {
      formModel.effective_date = moment(formModel.effective_date).utcOffset('+00:00', true).startOf('day').format();
    }
    return {
      id: formModel.id,
      participant_id: formModel.participant_id && formModel.participant_id.id ? formModel.participant_id.id : null,
      effective_date: formModel.effective_date,
      is_deactive: formModel.is_deactive,
      _id: this.participantRoutingDetail && this.participantRoutingDetail._id ? this.participantRoutingDetail._id : null
    };
  }

  private setValue(data: ParticipantRoutingDetail): void {
    this.participantRoutingDetailForm.patchValue({
      id: data.id,
      participant_id: {id: data.participant_id},
      effective_date: data.effective_date,
      is_deactive: data.is_deactive,
    });
    this.participantRoutingDetail = data;
    const effective_date = moment(this.participantRoutingDetail.effective_date);
    if (moment(this.minDate) > effective_date) {
      this.minDate = effective_date;
    }
    if (moment(this.participantRoutingDetail.effective_date) <= moment().utcOffset('+00:00', true).startOf('day')) {
      this.effectiveDateDisabled = true;
    }
    PageHeaderService.updatePageHeader({
      backPath: 'manage-participant-routing/participant-routing-details',
      subTitle: this.participantRoutingDetail.id
    });
  }

}
