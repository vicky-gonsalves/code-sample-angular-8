import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParticipantRoutingEditorComponent } from './participant-routing-editor.component';

describe('ParticipantRoutingEditorComponent', () => {
  let component: ParticipantRoutingEditorComponent;
  let fixture: ComponentFixture<ParticipantRoutingEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParticipantRoutingEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticipantRoutingEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
