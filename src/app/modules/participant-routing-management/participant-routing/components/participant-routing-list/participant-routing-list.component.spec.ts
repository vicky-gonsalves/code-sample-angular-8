import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParticipantRoutingListComponent } from './participant-routing-list.component';

describe('ParticipantRoutingListComponent', () => {
  let component: ParticipantRoutingListComponent;
  let fixture: ComponentFixture<ParticipantRoutingListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParticipantRoutingListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticipantRoutingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
