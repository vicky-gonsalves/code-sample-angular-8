import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserDataGuard} from '../../../core/guards/user-data/user-data.guard';
import {TestTemplateComponent} from './test-template.component';
import {TestTemplateListComponent} from './components/test-template-list/test-template-list.component';
import {TestTemplateEditorComponent} from './components/test-template-editor/test-template-editor.component';
import {TestTemplateViewComponent} from './components/test-template-view/test-template-view.component';

export const routes: Routes = [
  {
    path: '',
    canActivate: [UserDataGuard],
    component: TestTemplateComponent,
    children: [
      {
        path: '',
        canActivateChild: [UserDataGuard],
        component: TestTemplateListComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'test-template'}
      },
      {
        path: 'add',
        canActivateChild: [UserDataGuard],
        component: TestTemplateEditorComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'test-template-add'}
      },
      {
        path: 'view/:id',
        canActivateChild: [UserDataGuard],
        component: TestTemplateViewComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'test-template-view'}
      },
      {
        path: 'edit/:id',
        canActivateChild: [UserDataGuard],
        component: TestTemplateEditorComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'test-template-edit'}
      }
    ]
  },
  {path: '**', redirectTo: '', pathMatch: 'full'}
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);

