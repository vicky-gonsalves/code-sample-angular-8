import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {PageHeaderService} from '../../../../../shared/services/page-header/page-header.service';
import {NavItemInterface} from '../../../../../interface/nav-item.interface';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {PageTitleService} from '../../../../../shared/services/page-title/page-title.service';
import {NavItemService} from '../../../../../shared/services/nav-item/nav-item.service';
import {TestTemplateService} from '../../../../../shared/services/test-template/test-template.service';
import {TestTemplateInterface} from '../../../../../interface/test-template.interface';
import {HelpTextService} from '../../../../../core/services/help-text/help-text.service';
import {appAnimations} from '../../../../../core/animations/animations';

@Component({
  selector: 'test-template-view',
  templateUrl: './test-template-view.component.html',
  styleUrls: ['./test-template-view.component.scss'],
  animations: appAnimations
})
export class TestTemplateViewComponent implements OnInit {
  public testTemplateId: string;
  public testTemplate: TestTemplateInterface;
  public inProgress: Boolean = true;
  public helpText: any;
  private componentNavItem: NavItemInterface;

  constructor(public navItems: NavItemService,
              public pageTitle: PageTitleService,
              private errorMessageService: ErrorMessageService,
              private router: Router,
              private testTemplateService: TestTemplateService,
              private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe(params => {
      if (params.hasOwnProperty('id') && params.id.length) {
        this.testTemplateId = params.id;
      }
    });
    HelpTextService.helpText.subscribe(helpText => {
      if (helpText.testTemplate) {
        this.helpText = helpText.testTemplate;
      }
    });
  }

  ngOnInit() {
    this.componentNavItem = this.navItems.getItemById('test-template-view');
    if (this.componentNavItem) {
      this.pageTitle.setTitle(this.componentNavItem.name);
    }
    // Set Page Header Details
    PageHeaderService.updatePageHeader({
      backPath: 'manage-test-case/test-templates',
      subTitle: 'Loading...',
      actionButton: {
        title: 'Add New Test Template',
        url: 'manage-test-case/test-templates/add'
      },
      actionButton2: {
        title: 'Edit Test Template',
        url: 'manage-test-case/test-templates/edit/' + this.testTemplateId
      }
    });

    if (this.testTemplateId) {
      this.fetchTestTemplate(this.testTemplateId);
    }
  }

  private setInProgress(): void {
    this.inProgress = true;
  }

  private setNotInProgress(): void {
    this.inProgress = false;
  }

  private fetchTestTemplate(id: string): void {
    this.testTemplateService.getTestTemplate(id)
      .subscribe(response => {
          if (response && response.hasOwnProperty('data') && response.data) {
            this.testTemplate = response.data;
            setTimeout(() => {
              PageHeaderService.updatePageHeader({
                backPath: 'manage-test-case/test-templates', actionButton: {
                  title: 'Add New Test Template',
                  url: 'manage-test-case/test-templates/add'
                },
                actionButton2: {
                  title: 'Edit Test Template',
                  url: 'manage-test-case/test-templates/edit/' + this.testTemplateId
                },
                subTitle: this.testTemplate.template_name
              });
            });
          }
          this.setNotInProgress();
        },
        err => {
          this.setNotInProgress();
          this.errorMessageService.handleServerErrors(err);
        });
  }
}
