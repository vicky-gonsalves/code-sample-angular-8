import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TestTemplateViewComponent} from './test-template-view.component';

describe('TestTemplateViewComponent', () => {
  let component: TestTemplateViewComponent;
  let fixture: ComponentFixture<TestTemplateViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TestTemplateViewComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestTemplateViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
