import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {appAnimations} from '../../../../../core/animations/animations';
import {NavItemInterface} from '../../../../../interface/nav-item.interface';
import {TestTemplateInterface} from '../../../../../interface/test-template.interface';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {NavItemService} from '../../../../../shared/services/nav-item/nav-item.service';
import {PageHeaderService} from '../../../../../shared/services/page-header/page-header.service';
import {PageTitleService} from '../../../../../shared/services/page-title/page-title.service';
import {TestTemplateService} from '../../../../../shared/services/test-template/test-template.service';

@Component({
  selector: 'test-template-list',
  templateUrl: './test-template-list.component.html',
  styleUrls: ['./test-template-list.component.scss'],
  animations: appAnimations
})
export class TestTemplateListComponent implements OnInit {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  public searchTerm: {
    template_name: string,
    participant_id: string,
    testcase_name: string,
    initiating_msg_type: string
  };
  public displayedColumns =
    ['template_name', 'template_type', 'participant_id', 'testcase_name', 'initiating_msg_type', 'actions'];
  public dataSource: MatTableDataSource<TestTemplateInterface>;
  public paginatorOptions = {
    count: 0,
    pageSize: 10,
    pageSizeOptions: [5, 10, 25, 50, 100],
    showFirstLastButtons: true
  };
  public inProgress: Boolean = true;
  private sortBy = '-createdAt';
  private pageSize = 10;
  private componentNavItem: NavItemInterface;

  constructor(private navItems: NavItemService,
              private testTemplateService: TestTemplateService,
              private errorMsgService: ErrorMessageService,
              private pageTitle: PageTitleService) {
    this.searchTerm = {
      template_name: null,
      participant_id: null,
      testcase_name: null,
      initiating_msg_type: null
    };
  }

  ngOnInit() {
    // Get Navigation Component Name
    this.componentNavItem = this.navItems.getItemById('test-template');
    if (this.componentNavItem) {
      // Set Page Title
      this.pageTitle.setTitle(this.componentNavItem.name);
    }

    // Set Page Header Details
    PageHeaderService.updatePageHeader({
      showImage: true,
      icon: 'offline_pin',
      actionButton: {
        title: 'Add New Test Template',
        url: 'manage-test-case/test-templates/add'
      }
    });

    // Fetch TestTemplates
    this.fetchTestTemplateListing();
  }

  public sortData(sort: Sort): void {
    const data = this.dataSource;
    if (!sort.active || sort.direction === '') {
      this.sortBy = '-createdAt';
    } else {
      this.sortBy = sort.direction === 'asc' ? sort.active : '-' + sort.active;
    }
    this.paginator.pageIndex = 0;
    this.fetchTestTemplateListing();
  }

  public fetchTestTemplateListing(pageEvent?: PageEvent): void {
    if (pageEvent) {
      this.pageSize = pageEvent.pageSize;
    }
    const options = {
      limit: (pageEvent ? (pageEvent.pageSize) : this.pageSize ? this.pageSize : this.paginatorOptions.pageSize),
      page: (pageEvent ? (pageEvent.pageIndex + 1) : 1),
      sort: [this.sortBy]
    };
    if (this.searchTerm.template_name) {
      options['template_name'] = this.searchTerm.template_name;
    }
    if (this.searchTerm.participant_id) {
      options['participant_id'] = this.searchTerm.participant_id;
    }
    if (this.searchTerm.testcase_name) {
      options['testcase_name'] = this.searchTerm.testcase_name;
    }
    if (this.searchTerm.initiating_msg_type) {
      options['initiating_msg_type'] = this.searchTerm.initiating_msg_type;
    }
    this.inProgress = true;
    this.testTemplateService.getTestTemplates(options)
      .subscribe(response => {
          if (response && response.data && response.data.rows && response.data.rows.length) {
            this.dataSource = new MatTableDataSource(response.data.rows);
            this.paginatorOptions.count = response.data.count;
            this.dataSource.sort = this.sort;
          } else {
            this.errorMsgService.openSnackBar(this.errorMsgService.getNonFormError('NO_DATA'));
          }
          this.inProgress = false;
        },
        err => {
          this.errorMsgService.handleServerErrors(err);
          this.inProgress = false;
        });
  }

  public applyFilter(searchCriteria: { key: string, value: string }): void {
    this.searchTerm[searchCriteria.key] = searchCriteria.value;
    this.paginator.pageIndex = 0;
    this.fetchTestTemplateListing();
  }
}
