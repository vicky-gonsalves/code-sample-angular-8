import {AfterViewInit, Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Observable} from 'rxjs';
import {debounceTime, distinctUntilChanged, startWith, switchMap} from 'rxjs/internal/operators';
import {appAnimations} from '../../../../../core/animations/animations';
import {HelpTextService} from '../../../../../core/services/help-text/help-text.service';
import {MsgFieldDetailInterface} from '../../../../../interface/msg-field-detail.interface';
import {TestTemplateFieldValues} from '../../../../../model/test-template.model';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {MsgFieldDetailService} from '../../../../../shared/services/msg-field-detail/msg-field-detail.service';
import {TestTemplateService} from '../../../../../shared/services/test-template/test-template.service';

@Component({
  selector: 'test-template-detail-editor',
  templateUrl: './test-template-detail-editor.component.html',
  styleUrls: ['./test-template-detail-editor.component.scss'],
  animations: appAnimations
})
export class TestTemplateDetailEditorComponent implements OnInit, AfterViewInit {
  public IS_EDIT: Boolean = false;
  public testTemplateFieldValueForm: FormGroup;
  public testTemplateFieldValues: TestTemplateFieldValues = new TestTemplateFieldValues(
    null,
    null,
    null,
    null
  );
  public inProgress: Boolean = false;
  public submitted: Boolean = false;
  public filteredMsgFlds: Observable<MsgFieldDetailInterface[]>;
  public helpText: any;

  constructor(private dialogRef: MatDialogRef<TestTemplateDetailEditorComponent>,
              @Inject(MAT_DIALOG_DATA) private data: any,
              private msgFldDetailsService: MsgFieldDetailService,
              private testTemplateService: TestTemplateService,
              private errorMessageService: ErrorMessageService) {
    if (data.field) {
      this.IS_EDIT = true;
    }
    HelpTextService.helpText.subscribe(helpText => {
      if (helpText.testTemplate) {
        this.helpText = helpText.testTemplate;
      }
    });
  }

  get field_name() {
    return this.testTemplateFieldValueForm.get('field_name');
  }

  get field_value() {
    return this.testTemplateFieldValueForm.get('field_value');
  }

  ngOnInit() {
    this.setupForm();
  }

  ngAfterViewInit() {
    this.initAutocomplete();
  }

  public getErrorMessage(fieldName: string): string {
    return this.errorMessageService.getError(this[fieldName], fieldName);
  }

  public submitTestTemplateFieldValueForm(): void {
    this.submitted = true;
    this.testTemplateFieldValues = this.prepareFormFields();
    if (this.testTemplateFieldValueForm.valid) {
      this.setInProgress();
      if (this.IS_EDIT) {
        this.testTemplateService.updateTestTemplateFieldValues(this.testTemplateFieldValues, this.data.id, this.data.field._id)
          .subscribe(response => {
              if (response && response.hasOwnProperty('data') && response.data) {
                this.errorMessageService.openSnackBar('Test Template Field successfully updated.', 5000, 'OK');
                this.dialogRef.close({success: true, data: response.data});
              }
              this.setNotInProgress();
            },
            err => {
              this.setNotInProgress();
              this.errorMessageService.handleServerErrors(err, this.testTemplateFieldValueForm);
            });
      } else {
        this.testTemplateService.addTestTemplateFieldValues(this.testTemplateFieldValues, this.data.id)
          .subscribe(response => {
              if (response && response.hasOwnProperty('data') && response.data) {
                this.errorMessageService.openSnackBar('Test Template Field successfully added.', 5000, 'OK');
                this.dialogRef.close({success: true, data: response.data});
              }
              this.setNotInProgress();
            },
            err => {
              this.setNotInProgress();
              this.errorMessageService.handleServerErrors(err, this.testTemplateFieldValueForm);
            });
      }
    }
  }

  public msgFldDetailsDisplayFn(msgFieldDetail?: MsgFieldDetailInterface): string | undefined {
    return msgFieldDetail ? (msgFieldDetail.field_name) : undefined;
  }

  public onNoClick(): void {
    this.dialogRef.close();
  }

  private initAutocomplete(): void {
    this.filteredMsgFlds = this.field_name.valueChanges
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
        startWith(null),
        switchMap(
          (term: string | any) =>
            (typeof term === 'string' || term === '' || term === null) ?
              this.msgFldDetailsService.getMsgFldDetailsForAutocomplete({
                term: term,
                msg_type: this.data.msgType,
                specific: true,
                limit: 10
              }, this.data.usedFields) : [])
      );
    if (this.IS_EDIT) {
      this.setValues(this.data.field);
    }
  }

  private setInProgress(): void {
    this.testTemplateFieldValueForm.disable();
    this.inProgress = true;
  }

  private setNotInProgress(): void {
    this.inProgress = false;
    this.testTemplateFieldValueForm.enable();
  }

  private setupForm(): void {
    this.testTemplateFieldValueForm = new FormGroup({
      'field_name': new FormControl('', [
        Validators.required
      ]),
      'field_value': new FormControl('', [
        Validators.required,
        Validators.pattern(/^[A-Za-z\d_.\-<>():']+$/),
        Validators.minLength(1),
        Validators.maxLength(100)
      ])
    });
  }

  private prepareFormFields(): TestTemplateFieldValues {
    const formModel = this.testTemplateFieldValueForm.value;
    if (formModel.field_name && !formModel.field_name.field_name) {
      this.field_name.setValue(null);
    }
    return {
      field_name: formModel.field_name && formModel.field_name.field_name ? formModel.field_name.field_name : null,
      field_value: formModel.field_value
    };
  }

  private setValues(field: TestTemplateFieldValues): void {
    setTimeout(() => {
      this.testTemplateFieldValueForm.patchValue({
        field_name: {field_name: field.field_name},
        field_value: field.field_value
      });
    }, 100);
  }

}
