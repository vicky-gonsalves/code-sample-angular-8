import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TestTemplateDetailEditorComponent} from './test-template-detail-editor.component';

describe('TestTemplateDetailEditorComponent', () => {
  let component: TestTemplateDetailEditorComponent;
  let fixture: ComponentFixture<TestTemplateDetailEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TestTemplateDetailEditorComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestTemplateDetailEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
