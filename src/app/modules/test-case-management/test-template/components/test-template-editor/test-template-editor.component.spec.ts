import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TestTemplateEditorComponent} from './test-template-editor.component';

describe('TestTemplateEditorComponent', () => {
  let component: TestTemplateEditorComponent;
  let fixture: ComponentFixture<TestTemplateEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TestTemplateEditorComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestTemplateEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
