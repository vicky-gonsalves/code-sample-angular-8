import {AfterViewInit, Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {PageTitleService} from '../../../../../shared/services/page-title/page-title.service';
import {ActivatedRoute, Router} from '@angular/router';
import {debounceTime, distinctUntilChanged, startWith, switchMap} from 'rxjs/internal/operators';
import {PageHeaderService} from '../../../../../shared/services/page-header/page-header.service';
import {Observable} from 'rxjs';
import {HelpTextService} from '../../../../../core/services/help-text/help-text.service';
import {NavItemService} from '../../../../../shared/services/nav-item/nav-item.service';
import {NavItemInterface} from '../../../../../interface/nav-item.interface';
import {TestTemplateService} from '../../../../../shared/services/test-template/test-template.service';
import {TestTemplate} from '../../../../../model/test-template.model';
import {Participant} from '../../../../../model/participant.model';
import {ParticipantService} from '../../../../../shared/services/participant/participant.service';
import {ParticipantInterface} from '../../../../../interface/participant.interface';
import {TestCase} from '../../../../../model/test-case.model';
import {TestCaseService} from '../../../../../shared/services/test-case/test-case.service';
import {TestCaseInterface} from '../../../../../interface/test-case.interface';
import {appAnimations} from '../../../../../core/animations/animations';
import {Connection} from '../../../../../model/connection.model';
import {ConnectionService} from '../../../../../shared/services/connection/connection.service';
import {ConnectionInterface} from '../../../../../interface/connection.interface';

@Component({
  selector: 'test-template-editor',
  templateUrl: './test-template-editor.component.html',
  styleUrls: ['./test-template-editor.component.scss'],
  animations: appAnimations
})
export class TestTemplateEditorComponent implements OnInit, AfterViewInit {
  public IS_EDIT: Boolean = false;
  public testTemplateForm: FormGroup;
  public testTemplate: TestTemplate = new TestTemplate(
    null,
    null,
    null,
    null,
    null,
    null,
    null,
  );
  public inProgress: Boolean = false;
  public submitted: Boolean = false;
  public filteredParticipants: Observable<Participant[]>;
  public filteredTestCaseNames: Observable<TestCase[]>;
  public filteredConnections: Observable<Connection[]>;
  public helpText: any;
  private componentNavItem: NavItemInterface;
  private testTemplateId: string;

  constructor(public navItems: NavItemService,
              public pageTitle: PageTitleService,
              private errorMessageService: ErrorMessageService,
              private testTemplateService: TestTemplateService,
              private participantService: ParticipantService,
              private testCaseService: TestCaseService,
              private connectionService: ConnectionService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe(params => {
      if (params.hasOwnProperty('id') && params.id.length) {
        this.testTemplateId = params.id;
        this.IS_EDIT = true;
      }
    });
    HelpTextService.helpText.subscribe(helpText => {
      if (helpText.testTemplate) {
        this.helpText = helpText.testTemplate;
      }
    });
  }

  get participant_id() {
    return this.testTemplateForm.get('participant_id');
  }

  get template_name() {
    return this.testTemplateForm.get('template_name');
  }

  get template_type() {
    return this.testTemplateForm.get('template_type');
  }

  get testcase_name() {
    return this.testTemplateForm.get('testcase_name');
  }

  get weightage() {
    return this.testTemplateForm.get('weightage');
  }

  get conn_name() {
    return this.testTemplateForm.get('conn_name');
  }

  ngOnInit() {
    let id = 'test-template-add';
    if (this.IS_EDIT) {
      id = 'test-template-edit';
      PageHeaderService.updatePageHeader({backPath: 'manage-test-case/test-templates/view/' + this.testTemplateId, subTitle: 'Loading...'});
    } else {
      PageHeaderService.updatePageHeader({backPath: 'manage-test-case/test-templates'});
    }
    this.componentNavItem = this.navItems.getItemById(id);
    if (this.componentNavItem) {
      this.pageTitle.setTitle(this.componentNavItem.name);
    }
    this.setupForm();
    if (this.testTemplateId) {
      this.fetchTestTemplate(this.testTemplateId);
    }
  }

  ngAfterViewInit() {
    this.initAutocomplete();
  }

  public getErrorMessage(fieldName: string): string {
    return this.errorMessageService.getError(this[fieldName], fieldName);
  }

  public submitTestTemplateForm(): void {
    this.submitted = true;
    this.testTemplate = this.prepareFormFields();
    if (this.testTemplateForm.valid) {
      this.setInProgress();
      if (this.IS_EDIT) {
        this.testTemplateService.updateTestTemplate(this.testTemplate)
          .subscribe(response => {
              if (response && response.hasOwnProperty('data') && response.data) {
                this.setValue(response.data);
                this.router.navigate(['manage-test-case/test-templates/view/' + response.data._id]);
                this.errorMessageService.openSnackBar('TestTemplate successfully updated.', 5000, 'OK');
              }
              this.setNotInProgress();
            },
            err => {
              this.setNotInProgress();
              this.errorMessageService.handleServerErrors(err, this.testTemplateForm);
            });
      } else {
        this.testTemplateService.addTestTemplate(this.testTemplate)
          .subscribe(response => {
              if (response && response.hasOwnProperty('data') && response.data) {
                this.router.navigate(['manage-test-case/test-templates/view/' + response.data._id]);
                this.errorMessageService.openSnackBar('TestTemplate successfully added.', 5000, 'OK');
              }
              this.setNotInProgress();
            },
            err => {
              this.setNotInProgress();
              this.errorMessageService.handleServerErrors(err, this.testTemplateForm);
            });
      }
    }
  }

  public participantDisplayFn(participant?: ParticipantInterface): string | undefined {
    return participant ? (participant.id) : undefined;
  }

  public testCaseNameDisplayFn(testCase?: TestCaseInterface): string | undefined {
    return testCase ? (testCase.testcase_name) : undefined;
  }

  private initAutocomplete(): void {
    this.filteredParticipants = this.participant_id.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        startWith(null),
        switchMap(
          (term: string | any) =>
            (typeof term === 'string' || term === '' || term === null) ?
              this.participantService.getParticipantsForAutocomplete({
                term: term,
                limit: 10
              })
              : []
        )
      );

    this.filteredTestCaseNames = this.testcase_name.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        startWith(null),
        switchMap(
          (term: string | any) =>
            (typeof term === 'string' || term === '' || term === null) ?
              this.testCaseService.getTestCasesForAutocomplete({
                term: term,
                limit: 10
              })
              : []
        )
      );

    this.filteredConnections = this.conn_name.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        startWith(null),
        switchMap(
          (term: string | any) =>
            (typeof term === 'string' || term === '' || term === null) ?
              this.connectionService.getDefaultConnectionsForAutocomplete({
                term: term,
                limit: 10
              }) : [])
      );
  }

  private fetchTestTemplate(id: string): void {
    this.testTemplateService.getTestTemplate(id)
      .subscribe(response => {
          if (response && response.hasOwnProperty('data') && response.data) {
            this.setValue(response.data);
          }
          this.setNotInProgress();
        },
        err => {
          this.setNotInProgress();
          this.errorMessageService.handleServerErrors(err, this.testTemplateForm);
        });
  }


  public connectionDisplayFn(connection?: ConnectionInterface): string | undefined {
    return connection ? (connection.id) : undefined;
  }

  private setInProgress(): void {
    this.testTemplateForm.disable();
    this.inProgress = true;
  }

  private setNotInProgress(): void {
    this.inProgress = false;
    this.testTemplateForm.enable();
  }

  private setupForm(): void {
    this.testTemplateForm = new FormGroup({
      'participant_id': new FormControl('', [
        Validators.required
      ]),
      'template_name': new FormControl('', [
        Validators.required,
        Validators.pattern(/^[A-Za-z\d_\-]+$/),
        Validators.minLength(1),
        Validators.maxLength(18)
      ]),
      'template_type': new FormControl('', [
        Validators.required
      ]),
      'testcase_name': new FormControl('', [
        Validators.required
      ]),
      'weightage': new FormControl('', [
        Validators.min(1),
        Validators.pattern(/^[\d]+$/)
      ]),
      'conn_name': new FormControl('', [])
    });
  }

  private prepareFormFields(): TestTemplate {
    const formModel = this.testTemplateForm.value;
    if (formModel.participant_id && !formModel.participant_id.id) {
      this.participant_id.setValue(null);
    }
    if (formModel.testcase_name && !formModel.testcase_name.testcase_name) {
      this.testcase_name.setValue(null);
    }
    if (formModel.conn_name && !formModel.conn_name.id) {
      this.conn_name.setValue(null);
    }
    if (this.IS_EDIT) {
      return {
        _id: this.testTemplate && this.testTemplate._id ? this.testTemplate._id : null,
        weightage: formModel.weightage ? formModel.weightage : null,
        conn_name: formModel.conn_name && formModel.conn_name.id ? formModel.conn_name.id : null,
      };
    }
    return {
      participant_id: formModel.participant_id && formModel.participant_id.id ? formModel.participant_id.id : null,
      template_name: formModel.template_name,
      template_type: formModel.template_type,
      testcase_name: formModel.testcase_name && formModel.testcase_name.testcase_name ? formModel.testcase_name.testcase_name : null,
      participant: formModel.participant_id && formModel.participant_id.short_id ? formModel.participant_id.short_id : null,
      initiating_msg_type:
        formModel.testcase_name && formModel.testcase_name.initiating_msg_type ? formModel.testcase_name.initiating_msg_type : null,
      weightage: formModel.weightage ? formModel.weightage : null,
      conn_name: formModel.conn_name && formModel.conn_name.id ? formModel.conn_name.id : null,
    };
  }

  private setValue(data: TestTemplate): void {
    setTimeout(() => {
      this.testTemplateForm.patchValue({
        participant_id: {id: data.participant_id},
        template_name: data.template_name,
        template_type: data.template_type,
        testcase_name: {testcase_name: data.testcase_name},
        participant: {id: data.participant},
        initiating_msg_type: {id: data.initiating_msg_type},
        weightage: data.weightage,
        conn_name: {id: data.conn_name},
      });
      this.testTemplate = data;
      PageHeaderService.updatePageHeader({
        backPath: 'manage-test-case/test-templates/view/' + this.testTemplateId,
        subTitle: this.testTemplate.template_name
      });
    });
  }

}
