import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TestTemplateDetailListComponent} from './test-template-detail-list.component';

describe('TestTemplateDetailListComponent', () => {
  let component: TestTemplateDetailListComponent;
  let fixture: ComponentFixture<TestTemplateDetailListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TestTemplateDetailListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestTemplateDetailListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
