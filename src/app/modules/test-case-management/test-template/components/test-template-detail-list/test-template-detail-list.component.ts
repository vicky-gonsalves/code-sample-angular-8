import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {flatMap} from 'lodash';
import {appAnimations} from '../../../../../core/animations/animations';
import {TestTemplateFieldValuesInterface} from '../../../../../interface/test-template.interface';
import {TestTemplateFieldValues} from '../../../../../model/test-template.model';
import {ConfirmDialogComponent} from '../../../../../shared/components/dialogs/confirm-dialog/confirm-dialog.component';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {TestTemplateService} from '../../../../../shared/services/test-template/test-template.service';
import {TestTemplateDetailEditorComponent} from '../test-template-detail-editor/test-template-detail-editor.component';

@Component({
  selector: 'test-template-detail-list',
  templateUrl: './test-template-detail-list.component.html',
  styleUrls: ['./test-template-detail-list.component.scss'],
  animations: appAnimations
})
export class TestTemplateDetailListComponent implements OnInit {
  @Input() id: string;
  @Input() msgType: string;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  public searchTerm: {
    field_name: string
  };
  public displayedColumns =
    ['field_name', 'field_description', 'field_value', 'actions'];
  public dataSource: MatTableDataSource<TestTemplateFieldValuesInterface>;
  public paginatorOptions = {
    count: 0,
    pageSize: 10,
    pageSizeOptions: [5, 10, 25, 50, 100],
    showFirstLastButtons: true
  };
  public inProgress: Boolean = true;
  public pageEvent: any;
  private sortBy = '-_id';
  private pageSize = 10;
  private usedFields: string[] = [];

  constructor(private testTemplateService: TestTemplateService,
              private dialog: MatDialog,
              private errorMsgService: ErrorMessageService) {
    this.searchTerm = {
      field_name: null
    };
  }

  ngOnInit() {
    // Fetch Test Template Field Valuess
    this.fetchTestTemplateFieldsListing();
  }

  public sortData(sort: Sort): void {
    const data = this.dataSource;
    if (!sort.active || sort.direction === '') {
      this.sortBy = '-_id';
    } else {
      this.sortBy = sort.direction === 'asc' ? sort.active : '-' + sort.active;
    }
    this.paginator.pageIndex = 0;
    this.fetchTestTemplateFieldsListing();
  }

  public fetchTestTemplateFieldsListing(_pageEvent?: PageEvent): void {
    if (_pageEvent) {
      this.pageEvent = _pageEvent;
      this.pageSize = _pageEvent.pageSize;
    }
    const options = {
      limit: (this.pageEvent ? (this.pageEvent.pageSize) : this.pageSize ? this.pageSize : this.paginatorOptions.pageSize),
      page: (this.pageEvent ? (this.pageEvent.pageIndex + 1) : 1),
      sort: [this.sortBy]
    };
    if (this.searchTerm.field_name) {
      options['field_name'] = this.searchTerm.field_name;
    }
    this.inProgress = true;
    this.testTemplateService.getTestTemplateFieldValues(options, this.id)
      .subscribe(response => {
          if (response && response.data && response.data.rows && response.data.rows.length) {
            this.dataSource = new MatTableDataSource(response.data.rows);
            this.paginatorOptions.count = response.data.count;
            this.dataSource.sort = this.sort;
            this.usedFields.length = 0;
            this.usedFields = flatMap(response.data.rows, 'field_name');
          } else {
            this.errorMsgService.openSnackBar(this.errorMsgService.getNonFormError('NO_DATA'));
          }
          this.inProgress = false;
        },
        err => {
          this.errorMsgService.handleServerErrors(err);
          this.inProgress = false;
        });
  }

  public applyFilter(searchCriteria: { key: string, value: string }): void {
    this.searchTerm[searchCriteria.key] = searchCriteria.value;
    this.paginator.pageIndex = 0;
    this.fetchTestTemplateFieldsListing();
  }

  public editDetail(field: TestTemplateFieldValues): void {
    this.openDetailsDialog(field);
  }

  public deleteDetail(id: string): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '250px',
      disableClose: true,
      closeOnNavigation: true,
      data: {title: 'Confirm Delete', text: 'Do you want to delete this field?', okButtonText: 'Yes', cancelButtonText: 'No'}
    });

    dialogRef.afterClosed().subscribe(response => {
      if (response) {
        this.testTemplateService.deleteFieldValue(this.id, id)
          .subscribe(() => {
              this.fetchTestTemplateFieldsListing();
              this.inProgress = false;
            },
            err => {
              this.errorMsgService.handleServerErrors(err);
              this.inProgress = false;
            });
      }
    });
  }

  public addDetail(): void {
    this.openDetailsDialog();
  }

  private openDetailsDialog(field?: TestTemplateFieldValues): void {
    const conf: MatDialogConfig = {
      autoFocus: false,
      disableClose: true,
      closeOnNavigation: true,
      panelClass: 'panelClass',
      maxWidth: '800px',
      data: {msgType: this.msgType, id: this.id, usedFields: this.usedFields}
    };
    if (field) {
      conf.data['field'] = field;
    }
    const dialogRef = this.dialog.open(TestTemplateDetailEditorComponent, conf);
    dialogRef.afterClosed().subscribe(result => {
      if (result && result.success && result.data && result.data.field_values) {
        this.fetchTestTemplateFieldsListing();
      }
    });
  }
}
