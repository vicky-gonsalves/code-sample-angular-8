import {Component, OnInit} from '@angular/core';
import {appAnimations} from '../../../core/animations/animations';

@Component({
  selector: 'test-template',
  templateUrl: './test-template.component.html',
  styleUrls: ['./test-template.component.scss'],
  animations: appAnimations
})
export class TestTemplateComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
