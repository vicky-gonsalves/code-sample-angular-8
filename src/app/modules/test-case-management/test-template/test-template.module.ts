import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TestTemplateComponent} from './test-template.component';
import {TestTemplateEditorComponent} from './components/test-template-editor/test-template-editor.component';
import {TestTemplateListComponent} from './components/test-template-list/test-template-list.component';
import {routing} from './test-template.routes';
import {SharedModule} from '../../../shared/shared.module';
import {TestTemplateViewComponent} from './components/test-template-view/test-template-view.component';
import {TestTemplateDetailListComponent} from './components/test-template-detail-list/test-template-detail-list.component';
import {TestTemplateDetailEditorComponent} from './components/test-template-detail-editor/test-template-detail-editor.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    routing
  ],
  declarations: [
    TestTemplateComponent,
    TestTemplateEditorComponent,
    TestTemplateListComponent,
    TestTemplateViewComponent,
    TestTemplateDetailListComponent,
    TestTemplateDetailEditorComponent
  ],
  entryComponents: [TestTemplateDetailEditorComponent]
})
export class TestTemplateModule {
}
