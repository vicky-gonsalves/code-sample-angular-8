import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TestCaseManagementComponent} from './test-case-management.component';
import {routing} from './test-case-management.routes';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    routing
  ],
  declarations: [TestCaseManagementComponent]
})
export class TestCaseManagementModule {
}
