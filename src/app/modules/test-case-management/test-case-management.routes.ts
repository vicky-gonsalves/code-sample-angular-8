import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserDataGuard} from '../../core/guards/user-data/user-data.guard';

export const routes: Routes = [
  {
    path: '',
    canActivate: [UserDataGuard],
    children: [
      {
        path: 'test-templates',
        canActivateChild: [UserDataGuard],
        loadChildren: () => import('./test-template/test-template.module').then(mod => mod.TestTemplateModule),
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'test-template'}
      }
    ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);

