import {Component, OnInit} from '@angular/core';
import {appAnimations} from '../../core/animations/animations';

@Component({
  selector: 'test-case-management',
  templateUrl: './test-case-management.component.html',
  animations: appAnimations
})
export class TestCaseManagementComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
