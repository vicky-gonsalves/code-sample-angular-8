import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SettlementAccountComponent} from './settlement-account.component';
import {SettlementAccountEditorComponent} from './component/settlement-account-editor/settlement-account-editor.component';
import {SettlementAccountListComponent} from './component/settlement-account-list/settlement-account-list.component';
import {SharedModule} from '../../../shared/shared.module';
import {routing} from './settlement-account.routes';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    routing
  ],
  declarations: [
    SettlementAccountComponent,
    SettlementAccountEditorComponent,
    SettlementAccountListComponent
  ]
})
export class SettlementAccountModule {
}
