import {Component, OnInit} from '@angular/core';
import {PageHeaderService} from '../../../../../shared/services/page-header/page-header.service';
import {PageTitleService} from '../../../../../shared/services/page-title/page-title.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NavItemInterface} from '../../../../../interface/nav-item.interface';
import {NavItemService} from '../../../../../shared/services/nav-item/nav-item.service';
import {SettlementAccountService} from '../../../../../shared/services/settlement-account/settlement-account.service';
import {SettlementAccountOwnerService} from '../../../../../shared/services/settlement-account-owner/settlement-account-owner.service';
import {debounceTime, distinctUntilChanged, startWith, switchMap} from 'rxjs/internal/operators';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {SettlementAccountOwner} from '../../../../../model/settlement-account-owner.model';
import {Observable} from 'rxjs';
import {SettlementAccountOwnerInterface} from '../../../../../interface/settlement-account-owner.interface';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {SettlementAccount} from '../../../../../model/settlement-account.model';
import {HelpTextService} from '../../../../../core/services/help-text/help-text.service';

@Component({
  selector: 'settlement-account-editor',
  templateUrl: './settlement-account-editor.component.html',
  styleUrls: ['./settlement-account-editor.component.scss']
})
export class SettlementAccountEditorComponent implements OnInit {
  public IS_EDIT: Boolean = false;
  public helpText: any;
  public settlementAccountForm: FormGroup;
  public settlementAccount: SettlementAccount = new SettlementAccount(
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null
  );
  public inProgress: Boolean = false;
  public submitted: Boolean = false;
  public filteredSettlementAccountOwners: Observable<SettlementAccountOwner[]>;
  private componentNavItem: NavItemInterface;
  private settlementAccountId: string;

  constructor(public navItems: NavItemService,
              public pageTitle: PageTitleService,
              private errorMessageService: ErrorMessageService,
              private settlementAccountService: SettlementAccountService,
              private settlementAccountOwnerService: SettlementAccountOwnerService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe(params => {
      if (params.hasOwnProperty('id') && params.id.length) {
        this.settlementAccountId = params.id;
        this.IS_EDIT = true;
      }
    });
    HelpTextService.helpText.subscribe(helpText => {
      if (helpText.settlementAccount) {
        this.helpText = helpText.settlementAccount;
      }
    });
  }

  get id() {
    return this.settlementAccountForm.get('id');
  }

  get owner_id() {
    return this.settlementAccountForm.get('owner_id');
  }

  get account_type() {
    return this.settlementAccountForm.get('account_type');
  }

  get max_transaction_limit() {
    return this.settlementAccountForm.get('max_transaction_limit');
  }

  get balance() {
    return this.settlementAccountForm.get('balance');
  }

  get min_prefund_req_amount() {
    return this.settlementAccountForm.get('min_prefund_req_amount');
  }

  get alertmark() {
    return this.settlementAccountForm.get('alertmark');
  }

  get resetalertmark() {
    return this.settlementAccountForm.get('resetalertmark');
  }

  ngOnInit() {
    let id = 'settlement-add';
    if (this.IS_EDIT) {
      id = 'settlement-edit';
      PageHeaderService.updatePageHeader({backPath: 'manage-account/settlement', subTitle: 'Loading...'});
    } else {
      PageHeaderService.updatePageHeader({backPath: 'manage-account/settlement'});
    }
    this.componentNavItem = this.navItems.getItemById(id);
    if (this.componentNavItem) {
      this.pageTitle.setTitle(this.componentNavItem.name);
    }

    this.setupForm();

    if (this.settlementAccountId) {
      this.fetchSettlementAccount(this.settlementAccountId);
    }
  }

  public getErrorMessage(fieldName: string): string {
    return this.errorMessageService.getError(this[fieldName], fieldName);
  }

  public submitSettlementAccountForm(): void {
    this.submitted = true;
    this.settlementAccount = this.prepareFormFields();
    if (this.settlementAccountForm.valid) {
      this.setInProgress();
      if (this.IS_EDIT) {
        this.settlementAccountService.updateSettlementAccount(this.settlementAccount)
          .subscribe(response => {
              if (response && response.hasOwnProperty('data') && response.data) {
                this.setValue(response.data);
                this.errorMessageService.openSnackBar('Settlement account successfully updated.', 5000, 'OK');
              }
              this.setNotInProgress();
            },
            err => {
              this.setNotInProgress();
              this.errorMessageService.handleServerErrors(err, this.settlementAccountForm);
            });
      } else {
        this.settlementAccountService.addSettlementAccount(this.settlementAccount)
          .subscribe(response => {
              if (response && response.hasOwnProperty('data') && response.data) {
                this.router.navigate(['manage-account/settlement']);
                this.errorMessageService.openSnackBar('Settlement account successfully added.', 5000, 'OK');
              }
              this.setNotInProgress();
            },
            err => {
              this.setNotInProgress();
              this.errorMessageService.handleServerErrors(err, this.settlementAccountForm);
            });
      }
    }
  }

  public settlementAccountOwnerDisplayFn(settlementAccountOwner?: SettlementAccountOwnerInterface): string | undefined {
    return settlementAccountOwner ? (settlementAccountOwner.id) : undefined;
  }

  private fetchSettlementAccount(id: string): void {
    this.settlementAccountService.getSettlementAccount(id)
      .subscribe(response => {
          if (response && response.hasOwnProperty('data') && response.data) {
            this.setValue(response.data);
          }
          this.setNotInProgress();
        },
        err => {
          this.setNotInProgress();
          this.errorMessageService.handleServerErrors(err, this.settlementAccountForm);
        });
  }

  private setInProgress(): void {
    this.settlementAccountForm.disable();
    this.inProgress = true;
  }

  private setNotInProgress(): void {
    this.inProgress = false;
    this.settlementAccountForm.enable();
  }

  private setupForm(): void {
    this.settlementAccountForm = new FormGroup({
      'id': new FormControl(this.settlementAccount.id, [
        Validators.required,
        Validators.pattern(/^[A-Za-z\d]+$/),
        Validators.minLength(1),
        Validators.maxLength(35)
      ]),
      'owner_id': new FormControl(this.settlementAccount.owner_id, [
        Validators.required
      ]),
      'account_type': new FormControl(this.settlementAccount.account_type, [
        Validators.required
      ]),
      'max_transaction_limit': new FormControl(this.settlementAccount.max_transaction_limit, []),
      'balance': new FormControl(this.settlementAccount.balance, [
        Validators.required,
        Validators.pattern(/^[-]?\d+(\.\d{1,2})?$/)
      ]),
      'min_prefund_req_amount': new FormControl(this.settlementAccount.min_prefund_req_amount, [
        Validators.required,
        Validators.pattern(/^[-]?\d+(\.\d{1,2})?$/)
      ]),
      'alertmark': new FormControl(this.settlementAccount.alertmark, [
        Validators.required,
        Validators.min(0),
        Validators.max(100),
        Validators.pattern(/^[-]?\d+(\.\d{1,2})?$/)
      ]),
      'resetalertmark': new FormControl(this.settlementAccount.resetalertmark, [
        Validators.required,
        Validators.min(0),
        Validators.max(100),
        Validators.pattern(/^[-]?\d+(\.\d{1,2})?$/)
      ])
    });

    this.initExtraValidators();
  }


  private initExtraValidators(): void {
    this.account_type.valueChanges.subscribe(data => this.onAccountTypeChanged(data));
  }

  private onAccountTypeChanged(value: string): void {
    this.filteredSettlementAccountOwners = null;
    this.owner_id.setValue(null);
    if (value) {
      this.filteredSettlementAccountOwners = this.owner_id.valueChanges
        .pipe(
          debounceTime(100),
          distinctUntilChanged(),
          startWith(null),
          switchMap(
            (term: string | any) =>
              (typeof term === 'string' || term === '' || term === null) ?
                this.settlementAccountOwnerService.getSettlementAccountOwnersForAutocomplete({
                  account_type: this.settlementAccountForm.value.account_type ? this.settlementAccountForm.value.account_type : null,
                  term: term,
                  limit: 10
                }) : []
          )
        );
    }
    if (value === 'FundingAgent' || value === 'Tracking') {
      this.settlementAccountForm.controls['max_transaction_limit'].setValidators([Validators.required, Validators.pattern(/^[-]?\d+(\.\d{1,2})?$/)]);
    } else {
      this.settlementAccountForm.controls['max_transaction_limit'].setValidators(null);
    }
    this.settlementAccountForm.controls['max_transaction_limit'].updateValueAndValidity();
  }

  private prepareFormFields(): SettlementAccount {
    const formModel = this.settlementAccountForm.value;
    if (formModel.owner_id && !formModel.owner_id.id) {
      this.owner_id.setValue(null);
    }
    let max_tr_limit = formModel.max_transaction_limit ? formModel.max_transaction_limit : null;
    if (formModel.account_type !== 'FundingAgent' && formModel.account_type !== 'Tracking') {
      max_tr_limit = null;
    }
    return {
      id: formModel.id,
      owner_id: formModel.owner_id && formModel.owner_id.id ? formModel.owner_id.id : null,
      account_type: formModel.account_type ? formModel.account_type : null,
      max_transaction_limit: max_tr_limit,
      balance: formModel.balance,
      min_prefund_req_amount: formModel.min_prefund_req_amount,
      alertmark: formModel.alertmark,
      resetalertmark: formModel.resetalertmark,
      _id: this.settlementAccount && this.settlementAccount._id ? this.settlementAccount._id : null
    };
  }

  private setValue(data: SettlementAccount): void {
    setTimeout(() => {
      this.settlementAccountForm.patchValue({
        account_type: data.account_type,
        id: data.id,
        owner_id: {id: data.owner_id},
        max_transaction_limit: data.max_transaction_limit ? JSON.parse(data.max_transaction_limit).$numberDecimal : null,
        balance: JSON.parse(data.balance).$numberDecimal,
        min_prefund_req_amount: JSON.parse(data.min_prefund_req_amount).$numberDecimal,
        alertmark: JSON.parse(data.alertmark).$numberDecimal,
        resetalertmark: JSON.parse(data.resetalertmark).$numberDecimal
      });
      this.settlementAccount = data;
    });
    PageHeaderService.updatePageHeader({backPath: 'manage-account/settlement', subTitle: this.settlementAccount.id});
  }

}
