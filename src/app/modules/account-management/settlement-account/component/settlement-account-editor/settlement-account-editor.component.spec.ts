import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SettlementAccountEditorComponent} from './settlement-account-editor.component';

describe('SettlementAccountEditorComponent', () => {
  let component: SettlementAccountEditorComponent;
  let fixture: ComponentFixture<SettlementAccountEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SettlementAccountEditorComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettlementAccountEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
