import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {NavItemInterface} from '../../../../../interface/nav-item.interface';
import {SettlementAccountInterface} from '../../../../../interface/settlement-account.interface';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {NavItemService} from '../../../../../shared/services/nav-item/nav-item.service';
import {PageHeaderService} from '../../../../../shared/services/page-header/page-header.service';
import {PageTitleService} from '../../../../../shared/services/page-title/page-title.service';
import {SettlementAccountService} from '../../../../../shared/services/settlement-account/settlement-account.service';

@Component({
  selector: 'settlement-account-list',
  templateUrl: './settlement-account-list.component.html',
  styleUrls: ['./settlement-account-list.component.scss']
})
export class SettlementAccountListComponent implements OnInit {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  public searchTerm: {
    id: string,
    owner_id: string
  };
  public displayedColumns = ['id', 'account_type', 'owner_id', 'balance', 'min_prefund_req_amount', 'alertmark', 'resetalertmark', 'actions'];
  public dataSource: MatTableDataSource<SettlementAccountInterface>;
  public paginatorOptions = {
    count: 0,
    pageSize: 10,
    pageSizeOptions: [5, 10, 25, 50, 100],
    showFirstLastButtons: true
  };
  public inProgress: Boolean = true;
  private sortBy = '-createdAt';
  private pageSize = 10;
  private componentNavItem: NavItemInterface;

  constructor(private navItems: NavItemService,
              private settlementAccountService: SettlementAccountService,
              private errorMsgService: ErrorMessageService,
              private pageTitle: PageTitleService) {
    this.searchTerm = {
      id: null,
      owner_id: null
    };
  }

  ngOnInit() {
    // Get Navigation Component Name
    this.componentNavItem = this.navItems.getItemById('settlement');
    if (this.componentNavItem) {
      // Set Page Title
      this.pageTitle.setTitle(this.componentNavItem.name);
    }

    // Set Page Header Details
    PageHeaderService.updatePageHeader({
      showImage: true,
      icon: 'account_balance',
      actionButton: {
        title: 'Add new settlement account',
        url: 'manage-account/settlement/add'
      }
    });

    // Fetch SettlementAccounts
    this.fetchSettlementAccountListing();
  }

  public sortData(sort: Sort): void {
    const data = this.dataSource;
    if (!sort.active || sort.direction === '') {
      this.sortBy = '-createdAt';
    } else {
      this.sortBy = sort.direction === 'asc' ? sort.active : '-' + sort.active;
    }
    this.paginator.pageIndex = 0;
    this.fetchSettlementAccountListing();
  }

  public fetchSettlementAccountListing(pageEvent?: PageEvent): void {
    if (pageEvent) {
      this.pageSize = pageEvent.pageSize;
    }
    const options = {
      limit: (pageEvent ? (pageEvent.pageSize) : this.pageSize ? this.pageSize : this.paginatorOptions.pageSize),
      page: (pageEvent ? (pageEvent.pageIndex + 1) : 1),
      sort: [this.sortBy]
    };
    if (this.searchTerm.id) {
      options['id'] = this.searchTerm.id;
    }
    if (this.searchTerm.owner_id) {
      options['owner_id'] = this.searchTerm.owner_id;
    }
    this.inProgress = true;
    this.settlementAccountService.getSettlementAccounts(options)
      .subscribe(response => {
          if (response && response.data && response.data.rows && response.data.rows.length) {
            const source = response.data.rows.map(row => {
              row.balance = JSON.parse(row.balance).$numberDecimal;
              row.min_prefund_req_amount = row.min_prefund_req_amount ? JSON.parse(row.min_prefund_req_amount).$numberDecimal : null;
              row.alertmark = JSON.parse(row.alertmark).$numberDecimal;
              row.resetalertmark = JSON.parse(row.resetalertmark).$numberDecimal;
              return row;
            });
            this.dataSource = new MatTableDataSource(source);
            this.paginatorOptions.count = response.data.count;
            this.dataSource.sort = this.sort;
          } else {
            this.errorMsgService.openSnackBar(this.errorMsgService.getNonFormError('NO_DATA'));
          }
          this.inProgress = false;
        },
        err => {
          this.errorMsgService.handleServerErrors(err);
          this.inProgress = false;
        });
  }

  public applyFilter(searchCriteria: { key: string, value: string }): void {
    this.searchTerm[searchCriteria.key] = searchCriteria.value;
    this.paginator.pageIndex = 0;
    this.fetchSettlementAccountListing();
  }
}
