import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SettlementAccountListComponent} from './settlement-account-list.component';

describe('SettlementAccountListComponent', () => {
  let component: SettlementAccountListComponent;
  let fixture: ComponentFixture<SettlementAccountListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SettlementAccountListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettlementAccountListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
