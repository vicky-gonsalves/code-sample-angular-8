import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SettlementAccountListComponent} from './component/settlement-account-list/settlement-account-list.component';
import {SettlementAccountEditorComponent} from './component/settlement-account-editor/settlement-account-editor.component';
import {SettlementAccountComponent} from './settlement-account.component';
import {UserDataGuard} from '../../../core/guards/user-data/user-data.guard';

export const routes: Routes = [
  {
    path: '',
    canActivate: [UserDataGuard],
    component: SettlementAccountComponent,
    children: [
      {
        path: '',
        canActivateChild: [UserDataGuard],
        component: SettlementAccountListComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'settlement'}
      },
      {
        path: 'add',
        canActivateChild: [UserDataGuard],
        component: SettlementAccountEditorComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'settlement-add'}
      },
      {
        path: 'edit/:id',
        canActivateChild: [UserDataGuard],
        component: SettlementAccountEditorComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'settlement-edit'}
      }
    ]
  },
  {path: '**', redirectTo: '', pathMatch: 'full'}
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);

