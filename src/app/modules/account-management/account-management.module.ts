import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AccountManagementComponent} from './account-management.component';
import {SharedModule} from '../../shared/shared.module';
import {routing} from './account-management.routes';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    routing
  ],
  declarations: [AccountManagementComponent]
})
export class AccountManagementModule {
}
