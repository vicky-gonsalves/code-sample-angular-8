import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SettlementOwnerComponent} from './settlement-owner.component';
import {SettlementOwnerListComponent} from './components/settlement-owner-list/settlement-owner-list.component';
import {SettlementOwnerEditorComponent} from './components/settlement-owner-editor/settlement-owner-editor.component';
import {UserDataGuard} from '../../../core/guards/user-data/user-data.guard';

export const routes: Routes = [
  {
    path: '',
    canActivate: [UserDataGuard],
    component: SettlementOwnerComponent,
    children: [
      {
        path: '',
        canActivateChild: [UserDataGuard],
        component: SettlementOwnerListComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'settlement-owners'}
      },
      {
        path: 'add',
        canActivateChild: [UserDataGuard],
        component: SettlementOwnerEditorComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'settlement-owners-add'}
      },
      {
        path: 'edit/:id',
        canActivateChild: [UserDataGuard],
        component: SettlementOwnerEditorComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'settlement-owners-edit'}
      }
    ]
  },
  {path: '**', redirectTo: '', pathMatch: 'full'}
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);

