import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SettlementOwnerComponent} from './settlement-owner.component';
import {SettlementOwnerEditorComponent} from './components/settlement-owner-editor/settlement-owner-editor.component';
import {SettlementOwnerListComponent} from './components/settlement-owner-list/settlement-owner-list.component';
import {SharedModule} from '../../../shared/shared.module';
import {routing} from './settlement-owner.routes';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    routing
  ],
  declarations: [
    SettlementOwnerComponent,
    SettlementOwnerEditorComponent,
    SettlementOwnerListComponent
  ]
})
export class SettlementOwnerModule {
}
