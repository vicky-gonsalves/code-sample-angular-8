import {Component, OnInit} from '@angular/core';
import {PageHeaderService} from '../../../../../shared/services/page-header/page-header.service';
import {PageTitleService} from '../../../../../shared/services/page-title/page-title.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NavItemInterface} from '../../../../../interface/nav-item.interface';
import {NavItemService} from '../../../../../shared/services/nav-item/nav-item.service';
import {SettlementAccountOwnerService} from '../../../../../shared/services/settlement-account-owner/settlement-account-owner.service';
import {SettlementAccountOwner} from '../../../../../model/settlement-account-owner.model';
import {HttpErrorResponse} from '@angular/common/http';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';

@Component({
  selector: 'settlement-owner-editor',
  templateUrl: './settlement-owner-editor.component.html',
  styleUrls: ['./settlement-owner-editor.component.scss']
})
export class SettlementOwnerEditorComponent implements OnInit {

  public IS_EDIT: Boolean = false;
  public settlementOwnerForm: FormGroup;
  public settlementAccountOwner: SettlementAccountOwner = new SettlementAccountOwner(
    null,
    null
  );
  public inProgress: Boolean = false;
  public submitted: Boolean = false;
  private componentNavItem: NavItemInterface;
  private settlementAccountOwnerId: string;

  constructor(public navItems: NavItemService,
              public pageTitle: PageTitleService,
              private errorMessageService: ErrorMessageService,
              private settlementAccountOwnerService: SettlementAccountOwnerService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe(params => {
      if (params.hasOwnProperty('id') && params.id.length) {
        this.settlementAccountOwnerId = params.id;
        this.IS_EDIT = true;
      }
    });
  }

  get id() {
    return this.settlementOwnerForm.get('id');
  }

  get name() {
    return this.settlementOwnerForm.get('name');
  }

  ngOnInit() {
    let id = 'settlement-owners-add';
    if (this.IS_EDIT) {
      id = 'settlement-owners-edit';
      PageHeaderService.updatePageHeader({backPath: 'manage-account/settlement-owners', subTitle: 'Loading...'});
    } else {
      PageHeaderService.updatePageHeader({backPath: 'manage-account/settlement-owners'});
    }
    this.componentNavItem = this.navItems.getItemById(id);
    if (this.componentNavItem) {
      this.pageTitle.setTitle(this.componentNavItem.name);
    }

    this.setupForm();

    if (this.settlementAccountOwnerId) {
      this.fetchSettlementAccountOwner(this.settlementAccountOwnerId);
    }
  }

  public getErrorMessage(fieldName: string): string {
    return this.errorMessageService.getError(this[fieldName], fieldName);
  }

  public submitSettlementOwnerForm(): void {
    this.submitted = true;
    this.settlementAccountOwner = this.prepareFormFields();
    if (this.settlementOwnerForm.valid) {
      this.setInProgress();
      if (this.IS_EDIT) {
        this.settlementAccountOwnerService.updateSettlementAccountOwner(this.settlementAccountOwner)
          .subscribe(response => {
              if (response && response.hasOwnProperty('data') && response.data) {
                this.setValue(response.data);
                this.errorMessageService.openSnackBar('Settlement account owner successfully updated.', 5000, 'OK');
              }
              this.setNotInProgress();
            },
            err => {
              this.setNotInProgress();
              this.errorMessageService.handleServerErrors(err, this.settlementOwnerForm);
            });
      } else {
        this.settlementAccountOwnerService.addSettlementAccountOwner(this.settlementAccountOwner)
          .subscribe(response => {
              if (response && response.hasOwnProperty('data') && response.data) {
                this.router.navigate(['manage-account/settlement-owners']);
                this.errorMessageService.openSnackBar('Settlement account owner successfully added.', 5000, 'OK');
              }
              this.setNotInProgress();
            },
            (err: HttpErrorResponse) => {
              this.setNotInProgress();
              this.errorMessageService.handleServerErrors(err, this.settlementOwnerForm);
            });
      }
    }
  }

  private fetchSettlementAccountOwner(id: string): void {
    this.settlementAccountOwnerService.getSettlementAccountOwner(id)
      .subscribe(response => {
          if (response && response.hasOwnProperty('data') && response.data) {
            this.setValue(response.data);
          }
          this.setNotInProgress();
        },
        err => {
          this.setNotInProgress();
          this.errorMessageService.handleServerErrors(err, this.settlementOwnerForm);
        });
  }

  private setInProgress(): void {
    this.settlementOwnerForm.disable();
    this.inProgress = true;
  }

  private setNotInProgress(): void {
    this.inProgress = false;
    this.settlementOwnerForm.enable();
  }

  private setupForm(): void {
    this.settlementOwnerForm = new FormGroup({
      'id': new FormControl(this.settlementAccountOwner.id, [
        Validators.required,
        Validators.pattern(/^[A-Za-z\d]+$/),
        Validators.minLength(1),
        Validators.maxLength(35)
      ]),
      'name': new FormControl(this.settlementAccountOwner.name, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(100)
      ])
    });
  }

  private prepareFormFields(): SettlementAccountOwner {
    const formModel = this.settlementOwnerForm.value;
    return {
      id: formModel.id,
      name: formModel.name,
      _id: this.settlementAccountOwner && this.settlementAccountOwner._id ? this.settlementAccountOwner._id : null
    };
  }

  private setValue(data: SettlementAccountOwner): void {
    this.settlementOwnerForm.setValue({
      id: data.id,
      name: data.name
    });
    this.settlementAccountOwner = data;
    PageHeaderService.updatePageHeader({backPath: 'manage-account/settlement-owners', subTitle: this.settlementAccountOwner.id});
  }
}
