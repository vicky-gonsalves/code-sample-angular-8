import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SettlementOwnerEditorComponent} from './settlement-owner-editor.component';

describe('SettlementOwnerEditorComponent', () => {
  let component: SettlementOwnerEditorComponent;
  let fixture: ComponentFixture<SettlementOwnerEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SettlementOwnerEditorComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettlementOwnerEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
