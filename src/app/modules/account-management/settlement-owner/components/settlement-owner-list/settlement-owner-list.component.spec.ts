import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SettlementOwnerListComponent} from './settlement-owner-list.component';

describe('SettlementOwnerListComponent', () => {
  let component: SettlementOwnerListComponent;
  let fixture: ComponentFixture<SettlementOwnerListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SettlementOwnerListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettlementOwnerListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
