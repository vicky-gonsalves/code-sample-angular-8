import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {ConnectionOwnerInterface} from '../../../../../interface/connection-owner.interface';
import {NavItemInterface} from '../../../../../interface/nav-item.interface';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {NavItemService} from '../../../../../shared/services/nav-item/nav-item.service';
import {PageHeaderService} from '../../../../../shared/services/page-header/page-header.service';
import {PageTitleService} from '../../../../../shared/services/page-title/page-title.service';
import {SettlementAccountOwnerService} from '../../../../../shared/services/settlement-account-owner/settlement-account-owner.service';

@Component({
  selector: 'settlement-owner-list',
  templateUrl: './settlement-owner-list.component.html',
  styleUrls: ['./settlement-owner-list.component.scss']
})
export class SettlementOwnerListComponent implements OnInit {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  public searchTerm: {
    id: string,
    name: string
  };
  public displayedColumns = ['id', 'name', 'actions'];
  public dataSource: MatTableDataSource<ConnectionOwnerInterface>;
  public paginatorOptions = {
    count: 0,
    pageSize: 10,
    pageSizeOptions: [5, 10, 25, 50, 100],
    showFirstLastButtons: true
  };
  public inProgress: Boolean = true;
  private sortBy = '-createdAt';
  private pageSize = 10;
  private componentNavItem: NavItemInterface;

  constructor(private navItems: NavItemService,
              private settlementAccountOwnerService: SettlementAccountOwnerService,
              private errorMsgService: ErrorMessageService,
              private pageTitle: PageTitleService) {
    this.searchTerm = {
      id: null,
      name: null
    };
  }

  ngOnInit() {
    this.componentNavItem = this.navItems.getItemById('settlement-owners');
    if (this.componentNavItem) {
      this.pageTitle.setTitle(this.componentNavItem.name);
    }

    // Set Page Header Details
    PageHeaderService.updatePageHeader({
      showImage: true,
      icon: 'account_box',
      actionButton: {
        title: 'Add new settlement account owner',
        url: 'manage-account/settlement-owners/add'
      }
    });

    // Fetch Connection Owners
    this.fetchConnectionOwnersListing();
  }

  public sortData(sort: Sort): void {
    const data = this.dataSource;
    if (!sort.active || sort.direction === '') {
      this.sortBy = '-createdAt';
    } else {
      this.sortBy = sort.direction === 'asc' ? sort.active : '-' + sort.active;
    }
    this.paginator.pageIndex = 0;
    this.fetchConnectionOwnersListing();
  }

  public fetchConnectionOwnersListing(pageEvent?: PageEvent): void {
    if (pageEvent) {
      this.pageSize = pageEvent.pageSize;
    }
    const options = {
      limit: (pageEvent ? (pageEvent.pageSize) : this.pageSize ? this.pageSize : this.paginatorOptions.pageSize),
      page: (pageEvent ? (pageEvent.pageIndex + 1) : 1),
      sort: [this.sortBy]
    };
    if (this.searchTerm.id) {
      options['id'] = this.searchTerm.id;
    }
    if (this.searchTerm.name) {
      options['name'] = this.searchTerm.name;
    }
    this.inProgress = true;
    this.settlementAccountOwnerService.getSettlementAccountOwners(options)
      .subscribe(response => {
          if (response && response.data && response.data.rows && response.data.rows.length) {
            this.dataSource = new MatTableDataSource(response.data.rows);
            this.paginatorOptions.count = response.data.count;
          } else {
            this.errorMsgService.openSnackBar(this.errorMsgService.getNonFormError('NO_DATA'));
          }
          this.inProgress = false;
        },
        err => {
          this.errorMsgService.handleServerErrors(err);
          this.inProgress = false;
        });
  }

  public applyFilter(searchCriteria: { key: string, value: string }): void {
    this.searchTerm[searchCriteria.key] = searchCriteria.value;
    this.paginator.pageIndex = 0;
    this.fetchConnectionOwnersListing();
  }
}
