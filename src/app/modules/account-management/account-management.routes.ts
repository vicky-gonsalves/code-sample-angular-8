import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserDataGuard} from '../../core/guards/user-data/user-data.guard';

export const routes: Routes = [
  {
    path: '',
    canActivate: [UserDataGuard],
    children: [
      {
        path: 'settlement',
        canActivateChild: [UserDataGuard],
        loadChildren: () => import('./settlement-account/settlement-account.module').then(mod => mod.SettlementAccountModule),
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'settlement'}
      },
      {
        path: 'settlement-owners',
        canActivateChild: [UserDataGuard],
        loadChildren: () => import('./settlement-owner/settlement-owner.module').then(mod => mod.SettlementOwnerModule),
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'settlement-owners'}
      }
    ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);

