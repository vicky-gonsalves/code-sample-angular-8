import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ConnectionManagementComponent} from './connection-management.component';
import {SharedModule} from '../../shared/shared.module';
import {routing} from './connection-management.routes';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    routing
  ],
  declarations: [ConnectionManagementComponent]
})
export class ConnectionManagementModule {
}
