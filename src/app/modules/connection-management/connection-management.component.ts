import {Component, OnInit} from '@angular/core';
import {appAnimations} from '../../core/animations/animations';

@Component({
  selector: 'connection-management',
  templateUrl: './connection-management.component.html',
  animations: appAnimations
})
export class ConnectionManagementComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
