import {Component, OnInit} from '@angular/core';
import {appAnimations} from '../../../core/animations/animations';

@Component({
  selector: 'connection-owner',
  templateUrl: './connection-owner.component.html',
  styleUrls: ['./connection-owner.component.scss'],
  animations: appAnimations
})
export class ConnectionOwnerComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
