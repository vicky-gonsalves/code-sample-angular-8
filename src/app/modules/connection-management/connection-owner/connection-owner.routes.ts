import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ConnectionOwnerComponent} from './connection-owner.component';
import {ConnectionOwnerEditorComponent} from './components/connection-owner-editor/connection-owner-editor.component';
import {ConnectionOwnerListComponent} from './components/connection-owner-list/connection-owner-list.component';
import {UserDataGuard} from '../../../core/guards/user-data/user-data.guard';

export const routes: Routes = [
  {
    path: '',
    canActivate: [UserDataGuard],
    component: ConnectionOwnerComponent,
    children: [
      {
        path: '',
        canActivateChild: [UserDataGuard],
        component: ConnectionOwnerListComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'connection-owners'}
      },
      {
        path: 'add',
        canActivateChild: [UserDataGuard],
        component: ConnectionOwnerEditorComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'connection-owners-add'}
      },
      {
        path: 'edit/:id',
        canActivateChild: [UserDataGuard],
        component: ConnectionOwnerEditorComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'connection-owners-edit'}
      }
    ]
  },
  {path: '**', redirectTo: '', pathMatch: 'full'}
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);

