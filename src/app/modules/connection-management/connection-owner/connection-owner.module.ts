import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ConnectionOwnerComponent} from './connection-owner.component';
import {ConnectionOwnerEditorComponent} from './components/connection-owner-editor/connection-owner-editor.component';
import {ConnectionOwnerListComponent} from './components/connection-owner-list/connection-owner-list.component';
import {SharedModule} from '../../../shared/shared.module';
import {routing} from './connection-owner.routes';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    routing
  ],
  declarations: [
    ConnectionOwnerComponent,
    ConnectionOwnerEditorComponent,
    ConnectionOwnerListComponent
  ]
})
export class ConnectionOwnerModule {
}
