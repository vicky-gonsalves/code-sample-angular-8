import {Component, OnInit} from '@angular/core';
import {appAnimations} from '../../../../../core/animations/animations';
import {PageHeaderService} from '../../../../../shared/services/page-header/page-header.service';
import {PageTitleService} from '../../../../../shared/services/page-title/page-title.service';
import {NavItemInterface} from '../../../../../interface/nav-item.interface';
import {NavItemService} from '../../../../../shared/services/nav-item/nav-item.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {ConnectionOwnerService} from '../../../../../shared/services/connection-owner/connection-owner.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ConnectionOwner} from '../../../../../model/connection-owner.model';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'connection-owner-editor',
  templateUrl: './connection-owner-editor.component.html',
  styleUrls: ['./connection-owner-editor.component.scss'],
  animations: appAnimations
})
export class ConnectionOwnerEditorComponent implements OnInit {
  public IS_EDIT: Boolean = false;
  public connectionOwnerForm: FormGroup;
  public connectionOwner: ConnectionOwner = new ConnectionOwner(
    null,
    null
  );
  public inProgress: Boolean = false;
  public submitted: Boolean = false;
  private componentNavItem: NavItemInterface;
  private connectionOwnerId: string;

  constructor(public navItems: NavItemService,
              public pageTitle: PageTitleService,
              private errorMessageService: ErrorMessageService,
              private connectionOwnerService: ConnectionOwnerService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe(params => {
      if (params.hasOwnProperty('id') && params.id.length) {
        this.connectionOwnerId = params.id;
        this.IS_EDIT = true;
      }
    });
  }

  get id() {
    return this.connectionOwnerForm.get('id');
  }

  get name() {
    return this.connectionOwnerForm.get('name');
  }

  ngOnInit() {
    let id = 'connection-owners-add';
    if (this.IS_EDIT) {
      id = 'connection-owners-edit';
      PageHeaderService.updatePageHeader({backPath: 'manage-connection/connection-owners', subTitle: 'Loading...'});
    } else {
      PageHeaderService.updatePageHeader({backPath: 'manage-connection/connection-owners'});
    }
    this.componentNavItem = this.navItems.getItemById(id);
    if (this.componentNavItem) {
      this.pageTitle.setTitle(this.componentNavItem.name);
    }

    this.setupForm();

    if (this.connectionOwnerId) {
      this.fetchConnectionOwner(this.connectionOwnerId);
    }
  }

  public getErrorMessage(fieldName: string): string {
    return this.errorMessageService.getError(this[fieldName], fieldName);
  }

  public submitConnectionOwnerForm(): void {
    this.submitted = true;
    this.connectionOwner = this.prepareFormFields();
    if (this.connectionOwnerForm.valid) {
      this.setInProgress();
      if (this.IS_EDIT) {
        this.connectionOwnerService.updateConnectionOwner(this.connectionOwner)
          .subscribe(response => {
              if (response && response.hasOwnProperty('data') && response.data) {
                this.setValue(response.data);
                this.errorMessageService.openSnackBar('Connection Owner successfully updated.', 5000, 'OK');
              }
              this.setNotInProgress();
            },
            err => {
              this.setNotInProgress();
              this.errorMessageService.handleServerErrors(err, this.connectionOwnerForm);
            });
      } else {
        this.connectionOwnerService.addConnectionOwner(this.connectionOwner)
          .subscribe(response => {
              if (response && response.hasOwnProperty('data') && response.data) {
                this.router.navigate(['manage-connection/connection-owners']);
                this.errorMessageService.openSnackBar('Connection Owner successfully added.', 5000, 'OK');
              }
              this.setNotInProgress();
            },
            (err: HttpErrorResponse) => {
              this.setNotInProgress();
              this.errorMessageService.handleServerErrors(err, this.connectionOwnerForm);
            });
      }
    }
  }

  private fetchConnectionOwner(id: string): void {
    this.connectionOwnerService.getConnectionOwner(id)
      .subscribe(response => {
          if (response && response.hasOwnProperty('data') && response.data) {
            this.setValue(response.data);
          }
          this.setNotInProgress();
        },
        err => {
          this.setNotInProgress();
          this.errorMessageService.handleServerErrors(err, this.connectionOwnerForm);
        });
  }

  private setInProgress(): void {
    this.connectionOwnerForm.disable();
    this.inProgress = true;
  }

  private setNotInProgress(): void {
    this.inProgress = false;
    this.connectionOwnerForm.enable();
  }

  private setupForm(): void {
    this.connectionOwnerForm = new FormGroup({
      'id': new FormControl(this.connectionOwner.id, [
        Validators.required,
        Validators.pattern(/^[A-Za-z\d]+$/),
        Validators.minLength(1),
        Validators.maxLength(35),
      ]),
      'name': new FormControl(this.connectionOwner.name, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(100),
      ])
    });
  }

  private prepareFormFields(): ConnectionOwner {
    const formModel = this.connectionOwnerForm.value;
    return {
      id: formModel.id,
      name: formModel.name,
      _id: this.connectionOwner && this.connectionOwner._id ? this.connectionOwner._id : null
    };
  }

  private setValue(data: ConnectionOwner): void {
    this.connectionOwnerForm.setValue({
      id: data.id,
      name: data.name
    });
    this.connectionOwner = data;
    PageHeaderService.updatePageHeader({backPath: 'manage-connection/connection-owners', subTitle: this.connectionOwner.id});
  }
}
