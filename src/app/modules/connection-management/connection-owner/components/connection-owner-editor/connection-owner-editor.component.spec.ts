import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ConnectionOwnerEditorComponent} from './connection-owner-editor.component';

describe('ConnectionOwnerEditorComponent', () => {
  let component: ConnectionOwnerEditorComponent;
  let fixture: ComponentFixture<ConnectionOwnerEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ConnectionOwnerEditorComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectionOwnerEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
