import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ConnectionOwnerListComponent} from './connection-owner-list.component';

describe('ConnectionOwnerListComponent', () => {
  let component: ConnectionOwnerListComponent;
  let fixture: ComponentFixture<ConnectionOwnerListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ConnectionOwnerListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectionOwnerListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
