import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ConnectionComponent} from './connection.component';
import {ConnectionListComponent} from './components/connection-list/connection-list.component';
import {ConnectionEditorComponent} from './components/connection-editor/connection-editor.component';
import {UserDataGuard} from '../../../core/guards/user-data/user-data.guard';

export const routes: Routes = [
  {
    path: '',
    canActivate: [UserDataGuard],
    component: ConnectionComponent,
    children: [
      {
        path: '',
        canActivateChild: [UserDataGuard],
        component: ConnectionListComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'connections'}
      },
      {
        path: 'add',
        canActivateChild: [UserDataGuard],
        component: ConnectionEditorComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'connection-add'}
      },
      {
        path: 'edit/:id',
        canActivateChild: [UserDataGuard],
        component: ConnectionEditorComponent,
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'connection-edit'}
      }
    ]
  },
  {path: '**', redirectTo: '', pathMatch: 'full'}
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);

