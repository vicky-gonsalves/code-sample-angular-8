import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../../../shared/shared.module';
import {routing} from './connection.routes';
import {ConnectionListComponent} from './components/connection-list/connection-list.component';
import {ConnectionEditorComponent} from './components/connection-editor/connection-editor.component';
import {ConnectionComponent} from './connection.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    routing
  ],
  declarations: [
    ConnectionComponent,
    ConnectionListComponent,
    ConnectionEditorComponent
  ]
})
export class ConnectionModule {
}
