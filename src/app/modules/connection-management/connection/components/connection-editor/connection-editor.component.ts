import {AfterViewInit, Component, OnInit} from '@angular/core';
import {appAnimations} from '../../../../../core/animations/animations';
import {PageHeaderService} from '../../../../../shared/services/page-header/page-header.service';
import {NavItemService} from '../../../../../shared/services/nav-item/nav-item.service';
import {PageTitleService} from '../../../../../shared/services/page-title/page-title.service';
import {NavItemInterface} from '../../../../../interface/nav-item.interface';
import {ActivatedRoute, Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Connection} from '../../../../../model/connection.model';
import {ErrorMessageService} from '../../../../../shared/services/error-message/error-message.service';
import {ConnectionService} from '../../../../../shared/services/connection/connection.service';
import {ConnectionOwnerInterface} from '../../../../../interface/connection-owner.interface';
import {ConnectionOwner} from '../../../../../model/connection-owner.model';
import {Observable} from 'rxjs';
import {debounceTime, distinctUntilChanged, startWith, switchMap} from 'rxjs/internal/operators';
import {ConnectionOwnerService} from '../../../../../shared/services/connection-owner/connection-owner.service';
import * as moment from 'moment-timezone';
import {HelpTextService} from '../../../../../core/services/help-text/help-text.service';

@Component({
  selector: 'connection-editor',
  templateUrl: './connection-editor.component.html',
  styleUrls: ['./connection-editor.component.scss'],
  animations: appAnimations
})
export class ConnectionEditorComponent implements OnInit, AfterViewInit {
  public IS_EDIT: Boolean = false;
  public effectiveDateDisabled: Boolean = false;
  public connectionForm: FormGroup;
  public connection: Connection = new Connection(
    null,
    null,
    true,
    null,
    null,
    true,
    false,
    false,
    null,
    null,
    false,
    null
  );
  public inProgress: Boolean = false;
  public submitted: Boolean = false;
  public minDate = moment().utcOffset('+00:00', true).startOf('day').toISOString();
  public filteredConnectionOwners: Observable<ConnectionOwner[]>;
  public helpText: any;
  private componentNavItem: NavItemInterface;
  private connectionId: string;

  constructor(public navItems: NavItemService,
              public pageTitle: PageTitleService,
              private errorMessageService: ErrorMessageService,
              private connectionService: ConnectionService,
              private connectionOwnerService: ConnectionOwnerService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe(params => {
      if (params.hasOwnProperty('id') && params.id.length) {
        this.connectionId = params.id;
        this.IS_EDIT = true;
      }
    });
    HelpTextService.helpText.subscribe(helpText => {
      if (helpText.connection) {
        this.helpText = helpText.connection;
      }
    });
  }

  get id() {
    return this.connectionForm.get('id');
  }

  get owner_id() {
    return this.connectionForm.get('owner_id');
  }

  get msg_set_version() {
    return this.connectionForm.get('msg_set_version');
  }

  get validate_signature() {
    return this.connectionForm.get('validate_signature');
  }

  get echo_enabled() {
    return this.connectionForm.get('echo_enabled');
  }

  get echo_conn_status_update() {
    return this.connectionForm.get('echo_conn_status_update');
  }

  get echo_conn_unavailable_mark_duration() {
    return this.connectionForm.get('echo_conn_unavailable_mark_duration');
  }

  get echo_interval() {
    return this.connectionForm.get('echo_interval');
  }

  get initial_available() {
    return this.connectionForm.get('initial_available');
  }

  get effective_date() {
    return this.connectionForm.get('effective_date');
  }

  get is_deactive() {
    return this.connectionForm.get('is_deactive');
  }

  ngOnInit() {
    let id = 'connection-add';
    if (this.IS_EDIT) {
      id = 'connection-edit';
      PageHeaderService.updatePageHeader({backPath: 'manage-connection/connections', subTitle: 'Loading...'});
    } else {
      PageHeaderService.updatePageHeader({backPath: 'manage-connection/connections'});
    }
    this.componentNavItem = this.navItems.getItemById(id);
    if (this.componentNavItem) {
      this.pageTitle.setTitle(this.componentNavItem.name);
    }

    this.setupForm();

    if (this.connectionId) {
      this.fetchConnection(this.connectionId);
    }
  }

  ngAfterViewInit() {
    this.initAutocomplete();
  }

  public getErrorMessage(fieldName: string): string {
    return this.errorMessageService.getError(this[fieldName], fieldName);
  }

  public submitConnectionForm(): void {
    this.submitted = true;
    this.connection = this.prepareFormFields();
    if (this.connectionForm.valid) {
      this.setInProgress();
      if (this.IS_EDIT) {
        this.connectionService.updateConnection(this.connection)
          .subscribe(response => {
              if (response && response.hasOwnProperty('data') && response.data) {
                this.setValue(response.data);
                this.errorMessageService.openSnackBar('Connection successfully updated.', 5000, 'OK');
              }
              this.setNotInProgress();
            },
            err => {
              this.setNotInProgress();
              this.errorMessageService.handleServerErrors(err, this.connectionForm);
            });
      } else {
        this.connectionService.addConnection(this.connection)
          .subscribe(response => {
              if (response && response.hasOwnProperty('data') && response.data) {
                this.router.navigate(['manage-connection/connections']);
                this.errorMessageService.openSnackBar('Connection successfully added.', 5000, 'OK');
              }
              this.setNotInProgress();
            },
            err => {
              this.setNotInProgress();
              this.errorMessageService.handleServerErrors(err, this.connectionForm);
            });
      }
    }
  }

  public connectionOwnerDisplayFn(connectionOwner?: ConnectionOwnerInterface): string | undefined {
    return connectionOwner ? (connectionOwner.id) : undefined;
  }

  private fetchConnection(id: string): void {
    this.connectionService.getConnection(id)
      .subscribe(response => {
          if (response && response.hasOwnProperty('data') && response.data) {
            this.setValue(response.data);
          }
          this.setNotInProgress();
        },
        err => {
          this.setNotInProgress();
          this.errorMessageService.handleServerErrors(err, this.connectionForm);
        });
  }

  private initAutocomplete(): void {
    this.filteredConnectionOwners = this.owner_id.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        startWith(null),
        switchMap(
          (term: string | any) =>
            (typeof term === 'string' || term === '' || term === null) ?
              this.connectionOwnerService.getConnectionOwnersForAutocomplete({
                term: term,
                limit: 10
              })
              : []
        )
      );
  }

  private setInProgress(): void {
    this.connectionForm.disable();
    this.inProgress = true;
  }

  private setNotInProgress(): void {
    this.inProgress = false;
    this.connectionForm.enable();
  }

  private setupForm(): void {
    this.connectionForm = new FormGroup({
      'id': new FormControl(this.connection.id, [
        Validators.required,
        Validators.pattern(/^[A-Za-z\d]+$/),
        Validators.minLength(1),
        Validators.maxLength(35),
      ]),
      'owner_id': new FormControl(this.connection.owner_id, [
        Validators.required
      ]),
      'msg_set_version': new FormControl(this.connection.msg_set_version, [
        Validators.required,
        Validators.pattern(/^(?![0.]+$)[A-Za-z0-9]+(?:\.[A-Za-z0-9]+){0,2}$/)
      ]),
      'validate_signature': new FormControl(this.connection.validate_signature, [
        Validators.required
      ]),
      'echo_enabled': new FormControl(this.connection.echo_enabled, [
        Validators.required
      ]),
      'echo_conn_status_update': new FormControl(this.connection.echo_conn_status_update, [
        Validators.required
      ]),
      'echo_conn_unavailable_mark_duration': new FormControl(this.connection.echo_conn_unavailable_mark_duration, [
        Validators.required,
        Validators.min(1)
      ]),
      'echo_interval': new FormControl(this.connection.echo_interval, [
        Validators.required,
        Validators.min(1)
      ]),
      'initial_available': new FormControl(this.connection.initial_available, [
        Validators.required
      ]),
      'effective_date': new FormControl(this.connection.effective_date, [
        Validators.required
      ]),
      'is_deactive': new FormControl(this.connection.is_deactive, [
        Validators.required
      ])
    });
  }

  private prepareFormFields(): Connection {
    const formModel = this.connectionForm.value;
    if (formModel.owner_id && !formModel.owner_id.id) {
      this.owner_id.setValue(null);
    }
    if (formModel.effective_date) {
      formModel.effective_date = moment(formModel.effective_date).utcOffset('+00:00', true).startOf('day').format();
    }
    return {
      id: formModel.id,
      owner_id: formModel.owner_id && formModel.owner_id.id ? formModel.owner_id.id : null,
      msg_set_version: formModel.msg_set_version,
      validate_signature: formModel.validate_signature,
      echo_enabled: formModel.echo_enabled,
      echo_conn_status_update: formModel.echo_conn_status_update,
      echo_conn_unavailable_mark_duration: formModel.echo_conn_unavailable_mark_duration,
      echo_interval: formModel.echo_interval,
      initial_available: formModel.initial_available,
      effective_date: formModel.effective_date,
      is_deactive: formModel.is_deactive,
      _id: this.connection && this.connection._id ? this.connection._id : null
    };
  }

  private setValue(data: Connection): void {
    this.connectionForm.setValue({
      id: data.id,
      owner_id: {id: data.owner_id},
      msg_set_version: data.msg_set_version,
      validate_signature: data.validate_signature,
      echo_enabled: data.echo_enabled,
      echo_conn_status_update: data.echo_conn_status_update,
      echo_conn_unavailable_mark_duration: data.echo_conn_unavailable_mark_duration,
      echo_interval: data.echo_interval,
      initial_available: data.initial_available,
      effective_date: data.effective_date,
      is_deactive: data.is_deactive,
    });
    this.connection = data;
    const effective_date = moment(this.connection.effective_date);
    if (moment(this.minDate) > effective_date) {
      this.minDate = effective_date;
    }
    if (moment(this.connection.effective_date) <= moment().utcOffset('+00:00', true).startOf('day')) {
      this.effectiveDateDisabled = true;
    }
    PageHeaderService.updatePageHeader({backPath: 'manage-connection/connections', subTitle: this.connection.id});
  }

}
