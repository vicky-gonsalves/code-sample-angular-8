import {Component, OnInit} from '@angular/core';
import {appAnimations} from '../../../core/animations/animations';

@Component({
  selector: 'connection',
  templateUrl: './connection.component.html',
  styleUrls: ['./connection.component.scss'],
  animations: appAnimations
})
export class ConnectionComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
