import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserDataGuard} from '../../core/guards/user-data/user-data.guard';

export const routes: Routes = [
  {
    path: '',
    canActivate: [UserDataGuard],
    children: [
      {
        path: 'connections',
        canActivateChild: [UserDataGuard],
        loadChildren: () => import('./connection/connection.module').then(mod => mod.ConnectionModule),
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'connections'}
      },
      {
        path: 'connection-owners',
        loadChildren: () => import('./connection-owner/connection-owner.module').then(mod => mod.ConnectionOwnerModule),
        data: {hideSideNav: false, hideNavBar: false, hideHeader: false, pageId: 'connection-owners'}
      }
    ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);

