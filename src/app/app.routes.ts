import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UiConfigGuard} from './core/guards/ui-config/ui-config.guard';
import {UserDataGuard} from './core/guards/user-data/user-data.guard';

export const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    canActivate: [UserDataGuard, UiConfigGuard],
    loadChildren: () => import('./modules/landing/landing.module').then(mod => mod.LandingModule)
  },
  {
    path: 'account',
    canActivate: [UiConfigGuard],
    loadChildren: () => import('./modules/account/account.module').then(mod => mod.AccountModule)
  },
  {
    path: 'manage-connection',
    canActivate: [UserDataGuard, UiConfigGuard],
    loadChildren: () => import('./modules/connection-management/connection-management.module').then(mod => mod.ConnectionManagementModule)
  },
  {
    path: 'manage-account',
    canActivate: [UserDataGuard, UiConfigGuard],
    loadChildren: () => import('./modules/account-management/account-management.module').then(mod => mod.AccountManagementModule)
  },
  {
    path: 'manage-contact',
    canActivate: [UserDataGuard, UiConfigGuard],
    loadChildren: () => import('./modules/contact-management/contact-management.module').then(mod => mod.ContactManagementModule)
  },
  {
    path: 'manage-participant',
    canActivate: [UserDataGuard, UiConfigGuard],
    loadChildren: () => import('./modules/participant-management/participant-management.module').then(mod => mod.ParticipantManagementModule)
  },
  {
    path: 'manage-participant-routing',
    canActivate: [UserDataGuard, UiConfigGuard],
    loadChildren: () => import('./modules/participant-routing-management/participant-routing-management.module').then(mod => mod.ParticipantRoutingManagementModule)
  },
  {
    path: 'manage-test-case',
    canActivate: [UserDataGuard, UiConfigGuard],
    loadChildren: () => import('./modules/test-case-management/test-case-management.module').then(mod => mod.TestCaseManagementModule)
  },
  {
    path: 'manage-certificate-criteria',
    canActivate: [UserDataGuard, UiConfigGuard],
    loadChildren: () => import('./modules/certificate-criteria-management/certificate-criteria-management.module').then(mod => mod.CertificateCriteriaManagementModule)
  },
  {
    path: 'execute-certification',
    canActivate: [UserDataGuard, UiConfigGuard],
    loadChildren: () => import('./modules/certification-execution/certification-execution.module').then(mod => mod.CertificationExecutionModule)
  },
  {
    path: 'view-report',
    canActivate: [UserDataGuard, UiConfigGuard],
    loadChildren: () => import('./modules/view-report/view-report.module').then(mod => mod.ViewReportModule)
  },
  {
    path: 'manage-settlement-summary',
    canActivate: [UserDataGuard, UiConfigGuard],
    loadChildren: () => import('./modules/settlement-summary-management/settlement-summary-management.module').then(mod => mod.SettlementSummaryManagementModule)
  },
  {
    path: 'view-payment-transactions',
    canActivate: [UserDataGuard, UiConfigGuard],
    loadChildren: () => import('./modules/view-payment-transaction/view-payment-transaction.module').then(mod => mod.ViewPaymentTransactionModule)
  },
  {
    path: '**',
    redirectTo: ''
  }
];

export const Routing: ModuleWithProviders = RouterModule.forRoot(routes);
