import {Platform} from '@angular/cdk/platform';
import {DOCUMENT} from '@angular/common';
import {Component, Inject, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router, RouterEvent} from '@angular/router';
import {map, mergeMap} from 'rxjs/internal/operators';
import {filter} from 'rxjs/operators';
import {appAnimations} from './core/animations/animations';
import {SidenavComponent} from './shared/components/sidenav/sidenav.component';
import {NavItemService} from './shared/services/nav-item/nav-item.service';
import {PageHeaderService} from './shared/services/page-header/page-header.service';
import {ProgressBarService} from './shared/services/progress-bar/progress-bar.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: appAnimations
})
export class AppComponent implements OnInit {
  @ViewChild(SidenavComponent, {static: false}) sidenavComponent: SidenavComponent;
  public routeData: any = {
    hideNavBar: true,
    hideSideNav: true,
    hideHeader: true
  };
  public isProgressive: Boolean;

  constructor(private router: Router,
              private activatedRoute: ActivatedRoute,
              private navItemService: NavItemService,
              private platform: Platform,
              @Inject(DOCUMENT) private document: any) {
    let previousRoute = router.routerState.snapshot.url;

    router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe((data: NavigationEnd) => {
        // We want to reset the scroll position on navigation except when navigating within
        // the documentation for a single component.
        if (!isNavigationWithinComponentView(previousRoute, data.urlAfterRedirects)) {
          resetScrollPosition();
        }

        previousRoute = data.urlAfterRedirects;
        PageHeaderService.updatePageHeader(null);
      });

    router.events.subscribe((event: RouterEvent) => {
      this.navigationInterceptor(event);
    });

    router.events
      .pipe(filter(event => event instanceof NavigationEnd),
        map(() => {
          let route = activatedRoute.firstChild;
          let child = route;
          while (child) {
            if (child.firstChild) {
              child = child.firstChild;
              route = child;
            } else {
              child = null;
            }
          }
          return route;
        }),
        mergeMap(route => route.data)
      )
      .subscribe(data => {
        this.routeData = data;
        // TODO have a better guard
        if (this.routeData && this.routeData.pageId && !this.navItemService.getItemById(this.routeData.pageId)) {
          this.router.navigate(['/'], {replaceUrl: true});
        }
        ProgressBarService.updateLoading(false);
        setAppRootPosition(data.hideNavBar ? 'nonavbar' : 'withnavbar');
      });
  }

  ngOnInit() {
    ProgressBarService.isProgressive.subscribe(flag => {
      this.isProgressive = flag;
    });

    if (this.platform.ANDROID || this.platform.IOS) {
      this.document.body.className += ' is-mobile';
    }
  }

  // Shows and hides the loading spinner during RouterEvent changes
  navigationInterceptor(event: RouterEvent): void {
    if (event instanceof NavigationStart) {
      ProgressBarService.updateLoading(true);
    }
    if (event instanceof NavigationEnd) {
      ProgressBarService.updateLoading(false);
    }

    // Set loading state to false in both of the below events to hide the spinner in case a request fails
    if (event instanceof NavigationCancel) {
      ProgressBarService.updateLoading(false);
    }
    if (event instanceof NavigationError) {
      ProgressBarService.updateLoading(false);
    }
  }
}


function isNavigationWithinComponentView(oldUrl: string, newUrl: string) {
  return oldUrl && newUrl && oldUrl === newUrl;
}

function resetScrollPosition() {
  if (typeof document === 'object' && document) {
    const sidenavContent = document.querySelector('.mat-drawer-content');
    if (sidenavContent) {
      sidenavContent.scrollTop = 0;
    }
  }
}

function setAppRootPosition(className) {
  if (typeof document === 'object' && document) {
    const appRootContent = document.querySelector('app-root');
    if (appRootContent) {
      appRootContent.className = className;
    }
  }
}
