import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CoreModule} from './core/core.module';
import {SidenavComponent} from './shared/components/sidenav/sidenav.component';
import {NavbarComponent} from './shared/components/navbar/navbar.component';
import {NavComponent} from './shared/components/sidenav/nav.component';
import {PageHeaderComponent} from './shared/components/page-header/page-header.component';
import {PageFooterComponent} from './shared/components/page-footer/page-footer.component';
import {HttpClientModule} from '@angular/common/http';
import {PageTitleService} from './shared/services/page-title/page-title.service';
import {Routing} from './app.routes';

@NgModule({
  declarations: [
    AppComponent,
    SidenavComponent,
    NavbarComponent,
    NavComponent,
    PageHeaderComponent,
    PageFooterComponent
  ],
  imports: [
    BrowserAnimationsModule,
    CoreModule,
    Routing,
    HttpClientModule
  ],
  providers: [PageTitleService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
