# Code Sample Angular 8 - By Vicky Gonsalves

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.8.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Enable SSL (ONLY PRODUCTION)
1. Locate `keys` directory in root and put appropriate certificates and keys viz. `cert.crt`, `intermediate.crt` and `private.key`. Note that file names and their extensions must match with provided ones.
2. Goto `src\environments\environment.prod.ts` and be sure to enable production mode `production: true` and provide appropriate production api URL `apiUrl: 'https://localhost/api/v1/'`.
3. Production Frontend server is set to run on port `8443` If you wish to change, please locate `server.js` and change `securePort = 8443` to desired one.
4. Once all done you can proceed to Build step.

## Build

1. server.js file in project takes control of serving dist files. You can configure ip and port here to tun frontend.
2. Make sure to pull latest changes from repo.
3. Then run command `npm install`
4. Open file `src\environments\environment.prod.ts` and confirm all variables are correct, skip if already confirmed.
5. Run `npm run build`. This will create an optimized build ready for production
6. Run command `node server.js`. This will simply host those `dist` files via node server.
7. Point to http://localhost:8443, or whatever configured in server.js. It shd serve you production build.
8. If not running please check console or network tab in browser.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
